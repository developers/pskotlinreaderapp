package com.pagesuite.pskotlinreaderapp.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.pagesuite.widgets.InfinityWebView;

public class ContentsWebView extends InfinityWebView
{
    public ContentsWebView(Context context)
    {
        super(context);
    }

    public ContentsWebView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ContentsWebView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public ContentsWebView(Context context, AttributeSet attrs, int defStyleAttr, boolean privateBrowsing)
    {
        super(context, attrs, defStyleAttr, privateBrowsing);
    }

    @Override
    protected void init(Context context)
    {
        super.init(context);

    }

    public int getContentWidth()
    {
        int ret = super.computeHorizontalScrollRange(); // - getMeasuredWidth();//working after load of page
        return ret;
    }

    public int getContentHeight()
    {
        int ret = super.computeVerticalScrollRange(); // - getMeasuredHeight(); //working after load of page
        return ret;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt)
    {
        super.onScrollChanged(l, t, oldl, oldt);

    }


}
