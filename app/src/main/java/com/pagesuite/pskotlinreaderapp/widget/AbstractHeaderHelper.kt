package com.pagesuite.pskotlinreaderapp.widget

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.PictureDrawable
import android.text.SpannableString
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.pagesuite.images.ImageHandler
import com.pagesuite.images.component.Listeners
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicToolbarActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.config.HeaderConfig
import com.pagesuite.pskotlinreaderapp.edition.MenuSizeType
import com.pagesuite.pskotlinreaderapp.extensions.slideDown
import com.pagesuite.pskotlinreaderapp.extensions.slideIn
import com.pagesuite.pskotlinreaderapp.extensions.slideOut
import com.pagesuite.pskotlinreaderapp.extensions.slideUp
import com.pagesuite.pskotlinreaderapp.utils.Utils
import com.pagesuite.pskotlinreaderapp.utils.Utils.animateObjectHeight
import com.pagesuite.pskotlinreaderapp.widget.svg.SvgSoftwareLayerSetter
import kotlinx.android.synthetic.main.view_edition_toolbar.*


abstract class AbstractHeaderHelper(val basicActivity: BasicToolbarActivity) {

    val headerLogo: ImageView = basicActivity.headerLogo

    val headerDate: TextView = basicActivity.headerDate
    private var headerDateHeight: Int = 0

    val headerPaging: ViewGroup = basicActivity.headerPaging

    val headerBuffer: View = basicActivity.headerBuffer

    val headerLogoContainer: ViewGroup = basicActivity.headerLogoContainer

    val toolbar: ViewGroup = basicActivity.findViewById(R.id.toolbar)

    var visibilityType = View.GONE

    private val menuButton: View = basicActivity.menuButton

    val backButton: ImageView = basicActivity.backButton

    private val closeButton: View = basicActivity.closeButton

    var currentMenuType: MenuSizeType = MenuSizeType.MINIMIZED
    private var showingCloseButton: Boolean = false

    open fun prepHeader(completionListener: (() -> Unit)? = null) {

        if (DataStore.EditionApplication == null) {

            completionListener?.invoke()

            return
        }

        if (DataStore.EditionApplication?.header != null) {

            val url = DataStore.EditionApplication?.header?.headerlogo

            headerLogo.tag = url
            ImageHandler.getInstance().loadImage(headerLogo, url, false, object : Listeners.Listener_ImageLoader {

                override fun complete(p0: String?) {


                }

                override fun failed(p0: String?) {

                    headerLogo.tag = null

                    Glide.with(basicActivity)
                            .`as`(PictureDrawable::class.java)
                            .load(url)
                            .listener(SvgSoftwareLayerSetter())
                            .into(headerLogo)

                }

            })

        }

        loadEditionName()

        var semaphoreCount = 2

        val listener = {

            semaphoreCount--

            if (semaphoreCount == 0) {

                completionListener?.invoke()

            }

        }

        closeButton.slideDown(duration = basicActivity.animationDuration, outToBottom = false, postHandler = listener)

        applyColourScheme()

        increaseTopMenuSize(listener)

    }

    open fun applyColourScheme() {


        //toolbar.setBackgroundColor(basicActivity.headerBackgroundColour)

        if (menuButton is ImageView) {

            menuButton.setColorFilter(basicActivity.headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

        if (backButton is ImageView) {

            backButton.setColorFilter(basicActivity.headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

        if (closeButton is ImageView) {

            closeButton.setColorFilter(basicActivity.headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

        //toolbarFooter.setBackgroundColor(basicActivity.headerForegroundColour)

    }

    open fun loadEditionName() {

    }


    open fun reduceTopMenuSize() {

    }

    open fun increaseTopMenuSize(completionListener: (() -> Unit)? = null) {

    }


    open fun animateToolbar(menuSize: Int, dateAlpha: Float, pagingAlpha: Float, completionListener: (() -> Unit)? = null) {

    }

    open fun updatePageNumber(display: SpannableString? = null, simpleDisplay: String? = null, sectionColour: Int = Color.BLACK) {

    }

    fun showMenuButton() {

        if (menuButton.visibility != View.VISIBLE) {

            menuButton.slideIn(basicActivity.shortAnimationDuration)

        }

    }

    fun hideMenuButton(completionListener: (() -> Unit)? = null) {

        if (menuButton.visibility != visibilityType) {

            menuButton.slideOut(basicActivity.shortAnimationDuration, endVisibility = visibilityType, postHandler = completionListener)

        } else {

            completionListener?.invoke()

        }

    }

    fun showBackButton(completionListener: (() -> Unit)? = null) {

        if (backButton.visibility != View.VISIBLE) {

            backButton.slideIn(duration = basicActivity.shortAnimationDuration, inFromRight = false, postHandler = completionListener)

        }

    }

    fun hideBackButton(completionListener: (() -> Unit)? = null) {

        if (backButton.visibility != visibilityType) {

            backButton.slideOut(duration = basicActivity.shortAnimationDuration, outToRight = false, endVisibility = visibilityType, postHandler = completionListener)

        } else {

            completionListener?.invoke()

        }

    }

    fun hideToolbar() {

        if (toolbar.visibility != View.GONE) {

            toolbar.slideDown(duration = basicActivity.animationDuration, outToBottom = false)

        }

    }

    fun showToolbar() {

        if (toolbar.visibility != View.VISIBLE) {

            toolbar.slideUp(duration = basicActivity.animationDuration, inFromBottom = false)

        }

    }

    open fun getToolbarHeight(position: Int): Int {

        return if (position == 0) {

            getIncreasedMenuHeight()

        } else {

            getDecreasedMenuHeight()

        }

    }

    open fun showSimpleToolbar(completionListener: (() -> Unit)? = null) {

        if (!showingCloseButton) {

            showingCloseButton = true

            headerDateHeight = headerDate.measuredHeight

            Utils.animateObjectHeight(headerDate, 0, duration = basicActivity.animationDuration.toLong())

            Utils.animateObjectHeight(toolbar, getSimpleToolbarHeight(), completionListener = {

                menuButton.slideOut(basicActivity.shortAnimationDuration, endVisibility = visibilityType, postHandler = {

                    closeButton.slideUp(duration = basicActivity.shortAnimationDuration, inFromBottom = false, postHandler = {

                        completionListener?.invoke()

                    })

                })

            }, duration = basicActivity.animationDuration.toLong())


        } else {

            completionListener?.invoke()

        }
    }

    open fun hideSimpleToolbar(position: Int, completionListener: (() -> Unit)? = null) {

        if (showingCloseButton) {

            showingCloseButton = false

            closeButton.slideDown(duration = basicActivity.shortAnimationDuration, outToBottom = false, postHandler = {

                menuButton.slideIn(basicActivity.shortAnimationDuration, postHandler = {

                    animateObjectHeight(headerDate, headerDateHeight, duration = basicActivity.animationDuration.toLong())

                    animateObjectHeight(toolbar, getToolbarHeight(position), completionListener = {

                        completionListener?.invoke()

                    }, duration = basicActivity.animationDuration.toLong())


                })

            })

        } else {

            completionListener?.invoke()

        }

    }

    abstract fun getIncreasedMenuHeight(): Int
    abstract fun getDecreasedMenuHeight(): Int

    open fun getSimpleToolbarHeight(): Int {

        return HeaderConfig.Edition.mSimpleToolbarHeight

    }

}