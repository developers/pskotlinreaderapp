package com.pagesuite.pskotlinreaderapp.widget

import android.content.Context
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

import com.pagesuite.pskotlinreaderapp.R

class MyItemDecoration(context: Context) : RecyclerView.ItemDecoration() {

    private val decorationHeight: Int = context.resources.getDimensionPixelSize(R.dimen.decoration_height)

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        if (parent != null && view != null) {

            val itemPosition = parent.getChildAdapterPosition(view)
            val totalCount = parent.adapter?.itemCount ?: 0

            if (itemPosition in 0..totalCount) {
                outRect.left = decorationHeight / 2
                outRect.right = decorationHeight / 2
                outRect.bottom = decorationHeight
            }

        }

    }
}