package com.pagesuite.pskotlinreaderapp.widget

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import com.pagesuite.pskotlinreaderapp.R

/**
 * 类似螺纹的加载view<br></br>
 * 可以自定义的属性：颜色、旋转速度（X弧度/s）<br></br>
 * Created by Kyson on 2015/8/9.<br></br>
 * www.hikyson.cn<br></br>
 */
class WhorlView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {

    //当前动画时间
    private var mCircleTime: Long = 0
    //每层颜色
    private var mLayerColors: IntArray? = null
    //旋转速度
    private var mCircleSpeed: Int = 0
    //视差差速
    private var mParallaxSpeed: Int = 0
    //弧长
    private var mSweepAngle: Float = 0.toFloat()
    //弧宽
    private var mStrokeWidth: Float = 0.toFloat()

    var isCircling = false
        private set

    private var mArcPaint: Paint? = null

    private var mOval: RectF? = null

    private val minLength: Int
        get() = Math.min(width, height)

    private var mIntervalWidth: Float = 0.toFloat()

    init {


        val defaultCircleSpeed = 270
        val defaultSweepAngle = 90f
        val defaultStrokeWidth = 5f
        val defaultColors = "#F44336_#4CAF50_#5677fc"

        if (attrs != null) {

            val typedArray = context.obtainStyledAttributes(attrs, R.styleable.whorlview_style)

            var colors = typedArray.getString(R.styleable.whorlview_style_whorlview_circle_colors)

            if (TextUtils.isEmpty(colors)) {

                colors = defaultColors

            }

            parseStringToLayerColors(colors!!)

            mCircleSpeed = typedArray.getInt(R.styleable.whorlview_style_whorlview_circle_speed, defaultCircleSpeed)

            val index = typedArray.getInt(R.styleable.whorlview_style_whorlview_parallax, 0)

            setParallax(index)

            mSweepAngle = typedArray.getFloat(R.styleable.whorlview_style_whorlview_sweepAngle, defaultSweepAngle)

            if (mSweepAngle <= 0 || mSweepAngle >= 360) {

                throw IllegalArgumentException("sweep angle out of bound")

            }

            mStrokeWidth = typedArray.getFloat(R.styleable.whorlview_style_whorlview_strokeWidth, defaultStrokeWidth)

            typedArray.recycle()

        } else {

            parseStringToLayerColors(defaultColors)
            mCircleSpeed = defaultCircleSpeed
            mParallaxSpeed = PARALLAX_MEDIUM
            mSweepAngle = defaultSweepAngle
            mStrokeWidth = defaultStrokeWidth

        }
    }

    fun setColours(colourArr: IntArray) {

        mLayerColors = colourArr
        invalidateWrap()

    }

    /**
     * string类型的颜色分割并转换为色值
     * @param colors
     */
    private fun parseStringToLayerColors(colors: String) {

        val colorArray = colors.split(SPLI_CHAR.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        mLayerColors = IntArray(colorArray.size)

        mLayerColors?.let {

            for (i in colorArray.indices) {

                try {

                    it[i] = Color.parseColor(colorArray[i])

                } catch (ex: IllegalArgumentException) {

                    throw IllegalArgumentException("whorlview_circle_colors can not be parsed | " + ex.localizedMessage)

                }

            }

        }

    }

    private fun setParallax(index: Int) {

        when (index) {

            FAST -> mParallaxSpeed = PARALLAX_FAST
            MEDIUM -> mParallaxSpeed = PARALLAX_MEDIUM
            SLOW -> mParallaxSpeed = PARALLAX_SLOW
            else -> throw IllegalStateException("no such parallax type")

        }

    }

    override fun onDraw(canvas: Canvas) {

        super.onDraw(canvas)

        for (i in mLayerColors!!.indices) {

            val angle = (mCircleSpeed + mParallaxSpeed * i).toFloat() * mCircleTime.toFloat() * 0.001f

            drawArc(canvas, i, angle)

        }

    }

    /**
     * start anim
     */
    fun start() {

        isCircling = true

        Thread(Runnable {

            mCircleTime = 0L

            while (isCircling) {

                invalidateWrap()
                mCircleTime += REFRESH_DURATION

                try {

                    Thread.sleep(REFRESH_DURATION)

                } catch (e: InterruptedException) {

                    e.printStackTrace()

                }

            }

        }).start()
    }

    fun stop() {

        isCircling = false
        mCircleTime = 0L
        invalidateWrap()

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun invalidateWrap() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            postInvalidateOnAnimation()

        } else {

            postInvalidate()

        }

    }

    /**
     * 画弧
     *
     * @param canvas
     * @param index      由内而外
     * @param startAngle
     */
    private fun drawArc(canvas: Canvas, index: Int, startAngle: Float) {

        val paint = checkArcPaint(index)
        //最大圆是view的边界
        val oval = checkRectF(index)
        canvas.drawArc(oval, startAngle, mSweepAngle, false, paint)

    }

    private fun checkArcPaint(index: Int): Paint {

        if (mArcPaint == null) {

            mArcPaint = Paint()

        } else {

            mArcPaint!!.reset()

        }

        mArcPaint!!.color = mLayerColors!![index]
        mArcPaint!!.style = Paint.Style.STROKE
        mArcPaint!!.strokeWidth = mStrokeWidth
        mArcPaint!!.isAntiAlias = true

        return mArcPaint!!

    }

    private fun checkRectF(index: Int): RectF {

        if (mOval == null) {

            mOval = RectF()

        }

        val start = index * (mStrokeWidth + mIntervalWidth) + mStrokeWidth / 2
        val end = minLength - start
        mOval!!.set(start, start, end, end)
        return mOval!!

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val minSize = (mStrokeWidth * 4f * mLayerColors!!.size.toFloat() + mStrokeWidth).toInt()
        val wantSize = (mStrokeWidth * 8f * mLayerColors!!.size.toFloat() + mStrokeWidth).toInt()
        val size = measureSize(widthMeasureSpec, wantSize, minSize)
        calculateIntervalWidth(size)
        setMeasuredDimension(size, size)

    }

    /**
     * 计算间隔大小
     * @param size
     */
    private fun calculateIntervalWidth(size: Int) {

        mIntervalWidth = size / (mLayerColors!!.size * 2) - mStrokeWidth

    }

    companion object {

        private val SPLI_CHAR = "_"

        val FAST = 1
        val MEDIUM = 0
        val SLOW = 2

        private val PARALLAX_FAST = 60
        private val PARALLAX_MEDIUM = 72
        private val PARALLAX_SLOW = 90

        private val REFRESH_DURATION = 16L

        /**
         * 测量view的宽高
         *
         * @param measureSpec
         * @param wantSize
         * @param minSize
         * @return
         */
        fun measureSize(measureSpec: Int, wantSize: Int, minSize: Int): Int {

            var result = 0
            val specMode = View.MeasureSpec.getMode(measureSpec)
            val specSize = View.MeasureSpec.getSize(measureSpec)

            if (specMode == View.MeasureSpec.EXACTLY) {

                // 父布局想要view的大小
                result = specSize

            } else {

                result = wantSize

                if (specMode == View.MeasureSpec.AT_MOST) {

                    // wrap_content
                    result = Math.min(result, specSize)

                }

            }

            //测量的尺寸和最小尺寸取大
            return Math.max(result, minSize)

        }

    }

}