package com.pagesuite.pskotlinreaderapp.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.view.View

class VerticalItemDecoration(context: Context) : DividerItemDecoration(context, DividerItemDecoration.VERTICAL) {

    var mDivider: Drawable? = null

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        val position = parent.getChildAdapterPosition(view)
        val itemCount = parent.adapter?.itemCount ?: 0

        // hide the divider for the last child

        if (position == 0 || position == itemCount - 1) {

            outRect.setEmpty()

        } else {

            super.getItemOffsets(outRect, view, parent, state)

        }

    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {

        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.adapter?.itemCount ?: 0

        for (i in 1 until childCount - 1) {

            if (i == childCount - 1) {
                continue
            }

            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin
            val bottom = top + (mDivider?.intrinsicHeight ?: 0)

            mDivider?.setBounds(left, top, right, bottom)
            mDivider?.draw(c)

        }

    }

}