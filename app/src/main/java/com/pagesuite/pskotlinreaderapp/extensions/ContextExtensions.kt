package com.pagesuite.pskotlinreaderapp.extensions

import android.content.Context
import android.view.LayoutInflater

fun Context.getLayoutInflater(): LayoutInflater {

    return this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

}