package com.pagesuite.pskotlinreaderapp.extensions

import android.support.v4.app.Fragment
import com.pagesuite.pskotlinreaderapp.AndroidFeedApp

fun Fragment.getApplication(): AndroidFeedApp? {

    return activity?.application as? AndroidFeedApp

}