package com.pagesuite.pskotlinreaderapp.extensions

import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation

fun View.slideIn(duration: Int = 200, postHandler: (() -> Unit)? = null, inFromRight: Boolean = true) {

    post { visibility = View.VISIBLE }

    val animate = if (inFromRight) {

        TranslateAnimation(width.toFloat(), 0f, 0f, 0f)

    } else {

        TranslateAnimation(-width.toFloat(), 0f, 0f, 0f)

    }

    animate.duration = duration.toLong()
    animate.fillAfter = true

    animate.setAnimationListener(object : Animation.AnimationListener {

        override fun onAnimationRepeat(animation: Animation?) {

        }

        override fun onAnimationEnd(animation: Animation?) {

            post(postHandler)

        }

        override fun onAnimationStart(animation: Animation?) {

        }
    })

    post { startAnimation(animate) }

}

fun View.slideOut(duration: Int = 200, postHandler: (() -> Unit)? = null, outToRight: Boolean = true, endVisibility: Int = View.GONE) {

    val animate = if (outToRight) {

        TranslateAnimation(0f, width.toFloat(), 0f, 0f)

    } else {

        TranslateAnimation(0f, -width.toFloat(), 0f, 0f)

    }

    animate.duration = duration.toLong()

    animate.setAnimationListener(object : Animation.AnimationListener {

        override fun onAnimationRepeat(animation: Animation?) {

        }

        override fun onAnimationEnd(animation: Animation?) {

            visibility = endVisibility

            post(postHandler)

        }

        override fun onAnimationStart(animation: Animation?) {

        }
    })

    post { startAnimation(animate) }

}

fun View.slideUp(duration: Int = 500, postHandler: (() -> Unit)? = null, inFromBottom: Boolean = true) {

    post { visibility = View.VISIBLE }

    val animate = if (inFromBottom) {

        TranslateAnimation(0f, 0f, this.height.toFloat(), 0f)

    } else {

        TranslateAnimation(0f, 0f, -this.height.toFloat(), 0f)

    }

    animate.duration = duration.toLong()
    animate.fillAfter = true

    animate.setAnimationListener(object : Animation.AnimationListener {

        override fun onAnimationRepeat(animation: Animation?) {

        }

        override fun onAnimationEnd(animation: Animation?) {

            post(postHandler)

        }

        override fun onAnimationStart(animation: Animation?) {

        }
    })

    post { startAnimation(animate) }

}

fun View.slideDown(duration: Int = 500, postHandler: (() -> Unit)? = null, outToBottom: Boolean = true, endVisibility: Int = View.GONE) {

    val animate = if (outToBottom) {

        TranslateAnimation(0f, 0f, 0f, this.height.toFloat())

    } else {

        TranslateAnimation(0f, 0f, 0f, -this.height.toFloat())

    }

    animate.duration = duration.toLong()

    animate.setAnimationListener(object : Animation.AnimationListener {

        override fun onAnimationRepeat(animation: Animation?) {

        }

        override fun onAnimationEnd(animation: Animation?) {

            visibility = endVisibility

            post(postHandler)

        }

        override fun onAnimationStart(animation: Animation?) {

        }
    })

    post { startAnimation(animate) }


}