package com.pagesuite.pskotlinreaderapp.services

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.*
import android.util.Log
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.zip.ZipEntry
import java.util.zip.ZipFile


/**
 * Created by Vaibhav.Jani on 6/4/15.
 * Edited by Simon D to handle 301 / 302 errors
 */
class FileDownloadService : IntentService("") {

    override fun onHandleIntent(intent: Intent?) {

        if (intent != null) {

            val bundle = intent.extras

            if (bundle == null
                    || !bundle.containsKey(DOWNLOADER_RECEIVER)
                    || !bundle.containsKey(DOWNLOAD_DETAILS)) {

                return
            }

            val resultReceiver = bundle.getParcelable<ResultReceiver>(DOWNLOADER_RECEIVER)

            val downloadDetails = bundle.getParcelable<DownloadRequest>(DOWNLOAD_DETAILS)

            try {

                assert(downloadDetails != null)

                if (downloadDetails != null) {

                    val targetUrl = downloadDetails.serverFilePath

                    if (targetUrl != null) {

                        connect(targetUrl, resultReceiver, downloadDetails)

                    }

                }

            } catch (e: Exception) {

                e.printStackTrace()

                downloadFailed(resultReceiver)

            }

        }

    }

    private fun connect(targetUrl: String, resultReceiver: ResultReceiver?, downloadDetails: DownloadRequest) {

        val url = URL(targetUrl)

        downloadStarted(resultReceiver)

        val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection

        urlConnection.connect()

        if (urlConnection.responseCode != HttpURLConnection.HTTP_OK) {

            if (urlConnection.responseCode == HttpURLConnection.HTTP_MOVED_PERM || urlConnection.responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {

                val redirectUrl = urlConnection.getHeaderField("Location")
                if (redirectUrl != null) {

                    urlConnection.disconnect()

                    connect(redirectUrl, resultReceiver, downloadDetails)

                    return

                }

            }

            downloadFailed(resultReceiver)

            return

        }

        val lengthOfFile = urlConnection.contentLength

        if (UsefulConstants.IS_DEBUG) {
            Log.d(FileDownloadService::class.simpleName, "Length of file: $lengthOfFile")
        }

        val input = BufferedInputStream(url.openStream())

        val localPath = downloadDetails.localFilePath

        val output = FileOutputStream(localPath)

        val data = ByteArray(1024)

        var total: Long = 0

        var count: Int = -1

        var progress = 0

        //while ((count = input.read(data)) != -1) {
        while ({ count = input.read(data); count }() != -1) {

            total += count.toLong()

            progress = (total * 100 / lengthOfFile).toInt() / (if (downloadDetails.isRequiresUnzip) {
                2
            } else {
                1
            })

            sendProgress(progress, resultReceiver)

            output.write(data, 0, count)

        }

        output.flush()
        output.close()
        input.close()

        urlConnection.disconnect()

        fileDownloaded(downloadDetails, resultReceiver)

    }

    private fun fileDownloaded(downloadDetails: DownloadRequest, resultReceiver: ResultReceiver?) {

        val localPath = downloadDetails.localFilePath

        if (localPath != null) {

            if (downloadDetails.isRequiresUnzip) {

                var unzipDestination = downloadDetails.unzipAtFilePath

                if (unzipDestination == null) {

                    val file = File(localPath)

                    unzipDestination = file.parentFile.absolutePath
                }

                if (unzipDestination != null) {

                    unzip(localPath, unzipDestination, resultReceiver)

                }

            }

            if (downloadDetails.isDeleteZipAfterExtract) {

                val file = File(localPath)
                file.delete()

            }

        }

        downloadCompleted(resultReceiver)

    }


    private fun sendProgress(progress: Int, receiver: ResultReceiver?) {

        if (receiver != null) {

            val progressBundle = Bundle()
            progressBundle.putInt(DOWNLOAD_PROGRESS, progress)
            receiver.send(STATUS_OK, progressBundle)

        }

    }

    private fun downloadStarted(resultReceiver: ResultReceiver?) {

        if (resultReceiver != null) {

            val progressBundle = Bundle()
            progressBundle.putBoolean(DOWNLOAD_STARTED, true)
            resultReceiver.send(STATUS_OK, progressBundle)

        }

    }

    private fun downloadCompleted(resultReceiver: ResultReceiver?) {

        if (resultReceiver != null) {

            val progressBundle = Bundle()
            progressBundle.putBoolean(DOWNLOAD_COMPLETED, true)
            resultReceiver.send(STATUS_OK, progressBundle)

        }

    }

    private fun downloadFailed(resultReceiver: ResultReceiver?) {

        if (resultReceiver != null) {

            val progressBundle = Bundle()
            progressBundle.putBoolean(DOWNLOAD_FAILED, true)
            resultReceiver.send(STATUS_FAILED, progressBundle)

        }

    }

    @Throws(Exception::class)
    private fun unzip(zipFilePath: String, unzipAtLocation: String, resultReceiver: ResultReceiver?) {

        val archive = File(zipFilePath)

        try {

            val zipfile = ZipFile(archive)

            val itemCount = zipfile.size()

            if (itemCount > 0) {

                val e = zipfile.entries()

                var progress = 0
                var count = 0

                while (e.hasMoreElements()) {

                    val entry = e.nextElement() as ZipEntry

                    unzipEntry(zipfile, entry, unzipAtLocation)

                    count++

                    progress = (((count.toDouble() / itemCount.toDouble()) * 100).toInt() / 2) + 50

                    sendProgress(progress, resultReceiver)

                }

            }

            zipfile.close()

        } catch (e: Exception) {

            Log.e(FileDownloadService::class.simpleName, "Unzip exception", e)

        }

    }

    @Throws(IOException::class)
    private fun unzipEntry(zipfile: ZipFile, entry: ZipEntry, outputDir: String) {

        if (entry.isDirectory) {

            createDir(File(outputDir, entry.name))
            return

        }

        val outputFile = File(outputDir, entry.name)
        if (!outputFile.parentFile.exists()) {

            createDir(outputFile.parentFile)

        }

        if (UsefulConstants.IS_DEBUG) {
            Log.v(FileDownloadService::class.simpleName, "Extracting: $entry")
        }

        val zin = zipfile.getInputStream(entry)
        val inputStream = BufferedInputStream(zin)
        val outputStream = BufferedOutputStream(FileOutputStream(outputFile))

        try {

            //IOUtils.copy(inputStream, outputStream);

            outputStream.use { outputStream ->

                var c = inputStream.read()
                while (c != -1) {

                    outputStream.write(c)
                    c = inputStream.read()

                }

            }

        } finally {

            outputStream.close()
            inputStream.close()

        }

    }

    private fun createDir(dir: File) {

        if (dir.exists()) {

            return

        }

        if (UsefulConstants.IS_DEBUG) {
            Log.v(FileDownloadService::class.simpleName, "Creating dir " + dir.name)
        }

        if (!dir.mkdirs()) {

            throw RuntimeException("Can not create dir $dir")

        }

    }

    class FileDownloader private constructor(handler: Handler) : ResultReceiver(handler) {

        var downloadDetails: DownloadRequest? = null

        var onDownloadStatusListener: OnDownloadStatusListener? = null

        fun download(context: Context) {

            if (isOnline(context)) {

                val intent = Intent(context, FileDownloadService::class.java)
                intent.putExtra(DOWNLOADER_RECEIVER, this)
                intent.putExtra(DOWNLOAD_DETAILS, downloadDetails)
                context.startService(intent)

            } else {

                if (onDownloadStatusListener != null) {

                    onDownloadStatusListener?.onDownloadFailed()

                }

            }

        }

        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {

            super.onReceiveResult(resultCode, resultData)

            if (onDownloadStatusListener == null) {

                return

            }

            if (onDownloadStatusListener != null) {

                if (resultCode == STATUS_OK) {

                    if (resultData.containsKey(DOWNLOAD_STARTED) && resultData.getBoolean(DOWNLOAD_STARTED)) {

                        onDownloadStatusListener?.onDownloadStarted()

                    } else if (resultData.containsKey(DOWNLOAD_COMPLETED) && resultData.getBoolean(DOWNLOAD_COMPLETED)) {

                        onDownloadStatusListener?.onDownloadCompleted()

                    } else if (resultData.containsKey(DOWNLOAD_PROGRESS)) {

                        val progress = resultData.getInt(DOWNLOAD_PROGRESS)
                        onDownloadStatusListener?.onDownloadProgress(progress)

                    }

                } else if (resultCode == STATUS_FAILED) {

                    onDownloadStatusListener?.onDownloadFailed()
                }

            }

        }

        companion object {

            fun getInstance(downloadDetails: DownloadRequest, downloadStatusListener: OnDownloadStatusListener): FileDownloader {

                val handler = Handler(Looper.getMainLooper())

                val fileDownloader = FileDownloader(handler)

                fileDownloader.downloadDetails = downloadDetails

                fileDownloader.onDownloadStatusListener = downloadStatusListener

                return fileDownloader
            }

        }

    }

    interface OnDownloadStatusListener {

        fun onDownloadStarted()

        fun onDownloadCompleted()

        fun onDownloadFailed()

        fun onDownloadProgress(progress: Int)

    }

    class DownloadRequest : Parcelable {

        var tag: String? = null

        var isRequiresUnzip: Boolean = false

        var serverFilePath: String? = null

        var localFilePath: String? = null

        var unzipAtFilePath: String? = null

        var isDeleteZipAfterExtract = true

        constructor(serverFilePath: String, localPath: String) {

            this.serverFilePath = serverFilePath

            this.localFilePath = localPath

            this.isRequiresUnzip = isRequiresUnzip

        }

        constructor(input: Parcel) {

            isRequiresUnzip = input.readByte().toInt() != 0x00
            serverFilePath = input.readString()
            localFilePath = input.readString()
            unzipAtFilePath = input.readString()
            isDeleteZipAfterExtract = input.readByte().toInt() != 0x00

        }

        override fun describeContents(): Int {

            return 0

        }

        override fun writeToParcel(dest: Parcel?, flags: Int) {

            if (dest != null) {

                dest.writeByte((if (isRequiresUnzip) 0x01 else 0x00).toByte())
                dest.writeString(serverFilePath)
                dest.writeString(localFilePath)
                dest.writeString(unzipAtFilePath)
                dest.writeByte((if (isDeleteZipAfterExtract) 0x01 else 0x00).toByte())

            }

        }

        companion object {

            @JvmField
            val CREATOR: Parcelable.Creator<DownloadRequest> = object : Parcelable.Creator<DownloadRequest> {

                override fun createFromParcel(input: Parcel): DownloadRequest {

                    return DownloadRequest(input)

                }

                override fun newArray(size: Int): Array<DownloadRequest?> {

                    return arrayOfNulls(size)

                }

            }

        }

    }

    companion object {

        private const val STATUS_OK = 100

        private const val STATUS_FAILED = 200

        private const val DOWNLOADER_RECEIVER = "downloader_receiver"

        private const val DOWNLOAD_DETAILS = "download_details"

        private const val DOWNLOAD_STARTED = "download_started"

        private const val DOWNLOAD_FAILED = "download_failed"

        private const val DOWNLOAD_COMPLETED = "download_completed"

        private const val DOWNLOAD_PROGRESS = "download_progress"

        private fun isOnline(context: Context): Boolean {

            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val netInfo = cm.activeNetworkInfo

            return (netInfo != null
                    && netInfo.isConnectedOrConnecting
                    && cm.activeNetworkInfo.isAvailable
                    && cm.activeNetworkInfo.isConnected)

        }

    }

}