package com.pagesuite.pskotlinreaderapp.services

import com.pagesuite.pskotlinreaderapp.graphql.GetApplicationsByEndPointQuery


object ApplicationCacheService {

    var application: GetApplicationsByEndPointQuery.GetApplicationByEndPoint? = null

    fun put(application: GetApplicationsByEndPointQuery.GetApplicationByEndPoint?) {

        this.application = application

    }

    fun get(): GetApplicationsByEndPointQuery.GetApplicationByEndPoint? {

        return this.application

    }

}
