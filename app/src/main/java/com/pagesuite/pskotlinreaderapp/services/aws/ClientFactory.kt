package com.pagesuite.pskotlinreaderapp.services

import android.content.Context
import android.util.Log
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient
import com.amazonaws.mobileconnectors.appsync.sigv4.BasicAPIKeyAuthProvider
import com.amazonaws.regions.Regions


object ClientFactory {
    @Volatile
    private var client: AWSAppSyncClient? = null

    @Synchronized
    fun getInstance(context: Context): AWSAppSyncClient? {

        val credentialsProvider = CognitoCachingCredentialsProvider(
                getContext(context), // get the context for the current activity
                "eu-west-1:38607d40-a0dd-4d37-98f3-230be698a2ec", // your identity pool id
                Regions.EU_WEST_1//Region
        )

        try {

            if (client == null) {

                client = AWSAppSyncClient.builder()
                        .credentialsProvider(credentialsProvider)
                        .region(Regions.EU_WEST_1)
                        .context(context)
                        .apiKey(BasicAPIKeyAuthProvider("da2-udyy4w6xx5dgnmwcopzk7jx6fa"))
                        .serverUrl("https://lptlqlmwqjdmlbzhevamvh5u6q.appsync-api.eu-west-1.amazonaws.com/graphql")
                        .build()

            }

        } catch (e: Error) {

            Log.w(ClientFactory::class.simpleName, "FAILED TO AUTH $e")

        }

        return client

    }

    private fun getContext(context: Context): Context {

        return context

    }

}
