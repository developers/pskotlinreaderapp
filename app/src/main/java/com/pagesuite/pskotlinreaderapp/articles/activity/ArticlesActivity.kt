package com.pagesuite.pskotlinreaderapp.articles.activity

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicToolbarActivity
import com.pagesuite.pskotlinreaderapp.articles.adapter.BasicArticlesAdapter
import com.pagesuite.pskotlinreaderapp.articles.fragment.FragmentFromToPicker
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.edition.SearchType
import com.pagesuite.pskotlinreaderapp.edition.findArticle
import com.pagesuite.pskotlinreaderapp.utils.ColouringHelper
import kotlinx.android.synthetic.main.view_article_search_options.*
import java.util.*

abstract class ArticlesActivity : BasicToolbarActivity() {

    var mLayoutManager: RecyclerView.LayoutManager? = null

    var searchType: SearchType = SearchType.EDITION_ONLY

    var fromDate: String? = null
    var fromDateCalendar: Calendar? = null

    var toDate: String? = null
    var toDateCalendar: Calendar? = null

    var mAdapter: BasicArticlesAdapter? = null

    var mRecyclerView: RecyclerView? = null

    val itemClickListener: View.OnClickListener = View.OnClickListener { it ->

        val tag = it?.getTag(R.id.article_guid)
        if (tag is String) {

            val articles = mAdapter?.articles

            if (articles != null) {

                val article = findArticle(tag)

                if (article == null) {

                    val position = articles.indexOfFirst { it.getArticleGuid()?.contentEquals(tag) == true }

                    if (position != -1) {

                        mApplication?.loadArticleBrowser(this, articles[position])

                    }

                } else {

                    intent = Intent()
                    intent.putExtra(UsefulConstants.ARTICLE_GUID, tag)
                    setResult(AppCompatActivity.RESULT_OK, intent)
                    finish()

                }


            }

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        initAdapter()

        initRecyclerView()

    }

    open fun initAdapter() {

        mAdapter = getArticlesAdapter()

        mAdapter?.itemClickListener = itemClickListener

    }

    open fun initRecyclerView() {

        mRecyclerView = findViewById(R.id.recyclerView)

        mLayoutManager = getLayoutManager()

        /*if (itemDecoration == null) {

            if (mRecyclerView?.itemDecorationCount ?: 0 <= 1) {

                itemDecoration = MyItemDecoration(this)

                itemDecoration?.let {

                    mRecyclerView?.addItemDecoration(it)

                }

            }

        }*/

        mRecyclerView?.layoutManager = mLayoutManager
        mRecyclerView?.adapter = mAdapter

        addVerticalDivider()

    }

    private fun getLayoutManager(): RecyclerView.LayoutManager {

        return LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        //return GridLayoutManager(this, resources.getInteger(R.integer.savedArticles_card_columnCount))

    }

    fun addVerticalDivider() {

        val decorationCount = mRecyclerView?.itemDecorationCount ?: 0

        if (decorationCount <= 1) {

            val drawable = ContextCompat.getDrawable(this, R.drawable.vertical_divider)?.mutate()

            if (drawable != null) {

                drawable.setColorFilter(getVerticalDividerColour(), PorterDuff.Mode.SRC_ATOP)

                val verticalDivider = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)

                verticalDivider.setDrawable(drawable)

                mRecyclerView?.addItemDecoration(verticalDivider)

            }

        }

    }

    abstract fun getVerticalDividerColour(): Int

    override fun initColourScheme() {

        super.initColourScheme()

        val rightButtonRadii = floatArrayOf(0f, 0f, 4.0f, 4.0f, 4.0f, 4.0f, 0f, 0f)

        allEditionsButton.background = ColouringHelper.getButtonDrawable(normal = getNormalBackgroundColour(),
                pressed = getPressedBackgroundColour(),
                checked = getCheckedBackgroundColour(),
                selected = getSelectedBackgroundColour(),
                normalBorder = getNormalBorderColour(),
                pressedBorder = getPressedBorderColour(),
                checkBorder = getCheckedBorderColour(),
                selectedBorder = getSelectedBorderColour(),
                cornerRadii = rightButtonRadii,
                borderWidth = getBorderWidth())

        allEditionsButton.setTextColor(ColouringHelper.getSelector(getNormalForegroundColour(), getPressedForegroundColour(), getCheckedForegroundColour(), getSelectedForegroundColour()))


        val leftButtonRadii = floatArrayOf(4.0f, 4.0f, 0f, 0f, 0f, 0f, 4.0f, 4.0f)

        currentEditionButton.background = ColouringHelper.getButtonDrawable(normal = getNormalBackgroundColour(),
                pressed = getPressedBackgroundColour(),
                checked = getCheckedBackgroundColour(),
                selected = getSelectedBackgroundColour(),
                normalBorder = getNormalBorderColour(),
                pressedBorder = getPressedBorderColour(),
                checkBorder = getCheckedBorderColour(),
                selectedBorder = getSelectedBorderColour(),
                cornerRadii = leftButtonRadii,
                borderWidth = getBorderWidth())

        currentEditionButton.setTextColor(ColouringHelper.getSelector(getNormalForegroundColour(), getPressedForegroundColour(), getCheckedForegroundColour(), getSelectedForegroundColour()))

        dateFilterButton.background = ColouringHelper.getButtonDrawable(normal = getNormalBackgroundColour(),
                pressed = getPressedBackgroundColour(),
                checked = getCheckedBackgroundColour(),
                selected = getSelectedBackgroundColour(),
                normalBorder = getNormalBorderColour(),
                pressedBorder = getPressedBorderColour(),
                checkBorder = getCheckedBorderColour(),
                selectedBorder = getSelectedBorderColour(),
                borderWidth = getBorderWidth())

        dateFilterButton.setTextColor(ColouringHelper.getSelector(getNormalForegroundColour(), getPressedForegroundColour(), getCheckedForegroundColour(), getSelectedForegroundColour()))

        setSelected(currentEditionButton)

    }

    abstract fun getNormalBackgroundColour(): Int
    abstract fun getNormalForegroundColour(): Int
    abstract fun getNormalBorderColour(): Int

    abstract fun getPressedBackgroundColour(): Int
    abstract fun getPressedForegroundColour(): Int
    abstract fun getPressedBorderColour(): Int

    abstract fun getCheckedBackgroundColour(): Int
    abstract fun getCheckedForegroundColour(): Int
    abstract fun getCheckedBorderColour(): Int

    abstract fun getSelectedBackgroundColour(): Int
    abstract fun getSelectedForegroundColour(): Int
    abstract fun getSelectedBorderColour(): Int

    abstract fun getBorderWidth(): Int

    fun editionOnlyClicked(view: View) {

        searchType = SearchType.EDITION_ONLY

        setSelected(view)

        clickedSearch(view)

    }

    fun allEditionsClicked(view: View) {

        searchType = SearchType.ALL_EDITIONS

        setSelected(view)

        clickedSearch(view)

    }

    fun dateFilterClicked(view: View) {

        val toFromPicker = FragmentFromToPicker()
        toFromPicker.listener = object : FragmentFromToPicker.FromToPickerListener {

            override fun pickedDates(fromCalendar: Calendar?, toCalendar: Calendar?) {

                //fromDate = "$year-${month + 1}-$dayOfMonth"

                if (fromCalendar != null && toCalendar != null) {

                    fromDate = "${fromCalendar.get(Calendar.YEAR)}-${fromCalendar.get(Calendar.MONTH) + 1}-${fromCalendar.get(Calendar.DAY_OF_MONTH)}"
                    toDate = "${toCalendar.get(Calendar.YEAR)}-${toCalendar.get(Calendar.MONTH) + 1}-${toCalendar.get(Calendar.DAY_OF_MONTH)}"

                    fromDateCalendar = fromCalendar
                    toDateCalendar = toCalendar

                    searchType = SearchType.DATE_RANGE

                    setSelected(view)

                    clickedSearch(view)

                }

            }

        }
        toFromPicker.show(supportFragmentManager, "toFromPicker")

    }

    open fun setSelected(view: View?) {

        /*allEditionsButton?.background?.clearColorFilter()
        allEditionsButton?.setTextColor(headerForegroundColour)

        currentEditionButton?.background?.clearColorFilter()
        currentEditionButton?.setTextColor(headerForegroundColour)

        dateFilterButton?.background?.clearColorFilter()
        dateFilterButton?.setTextColor(headerForegroundColour)

        view?.background?.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

        if (view is TextView) {

            view.setTextColor(headerBackgroundColour)

        }*/

        allEditionsButton?.isSelected = false
        currentEditionButton?.isSelected = false
        dateFilterButton?.isSelected = false

        view?.isSelected = true

    }

    abstract fun clickedSearch(view: View?)
    abstract fun getArticlesAdapter(): BasicArticlesAdapter

}