package com.pagesuite.pskotlinreaderapp.articles.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.extensions.getLayoutInflater
import com.pagesuite.pskotlinreaderapp.models.IArticleContent
import com.pagesuite.pskotlinreaderapp.utils.Utils

open class BasicArticlesAdapter(context: Context, val foregroundColour: Int) : RecyclerView.Adapter<BasicArticlesAdapter.BasicViewHolder>() {

    var articles: ArrayList<IArticleContent>? = null
    var itemClickListener: View.OnClickListener? = null

    private val dateFormat: String = context.getString(R.string.dateFormat)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasicViewHolder {

        val inflater = parent.context.getLayoutInflater()

        val view = inflater.inflate(R.layout.card_article_basic, parent, false)

        val viewHolder = BasicViewHolder(itemView = view)

        viewHolder.itemView.setOnClickListener(itemClickListener)

        return viewHolder

    }

    override fun getItemCount(): Int {

        return articles?.size ?: 0

    }

    override fun onBindViewHolder(holder: BasicViewHolder, position: Int) {

        val article = articles?.getOrNull(position)

        if (article != null) {

            val imageUrl: String? = article.getArticleImage(holder.itemView.context)

            if (!TextUtils.isEmpty(imageUrl)) {

                val listener = object : RequestListener<Drawable> {

                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {

                        hideImageView(holder)

                        return true
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {

                        showImageView(holder)

                        return false
                    }

                }

                Glide.with(holder.itemView).load(imageUrl).listener(listener).into(holder.imageView)

            } else {

                hideImageView(holder)

            }

            holder.itemView.setTag(R.id.article_guid, article.getArticleGuid())

            val headline = article.getSpannedHeadline()

            if (!TextUtils.isEmpty(headline)) {

                holder.titleText.visibility = View.VISIBLE
                holder.titleText.text = headline

            } else {

                holder.titleText.visibility = View.GONE

            }

            val description = article.getArticleDescription()

            if (!TextUtils.isEmpty(description)) {

                holder.descriptionText.visibility = View.VISIBLE
                holder.descriptionText.text = Utils.getSpanned(description)

            } else {

                holder.descriptionText.visibility = View.GONE

            }

            val pubDate = article.getPublishDate()

            holder.editionName.text = if (pubDate != null && !TextUtils.isEmpty(pubDate)) {

                DataStore.EditionApplication?.getEditionDate(pubDate, dateFormat)

            } else {

                ""

            }

        }

    }

    override fun onViewRecycled(holder: BasicViewHolder) {

        super.onViewRecycled(holder)

        Glide.with(holder.itemView.context).clear(holder.imageView)

        holder.imageView.setImageResource(0)

    }

    open fun hideImageView(holder: BasicViewHolder) {

        holder.imageView.visibility = View.GONE

        (holder.imageView.layoutParams as? LinearLayout.LayoutParams)?.weight = 0f
        (holder.titleText.layoutParams as? LinearLayout.LayoutParams)?.weight = 3f

    }

    open fun showImageView(holder: BasicViewHolder) {

        holder.imageView.visibility = View.VISIBLE

        (holder.imageView.layoutParams as? LinearLayout.LayoutParams)?.weight = 1f
        (holder.titleText.layoutParams as? LinearLayout.LayoutParams)?.weight = 2f

    }

    open inner class BasicViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var imageView: ImageView = itemView.findViewById(R.id.imageView)
        var editionName: TextView = itemView.findViewById(R.id.textViewEditionName)
        var titleText: TextView = itemView.findViewById(R.id.textView)
        var descriptionText: TextView = itemView.findViewById(R.id.textViewDescription)

        init {

            editionName.setTextColor(foregroundColour)
            titleText.setTextColor(foregroundColour)
            descriptionText.setTextColor(foregroundColour)

        }

    }

}