package com.pagesuite.pskotlinreaderapp.articles.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.extensions.getLayoutInflater
import com.pagesuite.pskotlinreaderapp.utils.ColouringHelper

class SavedArticlesAdapter(context: Context, foregroundColour: Int) : BasicArticlesAdapter(context, foregroundColour) {

    var deleteClickListener: View.OnClickListener? = null
    var editLongPressListener: View.OnLongClickListener? = null
    var isEditing: Boolean = false

    override fun getItemViewType(position: Int): Int {

        if (isEditing) {

            return 1

        }

        return super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasicViewHolder {

        val savedArticleViewHolder: BasicViewHolder

        if (viewType == 1) {

            val view = parent.context.getLayoutInflater().inflate(R.layout.card_article_saved_editing, parent, false)

            savedArticleViewHolder = SavedArticleViewHolder(itemView = view)

            savedArticleViewHolder.deleteButton.setOnClickListener(deleteClickListener)

        } else {

            savedArticleViewHolder = super.onCreateViewHolder(parent, viewType)

        }

        savedArticleViewHolder.itemView.setOnLongClickListener(editLongPressListener)
        savedArticleViewHolder.itemView.setOnClickListener(itemClickListener)

        return savedArticleViewHolder

    }

    override fun onBindViewHolder(holder: BasicViewHolder, position: Int) {

        super.onBindViewHolder(holder, position)

        val article = articles?.getOrNull(position)

        if (article != null) {

            if (holder is SavedArticleViewHolder) {

                holder.deleteButton.setTag(R.id.article_guid, article.getArticleGuid())

            }

        }

    }

    open inner class SavedArticleViewHolder(itemView: View) : BasicViewHolder(itemView) {

        var deleteButton: Button = itemView.findViewById(R.id.deleteButton)

        init {

            deleteButton.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Articles.Saved.DeleteButton.mBackgroundColour,
                    pressed = ColourConfig.Articles.Saved.DeleteButton.mPressedBackgroundColour,
                    normalBorder = ColourConfig.Articles.Saved.DeleteButton.mBackgroundColour,
                    pressedBorder = ColourConfig.Articles.Saved.DeleteButton.mPressedBackgroundColour)

            deleteButton.setTextColor(ColouringHelper.getSelector(normal = ColourConfig.Articles.Saved.DeleteButton.mForegroundColour, pressed = ColourConfig.Articles.Saved.DeleteButton.mPressedForegroundColour))

        }

    }

}