package com.pagesuite.pskotlinreaderapp.articles.activity

import android.content.res.Configuration
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.articles.SavedArticlesManager
import com.pagesuite.pskotlinreaderapp.articles.adapter.BasicArticlesAdapter
import com.pagesuite.pskotlinreaderapp.articles.adapter.SavedArticlesAdapter
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.SearchType
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.IArticleContent
import kotlinx.android.synthetic.main.activity_articles_saved.*
import kotlinx.android.synthetic.main.view_article_search_options.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

class SavedArticlesActivity : ArticlesActivity() {

    private var deleteClickListener: View.OnClickListener? = null
    private var editLongPressListener: View.OnLongClickListener? = null

    private var isEditing: Boolean = false

    private var loadingRoutine: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        val extras = intent.extras
        if (extras?.getBoolean(UsefulConstants.ARGS_SHOW_ALL, false) == true) {

            allEditionsClicked(allEditionsButton)

        } else {

            loadSavedArticles()

        }

    }

    override fun initColourScheme() {

        super.initColourScheme()

        headerForegroundColour = ColourConfig.Articles.Saved.Toolbar.mForegroundColour
        headerBackgroundColour = ColourConfig.Articles.Saved.Toolbar.mBackgroundColour

        savedArticlesToolbar_edit.setTextColor(headerForegroundColour)

        rootView.setBackgroundColor(ColourConfig.Articles.Saved.mBackgroundColour)
        savedArticlesNoneAvailable.setTextColor(ColourConfig.Articles.Saved.mForegroundColour)

        keyline.setBackgroundColor(ColourConfig.Articles.Saved.mKeylineColour)

    }

    override fun getVerticalDividerColour(): Int {

        return ColourConfig.Articles.Saved.mVerticalDividerColour

    }

    override fun getNormalBackgroundColour(): Int {
        return ColourConfig.Articles.Saved.Button.mBackgroundColour
    }

    override fun getNormalForegroundColour(): Int {
        return ColourConfig.Articles.Saved.Button.mForegroundColour
    }

    override fun getCheckedBackgroundColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedBackgroundColour
    }

    override fun getPressedBackgroundColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedBackgroundColour
    }

    override fun getSelectedBackgroundColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedBackgroundColour
    }

    override fun getNormalBorderColour(): Int {
        return ColourConfig.Articles.Saved.Button.mForegroundColour
    }

    override fun getBorderWidth(): Int {
        return ColourConfig.Articles.Saved.Button.mStrokeWidth
    }

    override fun getCheckedBorderColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedBackgroundColour
    }

    override fun getPressedBorderColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedBackgroundColour
    }

    override fun getSelectedBorderColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedBackgroundColour
    }

    override fun getPressedForegroundColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedForegroundColour
    }

    override fun getCheckedForegroundColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedForegroundColour
    }

    override fun getSelectedForegroundColour(): Int {
        return ColourConfig.Articles.Saved.Button.mPressedForegroundColour
    }

    override fun getLayout(): Int {

        return R.layout.activity_articles_saved

    }

    private fun initClickListeners() {

        deleteClickListener = View.OnClickListener { it ->

            val tag = it?.getTag(R.id.article_guid)

            if (tag != null) {

                val articles = mAdapter?.articles

                if (articles != null) {

                    val guid = tag as String

                    val position = articles.indexOfFirst { it.getArticleGuid()?.contentEquals(guid) == true }

                    if (position != -1) {

                        val article = articles.removeAt(position)
                        mAdapter?.notifyItemRemoved(position)
                        checkForEmptySavedArticles()

                        GlobalScope.launch {

                            SavedArticlesManager.deleteArticle(it.context, article)

                        }

                    }

                }

            }

        }

        editLongPressListener = View.OnLongClickListener {

            onEditClicked(it)
            true

        }


    }

    override fun initAdapter() {

        super.initAdapter()

        if (mAdapter is SavedArticlesAdapter) {

            initClickListeners()

            (mAdapter as? SavedArticlesAdapter)?.let {

                it.isEditing = isEditing
                it.deleteClickListener = deleteClickListener
                it.editLongPressListener = editLongPressListener

            }

        }

    }

    private fun loadSavedArticles() {

        // load from dataStore
        // https://github.com/Kotlin/kotlinx.coroutines/blob/master/coroutines-guide.md#guide-to-kotlinxcoroutines-by-example

        if (loadingRoutine != null) {

            loadingRoutine?.cancel()

        }

        loadingRoutine = GlobalScope.launch {

            val articles: List<Article>? = SavedArticlesManager.loadArticles(application)?.mapNotNull { savedArticle -> savedArticle.article }

            if (articles != null) {

                val articlesArray = ArrayList<IArticleContent>()

                articlesArray.addAll(when (searchType) {

                    SearchType.EDITION_ONLY -> {

                        articles.filter { savedArticle -> savedArticle.editionGuid?.equals(DataStore.Edition?.getEditionGuid(), ignoreCase = true) == true }

                    }

                    SearchType.DATE_RANGE -> {

                        articles.filter { savedArticle -> checkDate(savedArticle) }

                    }

                    else -> articles

                })

                mAdapter?.articles = articlesArray

            }

            runOnUiThread {

                mAdapter?.notifyDataSetChanged()
                checkForEmptySavedArticles()

            }

        }

    }

    private fun checkDate(savedArticle: Article): Boolean {

        val pubDate = savedArticle.pubDate
        if (pubDate != null && !TextUtils.isEmpty(pubDate)) {

            val formattedDate = DataStore.EditionApplication?.getEditionDate(pubDate, getString(R.string.dateFormat))
            val thisDate = Date(formattedDate)

            return thisDate.time >= fromDateCalendar?.time?.time ?: 0 && thisDate.time <= toDateCalendar?.time?.time ?: 0

        }

        return false

    }

    private fun checkForEmptySavedArticles() {

        if (mAdapter?.articles != null && (mAdapter?.articles?.size ?: 0) > 0) {

            runOnUiThread {

                savedArticlesNoneAvailable.visibility = View.GONE

            }

            runOnUiThread {

                savedArticlesToolbar_edit.visibility = View.VISIBLE

            }

        } else {

            runOnUiThread {

                savedArticlesNoneAvailable.visibility = View.VISIBLE

            }

            runOnUiThread {

                savedArticlesToolbar_edit.visibility = View.GONE

            }

            if (isEditing) {

                onEditClicked(null)

            }

        }

    }

    override fun clickedSearch(view: View?) {

        setSelected(view)

        loadSavedArticles()

    }

    override fun getArticlesAdapter(): BasicArticlesAdapter {

        return SavedArticlesAdapter(this, ColourConfig.Articles.Saved.mForegroundColour)

    }

    @Suppress("UNUSED_PARAMETER")
    fun onEditClicked(view: View?) {

        isEditing = !isEditing

        if (isEditing) {

            savedArticlesToolbar_edit.text = getString(R.string.Done)

        } else {

            savedArticlesToolbar_edit.text = getString(R.string.toolbar_edit)

        }

        (mAdapter as? SavedArticlesAdapter)?.isEditing = isEditing
        mAdapter?.notifyDataSetChanged()

    }

    override fun onBackPressed() {

        if (!isEditing) {

            super.onBackPressed()

        } else {

            onEditClicked(null)

        }

        if (loadingRoutine != null) {

            loadingRoutine?.cancel()

        }

    }

    override fun onDestroy() {

        if (loadingRoutine != null) {

            loadingRoutine?.cancel()

        }

        super.onDestroy()

    }

    override fun onConfigurationChanged(newConfig: Configuration?) {

        super.onConfigurationChanged(newConfig)

        if (mAdapter != null && mLayoutManager != null) {

            mRecyclerView?.post {

                initAdapter()
                initRecyclerView()

                loadSavedArticles()


                //mLayoutManager?.spanCount = resources.getInteger(R.integer.savedArticles_card_columnCount)

                //mAdapter?.notifyDataSetChanged()

            }

        }

    }

}
