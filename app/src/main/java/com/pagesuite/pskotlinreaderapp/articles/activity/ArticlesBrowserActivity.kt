package com.pagesuite.pskotlinreaderapp.articles.activity

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicToolbarActivity
import com.pagesuite.pskotlinreaderapp.articles.SavedArticlesManager
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.Queries
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.fragment.page.EditionColumnPageFragment
import com.pagesuite.pskotlinreaderapp.edition.fragment.page.EditionPageFragment
import com.pagesuite.pskotlinreaderapp.edition.shareSelectedArticle
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.pskotlinreaderapp.models.SavedArticle
import com.pagesuite.pskotlinreaderapp.utils.Utils
import kotlinx.android.synthetic.main.activity_articles_browser.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ArticlesBrowserActivity : BasicToolbarActivity() {

    private var selectedArticle: SavedArticle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initArticle()

    }

    override fun initColourScheme() {

        super.initColourScheme()

        headerBackgroundColour = ColourConfig.Articles.Browser.Toolbar.mBackgroundColour
        headerForegroundColour = ColourConfig.Articles.Browser.Toolbar.mForegroundColour

        articlesBrowserFontResize.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

        articlesBrowserShare.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

    }

    override fun getLayout(): Int {

        return R.layout.activity_articles_browser

    }

    private fun initArticle() {

        val extras = intent?.extras

        if (extras != null) {

            val articleGuid = extras.getString(UsefulConstants.ARTICLE_GUID, null)

            if (!TextUtils.isEmpty(articleGuid)) {

                GlobalScope.launch {

                    selectedArticle = SavedArticlesManager.loadArticle(application, articleGuid)

                    if (selectedArticle != null) {

                        loadArticle(pageData = selectedArticle?.page, article = selectedArticle?.article, template = selectedArticle?.template)

                    } else {

                        // either the save data is missing or this article was from the article search

                        Queries.getArticle(context = mApplication as Context, articleGuid = articleGuid, successHandler = {

                            loadArticle(article = it)

                        })

                    }


                }

            }

        }

    }

    private fun loadArticle(pageData: Page? = null, article: Article? = null, template: String? = null) {

        try {

            loadingSpinner?.post {

                loadingSpinner?.visibility = View.GONE

            }

            val cache = DataStore.Edition?.cache

            val articleMode = (DataStore.Edition?.articlemode?.equals(UsefulConstants.ARTICLE_DISPLAY_COLUMNS, true)
                    ?: false) || DataStore.Edition?.articlemode?.equals(UsefulConstants.ARTICLE_DISPLAY_MULTICOLUMN, true)
                    ?: false

            val frags = (if (articleMode) {

                EditionColumnPageFragment()

            } else {

                EditionPageFragment()

            })

            if (article != null) {

                val articles: ArrayList<Article> = ArrayList()

                articles.add(article)

                frags.mPageArticles = articles.toTypedArray()

            }

            frags.pageData = pageData
            frags.editionCache = cache

            if (frags is EditionPageFragment) {

                frags.templateContent = template

            }

            runOnUiThread {

                val fragmentManager = supportFragmentManager
                val transaction = fragmentManager.beginTransaction()
                transaction.replace(R.id.articleContent, frags)
                transaction.commit()

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun fontResizeClicked(view: View) {

        val fontSize = UsefulConstants.getFontSize()

        when (fontSize) {

            Utils.FontSizes.NORMAL -> UsefulConstants.setFontSize(Utils.FontSizes.LARGE, this)
            Utils.FontSizes.LARGE -> UsefulConstants.setFontSize(Utils.FontSizes.LARGER, this)
            else -> UsefulConstants.setFontSize(Utils.FontSizes.NORMAL, this)

        }
        //prepFontSize()

    }

    @Suppress("UNUSED_PARAMETER")
    fun shareClicked(view: View) {

        if (selectedArticle != null) {

            selectedArticle.let {

                if (it != null) {

                    val article = it.article

                    if (article != null) {

                        shareSelectedArticle(this, article)

                    }

                }

            }

        }

    }

}
