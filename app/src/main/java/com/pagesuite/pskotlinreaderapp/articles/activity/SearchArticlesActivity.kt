package com.pagesuite.pskotlinreaderapp.articles.activity

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.articles.adapter.BasicArticlesAdapter
import com.pagesuite.pskotlinreaderapp.articles.adapter.SearchArticlesAdapter
import com.pagesuite.pskotlinreaderapp.articles.provider.ArticleSearchProvider
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.EndpointQueryRequests
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.SearchType
import com.pagesuite.pskotlinreaderapp.extensions.slideDown
import com.pagesuite.pskotlinreaderapp.extensions.slideUp
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.IArticleContent
import com.pagesuite.pskotlinreaderapp.utils.Utils
import com.pagesuite.utilities.NetworkUtils
import kotlinx.android.synthetic.main.activity_articles_search.*

class SearchArticlesActivity : ArticlesActivity() {

    private var searchRunnerOffline: Runnable? = null
    private var searchRunnerOnline: Runnable? = null

    private var searchTerm: String? = null

    private var currentResultsPage: Int = 0
    private var mTotalResults: Int = 0

    private var isLoadingResults: Boolean = false

    private var totalResultsCount: String? = null

    private val MINIMUM_SEARCH_DELAY: Long = 2000

    override fun getLayout(): Int {

        return R.layout.activity_articles_search

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        initSearchRunnerOffline()

        initSearchRunnerOnline()

        initSearch()

        checkForIntent(intent)

    }

    override fun initColourScheme() {

        super.initColourScheme()

        headerForegroundColour = ColourConfig.Articles.Search.Toolbar.mForegroundColour
        headerBackgroundColour = ColourConfig.Articles.Search.Toolbar.mBackgroundColour

        rootView.setBackgroundColor(ColourConfig.Articles.Search.mBackgroundColour)
        resultsCount.setTextColor(ColourConfig.Articles.Search.mForegroundColour)
        noResults.setTextColor(ColourConfig.Articles.Search.mForegroundColour)

        keyline.setBackgroundColor(ColourConfig.Articles.Search.mKeylineColour)

    }

    override fun onNewIntent(intent: Intent) {

        setIntent(intent)
        checkForIntent(intent)

    }

    private fun checkForIntent(intent: Intent?) {

        if (intent != null) {

            if (Intent.ACTION_SEARCH == intent.action) {

                intent.getStringExtra(SearchManager.QUERY)?.also { query ->

                    searchTerm = query

                    clickedSearch(null)

                }

            }

        }

    }

    override fun initToolbar() {

        super.initToolbar()

        supportActionBar?.setDisplayShowTitleEnabled(false)

    }

    private fun initSearch() {

        totalResultsCount = getString(R.string.searchArticles_totalResults)

        titleText.setTextColor(headerForegroundColour)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        searchView.setOnSearchClickListener {

            titleText.visibility = View.GONE

        }

        searchView.setOnCloseListener {

            titleText.visibility = View.VISIBLE

            searchButtonClosed()

        }

        searchView.imeOptions = searchView.imeOptions or EditorInfo.IME_FLAG_NO_EXTRACT_UI

        searchView.queryHint = getString(R.string.searchArticles_hintText)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {

                try {

                    searchTerm = query
                    searchView.clearFocus()
                    searchView.setQuery("", false)
                    searchView.isIconified = true

                    SearchRecentSuggestions(searchView.context, ArticleSearchProvider.AUTHORITY, ArticleSearchProvider.MODE)
                            .saveRecentQuery(query, null)

                    clickedSearch(null)

                } catch (ex: Exception) {

                    ex.printStackTrace()

                }

                return true

            }

            override fun onQueryTextChange(newText: String): Boolean {

                return false

            }

        })

        changeSearchViewTextColor(searchView)

        searchView.isIconified = false

    }

    private fun initSearchRunnerOnline() {

        searchRunnerOnline = Runnable {

            if (searchTerm != null && !TextUtils.isEmpty(searchTerm)) {

                if (!isLoadingResults) {

                    isLoadingResults = true

                    searchTerm?.let {

                        EndpointQueryRequests.searchArticles(context = this, listener = { results, totalCount ->

                            updateResults(results, totalCount)

                        }, failureListener = {

                            updateResults(null, 0)

                        }, searchUrl = getSearchUrl(it), searchTerm = it)


                    }

                }

            }

        }

    }

    private fun getSearchUrl(searchTerm: String): String {

        var searchUrl: String = when (searchType) {

            SearchType.EDITION_ONLY -> {

                getString(R.string.urls_articleSearch_currentEdition)
                        .replace("{EGUID}", DataStore.Edition?.getEditionGuid() ?: "")

            }

            SearchType.DATE_RANGE -> {

                getString(R.string.urls_articleSearch_dateRange)
                        .replace("{FROM_DATE}", fromDate ?: "")
                        .replace("{TO_DATE}", toDate ?: "")

            }

            else -> {

                getString(R.string.urls_articleSearch_allEditions)

            }

        }

        searchUrl = searchUrl
                .replace("{APP_ID}", DataStore.EditionApplication?.app?.guid ?: "")
                .replace("{SEARCH_TERM}", searchTerm)
                .replace("{PAGE}", currentResultsPage.toString())

        return searchUrl

    }

    private fun initSearchRunnerOffline() {

        searchRunnerOffline = Runnable {

            if (searchTerm != null && !TextUtils.isEmpty(searchTerm)) {

                searchTerm?.let {

                    val edition = DataStore.Edition

                    if (edition != null) {

                        val sections = edition.sections

                        if (sections != null && sections.isNotEmpty()) {

                            val results = arrayListOf<Article>()
                            var articles: Array<Article>?

                            sectionLoop@ for (section in sections) {

                                articles = section.articles

                                if (articles != null) {

                                    articleLoop@ for (article in articles) {

                                        when {

                                            article.headline?.contains(it, true) == true -> results.add(article)
                                            article.author?.contains(it, ignoreCase = true) == true -> results.add(article)
                                            article.descriptionnoscript?.contains(it, ignoreCase = true) == true -> results.add(article)

                                        }

                                    }

                                }

                            }

                            results.trimToSize()

                            updateResults(results, results.size)

                        }

                    }

                }

            }

        }

    }

    override fun initRecyclerView() {

        super.initRecyclerView()

        mRecyclerView?.clearOnScrollListeners()

        mRecyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if (mTotalResults > 0 && (mAdapter?.itemCount ?: 0) < mTotalResults) {

                    if (NetworkUtils.isConnected(recyclerView.context)) {

                        if (!recyclerView.canScrollVertically(1) && !isLoadingResults) {

                            pagingProgress.slideUp(duration = animationDuration)
                            doSearch()

                        }

                    }

                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {


            }

        })

    }

    override fun getArticlesAdapter(): BasicArticlesAdapter {

        return SearchArticlesAdapter(this, ColourConfig.Articles.Search.mForegroundColour)

    }

    override fun getVerticalDividerColour(): Int {

        return ColourConfig.Articles.Search.mVerticalDividerColour

    }

    private fun updateResults(results: ArrayList<out IArticleContent>?, totalCount: Int) {

        isLoadingResults = false

        mTotalResults = totalCount

        if (mAdapter?.articles == null) {

            mAdapter?.articles = arrayListOf()

        }

        resultsViewFlipper.postDelayed({

            if (results != null) {

                mAdapter?.articles?.addAll(results)

                currentResultsPage = (mAdapter?.articles?.size ?: 0)

                mRecyclerView?.post {

                    mAdapter?.notifyDataSetChanged()

                }

            }

            resultsCount.post {

                resultsCount.text = Utils.getSpanned(totalResultsCount?.replace("{0}", "<b>$totalCount</b>"))

            }

            if (searchProgress.visibility != View.GONE) {

                searchProgress.visibility = View.GONE

            }

            pagingProgress.slideDown(duration = animationDuration)

            if (mAdapter?.articles == null || mAdapter?.articles?.isEmpty() == true) {

                failedSearch()

            } else {

                searchSuccessful()

            }

        }, MINIMUM_SEARCH_DELAY)

    }

    private fun searchSuccessful() {

        resultsViewFlipper.post {

            resultsViewFlipper.displayedChild = 1

        }

    }

    private fun failedSearch() {

        resultsViewFlipper.post {

            resultsViewFlipper.displayedChild = 2

        }

    }

    override fun clickedSearch(view: View?) {

        /*searchArticles_searchTerm.clearFocus()

        val service = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        service.hideSoftInputFromWindow(searchArticles_searchTerm.windowToken, 0)

        searchTerm = searchArticles_searchTerm.text.toString()*/

        resultsCount.post {

            resultsCount.text = ""

        }

        if (!TextUtils.isEmpty(searchTerm)) {

            searchView.isIconified = true

            titleText.text = "\"$searchTerm\""
            titleText.visibility = View.VISIBLE

            currentResultsPage = 0

            resultsViewFlipper.displayedChild = 0

            searchProgress.visibility = View.VISIBLE

            mRecyclerView?.layoutManager?.scrollToPosition(0)

            if (mAdapter is SearchArticlesAdapter) {

                mAdapter?.let { (it as SearchArticlesAdapter).expandedItems.clear() }

            }

            mAdapter?.articles?.clear()

            doSearch()

        } else if (view == null) {

            Toast.makeText(this, R.string.toast_error_nothingToSearch, Toast.LENGTH_SHORT).show()

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun titleClicked(view: View) {

        titleText.visibility = View.GONE
        searchView.isIconified = false

    }

    private fun doSearch() {

        if (!TextUtils.isEmpty(searchTerm)) {

            searchView.isIconified = true

            cancelSearches()

            if (NetworkUtils.isConnected(this)) {

                if (searchRunnerOnline != null) {

                    mApplication?.mOffUiHandler?.post(searchRunnerOnline)

                }

            } else {

                if (searchRunnerOffline != null) {

                    mApplication?.mOffUiHandler?.post(searchRunnerOffline)

                }
            }

        }

    }

    private fun cancelSearches() {

        if (searchRunnerOffline != null) {

            mApplication?.mOffUiHandler?.removeCallbacks(searchRunnerOffline)

        }

        if (searchRunnerOnline != null) {

            mApplication?.mOffUiHandler?.removeCallbacks(searchRunnerOnline)

        }

    }

    override fun onDestroy() {

        cancelSearches()

        super.onDestroy()

    }

    private fun changeSearchViewTextColor(view: View?) {

        try {

            if (view != null) {

                view.background = null

                when (view) {

                    is TextView -> {

                        view.setTextColor(headerForegroundColour)

                        if (view is EditText) {

                            view.setHintTextColor(view.textColors)
                            view.hint = getString(R.string.searchArticles_hintText)

                        }

                        if (view.compoundDrawables.isNotEmpty()) {

                            for (drawable in view.compoundDrawables) {

                                drawable?.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

                            }

                        }

                    }
                    is ImageView -> {

                        val draw = view.drawable

                        draw?.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

                    }
                    is ViewGroup -> {

                        val childCount = view.childCount

                        for (i in 0 until childCount) {

                            changeSearchViewTextColor(view.getChildAt(i))

                        }

                    }
                }

            }

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

    }

    private fun searchButtonClosed(): Boolean {

        return false

    }

    override fun getNormalBackgroundColour(): Int {
        return ColourConfig.Articles.Search.Button.mBackgroundColour
    }

    override fun getNormalForegroundColour(): Int {
        return ColourConfig.Articles.Search.Button.mForegroundColour
    }

    override fun getCheckedBackgroundColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedBackgroundColour
    }

    override fun getPressedBackgroundColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedBackgroundColour
    }

    override fun getSelectedBackgroundColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedBackgroundColour
    }

    override fun getNormalBorderColour(): Int {
        return ColourConfig.Articles.Search.Button.mForegroundColour
    }

    override fun getBorderWidth(): Int {
        return ColourConfig.Articles.Search.Button.mStrokeWidth
    }

    override fun getCheckedBorderColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedBackgroundColour
    }

    override fun getPressedBorderColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedBackgroundColour
    }

    override fun getSelectedBorderColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedBackgroundColour
    }

    override fun getPressedForegroundColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedForegroundColour
    }

    override fun getCheckedForegroundColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedForegroundColour
    }

    override fun getSelectedForegroundColour(): Int {
        return ColourConfig.Articles.Search.Button.mPressedForegroundColour
    }
}
