package com.pagesuite.pskotlinreaderapp.articles.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import java.util.*

class FragmentFromToPicker : DialogFragment() {

    private var toDateButton: TextView? = null
    private var fromDateButton: TextView? = null
    private var cancelButton: View? = null
    private var okButton: View? = null

    private val c = Calendar.getInstance()
    private val year = c.get(Calendar.YEAR)
    private val month = c.get(Calendar.MONTH)
    private val day = c.get(Calendar.DAY_OF_MONTH)

    var listener: FromToPickerListener? = null

    private var fromDateCalendar: Calendar? = null
    private var toDateCalendar: Calendar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.dialog_article_search_datepicker, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toDateButton = view.findViewById(R.id.toDate)

        toDateButton?.setOnClickListener {

            val datePicker = DatePickerDialog(it.context, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->

                toDateButton?.text = "$dayOfMonth/${month + 1}/$year"
                val calendar = Calendar.getInstance()
                calendar.set(year, month, dayOfMonth, 0, 0, 0)
                calendar.set(Calendar.MILLISECOND, 0)
                toDateCalendar = calendar

            }, year, month, day)

            datePicker.show()

        }

        fromDateButton = view.findViewById(R.id.fromDate)

        fromDateButton?.setOnClickListener {

            val datePicker = DatePickerDialog(it.context, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->

                fromDateButton?.text = "$dayOfMonth/${month + 1}/$year"
                val calendar = Calendar.getInstance()
                calendar.set(year, month, dayOfMonth, 0, 0, 0)
                calendar.set(Calendar.MILLISECOND, 0)
                fromDateCalendar = calendar

            }, year, month, day)

            datePicker.show()

        }

        cancelButton = view.findViewById(R.id.cancel)

        cancelButton?.setOnClickListener { dismiss() }

        okButton = view.findViewById(R.id.ok)

        okButton?.setOnClickListener {

            listener?.pickedDates(fromDateCalendar, toDateCalendar)

            dismiss()

        }

    }

    interface FromToPickerListener {

        fun pickedDates(fromText: Calendar?, toText: Calendar?)

    }

}