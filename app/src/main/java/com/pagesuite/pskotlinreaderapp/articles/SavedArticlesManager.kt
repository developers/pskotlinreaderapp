package com.pagesuite.pskotlinreaderapp.articles

import android.content.Context
import android.os.Looper
import android.util.Log
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.IArticleContent
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.pskotlinreaderapp.models.SavedArticle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.isActive
import java.io.*

object SavedArticlesManager {

    private const val ARTICLE_EXT: String = ".article"
    private const val PAGE_EXT: String = ".page"
    private const val TEMPLATE_EXT: String = ".template"
    private const val savedArticleDir: String = "savedArticles/"

    fun saveArticle(context: Context, savedArticle: Article, editionGuid: String, savedPage: Page, template: String? = null): Boolean {

        try {

            checkThread()

            var image = savedArticle.getFirstImagePath(context)

            if (image != null) {

                if (!image.startsWith("http") && !image.startsWith("file://")) {

                    image = "file://${context.filesDir?.absolutePath}/edition/$editionGuid/$image"
                    savedArticle.setFirstImagePath(image)

                }

            }

            savedArticle.pubDate = DataStore.Edition?.getPublicationDate()
            savedArticle.editionGuid = DataStore.Edition?.getEditionGuid()

            val articlesDir = File(context.filesDir.absolutePath + "/" + savedArticleDir)

            if (!articlesDir.exists()) {

                articlesDir.mkdirs()

            }

            val filePath = articlesDir.absolutePath + "/" + savedArticle.articleguid

            var success = writeObject(savedArticle, "$filePath.$ARTICLE_EXT")

            if (success) {

                success = writeObject(savedPage, "$filePath.$PAGE_EXT")

                if (success) {

                    return if (template != null) {

                        writeObject(template, "$filePath.$TEMPLATE_EXT")

                    } else {

                        true

                    }

                } else {

                    Log.w(SavedArticlesManager::class.simpleName, "Error writing: $filePath.$PAGE_EXT")

                }

            } else {

                Log.w(SavedArticlesManager::class.simpleName, "Error writing: $filePath.$ARTICLE_EXT")

            }

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return false

    }

    private fun writeObject(savedObject: Serializable, filePath: String): Boolean {

        try {

            checkThread()

            val success = deleteObject(filePath)

            if (success) {

                val fileOutputStream = FileOutputStream(filePath)
                val objectOutputStream = ObjectOutputStream(fileOutputStream)
                objectOutputStream.writeObject(savedObject)
                objectOutputStream.flush()
                objectOutputStream.close()
                fileOutputStream.flush()
                fileOutputStream.close()

                return true

            }

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return false

    }

    private fun deleteObject(filePath: String): Boolean {

        try {

            checkThread()

            val existingFile = File(filePath)

            return if (existingFile.exists()) {

                existingFile.delete()

            } else {

                true

            }

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return false

    }

    fun loadArticles(context: Context): ArrayList<SavedArticle>? {

        try {

            checkThread()

            val articlesDir = File(context.filesDir.absolutePath + "/" + savedArticleDir)

            if (articlesDir.exists()) {

                val files = articlesDir.listFiles()

                if (files.isNotEmpty()) {

                    val articlePairs = HashMap<String, SavedArticle>()

                    var fileInputStream: FileInputStream
                    var objectInputStream: ObjectInputStream
                    var fileContent: Any?
                    var savedArticle: SavedArticle?
                    var articleGuid: String?

                    for (file in files) {

                        if (GlobalScope.isActive) {

                            articleGuid = file.absolutePath.replace(articlesDir.absolutePath, "").replace(ARTICLE_EXT, "")

                            if (file.absolutePath.endsWith(ARTICLE_EXT)) {

                                fileInputStream = FileInputStream(file)
                                objectInputStream = ObjectInputStream(fileInputStream)
                                fileContent = objectInputStream.readObject()

                                if (fileContent is Article) {

                                    //articles.add(fileContent)
                                    savedArticle = articlePairs[articleGuid]

                                    if (savedArticle == null) {

                                        savedArticle = SavedArticle()

                                    }

                                    savedArticle.article = fileContent

                                    articlePairs[articleGuid] = savedArticle

                                }

                                objectInputStream.close()
                                fileInputStream.close()

                            } else if (file.absolutePath.endsWith(PAGE_EXT)) {

                                fileInputStream = FileInputStream(file)
                                objectInputStream = ObjectInputStream(fileInputStream)
                                fileContent = objectInputStream.readObject()

                                if (fileContent is Page) {

                                    savedArticle = articlePairs[articleGuid]

                                    if (savedArticle == null) {

                                        savedArticle = SavedArticle()

                                    }

                                    savedArticle.page = fileContent

                                    articlePairs[articleGuid] = savedArticle

                                }

                                objectInputStream.close()
                                fileInputStream.close()

                            }

                        } else {

                            break

                        }

                    }

                    if (articlePairs.isNotEmpty()) {

                        val articles = ArrayList<SavedArticle>()

                        articles.addAll(articlePairs.values)

                        return articles
                    }

                }

            }

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return null

    }

    fun loadArticle(context: Context, articleGuid: String): SavedArticle? {

        try {

            checkThread()

            val articlesDir = File(context.filesDir.absolutePath + "/" + savedArticleDir)

            val articleFile = File("${articlesDir.absolutePath}/$articleGuid.$ARTICLE_EXT")

            if (articleFile.exists()) {

                val savedArticle = SavedArticle()

                val fileInputStream = FileInputStream(articleFile)
                val objectInputStream = ObjectInputStream(fileInputStream)
                val fileContent = objectInputStream.readObject()

                if (fileContent is Article) {

                    savedArticle.article = fileContent

                }

                objectInputStream.close()
                fileInputStream.close()

                val pageFile = File("${articlesDir.absolutePath}/$articleGuid.$PAGE_EXT")

                if (pageFile.exists()) {

                    val fileInputStream = FileInputStream(pageFile)
                    val objectInputStream = ObjectInputStream(fileInputStream)
                    val fileContent = objectInputStream.readObject()

                    if (fileContent is Page) {

                        savedArticle.page = fileContent

                    }

                    objectInputStream.close()
                    fileInputStream.close()

                }

                val templateFile = File("${articlesDir.absolutePath}/$articleGuid.$TEMPLATE_EXT")

                if (templateFile.exists()) {

                    val fileInputStream = FileInputStream(templateFile)
                    val objectInputStream = ObjectInputStream(fileInputStream)
                    val fileContent = objectInputStream.readObject()

                    if (fileContent is String) {

                        savedArticle.template = fileContent

                    }

                    objectInputStream.close()
                    fileInputStream.close()

                }

                if (savedArticle.article != null && savedArticle.page != null) {

                    return savedArticle

                }

            }

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return null

    }

    fun deleteArticle(context: Context, article: IArticleContent): Boolean {

        try {

            checkThread()

            val articlesDir = File(context.filesDir.absolutePath + "/" + savedArticleDir)
            val articleFile = File("${articlesDir.absolutePath}/${article.getArticleGuid()}.$ARTICLE_EXT")

            val pageFile = File("${articlesDir.absolutePath}/${article.getArticleGuid()}.$PAGE_EXT")

            val deletedArticleFile = if (articleFile.exists()) {

                articleFile.delete()

            } else {

                true

            }

            val deletedPageFile = if (pageFile.exists()) {

                pageFile.delete()

            } else {

                true

            }

            return deletedArticleFile && deletedPageFile

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return false

    }

    private fun checkThread() {

        if (Looper.getMainLooper() == Looper.myLooper()) {

            throw Exception("WrongThreadException - this must not be called from the UI thread!")

        }

    }

}