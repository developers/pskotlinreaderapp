package com.pagesuite.pskotlinreaderapp.articles.provider

import android.content.SearchRecentSuggestionsProvider

class ArticleSearchProvider : SearchRecentSuggestionsProvider() {
    init {
        setupSuggestions(AUTHORITY, MODE)
    }

    companion object {
        const val AUTHORITY = "com.pagesuite.ArticleSearchProvider"
        const val MODE: Int = SearchRecentSuggestionsProvider.DATABASE_MODE_QUERIES
    }
}