package com.pagesuite.pskotlinreaderapp.articles.adapter

import android.content.Context
import android.graphics.PorterDuff
import android.text.TextUtils
import android.util.SparseBooleanArray
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.extensions.getLayoutInflater
import com.pagesuite.pskotlinreaderapp.models.SearchResult
import com.pagesuite.pskotlinreaderapp.utils.Utils

open class SearchArticlesAdapter(context: Context, foregroundColour: Int) : BasicArticlesAdapter(context, foregroundColour) {

    private val keywordText: String = context.getString(R.string.searchArticles_keywordFound)
    private val keywordsText: String = context.getString(R.string.searchArticles_keywordsFound)

    val expandedItems: SparseBooleanArray = SparseBooleanArray()

    val expandClickHandler: View.OnClickListener = View.OnClickListener {

        val tag = it.getTag(R.id.tag_index)

        if (tag is Int) {

            val key = expandedItems.indexOfKey(tag)

            if (key < 0) {

                expandedItems.put(tag, true)

            } else {

                expandedItems.delete(tag)

            }

            notifyItemChanged(tag)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasicViewHolder {

        if (viewType == 0) {

            return super.onCreateViewHolder(parent, viewType)

        }

        val inflater = parent.context.getLayoutInflater()

        val view = inflater.inflate(
                if (viewType == 1) {

                    R.layout.card_article_extended

                } else {

                    R.layout.card_article_extended_expanded

                }, parent, false)

        val viewHolder = ExtendedViewHolder(itemView = view)

        viewHolder.itemView.setOnClickListener(itemClickListener)

        return viewHolder

    }

    override fun getItemViewType(position: Int): Int {

        val article = articles?.get(position)

        if (article is SearchResult) {

            return if (expandedItems.indexOfKey(position) < 0) {

                1

            } else {

                2

            }

        }

        return super.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: BasicViewHolder, position: Int) {

        super.onBindViewHolder(holder, position)

        val article = articles?.getOrNull(position)

        if (article is SearchResult) {

            if (holder is ExtendedViewHolder) {

                val isExpanded = expandedItems.indexOfKey(position) < 0

                val description: String? = if (isExpanded) {

                    article.getArticleDescription()

                } else {

                    article.getExpandedArticleDescription()

                }

                if (!TextUtils.isEmpty(description)) {

                    holder.descriptionText.visibility = View.VISIBLE
                    holder.descriptionText.text = Utils.getSpanned(description
                            ?.removePrefix("\n\n")
                            ?.removePrefix("<br /><br />")
                            ?.removeSuffix("\n\n")
                            ?.removeSuffix("<br /><br />"))

                } else {

                    holder.descriptionText.visibility = View.GONE

                }

                val hitCount = article.hitCount

                if (hitCount == 1) {

                    holder.hitCountText.text = keywordText
                    holder.expandButton.visibility = View.GONE
                    holder.shadow.visibility = View.GONE

                } else {

                    holder.hitCountText.text = keywordsText.replace("{0}", hitCount.toString())
                    holder.expandButton.visibility = View.VISIBLE

                    if (!isExpanded) {

                        holder.shadow.visibility = View.GONE

                    } else {

                        holder.shadow.visibility = View.VISIBLE

                    }

                }

                holder.expandButton.setTag(R.id.tag_index, position)

            }

        }

        holder.itemView.setTag(R.id.tag_index, position)

    }

    override fun hideImageView(holder: BasicViewHolder) {

        holder.imageView.visibility = View.GONE

        (holder.imageView.layoutParams as? LinearLayout.LayoutParams)?.weight = 0f
        ((holder as ExtendedViewHolder).textContainer.layoutParams as? LinearLayout.LayoutParams)?.weight = 3f

    }

    override fun showImageView(holder: BasicViewHolder) {

        holder.imageView.visibility = View.VISIBLE

        (holder.imageView.layoutParams as? LinearLayout.LayoutParams)?.weight = 1f
        ((holder as ExtendedViewHolder).textContainer.layoutParams as? LinearLayout.LayoutParams)?.weight = 2f

    }

    open inner class ExtendedViewHolder(itemView: View) : BasicViewHolder(itemView) {

        var hitCountText: TextView = itemView.findViewById(R.id.textViewHitCount)
        var textContainer: ViewGroup = itemView.findViewById(R.id.textContainer)
        var expandButton: View = itemView.findViewById(R.id.expandDescriptionButton)
        var shadow: View = itemView.findViewById(R.id.shadow)

        init {

            expandButton.setOnClickListener(expandClickHandler)
            hitCountText.setTextColor(foregroundColour)

            shadow.background?.setColorFilter(ColourConfig.Articles.Search.mResultShadow, PorterDuff.Mode.SRC_ATOP)

        }

    }

}