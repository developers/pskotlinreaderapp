package com.pagesuite.pskotlinreaderapp.kiosk.adapter

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.models.Item
import com.pagesuite.pskotlinreaderapp.models.ItemGroup

class KioskMenuAdapter(private val parentContext: Context, val item: ItemGroup, private val itemClickListener: View.OnClickListener) : RecyclerView.Adapter<KioskMenuAdapter.SettingsHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsHolder {

        val view = LayoutInflater.from(parentContext).inflate(R.layout.edition_settings_menu_cell, parent, false)
        view.setOnClickListener(itemClickListener)
        return SettingsHolder(view)

    }

    override fun getItemCount(): Int {

        return item.items?.size ?: 0

    }

    override fun onBindViewHolder(holder: SettingsHolder, position: Int) {

        holder.bindSettingCell(item.items?.get(position))

    }

    inner class SettingsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var buttonTitle: TextView = itemView.findViewById(R.id.SectionName)

        private val rgbBackground = ColourConfig.Kiosk.NavigationDrawer.mBackgroundColour
        private val rgbForeground = ColourConfig.Kiosk.NavigationDrawer.mForegroundColour

        fun bindSettingCell(item: Item?) {

            if (item != null) {

                buttonTitle.text = item.text
                buttonTitle.setTypeface(null, Typeface.BOLD)
                itemView.setBackgroundColor(rgbBackground)
                buttonTitle.setTextColor(rgbForeground)
                itemView.setTag(R.id.tag_type, item.type)
                //itemView.setOnClickListener { settingSelected(item.type) }

            }

        }

    }


}