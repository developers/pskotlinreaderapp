package com.pagesuite.pskotlinreaderapp.kiosk.fragment

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.EndpointQueryRequests
import com.pagesuite.pskotlinreaderapp.kiosk.EditionManager
import com.pagesuite.pskotlinreaderapp.kiosk.adapter.KioskCellAdapter
import com.pagesuite.pskotlinreaderapp.models.KioskApplications

//The fragment to be shown in the Kiosk view pager. Each fragment will show the covers of all of the edition/replica in the category

class KioskCategoryPageFragment : Fragment() {

    private var editionActivityList: RecyclerView? = null
    private var cellAdaptor: KioskCellAdapter? = null
    var cellClickListener: View.OnClickListener? = null
    var alreadyLoading: (() -> Unit)? = null
    var categoryIndex: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_kiosk_category, container, false)

        editionActivityList = view.findViewById(R.id.recyclerView)

        return view

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        initAdapter()
        initRecyclerView()

    }

    private fun initAdapter() {

        //if the section selected is 'show all', the use the getAllApplicationsList to show all editions/Replicas. If not, just show the editions/ replicas in the category

        val category = DataStore.Kiosk?.categories?.getOrNull(categoryIndex)

        val applications: Array<KioskApplications?>? = if (category?.showall == true) {

            DataStore.Kiosk?.getAllApplicationsList()

        } else {

            category?.applications

        }

        if (applications != null) {

            cellAdaptor = KioskCellAdapter(activity as Context, applications, categoryIndex, View.OnClickListener {

                val tag = it.getTag(R.id.tag_index)

                if (tag is Int) {

                    if (cellAdaptor?.loadingIndex == -1) {

                        cellAdaptor?.loadingIndex = tag

                        cellAdaptor?.isNewEditionArray?.delete(tag)

                        val guid = cellAdaptor?.editions?.get(tag)?.applicationguid ?: ""

                        val currentStatus = EditionManager.getEditionStatus(activity as Context, guid)

                        EditionManager.setEditionStatus(activity as Context, guid, currentStatus[0] as Long, true)

                        cellAdaptor?.notifyItemChanged(tag)

                        cellClickListener?.onClick(it)

                    } else {

                        alreadyLoading?.invoke()

                    }

                }

            })

            requestLatestEditionData(applications)

        }

    }

    private fun initRecyclerView() {

        editionActivityList?.adapter = cellAdaptor
        //val span = (if (orientation == Configuration.ORIENTATION_PORTRAIT) 2 else 3)
        val span = resources.getInteger(R.integer.kiosk_edition_tileCount)

        val layoutManager = GridLayoutManager(context, span)
        editionActivityList?.layoutManager = layoutManager
        editionActivityList?.setHasFixedSize(true)

    }

    private fun requestLatestEditionData(applications: Array<KioskApplications?>) {

        val errorListener = {


        }

        var appGuid: String?

        for ((index, application) in applications.withIndex()) {

            appGuid = application?.applicationguid

            if (appGuid != null) {

                EndpointQueryRequests.getEditionLatestDate(activity as Context, { appGuid: String, cache: String ->

                    val currentStatus = EditionManager.getEditionStatus(activity as Context, appGuid)

                    if (currentStatus[0] == 0L) {

                        // new!
                        cellAdaptor?.isNewEditionArray?.put(index, 0)
                        cellAdaptor?.notifyItemChanged(index)

                        EditionManager.setEditionStatus(activity as Context, appGuid, cache.toLong(), false)

                    } else {

                        val updatedCache = cache.toLong()
                        val userHasRead = currentStatus[1] as Boolean

                        if (updatedCache != currentStatus[0] || !userHasRead) {

                            // new!
                            cellAdaptor?.isNewEditionArray?.put(index, 0)
                            cellAdaptor?.notifyItemChanged(index)

                            EditionManager.setEditionStatus(activity as Context, appGuid, cache.toLong(), false)

                        }

                    }

                }, errorListener, appGuid)

            }

        }

    }

    override fun onConfigurationChanged(newConfig: Configuration?) {

        super.onConfigurationChanged(newConfig)

        initRecyclerView()

    }

    fun resetLoadingItem() {

        cellAdaptor?.resetLoadingItem()

    }

}