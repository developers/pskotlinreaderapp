package com.pagesuite.pskotlinreaderapp.kiosk.adapter

import android.content.Context
import android.graphics.PorterDuff
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.SparseIntArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.models.KioskApplications


class KioskCellAdapter(val context: Context, val editions: Array<KioskApplications?>, val categoryIndex: Int, private val clickListener: View.OnClickListener?) : RecyclerView.Adapter<KioskCellAdapter.Holder>() {

    var isNewEditionArray: SparseIntArray = SparseIntArray()

    var loadingIndex: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        val view = LayoutInflater.from(context).inflate(R.layout.kiosk_cell, parent, false)

        view.setOnClickListener(clickListener)

        return if (viewType == 0) {

            Holder(view)

        } else {

            LoadingHolder(view)

        }

    }

    override fun getItemViewType(position: Int): Int {

        if (position == loadingIndex) {

            return 1

        }

        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {

        return editions.count()

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        val edition = editions.getOrNull(position)

        if (edition != null) {

            holder.bindCategory(edition, position, context)

        }

        holder.itemView.setTag(R.id.tag_index, position)
    }

    override fun onViewRecycled(holder: Holder) {

        super.onViewRecycled(holder)

        Glide.with(context).clear(holder.editionCover)

        holder.editionCover.setImageResource(0)
    }

    fun resetLoadingItem() {

        val updateItem = loadingIndex
        loadingIndex = -1
        notifyItemChanged(updateItem)

    }

    inner class LoadingHolder(itemView: View) : Holder(itemView) {

        private val editionLoading: ProgressBar = itemView.findViewById(R.id.editionLoading)

        override fun bindCategory(editionItem: KioskApplications, editionIndex: Int, context: Context) {

            super.bindCategory(editionItem, editionIndex, context)

            editionLoading.visibility = View.VISIBLE

        }

    }

    open inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val editionName: TextView = itemView.findViewById(R.id.categoryName)
        val editionCover: ImageView = itemView.findViewById(R.id.editionCover)
        private val pDFIndicator: View = itemView.findViewById(R.id.PDFIndicationContainer)
        private val isNewEdition: View = itemView.findViewById(R.id.newEditionIndicator)
        private val isNewEditionText: TextView = itemView.findViewById(R.id.newEditionIndicatorText)

        init {

            isNewEdition.background?.setColorFilter(ColourConfig.Kiosk.KioskApplications.mIsNewEditionBackgroundColour, PorterDuff.Mode.SRC_ATOP)
            isNewEditionText.setTextColor(ColourConfig.Kiosk.KioskApplications.mIsNewEditionForegroundColour)

        }

        open fun bindCategory(editionItem: KioskApplications, editionIndex: Int, context: Context) {

            if (editionItem.apptype == UsefulConstants.APP_TYPE_REPLICA) {

                pDFIndicator.visibility = View.VISIBLE

            } else {

                pDFIndicator.visibility = View.GONE

            }

            if (isNewEditionArray.get(editionIndex, -1) == -1) {

                isNewEdition.visibility = View.GONE

            } else {

                isNewEdition.visibility = View.VISIBLE

            }

            editionName.text = editionItem.name

            if (!TextUtils.isEmpty(editionItem.image)) {

                Glide
                        .with(context)
                        .load(editionItem.image)
                        .into(editionCover)

            } else {

                editionCover.setImageResource(0)

            }

            itemView.setTag(R.id.tag_edition, editionIndex)

            itemView.setTag(R.id.tag_categoryIndex, categoryIndex)

        }

    }

}