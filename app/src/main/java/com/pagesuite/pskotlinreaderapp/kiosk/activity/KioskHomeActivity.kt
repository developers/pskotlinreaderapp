package com.pagesuite.pskotlinreaderapp.kiosk.activity

import android.graphics.Color
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.Toast
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.kiosk.adapter.KioskPagerAdaptor
import com.pagesuite.pskotlinreaderapp.utils.Utils
import kotlinx.android.synthetic.main.activity_kiosk_home.*

class KioskHomeActivity : KioskBaseActivity() {

    private var pagerAdapter: KioskPagerAdaptor? = null
    var isFromPageSwipe: Boolean = false

    override fun getLayout(): Int {

        return R.layout.activity_kiosk_home

    }

    override fun prepKioskComponents() {

        prepAdapter()
        prepViewPager()
        prepSectionToolbar()

    }

    private fun prepViewPager() {

        val kioskData = DataStore.Kiosk ?: return

        categoryViewPager.adapter = pagerAdapter

        val pageNumber = intent.getIntExtra(UsefulConstants.ARGS_PAGE_NUMBER, 0)
        categoryViewPager.setCurrentItem(pageNumber, false)

        val indicatorColour = DataStore.Kiosk?.indicatorcolor

        kioskCategorySelectionTabBar.setSelectedTabIndicatorColor(if (indicatorColour != null) {

            Utils.convertRgbToColour(indicatorColour)

        } else {

            headerForegroundColour

        })

        val selectedColour = DataStore.Kiosk?.selectedcolor
        val availableColour = DataStore.Kiosk?.availablecolor

        val selectedTextColour = if (selectedColour != null) {

            Utils.convertRgbToColour(selectedColour)

        } else {

            Color.WHITE

        }

        val availableTextColour = if (availableColour != null) {

            Utils.convertRgbToColour(availableColour)

        } else {

            Color.WHITE

        }

        kioskCategorySelectionTabBar.setTabTextColors(availableTextColour, selectedTextColour)

        for (category in kioskData.categories) {

            val newTab = kioskCategorySelectionTabBar.newTab().setText(category.name)
            kioskCategorySelectionTabBar.addTab(newTab)

        }

    }

    private fun prepAdapter() {

        if (categoryViewPager != null) {

            categoryViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {

                    isFromPageSwipe = true
                    kioskCategorySelectionTabBar.getTabAt(position)?.select()

                }

            })

        }

        pagerAdapter = KioskPagerAdaptor(supportFragmentManager, View.OnClickListener {

            onEditionCellClick(it)

        }, alreadyLoading = {

            Toast.makeText(this, R.string.toast_error_alreadyLoadingApplication, Toast.LENGTH_SHORT).show()

        })

    }

    private fun prepSectionToolbar() {

        kioskCategorySelectionTabBar.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {

                if (!isFromPageSwipe) {

                    categoryViewPager.setCurrentItem(kioskCategorySelectionTabBar.selectedTabPosition, true)

                }

                isFromPageSwipe = false

            }

        })

    }

    override fun showLoadingSpinner() {

    }

    override fun hideLoadingSpinner() {

        pagerAdapter?.resetLoadingItem(categoryViewPager.currentItem)

    }

}