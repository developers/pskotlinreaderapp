package com.pagesuite.pskotlinreaderapp.kiosk.activity

import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicToolbarActivity
import com.pagesuite.pskotlinreaderapp.archive.activity.ArchiveActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.EndpointQueryRequests
import com.pagesuite.pskotlinreaderapp.centraldatapoints.Queries
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.config.MenuConfig
import com.pagesuite.pskotlinreaderapp.kiosk.adapter.KioskMenuGroupAdapter
import com.pagesuite.pskotlinreaderapp.models.*
import com.pagesuite.readersdk.components.Listeners
import com.pagesuite.readersdk.objects.EditionStub
import com.pagesuite.readersdk.statics.ReaderManager
import kotlinx.android.synthetic.main.view_kiosk_menu.*
import kotlinx.android.synthetic.main.view_kiosk_toolbar.*
import java.util.*

abstract class KioskBaseActivity : BasicToolbarActivity() {

    abstract fun prepKioskComponents()

    var kioskToolbar: View? = null
    var kioskBody: View? = null
    var KioskLogoHeader: ImageView? = null
    var backgroundImage: ImageView? = null
    var backgroundImageGradient: ImageView? = null

    var navigationDrawer: DrawerLayout? = null
    private var settingItems: ArrayList<ItemGroup> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        kioskToolbar = findViewById(R.id.kioskToolbar)
        kioskBody = findViewById(R.id.kioskBody)
        KioskLogoHeader = findViewById(R.id.KioskLogoHeader)
        backgroundImage = findViewById(R.id.backgroundImage)
        backgroundImageGradient = findViewById(R.id.backgroundImageGradient)

        navigationDrawer = findViewById(R.id.drawerMenu)

        prepDrawerMenu()

        prepKioskAppearance()

        prepKioskComponents()

        mApplication?.isFromKiosk = true

    }

    override fun initColourScheme() {

        super.initColourScheme()

        headerForegroundColour = ColourConfig.Kiosk.Toolbar.mForegroundColour
        headerBackgroundColour = ColourConfig.Kiosk.Toolbar.mBackgroundColour

        kioskBody?.setBackgroundColor(ColourConfig.Kiosk.mBackgroundColour)

        kioskToolbar?.setBackgroundColor(headerBackgroundColour)

        menuButton.drawable?.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

        recyclerView.setBackgroundColor(ColourConfig.Kiosk.NavigationDrawer.mBackgroundColour)

    }

    private fun prepDrawerMenu() {

        val defaultMenuItems = MenuConfig.Kiosk.mItemGroups

        if (defaultMenuItems != null) {

            settingItems.addAll(defaultMenuItems)

        }

        recyclerView.adapter = KioskMenuGroupAdapter(this, settingItems, View.OnClickListener {

            val tag = it.getTag(R.id.tag_type)

            if (tag is String) {

                mApplication?.settingSelected(this, tag)

            }

        })

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

    }

    private fun prepKioskAppearance() {

        if (DataStore.Kiosk == null) return

        if (backgroundImageGradient != null) {

            /*val topGradient = DataStore.Kiosk?.backgroundgradienttop
            val bottomGradient = DataStore.Kiosk?.backgroundgradientbottom

            if (topGradient != null && bottomGradient != null) {

                val gradientDrawable = GradientDrawable()
                val colours = IntArray(2)
                colours[0] = Utils.convertRgbToColour(topGradient, "0.6")
                colours[1] = Utils.convertRgbToColour(bottomGradient, "0.6")
                gradientDrawable.colors = colours

                backgroundImageGradient?.setImageDrawable(gradientDrawable)

            }*/

            val topGradient = ColourConfig.Kiosk.mBackgroundGradientTop
            val middleGradient = ColourConfig.Kiosk.mBackgroundGradientMiddle
            val bottomGradient = ColourConfig.Kiosk.mBackgroundGradientBottom

            val gradientDrawable = GradientDrawable()
            gradientDrawable.colors = intArrayOf(topGradient, middleGradient, bottomGradient)

            backgroundImageGradient?.setImageDrawable(gradientDrawable)

        }

        val backgroundImageUrl = DataStore.Kiosk?.backgroundimage

        if (backgroundImageUrl != null && backgroundImage != null) {

            backgroundImage?.let {

                Glide
                        .with(this)
                        .load(backgroundImageUrl)
                        .into(it)

            }

        } /*else {

            val headerColor = DataStore.Kiosk?.headercolor

            if (headerColor != null) {

                kioskToolbar?.setBackgroundColor(Utils.covertStringToRGB(headerColor))

            } else {

                kioskToolbar?.setBackgroundColor(Color.WHITE)

            }

            val backgroundColor = DataStore.Kiosk?.backgroundcolor

            if (backgroundColor != null) {

                kioskBody?.setBackgroundColor(Utils.covertStringToRGB(backgroundColor))

            } else {

                kioskBody?.setBackgroundColor(Color.WHITE)

            }

        }*/

        var headerLogo = DataStore.Kiosk?.appHeader?.headerlogo

        if (headerLogo == null) {

            headerLogo = DataStore.Application?.header?.headerlogo

        }

        if (headerLogo != null && KioskLogoHeader != null) {

            KioskLogoHeader?.let {

                Glide
                        .with(this)
                        .load(headerLogo)
                        .into(it)

            }

        }

    }

    ///////////Navigation from kiosk to activity/////////////

    fun onEditionCellClick(view: View) {

        val index = view.getTag(R.id.tag_categoryIndex)
        val editionIndex = view.getTag(R.id.tag_edition)

        if (index is Int && editionIndex is Int) {

            val category = DataStore.Kiosk?.categories?.getOrNull(index)

            val applications: Array<KioskApplications?>? = if (category?.showall == true) {

                DataStore.Kiosk?.getAllApplicationsList()

            } else {

                category?.applications

            }

            val application = applications?.get(editionIndex)

            if (application != null) {

                val appType = application.apptype //get the app type for the edition typed. if it is set to null, set it to disabled. Disabled is used for edition not yet ready to be shown

                if (appType != UsefulConstants.APP_TYPE_DISABLED) { //only move to a new activity if the app type isn't disabled

                    if (appType == UsefulConstants.APP_TYPE_REPLICA) {

                        val currentActivity = this

                        val pubGuid = application.publicationguid

                        if (pubGuid != null) {

                            val stubs = ReaderManager.getInstance().mEditionStubs[pubGuid]

                            if (stubs != null && stubs.isNotEmpty()) {

                                mApplication?.loadReader(currentActivity, stubs[0].mEditionGuid)

                            } else {

                                showLoadingSpinner()

                                ReaderManager.getInstance().mEditionsDownloadlisteners[pubGuid] = object : Listeners.EditionsDownloadListener {

                                    override fun editionsDownloaded(stubs: ArrayList<EditionStub>?) {

                                        hideLoadingSpinner()

                                        ReaderManager.getInstance().mEditionsDownloadlisteners.remove(pubGuid)

                                        if (stubs != null && stubs.isNotEmpty()) {

                                            mApplication?.loadReader(currentActivity, stubs[0].mEditionGuid)

                                        } else {

                                            ReaderManager.displayToast(currentActivity, getString(R.string.toast_error_noEditions), false)

                                        }

                                    }

                                    override fun editionsFailed() {

                                        hideLoadingSpinner()

                                        ReaderManager.getInstance().mEditionsDownloadlisteners.remove(pubGuid)

                                        ReaderManager.displayToast(currentActivity, getString(R.string.toast_error_noEditions), false)

                                    }

                                }

                                ReaderManager.getInstance().downloadEditions(pubGuid)

                            }

                        } else {

                            ReaderManager.displayToast(currentActivity, getString(R.string.toast_error_noEditions), false)

                        }

                    } else {

                        showLoadingSpinner()
                        makeApplicationQuery(application)

                    }

                }

            }

        }

    }

    //Make the application query, based on the app guide from the selection made in the kiosk

    private fun makeApplicationQuery(application: KioskApplications?) {

        if (application != null) {

            val shortCode = application.shortcode

            if (shortCode != null) {

                Queries.getCacheKey(this, shortCode, successHandler = {

                    if (it != null) {

                        val applicationGuid = application.applicationguid

                        if (applicationGuid != null) {

                            Queries.getApplication(this, applicationGuid, it) { application, json ->

                                if (application != null) {

                                    if (json != null) {

                                        mApplication?.saveAppConfig(json, suffix = shortCode, successListener = {

                                            loadKioskEdition(application)

                                        })

                                    }

                                } else {

                                    Log.w(KioskHomeActivity::class.simpleName, "makeApplicationQuery is null")

                                    failedToLoadEdition()

                                }

                            }

                            return@getCacheKey

                        }

                    }

                    hideLoadingSpinner()
                    failedToLoadEdition()

                })

            }

        }

    }

    abstract fun showLoadingSpinner()
    abstract fun hideLoadingSpinner()

    private fun failedToLoadEdition() {

        runOnUiThread {

            Toast.makeText(this, R.string.toast_error_noEditions, Toast.LENGTH_SHORT).show()

        }

    }

    private fun loadKioskEdition(application: Application?) {

        if (application != null) {

            if (application.app?.openmode?.equals(UsefulConstants.KIOSK_LAUNCH_EDITION, ignoreCase = true) != true) {

                EndpointQueryRequests.downloadArchive(context = this, listener = {

                    hideLoadingSpinner()

                    val stackZero = DataStore.EditionApplication?.editionsOrganisedByDay?.firstOrNull()

                    if (stackZero != null) {

                        if (stackZero.size > 1 || application.app?.openmode?.equals(UsefulConstants.KIOSK_LAUNCH_ARCHIVE, ignoreCase = true) == true) {

                            runOnUiThread {

                                mApplication?.moveToActivity(this, ArchiveActivity::class.java, finishCurrentActivity = false)

                            }

                        } else {

                            val edition = stackZero[0]

                            if (edition is Edition) {

                                DataStore.Edition = edition
                                DataStore.EditionApplication?.activeEdition = edition.getEditionGuid()

                                moveToEditionViaEditionGuid()


                            } else if (edition is ReplicaEdition) {

                                mApplication?.loadReader(this, edition.getEditionGuid())

                            }


                        }

                    }

                }, failureListener = {

                    hideLoadingSpinner()

                    if (application.liveEditions?.isNotEmpty() == true) {

                        DataStore.Edition = application.liveEditions?.firstOrNull()

                    }

                    moveToEditionViaEditionGuid()

                })

            } else {

                hideLoadingSpinner()

                if (application.liveEditions?.isNotEmpty() == true) {

                    DataStore.Edition = application.liveEditions?.firstOrNull()

                }

                moveToEditionViaEditionGuid()

            }

        }

    }

    //move to the edition view with an edition guid
    private fun moveToEditionViaEditionGuid() {

        runOnUiThread {

            mApplication?.reloadEdition(this, false)

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun clickedMenuButton(view: View) {

        navigationDrawer?.openDrawer(Gravity.END)

    }

    override fun onBackPressed() {

        if (navigationDrawer?.isDrawerOpen(Gravity.END) == true) {

            navigationDrawer?.closeDrawers()

            return

        }

        super.onBackPressed()
    }

}