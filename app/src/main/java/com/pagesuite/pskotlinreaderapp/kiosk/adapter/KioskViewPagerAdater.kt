package com.pagesuite.pskotlinreaderapp.kiosk.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.kiosk.fragment.KioskCategoryPageFragment

//The adaptor for the view pager, showing the editions in each category

class KioskPagerAdaptor(fragmentManager: FragmentManager, private val cellClickListener: View.OnClickListener?, private var alreadyLoading: (() -> Unit)? = null) : FragmentStatePagerAdapter(fragmentManager) {

    private val fragments: SparseArray<KioskCategoryPageFragment> = SparseArray()

    override fun getItem(index: Int): Fragment {

        val fragment = KioskCategoryPageFragment()

        fragment.categoryIndex = index

        fragment.cellClickListener = cellClickListener
        fragment.alreadyLoading = alreadyLoading

        fragments.put(index, fragment)

        return fragment

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        fragments.remove(position)

        super.destroyItem(container, position, `object`)
    }

    override fun getCount(): Int {

        return DataStore.Kiosk?.categories?.count() ?: 0

    }

    fun resetLoadingItem(position: Int) {

        val fragment = fragments.get(position, null)

        if (fragment != null) {

            fragment.resetLoadingItem()

        }

    }

}