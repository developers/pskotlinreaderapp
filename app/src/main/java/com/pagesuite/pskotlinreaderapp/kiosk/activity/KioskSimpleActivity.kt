package com.pagesuite.pskotlinreaderapp.kiosk.activity

import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.kiosk.fragment.KioskCategoryPageFragment


class KioskSimpleActivity : KioskBaseActivity() {

    private var fragmentContent: KioskCategoryPageFragment? = null

    override fun prepKioskComponents() {

        fragmentContent = KioskCategoryPageFragment()

        fragmentContent?.categoryIndex = 0

        fragmentContent?.cellClickListener = View.OnClickListener {

            onEditionCellClick(it)

        }

        fragmentContent?.alreadyLoading = {


        }

        fragmentContent?.let {

            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragmentHolder_container, it)
            transaction.commit()

        }

    }

    override fun getLayout(): Int {

        return R.layout.activity_kiosk_simple

    }

    override fun showLoadingSpinner() {

    }

    override fun hideLoadingSpinner() {

        fragmentContent?.resetLoadingItem()

    }

}