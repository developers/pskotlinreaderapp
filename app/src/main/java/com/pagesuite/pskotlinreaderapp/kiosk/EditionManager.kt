package com.pagesuite.pskotlinreaderapp.kiosk

import android.content.Context
import android.content.SharedPreferences

object EditionManager {

    private const val fileName: String = "editionStatus.bodge"
    private const val keyUerHasRead: String = "userHasRead_"
    private const val keyEditionDate: String = "editionDate_"

    private fun getSharedPreferences(context: Context): SharedPreferences {

        return context.getSharedPreferences(fileName, Context.MODE_PRIVATE)

    }

    fun getEditionStatus(context: Context, editionGuid: String): Array<Any> {

        val prefs = getSharedPreferences(context)

        return arrayOf(prefs.getLong("$keyEditionDate$editionGuid", 0L),
                prefs.getBoolean("$keyUerHasRead$editionGuid", false))

    }

    fun setEditionStatus(context: Context, editionGuid: String, cache: Long, userHasRead: Boolean) {

        val prefs = getSharedPreferences(context).edit()

        prefs.putBoolean("$keyUerHasRead$editionGuid", userHasRead)
        prefs.putLong("$keyEditionDate$editionGuid", cache)

        prefs.apply()

    }

}