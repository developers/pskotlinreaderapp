package com.pagesuite.pskotlinreaderapp.subscriptions

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.Base64
import android.util.Log
import com.android.billingclient.api.*
import com.pagesuite.pskotlinreaderapp.BuildConfig
import com.pagesuite.pskotlinreaderapp.subscriptions.db.PurchaseRecord
import com.pagesuite.pskotlinreaderapp.subscriptions.db.PurchaseRepository
import org.json.JSONArray
import org.json.JSONObject
import java.nio.charset.Charset


class InAppManager : PurchasesUpdatedListener {

    private val TAG = InAppManager::class.simpleName

    private var billingClient: BillingClient? = null

    private var availableStoreSkus: List<SkuDetails>? = null

    private var applicationContent: Context? = null

    //private val apiKey = "AIzaSyDJGWNeJ8fL6QUTZ_8p7JMj87JcVNCPsfs"

    private var mPurchasesRepo: PurchaseRepository? = null

    fun init(context: AppCompatActivity, listener: ((success: Boolean, String?) -> Unit)?) {

        applicationContent = context

        val isStoreVersion = isStoreVersion(context)
        if (!isStoreVersion && !BuildConfig.DEBUG && !BuildConfig.IS_INTERNAL) {

            listener?.invoke(false, "NOT_FROM_STORE")
            return

        }

        initPurchasesDb(context)

        initBillingClient(context, listener)

        /*if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {

            // The SafetyNet Attestation API is available.

            SafetyNet.getClient(context).attest(System.currentTimeMillis().toString().toByteArray(Charsets.UTF_8), apiKey)
                    .addOnSuccessListener(context) { response ->

                        // Indicates communication with the service was successful.
                        // Use response.jwsResult to get the result data.

                        val attestationStatement = OfflineVerify.process(response.jwsResult)
                        if (attestationStatement != null) {

                            if (attestationStatement.isCtsProfileMatch) {

                                initBillingClient(context, listener)

                            } else {

                                val hasAdvice = attestationStatement.containsKey("advice")
                                if (hasAdvice) {

                                    listener?.invoke(false, attestationStatement["advice"] as String)

                                }

                            }
                        } else {

                            listener?.invoke(false, null)

                        }

                    }
                    .addOnFailureListener(context) { e: Exception ->

                        // An error occurred while communicating with the service.
                        if (e is ApiException) {
                            // An error with the Google Play services API contains some
                            // additional details.
                            // You can retrieve the status code using the
                            // e.statusCode property.
                        } else {
                            // A different, unknown type of error occurred.
                            Log.d(TAG, "Error: ${e.message}")
                        }

                    }

        }*/

    }

    private fun initPurchasesDb(context: AppCompatActivity) {

        mPurchasesRepo = PurchaseRepository(context.application)

        mPurchasesRepo?.allPurchases?.observeForever {

            // Do *NOT* call InAppManager#savePurchasesToDb() from this observer, you will cause an infinite loop

            if (it != null) {

                for (purchaseRecord in it) {

                    Log.w("Simon", "PurchaseRecord: ${purchaseRecord.getSku()}")

                }

                verifyPurchases(it)

            }

        }

    }

    private fun initBillingClient(context: AppCompatActivity, listener: ((success: Boolean, String?) -> Unit)?) {

        billingClient = BillingClient.newBuilder(context).setListener(this).build()

        billingClient?.startConnection(object : BillingClientStateListener {

            override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {

                if (billingResponseCode == BillingClient.BillingResponse.OK) {

                    // The billing client is ready. You can query purchases here.
                    Log.w(TAG, "Billing client ready")

                } else {

                    Log.e(TAG, "Billing not available")

                }

                listener?.invoke(true, null)

            }

            override fun onBillingServiceDisconnected() {

                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.

                Log.w(TAG, "Disconnected from billing service")
            }

        })

    }

    fun queryInAppProducts(productType: String = BillingClient.SkuType.INAPP, skuList: ArrayList<String> = arrayListOf("android.test.purchased"), listener: ((Int) -> Unit)? = null) {

        if (billingClient != null) {

            val params = SkuDetailsParams.newBuilder()
            params.setSkusList(skuList).setType(productType)

            billingClient?.querySkuDetailsAsync(params.build()) { responseCode, skuDetailsList ->

                Log.w(TAG, "Response Code: $responseCode")

                if (responseCode == BillingClient.BillingResponse.OK) {

                    availableStoreSkus = skuDetailsList

                    if (skuDetailsList != null) {

                        for (skuDetails in skuDetailsList) {

                            val sku = skuDetails.sku
                            val price = skuDetails.price

                            Log.w(TAG, "$sku : $price")

                        }

                    }

                }

                listener?.invoke(responseCode)

            }

        } else {

            listener?.invoke(BillingClient.BillingResponse.BILLING_UNAVAILABLE)

        }

    }

    /**
     * Update the app's list of purchases for the active user.
     */
    fun queryPurchases(purchaseType: String = BillingClient.SkuType.INAPP, listener: ((Int) -> Unit)? = null) {

        if (billingClient != null) {

            val purchases = billingClient?.queryPurchases(purchaseType)

            if (purchases != null) {

                if (purchases.responseCode == BillingClient.BillingResponse.OK) {

                    if (purchases.purchasesList != null && purchases.purchasesList.isNotEmpty()) {

                        savePurchasesToDb(purchases.purchasesList)

                    } else {

                        Log.w(TAG, "No purchases found for type $purchaseType")

                    }

                }

                listener?.invoke(purchases.responseCode)

            } else {

                listener?.invoke(BillingClient.BillingResponse.ERROR)

            }

        } else {

            listener?.invoke(BillingClient.BillingResponse.BILLING_UNAVAILABLE)

        }

    }

    /**
     * Forcibly update the list of purchases for the active user from the Play Store.
     *
     * Note, this requires the device to be online
     */
    fun restorePurchases(purchaseType: String = BillingClient.SkuType.INAPP, listener: ((Int) -> Unit)? = null) {

        if (billingClient != null) {

            billingClient?.queryPurchaseHistoryAsync(purchaseType) { responseCode, purchasesList ->

                if (responseCode == BillingClient.BillingResponse.OK && purchasesList != null) {

                    savePurchasesToDb(purchasesList)

                }

                listener?.invoke(responseCode)
            }

        } else {

            listener?.invoke(BillingClient.BillingResponse.BILLING_UNAVAILABLE)

        }

    }

    fun purchaseInAppItem(context: AppCompatActivity, skuId: String, listener: ((Int) -> Unit)? = null) {

        if (billingClient != null) {

            val skuDetails = availableStoreSkus?.firstOrNull { it.sku.equals(skuId, true) }

            if (skuDetails != null) {

                val flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails)
                        .build()

                val responseCode = billingClient?.launchBillingFlow(context, flowParams)
                        ?: BillingClient.BillingResponse.BILLING_UNAVAILABLE

                if (responseCode != BillingClient.BillingResponse.OK) {

                    listener?.invoke(responseCode)

                }

            }

        } else {

            listener?.invoke(BillingClient.BillingResponse.BILLING_UNAVAILABLE)

        }

    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {

        when (responseCode) {

            BillingClient.BillingResponse.OK -> {

                if (purchases != null) {

                    savePurchasesToDb(purchases)

                }

            }
            BillingClient.BillingResponse.USER_CANCELED -> {

            }
            BillingClient.BillingResponse.ITEM_ALREADY_OWNED -> {

            }
            BillingClient.BillingResponse.BILLING_UNAVAILABLE -> {

            }
            BillingClient.BillingResponse.SERVICE_UNAVAILABLE -> {

            }
            BillingClient.BillingResponse.DEVELOPER_ERROR,
            BillingClient.BillingResponse.ERROR,
            BillingClient.BillingResponse.FEATURE_NOT_SUPPORTED,
            BillingClient.BillingResponse.SERVICE_DISCONNECTED,
            BillingClient.BillingResponse.ITEM_NOT_OWNED,
            BillingClient.BillingResponse.ITEM_UNAVAILABLE -> {

            }

        }

    }

    private fun savePurchasesToDb(purchases: List<Purchase>) {

        Log.w(TAG, "Purchased: $purchases")

        val orderIds = purchases.map { it.orderId }

        mPurchasesRepo?.hasRecord(orderIds) { recordList ->

            var toProcess = purchases

            if (recordList.isNotEmpty()) {

                for (record in recordList) {

                    Log.w("Simon", "Order ID already exists for ${record.getOrderId()}")

                }

                val existingIds = recordList.map { it.getOrderId() }

                toProcess = toProcess.filter { unprocess -> existingIds.firstOrNull { it == unprocess.orderId } == null }
            }

            if (toProcess.isNotEmpty()) {

                var purchaseRecord: PurchaseRecord?

                for (purchase in toProcess) {

                    purchaseRecord = PurchaseRecord()
                    //purchaseRecord.setLastUpdated(System.currentTimeMillis())
                    purchaseRecord.setOrderId(purchase.orderId)
                    purchaseRecord.setOriginalJson(purchase.originalJson)
                    purchaseRecord.setPurchaseToken(purchase.purchaseToken)
                    purchaseRecord.setSignature(purchase.signature)
                    purchaseRecord.setSku(purchase.sku)

                    mPurchasesRepo?.insert(purchaseRecord)

                }

            }

        }


        /*//https://stackoverflow.com/a/35388483/6082224

        val locallyValid = SignatureValidator.validate(base64PublicKey = "", signedData = purchase.originalJson, signature = purchase.signature)

        purchase.purchaseToken

        if (NetworkUtils.isConnected(applicationContent)) {

            // send to end-point


        } else {

            // verify locally?
            Log.w(TAG, "    Original JSON: ${purchase.originalJson}")

        }*/

    }

    private fun verifyPurchases(purchaseRecords: List<PurchaseRecord>) {

        val toVerify: List<PurchaseRecord> = purchaseRecords.filter { purchaseRecord ->

            (purchaseRecord.getLastUpdated() == 0L || (purchaseRecord.getLastUpdated() + DateUtils.MINUTE_IN_MILLIS < System.currentTimeMillis()))

        }

        val receiptJson = JSONObject()
        val receiptArray = JSONArray()
        var originalJson: JSONObject?

        for (purchase in toVerify) {

            Log.w("Simon", "${purchase.getOrderId()} : ${purchase.getOriginalJson()}")

            if (!TextUtils.isEmpty(purchase.getOriginalJson())) {

                originalJson = JSONObject(purchase.getOriginalJson())
                receiptArray.put(originalJson)
            }

        }

        receiptJson.put("orderReceipts", receiptArray)

        val encoded = Base64.encode(receiptJson.toString().toByteArray(Charset.defaultCharset()), DEFAULT_BUFFER_SIZE)
        Log.w("Simon", "Encoded: ${String(encoded)}")

    }

    private fun isStoreVersion(context: Context): Boolean {
        var isStoreVersion = false

        try {

            val installer = context.packageManager.getInstallerPackageName(context.packageName)
            isStoreVersion = !TextUtils.isEmpty(installer)

        } catch (e: Throwable) {

            e.printStackTrace()

        }

        return isStoreVersion

    }

}