package com.pagesuite.pskotlinreaderapp.subscriptions.db

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask


class PurchaseRepository internal constructor(application: Application) {

    private var mPurchaseDao: PurchaseDao? = null
    internal var allPurchases: LiveData<List<PurchaseRecord>>? = null

    init {

        val db = PurchaseDb.getDatabase(application)

        if (db != null) {

            mPurchaseDao = db.purchaseDao()
            allPurchases = db.purchaseDao().allPurchaseRecords

        }

    }


    fun insert(PurchaseRecord: PurchaseRecord) {

        mPurchaseDao?.let {

            insertAsyncTask(it).execute(PurchaseRecord)

        }

    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: PurchaseDao) : AsyncTask<PurchaseRecord, Void, Void>() {

        override fun doInBackground(vararg params: PurchaseRecord): Void? {

            mAsyncTaskDao.insert(params[0])
            return null

        }

    }

    fun hasRecord(orderIds: List<String>, listener: ((List<PurchaseRecord>) -> Unit)? = null) {

        mPurchaseDao?.let {

            val task = hasRecordsAsyncTask(it)
            task.listener = listener
            task.execute(orderIds)

        }

    }

    private class hasRecordsAsyncTask internal constructor(private val mAsyncTaskDao: PurchaseDao) : AsyncTask<List<String>, Void, List<PurchaseRecord>>() {

        var listener: ((List<PurchaseRecord>) -> Unit)? = null

        override fun doInBackground(vararg params: List<String>): List<PurchaseRecord>? {

            val results = mAsyncTaskDao.hasRecord(params[0])
            listener?.invoke(results)
            return results

        }

    }

}