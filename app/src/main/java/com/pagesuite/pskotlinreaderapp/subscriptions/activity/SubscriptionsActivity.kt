package com.pagesuite.pskotlinreaderapp.subscriptions.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicToolbarActivity
import com.pagesuite.pskotlinreaderapp.subscriptions.fragment.LoginFragment
import com.pagesuite.pskotlinreaderapp.subscriptions.fragment.RegistrationFragment

class SubscriptionsActivity : BasicToolbarActivity() {

    private var mCurrentFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        loadLoginFragment()

    }

    private fun loadLoginFragment() {

        mCurrentFragment = LoginFragment()

        mCurrentFragment?.let {

            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragmentHolder_container, it)
            transaction.commit()

        }

    }

    private fun loadRegistrationFragment() {

        mCurrentFragment = RegistrationFragment()

        mCurrentFragment?.let {

            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragmentHolder_container, it)
            transaction.commit()

        }

    }

    override fun getLayout(): Int {

        return R.layout.activity_subscriptions

    }

    @Suppress("UNUSED_PARAMETER")
    fun registerButtonClicked(view: View) {

        loadRegistrationFragment()

    }

    @Suppress("UNUSED_PARAMETER")
    fun haveAccountClicked(view: View) {

        loadLoginFragment()

    }

    override fun onBackPressed() {

        if (mCurrentFragment is RegistrationFragment) {

            loadLoginFragment()

            return

        }

        super.onBackPressed()
    }

}