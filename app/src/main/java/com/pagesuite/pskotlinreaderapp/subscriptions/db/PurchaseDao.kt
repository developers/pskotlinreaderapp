package com.pagesuite.pskotlinreaderapp.subscriptions.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query


@Dao
interface PurchaseDao {

    @get:Query("SELECT * from purchase_table ORDER BY orderId ASC")
    val allPurchaseRecords: LiveData<List<PurchaseRecord>>

    @Query("SELECT * from purchase_table WHERE orderId IN (:selectedOrderId) ORDER BY orderId ASC")
    fun hasRecord(selectedOrderId: List<String>): List<PurchaseRecord>

    @Insert
    fun insert(purchaseRecord: PurchaseRecord)

    @Query("DELETE FROM purchase_table")
    fun deleteAll()

}