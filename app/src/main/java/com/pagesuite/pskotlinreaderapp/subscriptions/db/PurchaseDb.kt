package com.pagesuite.pskotlinreaderapp.subscriptions.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context


@Database(entities = arrayOf(PurchaseRecord::class), version = 1, exportSchema = false)
abstract class PurchaseDb : RoomDatabase() {

    abstract fun purchaseDao(): PurchaseDao

    companion object {

        @Volatile
        private var INSTANCE: PurchaseDb? = null

        internal fun getDatabase(context: Context): PurchaseDb? {
            if (INSTANCE == null) {
                synchronized(PurchaseDb::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                PurchaseDb::class.java, "purchase_database")
                                .build()
                    }

                }

            }

            return INSTANCE

        }

    }

}