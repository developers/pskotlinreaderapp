package com.pagesuite.pskotlinreaderapp.subscriptions.db

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData


class PurchaseRecordViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: PurchaseRepository = PurchaseRepository(application)

    private val allPurchases: LiveData<List<PurchaseRecord>>?

    init {

        allPurchases = mRepository.allPurchases

    }

    fun insert(PurchaseRecord: PurchaseRecord) {

        mRepository.insert(PurchaseRecord)

    }

}