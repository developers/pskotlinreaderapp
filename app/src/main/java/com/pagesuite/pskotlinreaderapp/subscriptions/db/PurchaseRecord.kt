package com.pagesuite.pskotlinreaderapp.subscriptions.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.android.billingclient.api.BillingClient


@Entity(tableName = "purchase_table")
class PurchaseRecord {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "orderId")
    private var mOrderId: String = ""

    fun setOrderId(orderId: String) {

        mOrderId = orderId

    }

    fun getOrderId(): String {

        return mOrderId

    }

    @NonNull
    @ColumnInfo(name = "sku")
    private var mSku: String = ""

    fun setSku(sku: String) {

        mSku = sku

    }

    fun getSku(): String {

        return mSku

    }

    @NonNull
    @ColumnInfo(name = "signature")
    private var mSignature: String = ""

    fun setSignature(signature: String) {

        mSignature = signature

    }

    fun getSignature(): String {

        return mSignature

    }

    @NonNull
    @ColumnInfo(name = "lastUpdated")
    private var mLastUpdated: Long = 0L

    fun setLastUpdated(lasUpdated: Long) {

        mLastUpdated = lasUpdated

    }

    fun getLastUpdated(): Long {

        return mLastUpdated

    }

    @NonNull
    @ColumnInfo(name = "purchaseToken")
    private var mPurchaseToken: String = ""

    fun setPurchaseToken(purchaseToken: String) {

        mPurchaseToken = purchaseToken

    }

    fun getPurchaseToken(): String {

        return mPurchaseToken

    }

    @NonNull
    @ColumnInfo(name = "skuType")
    private var mSkuType: String = BillingClient.SkuType.INAPP

    fun setSkuType(skuType: String) {

        mSkuType = skuType

    }

    fun getSkuType(): String {

        return mSkuType

    }

    @NonNull
    @ColumnInfo(name = "originalJson")
    private var mOriginalJson: String = ""

    fun setOriginalJson(originalJson: String) {

        mOriginalJson = originalJson

    }

    fun getOriginalJson(): String {

        return mOriginalJson

    }

}