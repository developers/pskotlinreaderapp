package com.pagesuite.pskotlinreaderapp.activities

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.config.ColourConfig

abstract class BasicToolbarActivity : BasicActivity() {

    var headerBackgroundColour: Int = Color.WHITE
    var headerForegroundColour: Int = Color.BLACK

    var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        initToolbar()

    }

    override fun initColourScheme() {

        super.initColourScheme()

        headerBackgroundColour = ColourConfig.Toolbar.mBackgroundColour
        headerForegroundColour = ColourConfig.Toolbar.mForegroundColour

    }

    open fun initToolbar() {

        toolbar = findViewById(R.id.toolbar)

        if (toolbar is Toolbar) {

            setSupportActionBar(toolbar)

            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)

            if (toolbar?.background is LayerDrawable) {

                val layerDrawable = toolbar?.background as LayerDrawable

                val background = layerDrawable.findDrawableByLayerId(R.id.background)
                background.setColorFilter(headerBackgroundColour, PorterDuff.Mode.SRC_ATOP)

                val border = layerDrawable.findDrawableByLayerId(R.id.border)
                border.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

            }

            toolbar?.setTitleTextColor(headerForegroundColour)

            toolbar?.navigationIcon?.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)
            toolbar?.setNavigationOnClickListener { finish() }

        }

    }
}