package com.pagesuite.pskotlinreaderapp.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.application.activity.ApplicationActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.Queries
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.kiosk.activity.KioskHomeActivity
import com.pagesuite.pskotlinreaderapp.kiosk.activity.KioskSimpleActivity
import com.pagesuite.pskotlinreaderapp.replica.pdfreader.activity.PDFReaderActivity
import com.pagesuite.pskotlinreaderapp.services.ApplicationCacheService
import com.pagesuite.pskotlinreaderapp.utils.PSSharedPreferences
import com.pagesuite.readerui.statics.ReaderUI
import com.pagesuite.utilities.DeviceUtils
import com.pagesuite.utilities.NetworkUtils
import kotlinx.android.synthetic.main.activity_home.*


class Home : BasicActivity() {

    private val sharedPrefs = PSSharedPreferences()

    ///////onCreateActivity//////
    //Set up app sycn
    //Check for saved app code, and make the application query if there is an app code saved.
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        ReaderUI.getInstance().init(mApplication)
        ReaderUI.getInstance().mPageTapShowsThumbnails = true

        ReaderUI.getInstance().mReaderShowsUiByDefault = true
        ReaderUI.getInstance().mToolbarShowsThumbnails = true
        ReaderUI.getInstance().mReaderHasPageClickBlocker = false
        ReaderUI.getInstance().mReaderContentBelowToolbar = true
        ReaderUI.getInstance().mReaderUsesPageBrowser = false
        ReaderUI.getInstance().mReaderUsesSinglePdfEdition = false
        ReaderUI.getInstance().mReaderHasDPSButton = true
        ReaderUI.getInstance().mReaderAlwaysPromptForDownload = false
        ReaderUI.getInstance().mReaderPromptForDownload = false
        ReaderUI.getInstance().mReaderSpinnerColour = Color.BLACK

        mApplication?.initializeAppSyncClient(this)

        mApplication?.initializeInAppManager(this) { success, error ->

            when {
                success -> loadAppWithSavedCode()
                error != null -> showError(error)
                else -> showError("")
            }

        }

    }

    override fun getLayout(): Int {

        return R.layout.activity_home

    }

    private fun showError(error: String) {

        val builder = AlertDialog.Builder(this)
        builder.setNeutralButton(android.R.string.ok) { dialog, _ ->

            dialog.dismiss()

        }

        val message = when (error) {

            "LOCK_BOOTLOADER" -> R.string.safetyNet_error_bootloaderUnlocker
            "RESTORE_TO_FACTORY_ROM" -> R.string.safetyNet_error_unknownRom
            "NOT_FROM_STORE" -> R.string.safetyNet_error_notFromStore
            else -> R.string.safetyNet_error_unknownError
        }

        builder.setMessage(message)
        builder.setTitle(R.string.safetyNet_error)
        builder.setOnDismissListener { finish() }
        builder.show()

    }

    ///////Prepare App On Launch, If Valid App Code Saved//////

    //Load the app code, and load the application assocated with this app code
    private fun loadAppWithSavedCode() {

        val appCode = PSSharedPreferences().getAppCode(this)

        if (appCode != null && appCode != "") {

            prepApp()
            mApplication?.initProviders(this)
            //this.makeApplicationQuery(appCode)

            if (NetworkUtils.isConnected(this)) {

                makeCacheQuery(appCode)

            } else {

                mApplication?.loadAppConfig(successListener = {

                    mApplication?.showOffline(this)

                })

            }

        } else {

            viewFlipper.displayedChild = 1

        }

    }

    //Load data to prepare thr app
    private fun prepApp() {

        val loadedFontSize = PSSharedPreferences().getSavedFontSize(this)
        UsefulConstants.setFontSize(loadedFontSize, this)

    }


    ///////On Return To Activity After App Code Reset//////

    //When the user has returned to this activity after the app code has been reset, reset the app setting ready for another app code
    override fun onResume() {

        super.onResume()
        resetApp()

    }

    //resetting the app settings ready for another app code to be entered
    private fun resetApp() {

        ApplicationCacheService.put(null)

    }


    ///////On App Code Submitted//////


    @Suppress("UNUSED_PARAMETER")
    //onSubmitButtonPressed
    //Make a query for the application based on this app code entered
    fun onSubmitButtonPress(view: View) {

        viewFlipper.displayedChild = 0

        val enteredAppCode = (if (!TextUtils.isEmpty(appCodeTextBox.text.toString())) {

            appCodeTextBox.text.toString()

        } else {

            spinner.selectedItem.toString()

        }).toUpperCase()

        makeCacheQuery(enteredAppCode)

        saveAppCode(enteredAppCode)

    }

    //Save the app code once the user has pushed submit. This will be set until the user resets the app code in the settings of the app
    private fun saveAppCode(code: String) {

        sharedPrefs.setAppCode(this, code)

    }

    private fun makeCacheQuery(appCode: String) {

        Queries.getCacheKey(this, appCode) {

            if (it != null) {

                makeApplicationQuery(appCode, it)

            } else {

                runOnUiThread {

                    viewFlipper.displayedChild = 1
                    Toast.makeText(this, R.string.toast_error_invalidAppCode, Toast.LENGTH_LONG).show()

                }

            }

        }

    }

    ///////On Application data request//////
    //Take the app code (from saved, or from being entered and the user pushing submit, and make an application query for this application
    private fun makeApplicationQuery(appCode: String, cacheKey: String) {

        Queries.getApplicationByEndPoint(this, appCode, cacheKey) { application, json ->

            if (application != null) {

                if (json != null) {

                    mApplication?.saveAppConfig(json, successListener = {

                        runOnUiThread {

                            startAppNavigation()

                        }

                    })

                }

            } else {

                runOnUiThread {

                    viewFlipper.displayedChild = 1
                    Toast.makeText(this, R.string.toast_error_invalidAppCode, Toast.LENGTH_LONG).show()

                }

            }

        }

    }


    ///////Navigation//////


    //On start navigation
    //Gather the information about where to move to in the app, depending on the app code entered
    private fun startAppNavigation() {

        val templateType = (if (DeviceUtils.isPhone(this)) {

            DataStore.Application?.app?.phonetype

        } else {

            DataStore.Application?.app?.tablettype

        })
                ?: return

        when (templateType) {

            UsefulConstants.APP_TYPE_KIOSK -> {

                if (DataStore.Kiosk?.categories?.size ?: 0 > 1) {

                    onMove(KioskHomeActivity::class.java)

                } else {

                    onMove(KioskSimpleActivity::class.java)

                }

            }
            //UsefulConstants.APP_TYPE_KIOSK -> onMove(KioskSimpleActivity::class.java)
            UsefulConstants.APP_TYPE_EDITION -> onMove(EditionActivity::class.java)
            UsefulConstants.APP_TYPE_REPLICA -> onMove(PDFReaderActivity::class.java)
            UsefulConstants.APP_TYPE_APPLICATION -> onMove(ApplicationActivity::class.java)
            UsefulConstants.OPEN_SETTINGS -> mApplication?.moveToSettings(this)

            else -> {

                viewFlipper.displayedChild = 1
                Toast.makeText(this, R.string.toast_error_invalidAppCode, Toast.LENGTH_LONG).show()

            }

        }

    }

    //On move to a new activity
    //Take the data from startAppNavigation and move to the correct activity
    private fun onMove(targetActivity: Class<out AppCompatActivity>) {

        val editions = DataStore.Application?.liveEditions
        if (editions != null) {

            if (!editions.isEmpty()) {

                val edition = editions.firstOrNull()
                DataStore.Edition = edition
                //DataStore.Application?.organiseEditionsIntoDates()
                DataStore.Application?.activeEdition = edition?.getEditionGuid() ?: ""

            }

            mApplication?.moveToActivity(this, targetActivity)

        }

    }

    override fun onDestroy() {

        imageView.setImageResource(0)

        super.onDestroy()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == UsefulConstants.REQUEST_OFFLINE) {

            if (resultCode == Activity.RESULT_OK) {

                loadAppWithSavedCode()

                return

            }

        }

        finish()

    }


}
