package com.pagesuite.pskotlinreaderapp.activities

abstract class EditionBasicActivity : BasicToolbarActivity() {

    abstract fun changeEdition()
    abstract fun alreadyLoadedEdition()

}