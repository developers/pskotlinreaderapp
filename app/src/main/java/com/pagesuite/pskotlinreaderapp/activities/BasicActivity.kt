package com.pagesuite.pskotlinreaderapp.activities

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.pagesuite.pskotlinreaderapp.AndroidFeedApp
import com.pagesuite.utilities.DeviceUtils

abstract class BasicActivity : AppCompatActivity() {

    var mApplication: AndroidFeedApp? = null
    var mScreenDensityScale: Float = 0f

    var animationDuration: Int = 0
    var shortAnimationDuration: Int = 0

    var handler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mApplication = application as AndroidFeedApp

        handler = Handler()

        mScreenDensityScale = DeviceUtils.getScreenDensityScale(this)

        animationDuration = resources.getInteger(android.R.integer.config_mediumAnimTime)
        shortAnimationDuration = resources.getInteger(android.R.integer.config_shortAnimTime)

        setContentView(getLayout())

        initColourScheme()

    }

    open fun initColourScheme() {

        /*val headerConfig = DataStore.Application?.header

        if (headerConfig != null) {

            val backgroundColourStr = headerConfig.backgroundcolor

            if (backgroundColourStr != null && !TextUtils.isEmpty(backgroundColourStr)) {

                headerBackgroundColour = Utils.covertStringToRGB(backgroundColourStr)

            }

            val foregroundColourStr = headerConfig.buttoncolor

            if (foregroundColourStr != null && !TextUtils.isEmpty(foregroundColourStr)) {

                headerForegroundColour = Utils.covertStringToRGB(foregroundColourStr)

            }

        }*/

    }

    abstract fun getLayout(): Int

}