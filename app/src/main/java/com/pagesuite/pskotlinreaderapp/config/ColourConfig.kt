package com.pagesuite.pskotlinreaderapp.config

import android.content.Context
import android.graphics.Color
import com.pagesuite.pskotlinreaderapp.R

object ColourConfig {

    fun loadDefaultColours(context: Context) {

        Edition.loadDefaultColours(context)
        Articles.loadDefaultColours(context)
        Downloads.loadDefaultColours(context)
        Settings.loadDefaultColours(context)
        Toolbar.loadDefaultColours(context)
        Offline.loadDefaultColours(context)
        Kiosk.loadDefaultColours(context)
        ImageGallery.loadDefaultColours(context)
    }

    object Toolbar : BasicConfig() {

        override fun loadDefaultColours(context: Context) {

            mBackgroundColour = context.resources.getColor(R.color.toolbar_background)
            mForegroundColour = context.resources.getColor(R.color.toolbar_foreground)

        }

    }

    object Kiosk : BasicConfig() {

        var mBackgroundGradientTop: Int = Color.TRANSPARENT
        var mBackgroundGradientMiddle: Int = Color.TRANSPARENT
        var mBackgroundGradientBottom: Int = Color.TRANSPARENT

        override fun loadDefaultColours(context: Context) {

            mBackgroundGradientTop = context.resources.getColor(R.color.kiosk_background_gradientTop)
            mBackgroundGradientMiddle = context.resources.getColor(R.color.kiosk_background_gradientMiddle)
            mBackgroundGradientBottom = context.resources.getColor(R.color.kiosk_background_gradientBottom)

            Toolbar.loadDefaultColours(context)
            NavigationDrawer.loadDefaultColours(context)
            KioskApplications.loadDefaultColours(context)

        }

        object Toolbar : BasicConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.kiosk_toolbar_background)
                mForegroundColour = context.resources.getColor(R.color.kiosk_toolbar_foreground)

            }

        }

        object KioskApplications : BasicConfig() {

            var mIsNewEditionForegroundColour: Int = Color.TRANSPARENT
            var mIsNewEditionBackgroundColour: Int = Color.TRANSPARENT

            override fun loadDefaultColours(context: Context) {

                mIsNewEditionBackgroundColour = context.resources.getColor(R.color.kiosk_applications_isNewEdition_background)
                mIsNewEditionForegroundColour = context.resources.getColor(R.color.kiosk_applications_isNewEdition_foreground)

            }
        }

        object NavigationDrawer : BasicConfig() {

            var mVerticalDividerColour: Int = Color.TRANSPARENT

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_background)
                mForegroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_foreground)

                mVerticalDividerColour = context.resources.getColor(R.color.kiosk_navigationDrawer_verticalDivider)

                Home.loadDefaultColours(context)
                Header.loadDefaultColours(context)
                Normal.loadDefaultColours(context)

                MainMenu.loadDefaultColours(context)

            }

            object MainMenu : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_background)
                    mForegroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_foreground)

                }

            }

            object Home : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_background)
                    mForegroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_foreground)

                }

            }

            object Header : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_header_background)
                    mForegroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_header_foreground)

                }

            }

            object Normal : BasicConfig() {

                var mBackgroundHighlightColour: Int = Color.TRANSPARENT
                var mForegroundHighlightColour: Int = Color.TRANSPARENT

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_background)
                    mForegroundColour = context.resources.getColor(R.color.kiosk_navigationDrawer_foreground)

                    mBackgroundHighlightColour = context.resources.getColor(R.color.kiosk_navigationDrawer_highlight_background)
                    mForegroundHighlightColour = context.resources.getColor(R.color.kiosk_navigationDrawer_highlight_foreground)

                }

            }

        }

    }

    object Edition : BasicConfig() {

        var mSelectionColour: Int = Color.TRANSPARENT

        override fun loadDefaultColours(context: Context) {

            mSelectionColour = context.resources.getColor(R.color.selection_colour)

            Toolbar.loadDefaultColours(context)
            BottomToolbar.loadDefaultColours(context)
            Archive.loadDefaultColours(context)
            Pages.loadDefaultColours(context)
            Scrubber.loadDefaultColours(context)
            LoadingScreen.loadDefaultColours(context)
            DownloadPrompt.loadDefaultColours(context)
            NavigationDrawer.loadDefaultColours(context)
        }

        object DownloadPrompt : BasicConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.downloadPrompt_background)

                Options.loadDefaultColours(context)

            }

            object Options : BasicConfig() {

                var mIconBackground: Int = Color.TRANSPARENT
                var mIconTint: Int = Color.TRANSPARENT
                var mDescriptionForeground: Int = Color.TRANSPARENT

                var mBackgroundTopLayer: Int = Color.TRANSPARENT
                var mBackgroundMiddleLayer: Int = Color.TRANSPARENT
                var mBackgroundBottomLayer: Int = Color.TRANSPARENT

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.downloadPrompt_options_background)
                    mForegroundColour = context.resources.getColor(R.color.downloadPrompt_options_foreground)

                    mIconBackground = context.resources.getColor(R.color.downloadPrompt_options_iconBackground)
                    mIconTint = context.resources.getColor(R.color.downloadPrompt_options_iconTint)

                    mDescriptionForeground = context.resources.getColor(R.color.downloadPrompt_options_description_foreground)

                    mBackgroundTopLayer = context.resources.getColor(R.color.downloadPrompt_background_topLayer)
                    mBackgroundMiddleLayer = context.resources.getColor(R.color.downloadPrompt_background_middleLayer)
                    mBackgroundBottomLayer = context.resources.getColor(R.color.downloadPrompt_background_bottomLayer)

                    DownloadButton.loadDefaultColours(context)
                    CancelButton.loadDefaultColours(context)

                }

                object DownloadButton : ButtonConfig() {

                    override fun loadDefaultColours(context: Context) {

                        mBackgroundColour = context.resources.getColor(R.color.downloadPrompt_downloadButton_background)
                        mForegroundColour = context.resources.getColor(R.color.downloadPrompt_downloadButton_foreground)

                        mPressedBackgroundColour = context.resources.getColor(R.color.downloadPrompt_downloadButton_pressed_background)
                        mPressedForegroundColour = context.resources.getColor(R.color.downloadPrompt_downloadButton_pressed_foreground)

                        mStrokeColour = context.resources.getColor(R.color.downloadPrompt_downloadButton_strokeColour)

                        mStrokeWidth = context.resources.getDimensionPixelSize(R.dimen.downloadPrompt_downloadButton_strokeWidth)

                    }

                }

                object CancelButton : ButtonConfig() {

                    override fun loadDefaultColours(context: Context) {

                        mBackgroundColour = context.resources.getColor(R.color.downloadPrompt_cancelButton_background)
                        mForegroundColour = context.resources.getColor(R.color.downloadPrompt_cancelButton_foreground)

                        mPressedBackgroundColour = context.resources.getColor(R.color.downloadPrompt_cancelButton_pressed_background)
                        mPressedForegroundColour = context.resources.getColor(R.color.downloadPrompt_cancelButton_pressed_foreground)

                        mStrokeColour = context.resources.getColor(R.color.downloadPrompt_cancelButton_strokeColour)

                        mStrokeWidth = context.resources.getDimensionPixelSize(R.dimen.downloadPrompt_cancelButton_strokeWidth)

                    }

                }

            }

        }

        object LoadingScreen : BasicConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.edition_loadingScreen_background)
                mForegroundColour = context.resources.getColor(R.color.edition_loadingScreen_foreground)

            }

        }

        object Scrubber : BasicConfig() {

            var mProgressBackgroundColour: Int = Color.TRANSPARENT
            var mProgressForegroundColour: Int = Color.TRANSPARENT

            var mSeekbarBackgroundColour: Int = Color.TRANSPARENT
            var mSeekbarForegroundColour: Int = Color.TRANSPARENT

            var mScrubberArrow: Int = Color.TRANSPARENT

            override fun loadDefaultColours(context: Context) {

                mProgressBackgroundColour = context.resources.getColor(R.color.edition_scrubber_progressBar_background)
                mProgressForegroundColour = context.resources.getColor(R.color.edition_scrubber_progressBar_foreground)

                mSeekbarBackgroundColour = context.resources.getColor(R.color.edition_scrubber_seekBar_background)
                mSeekbarForegroundColour = context.resources.getColor(R.color.edition_scrubber_seekBar_foreground)

                mScrubberArrow = context.resources.getColor(R.color.edition_scrubber_arrow)

                Popup.loadDefaultColours(context)

            }

            object Popup : BasicConfig() {

                var mBorderColour: Int = Color.TRANSPARENT
                var mBorderWidth: Int = 0

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_scrubber_popup_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_scrubber_popup_foreground)

                    mBorderColour = context.resources.getColor(R.color.edition_scrubber_popup_border)

                    mBorderWidth = context.resources.getDimensionPixelSize(R.dimen.edition_scrubber_popup_strokeWidth)

                }
            }

        }

        object Toolbar : BasicConfig() {

            var mHeaderBufferBackground: Int = Color.TRANSPARENT
            var mDownloadProgressBackground: Int = Color.TRANSPARENT
            var mDownloadProgressForeground: Int = Color.TRANSPARENT

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.edition_toolbar_background)
                mForegroundColour = context.resources.getColor(R.color.edition_toolbar_foreground)

                mDownloadProgressBackground = context.resources.getColor(R.color.edition_toolbar_downloadProgress_background)
                mDownloadProgressForeground = context.resources.getColor(R.color.edition_toolbar_downloadProgress_foreground)

                mHeaderBufferBackground = context.resources.getColor(R.color.edition_toolbar_headerBuffer_background)

            }

        }

        object BottomToolbar : BasicConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.edition_bottomToolbar_background)
                mForegroundColour = context.resources.getColor(R.color.edition_bottomToolbar_foreground)

            }

        }

        object Archive : BasicConfig() {

            var mEditionColour: Int = Color.TRANSPARENT
            var mOldEditionColour: Int = Color.TRANSPARENT
            var mOldestEditionColour: Int = Color.TRANSPARENT

            var mGroupsBackgroundColour: Int = Color.TRANSPARENT
            var mGroupsToggleButtonForeground: Int = Color.TRANSPARENT

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.edition_archive_background)
                mForegroundColour = context.resources.getColor(R.color.edition_archive_foreground)

                mEditionColour = context.resources.getColor(R.color.card_edition_stack_latest)
                mOldEditionColour = context.resources.getColor(R.color.card_edition_stack_old)
                mOldestEditionColour = context.resources.getColor(R.color.card_edition_stack_oldest)

                mGroupsBackgroundColour = context.resources.getColor(R.color.edition_archive_groups_background)
                mGroupsToggleButtonForeground = context.resources.getColor(R.color.edition_archive_groups_toggleButton_foreground)

                Button.loadDefaultColours(context)

            }

            object Button : ButtonConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_archive_button_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_archive_button_foreground)

                    mPressedForegroundColour = context.resources.getColor(R.color.edition_archive_button_pressed_foreground)
                    mPressedBackgroundColour = context.resources.getColor(R.color.edition_archive_button_pressed_background)

                    mStrokeColour = context.resources.getColor(R.color.edition_archive_button_stroke)
                    mStrokeWidth = context.resources.getDimensionPixelSize(R.dimen.edition_archive_button_stroke_width)

                }

            }

        }

        object Pages : BasicConfig() {

            var mSubtitleColour: Int = Color.TRANSPARENT

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.edition_article_container_background)
                mForegroundColour = context.resources.getColor(R.color.edition_article_container_foreground)

                mSubtitleColour = context.resources.getColor(R.color.edition_article_container_subTitleColour)

                Tabs.loadDefaultColours(context)

            }

            object Tabs : BasicConfig() {

                var mSelectionColour: Int = Color.TRANSPARENT
                var mSelectTabForeground: Int = Color.TRANSPARENT

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_article_container_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_article_tab_foreground)

                    mSelectionColour = context.resources.getColor(R.color.edition_article_tab_selectionColour)
                    mSelectTabForeground = context.resources.getColor(R.color.edition_article_tab_selectedForegroundColour)

                }

            }

        }

        object NavigationDrawer : BasicConfig() {

            var mVerticalDividerColour: Int = Color.TRANSPARENT

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.edition_navigationDrawer_background)
                mForegroundColour = context.resources.getColor(R.color.edition_navigationDrawer_foreground)

                mVerticalDividerColour = context.resources.getColor(R.color.edition_navigationDrawer_verticalDivider)

                Home.loadDefaultColours(context)
                Header.loadDefaultColours(context)
                Normal.loadDefaultColours(context)
                Article.loadDefaultColours(context)

                MainMenu.loadDefaultColours(context)
                ArticlesMenu.loadDefaultColours(context)

            }

            object MainMenu : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_navigationDrawer_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_navigationDrawer_foreground)

                }

            }

            object ArticlesMenu : BasicConfig() {

                var mVerticalDividerColour: Int = Color.TRANSPARENT

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_navigationDrawer_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_navigationDrawer_foreground)

                    mVerticalDividerColour = context.resources.getColor(R.color.edition_navigationDrawer_article_divider)

                }

            }

            object Home : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_navigationDrawer_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_navigationDrawer_foreground)

                }

            }

            object Header : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_navigationDrawer_header_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_navigationDrawer_header_foreground)

                }

            }

            object Normal : BasicConfig() {

                var mBackgroundHighlightColour: Int = Color.TRANSPARENT
                var mForegroundHighlightColour: Int = Color.TRANSPARENT

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_navigationDrawer_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_navigationDrawer_foreground)

                    mBackgroundHighlightColour = context.resources.getColor(R.color.edition_navigationDrawer_highlight_background)
                    mForegroundHighlightColour = context.resources.getColor(R.color.edition_navigationDrawer_highlight_foreground)

                }

            }

            object Article : BasicConfig() {

                var mSectionForegroundColour: Int = Color.TRANSPARENT

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_navigationDrawer_article_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_navigationDrawer_article_foreground)

                    mSectionForegroundColour = context.resources.getColor(R.color.edition_navigationDrawer_article_section_foreground)

                }

            }

        }

    }

    object Articles : BasicConfig() {

        override fun loadDefaultColours(context: Context) {

            Saved.loadDefaultColours(context)
            Search.loadDefaultColours(context)
            Browser.loadDefaultColours(context)

        }

        object Saved : BasicConfig() {

            var mVerticalDividerColour: Int = Color.TRANSPARENT
            var mKeylineColour: Int = Color.TRANSPARENT

            object Toolbar : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_toolbar_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_toolbar_foreground)

                }

            }

            object Button : ButtonConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.articles_saved_button_background)
                    mForegroundColour = context.resources.getColor(R.color.articles_saved_button_foreground)

                    mPressedForegroundColour = context.resources.getColor(R.color.articles_saved_button_pressed_foreground)
                    mPressedBackgroundColour = context.resources.getColor(R.color.articles_saved_button_pressed_background)

                    mStrokeColour = context.resources.getColor(R.color.articles_saved_button_stroke)
                    mStrokeWidth = context.resources.getDimensionPixelSize(R.dimen.articles_saved_button_stroke_width)

                }

            }

            object DeleteButton : ButtonConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.articles_saved_deleteButton_background)
                    mForegroundColour = context.resources.getColor(R.color.articles_saved_deleteButton_foreground)

                    mPressedForegroundColour = context.resources.getColor(R.color.articles_saved_deleteButton_pressed_foreground)
                    mPressedBackgroundColour = context.resources.getColor(R.color.articles_saved_deleteButton_pressed_background)

                    mStrokeColour = context.resources.getColor(R.color.articles_saved_deleteButton_stroke)
                    mStrokeWidth = context.resources.getDimensionPixelSize(R.dimen.articles_saved_deleteButton_stroke_width)

                }

            }

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.articles_saved_background)
                mForegroundColour = context.resources.getColor(R.color.articles_saved_foreground)

                mVerticalDividerColour = context.resources.getColor(R.color.articles_saved_verticalDivider)
                mKeylineColour = context.resources.getColor(R.color.articles_saved_keyline)

                Toolbar.loadDefaultColours(context)
                Button.loadDefaultColours(context)
                DeleteButton.loadDefaultColours(context)

            }

        }

        object Search : BasicConfig() {

            var mVerticalDividerColour: Int = Color.TRANSPARENT
            var mKeylineColour: Int = Color.TRANSPARENT
            var mResultShadow: Int = Color.TRANSPARENT

            object Toolbar : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_toolbar_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_toolbar_foreground)

                }

            }

            object Button : ButtonConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.articles_search_button_background)
                    mForegroundColour = context.resources.getColor(R.color.articles_search_button_foreground)

                    mPressedForegroundColour = context.resources.getColor(R.color.articles_search_button_pressed_foreground)
                    mPressedBackgroundColour = context.resources.getColor(R.color.articles_search_button_pressed_background)

                    mStrokeColour = context.resources.getColor(R.color.articles_search_button_stroke)
                    mStrokeWidth = context.resources.getDimensionPixelSize(R.dimen.articles_search_button_stroke_width)

                }

            }

            override fun loadDefaultColours(context: Context) {

                mVerticalDividerColour = context.resources.getColor(R.color.articles_search_verticalDivider)
                mKeylineColour = context.resources.getColor(R.color.articles_search_keyline)
                mResultShadow = context.resources.getColor(R.color.articles_search_resultShadow)

                mBackgroundColour = context.resources.getColor(R.color.articles_search_background)
                mForegroundColour = context.resources.getColor(R.color.articles_search_foreground)

                Toolbar.loadDefaultColours(context)
                Button.loadDefaultColours(context)

            }

        }

        object Browser : BasicConfig() {

            object Toolbar : BasicConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.edition_toolbar_background)
                    mForegroundColour = context.resources.getColor(R.color.edition_toolbar_foreground)

                }

            }

            override fun loadDefaultColours(context: Context) {

                Toolbar.loadDefaultColours(context)

            }

        }

    }

    object Downloads : BasicConfig() {

        override fun loadDefaultColours(context: Context) {

            mBackgroundColour = context.resources.getColor(R.color.downloads_background)
            mForegroundColour = context.resources.getColor(R.color.downloads_foreground)

            DeleteButton.loadDefaultColours(context)
            Toolbar.loadDefaultColours(context)

        }

        object DeleteButton : ButtonConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.downloads_deleteButton_background)
                mForegroundColour = context.resources.getColor(R.color.downloads_deleteButton_foreground)

                mPressedForegroundColour = context.resources.getColor(R.color.downloads_deleteButton_pressed_foreground)
                mPressedBackgroundColour = context.resources.getColor(R.color.downloads_deleteButton_pressed_background)

                mStrokeColour = context.resources.getColor(R.color.downloads_deleteButton_stroke)
                mStrokeWidth = context.resources.getDimensionPixelSize(R.dimen.downloads_deleteButton_stroke_width)

            }

        }

        object Toolbar : BasicConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.downloads_toolbar_background)
                mForegroundColour = context.resources.getColor(R.color.downloads_toolbar_foreground)

            }

        }

    }

    object Settings : BasicConfig() {

        override fun loadDefaultColours(context: Context) {

            mBackgroundColour = context.resources.getColor(R.color.settings_background)
            mForegroundColour = context.resources.getColor(R.color.settings_foreground)

            Menu.loadDefaultColours(context)
            Toolbar.loadDefaultColours(context)

        }

        object Menu : BasicConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.settings_menu_background)
                mForegroundColour = context.resources.getColor(R.color.settings_menu_foreground)

            }

        }

        object Toolbar : BasicConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.settings_toolbar_background)
                mForegroundColour = context.resources.getColor(R.color.settings_toolbar_foreground)

            }

        }
    }

    object Offline : BasicConfig() {


        override fun loadDefaultColours(context: Context) {

            mBackgroundColour = context.resources.getColor(R.color.offline_background)

            Options.loadDefaultColours(context)

        }

        object Options : BasicConfig() {

            var mIconBackground: Int = Color.TRANSPARENT
            var mIconTint: Int = Color.TRANSPARENT
            var mDescriptionForeground: Int = Color.TRANSPARENT

            var mBackgroundTopLayer: Int = Color.TRANSPARENT
            var mBackgroundMiddleLayer: Int = Color.TRANSPARENT
            var mBackgroundBottomLayer: Int = Color.TRANSPARENT

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.offline_options_background)
                mForegroundColour = context.resources.getColor(R.color.offline_options_foreground)

                mIconBackground = context.resources.getColor(R.color.offline_options_iconBackground)
                mIconTint = context.resources.getColor(R.color.offline_options_iconTint)

                mDescriptionForeground = context.resources.getColor(R.color.offline_options_description_foreground)

                mBackgroundTopLayer = context.resources.getColor(R.color.offline_background_topLayer)
                mBackgroundMiddleLayer = context.resources.getColor(R.color.offline_background_middleLayer)
                mBackgroundBottomLayer = context.resources.getColor(R.color.offline_background_bottomLayer)

                Button.loadDefaultColours(context)

            }

            object Button : ButtonConfig() {

                override fun loadDefaultColours(context: Context) {

                    mBackgroundColour = context.resources.getColor(R.color.offline_button_background)
                    mForegroundColour = context.resources.getColor(R.color.offline_button_foreground)

                    mPressedBackgroundColour = context.resources.getColor(R.color.offline_button_pressed_background)
                    mPressedForegroundColour = context.resources.getColor(R.color.offline_button_pressed_foreground)

                    mStrokeColour = context.resources.getColor(R.color.offline_button_strokeColour)

                    mStrokeWidth = context.resources.getDimensionPixelSize(R.dimen.offline_button_strokeWidth)

                }

            }

        }

    }

    object ImageGallery : BasicConfig() {

        override fun loadDefaultColours(context: Context) {

            mBackgroundColour = context.resources.getColor(R.color.imageGallery_backgroundColour)
            mForegroundColour = context.resources.getColor(R.color.imageGallery_foregroundColour)

            Toolbar.loadDefaultColours(context)

        }

        object Toolbar : BasicConfig() {

            override fun loadDefaultColours(context: Context) {

                mBackgroundColour = context.resources.getColor(R.color.imageGallery_toolbar_backgroundColour)
                mForegroundColour = context.resources.getColor(R.color.imageGallery_toolbar_foregroundColour)

            }

        }

    }

    abstract class ButtonConfig : BasicConfig() {

        var mPressedForegroundColour: Int = Color.TRANSPARENT
        var mPressedBackgroundColour: Int = Color.TRANSPARENT

        var mStrokeColour: Int = Color.TRANSPARENT
        var mStrokeWidth: Int = 0

    }

    abstract class BasicConfig {

        var mBackgroundColour: Int = Color.TRANSPARENT
        var mForegroundColour: Int = Color.TRANSPARENT

        abstract fun loadDefaultColours(context: Context)

    }

}