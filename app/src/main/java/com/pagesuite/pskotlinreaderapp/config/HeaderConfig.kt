package com.pagesuite.pskotlinreaderapp.config

import android.content.Context
import com.pagesuite.pskotlinreaderapp.R

object HeaderConfig {

    var mHeaderLogo: String? = ""

    fun loadDefaults(context: Context) {

        Edition.loadDefaults(context)
        Archive.loadDefaults(context)

    }

    object Archive : BasicHeaderConfig() {

        override fun loadDefaults(context: Context) {

            mHomeHeight = context.resources.getDimensionPixelSize(R.dimen.toolbar_expanded_size)
            mNormalHeight = context.resources.getDimensionPixelSize(R.dimen.toolbar_beta_size)

        }

    }

    object Edition {

        var mSimpleToolbarHeight: Int = 0

        fun loadDefaults(context: Context) {

            Alpha.loadDefaults(context)
            Beta.loadDefaults(context)

            mSimpleToolbarHeight = context.resources.getDimensionPixelSize(R.dimen.edition_toolbar_simpleToolbarHeight)

        }

        object Alpha : BasicHeaderConfig() {

            override fun loadDefaults(context: Context) {

                mHomeHeight = context.resources.getDimensionPixelSize(R.dimen.toolbar_expanded_size)
                mNormalHeight = context.resources.getDimensionPixelSize(R.dimen.toolbar_reduced_size)

            }
        }

        object Beta : BasicHeaderConfig() {

            override fun loadDefaults(context: Context) {

                mHomeHeight = context.resources.getDimensionPixelSize(R.dimen.toolbar_expanded_size)
                mNormalHeight = context.resources.getDimensionPixelSize(R.dimen.toolbar_beta_size)

            }
        }

    }

    abstract class BasicHeaderConfig {

        var mHomeHeight: Int = 0
        var mNormalHeight: Int = 0

        abstract fun loadDefaults(context: Context)

    }


}