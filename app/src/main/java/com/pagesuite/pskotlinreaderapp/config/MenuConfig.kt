package com.pagesuite.pskotlinreaderapp.config

import android.content.Context
import android.graphics.Color
import com.pagesuite.pskotlinreaderapp.BuildConfig
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.models.Item
import com.pagesuite.pskotlinreaderapp.models.ItemGroup

object MenuConfig {

    fun loadDefaults(context: Context) {

        Edition.loadDefaults(context)
        Kiosk.loadDefaults(context)
        Settings.loadDefaults(context)

    }

    object Edition : DefaultMenu() {

        override fun loadDefaults(context: Context) {

            mItemGroups = arrayListOf()

            // generate dummy home item
            val homeGroup = ItemGroup()
            homeGroup.name = context.getString(R.string.menu_header_home)
            homeGroup.type = UsefulConstants.TYPE_HOME
            mItemGroups?.add(homeGroup)

            // generate dummy sections item

            val sectionsGroup = ItemGroup()

            sectionsGroup.name = context.getString(R.string.menu_header_sections)
            sectionsGroup.type = UsefulConstants.TYPE_SECTIONS

            mItemGroups?.add(sectionsGroup)

            // generate dummy settings items

            val settingsGroup = ItemGroup()
            settingsGroup.name = context.getString(R.string.menu_header_options)
            settingsGroup.type = UsefulConstants.TYPE_SETTINGS
            settingsGroup.items = arrayOf(getSearchItem(context),
                    getSavedItem(context),
                    getDownloadsItem(context),
                    getSettingsItem(context))

            mItemGroups?.add(settingsGroup)

        }

    }

    object Kiosk : DefaultMenu() {

        override fun loadDefaults(context: Context) {

            mItemGroups = arrayListOf()

            val settingsGroup = ItemGroup()

            settingsGroup.type = UsefulConstants.TYPE_SETTINGS
            settingsGroup.items = arrayOf(getClearAppCacheItem(context),
                    getHelpItem(context),
                    getClearAppCodeItem(context),
                    getAppVersionItem(context))

            mItemGroups?.add(settingsGroup)

        }

    }

    object Settings : DefaultMenu() {

        override fun loadDefaults(context: Context) {

            mItemGroups = arrayListOf()

            val settingsGroup = ItemGroup()

            settingsGroup.type = UsefulConstants.TYPE_SETTINGS
            settingsGroup.items = arrayOf(getClearAppCodeItem(context),
                    getClearAppCacheItem(context),
                    getAppFontItem(context),
                    getHelpItem(context),
                    getAppVersionItem(context))

            mItemGroups?.add(settingsGroup)

        }

    }

    abstract class DefaultMenu {

        var mItemGroups: ArrayList<ItemGroup>? = null
        var mBackgroundColour: Int = Color.TRANSPARENT
        var mForegroundColour: Int = Color.TRANSPARENT

        abstract fun loadDefaults(context: Context)

        fun getClearAppCodeItem(context: Context): Item {

            val clearAppCodeItem = Item()

            clearAppCodeItem.text = context.getString(R.string.settings_clearAppCode)
            clearAppCodeItem.type = UsefulConstants.SETTINGS_CLEARAPPCODE

            return clearAppCodeItem

        }

        fun getClearAppCacheItem(context: Context): Item {

            val clearAppCacheItem = Item()

            clearAppCacheItem.text = context.getString(R.string.settings_clearCache)
            clearAppCacheItem.type = UsefulConstants.SETTINGS_CLEARCACHE

            return clearAppCacheItem

        }

        fun getAppFontItem(context: Context): Item {

            val appFontItem = Item()

            appFontItem.text = context.getString(R.string.settings_appFontSelection)
            appFontItem.type = UsefulConstants.SETTINGS_APPFONT

            return appFontItem

        }

        fun getHelpItem(context: Context): Item {

            val helpItem = Item()

            helpItem.text = context.getString(R.string.settings_showAppHelpGuide)
            helpItem.type = UsefulConstants.SETTINGS_SHOWHELP

            return helpItem

        }

        fun getAppVersionItem(context: Context): Item {

            val versionItem = Item()

            versionItem.text = String.format(context.getString(R.string.settings_versionName), BuildConfig.VERSION_NAME)

            return versionItem

        }

        fun getSearchItem(context: Context): Item {

            val searchItem = Item()
            searchItem.text = context.getString(R.string.title_activity_searchArticles)
            searchItem.type = UsefulConstants.OPEN_SEARCH

            return searchItem

        }

        fun getSavedItem(context: Context): Item {

            val savedItem = Item()
            savedItem.text = context.getString(R.string.title_activity_savedArticles)
            savedItem.type = UsefulConstants.OPEN_SAVED

            return savedItem

        }

        fun getDownloadsItem(context: Context): Item {

            val downloadsItem = Item()
            downloadsItem.text = context.getString(R.string.title_activity_downloads)
            downloadsItem.type = UsefulConstants.OPEN_DOWNLOADS

            return downloadsItem

        }

        fun getSettingsItem(context: Context): Item {

            val settingsItem = Item()
            settingsItem.text = context.getString(R.string.title_activity_app_config_settings)
            settingsItem.type = UsefulConstants.OPEN_SETTINGS

            return settingsItem

        }


    }


}