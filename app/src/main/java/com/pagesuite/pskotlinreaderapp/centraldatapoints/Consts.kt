package com.pagesuite.pskotlinreaderapp.centraldatapoints

import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.pagesuite.pskotlinreaderapp.utils.PSSharedPreferences
import com.pagesuite.pskotlinreaderapp.utils.Utils

object UsefulConstants {

    const val IS_DEBUG = false

    const val EDITION_GUID: String = "editionGUID"

    const val ARTICLE_GUID: String = "articleGuid"
    const val ARTICLE_DISPLAY_COLUMNS: String = "columns"
    const val ARTICLE_DISPLAY_MULTICOLUMN: String = "multicolumn"

    const val APP_TYPE_REPLICA = "replica"
    const val APP_TYPE_DISABLED = "disabled"
    const val APP_TYPE_KIOSK = "kiosk"
    const val APP_TYPE_EDITION = "edition"
    const val APP_TYPE_APPLICATION = "applicationactivity"
    const val OPEN_SETTINGS = "settings"
    const val OPEN_SAVED = "bookmarks"
    const val OPEN_SEARCH = "search"
    const val OPEN_DOWNLOADS = "downloads"
    const val OPEN_SUBSCRIPTIONS = "subscriptions"

    const val TYPE_SECTIONS = "sections"
    const val TYPE_SECTION = "templateSection"
    const val TYPE_ARTICLE = "templateArticle"
    const val TYPE_COVER = "cover"
    const val TYPE_INTERSTITIAL = "interstitial"
    const val TYPE_HOME = "home"
    const val TYPE_PUZZLE = "puzzle"
    const val TYPE_SETTINGS = "settings"

    const val TEXT_SIZE_NORMAL = "normal"
    const val TEXT_SIZE_LARGE = "large"
    const val TEXT_SIZE_LARGER = "larger"

    const val ARGS_TYPE = "type"
    const val ARGS_CACHE_CODE = "cacheCode"
    const val ARGS_PAGE_NUMBER = "pageNumber"
    const val ARGS_PROGRESS = "progress"
    const val ARGS_SHOW_ALL = "showAll"

    const val REQUEST_ARTICLE_JUMP = 101
    const val REQUEST_OFFLINE = 102

    const val BROADCAST_UPDATED_ARTICLE = "updateArticle"
    const val BROADCAST_UPDATED_EDITION = "updateEdition"
    const val BROADCAST_DOWNLOAD_QUEUED = "downloadQueued"
    const val BROADCAST_DOWNLOAD_STARTED = "downloadStarted"
    const val BROADCAST_DOWNLOAD_COMPLETED = "downloadCompleted"
    const val BROADCAST_DOWNLOAD_PROGRESS = "downloadProgress"
    const val BROADCAST_DOWNLOAD_FAILED = "downloadFailed"

    const val QUERY_GET_APPLICATION = "getApplication"
    const val QUERY_GET_APPLICATION_BY_END_POINT = "getApplicationByEndPoint"
    const val QUERY_GET_EDITION = "getEdition"
    const val QUERY_GET_PAGE = "getPage"
    const val QUERY_GET_ARTICLE = "getArticle"
    const val QUERY_SUBSCRIBE_TO_PAGE = "new subscriber to page change failed"
    const val QUERY_SUBSCRIBE_TO_EDITION = "subscription failed. subscribeToEditionChange"

    //broadcasts
    const val pageUpdatedBroadcastID: String = "PAGEUPDATED"
    const val editionUpdatedBroadcastID: String = "EDITIONUPDATED"
    const val fontSizeChangeNotifications: String = "FONTUPDATED"
    const val fileDownloadBroadcastID: String = "FILEDOWNLOAD"

    const val FLOW_SECTION_TO_SECTION = "sectionToSection"
    const val FLOW_COVER_TO_COVER = "coverToCover"

    const val LEVEL_SECTION = "section"
    const val LEVEL_ARTICLE = "article"

    const val ADVERT_TYPE_DFP_GOOGLE = "GoogleDFP"

    const val KIOSK_LAUNCH_ARCHIVE = "archive"
    const val KIOSK_LAUNCH_EDITION = "edition"
    const val KIOSK_LAUNCH_AUTO = "auto"

    const val SEARCH_FIELD_DESCRIPTION = "textdescription"

    const val SETTINGS_CLEARAPPCODE = "clearAppCode"
    const val SETTINGS_CLEARCACHE = "clearAppCache"
    const val SETTINGS_APPFONT = "appFontSelection"
    const val SETTINGS_SHOWHELP = "showHelp"

    private var fontSize = Utils.FontSizes.NORMAL

    fun setFontSize(newFontSize: Utils.FontSizes, context: Context) {

        Log.d(UsefulConstants::class.simpleName, "SETTING NEW FONT")

        fontSize = newFontSize
        val broadcastIntent = Intent(fontSizeChangeNotifications)
        LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)

        PSSharedPreferences().saveFontSize(context, fontSize)

    }

    fun getFontSize(): Utils.FontSizes {

        return fontSize

    }

}