package com.pagesuite.pskotlinreaderapp.centraldatapoints

import com.pagesuite.pskotlinreaderapp.models.Application
import com.pagesuite.pskotlinreaderapp.models.Edition
import com.pagesuite.pskotlinreaderapp.models.Kiosk

object DataStore {

    var Application: Application? = null
    var Edition: Edition? = null
    var EditionApplication: Application? = null
    var Kiosk: Kiosk? = null

}

