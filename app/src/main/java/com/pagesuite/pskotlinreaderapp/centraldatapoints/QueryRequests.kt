package com.pagesuite.pskotlinreaderapp.centraldatapoints

import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient
import com.amazonaws.mobileconnectors.appsync.AppSyncSubscriptionCall
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers
import com.apollographql.apollo.GraphQLCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.gson.Gson
import com.pagesuite.pskotlinreaderapp.graphql.*
import com.pagesuite.pskotlinreaderapp.models.Application
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.Edition
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.pskotlinreaderapp.services.ClientFactory
import com.pagesuite.pskotlinreaderapp.utils.Utils
import com.pagesuite.utilities.NetworkUtils
import java.io.File


object Queries {


    private var mAWSAppSyncClient: AWSAppSyncClient? = null
    val gson = Gson()


    ///////QUERIES SUCCESS RESPONSE///////

    fun onGetCacheKeySuccess(data: GetCacheKeyQuery.Data?, successHandler: ((String?) -> Unit)?) {

        try {

            if (data == null) {

                successHandler?.invoke(null)
                return

            }

            val cacheKey = data.cacheUpdate?.application()?.cache()

            successHandler?.invoke(cacheKey)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    //onGetApplication Success

    fun onGetApplicationSuccess(data: GetApplicationsQuery.Data?, successHandler: ((Application?, String?) -> Unit)?) {

        try {

            if (data == null) {

                successHandler?.invoke(null, null)
                return

            }

            val json = convertToJSON(data.application)

            val application = gson.fromJson(json, Application::class.java)

            DataStore.EditionApplication = application
            DataStore.EditionApplication?.getLiveEditions()

            /*DataStore.Application = application
            DataStore.Application?.getLiveEditions()

            DataStore.Kiosk = application?.kiosk*/

            successHandler?.invoke(application, json)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }


    //onGetApplicationByEndPoint Success
    fun onGetApplicationSuccess(data: GetApplicationsByEndPointQuery.Data?, successHandler: ((Application?, String?) -> Unit)?) {

        try {

            if (data == null) {

                successHandler?.invoke(null, null)
                return

            }

            val json = convertToJSON(data.applicationByEndPoint)

            val application = gson.fromJson(json, Application::class.java)

            DataStore.Application = application
            DataStore.EditionApplication = application
            DataStore.Application?.getLiveEditions()
            DataStore.Kiosk = DataStore.Application?.kiosk

            successHandler?.invoke(application, json)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    //onGetEdition succcess

    fun onGetEditionSuccess(data: Any?, successHandler: ((Edition?) -> Unit)?) {

        try {

            if (data == null) {

                successHandler?.invoke(null)
                return

            }

            val json = (if (data !is String) convertToJSON(data) else data)

            val edition = gson.fromJson(json, Edition::class.java)
            DataStore.Edition = edition
            DataStore.Edition?.populateListOfHeadlines()
            successHandler?.invoke(edition)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    //onGetPageEdition success

    fun onGetPageSuccess(data: Any?, successHandler: ((Page?) -> Unit)?) {

        try {

            if (data == null) {

                successHandler?.invoke(null)
                return

            }

            val json = (if (data !is String) convertToJSON(data) else data)
            val page = gson.fromJson(json, Page::class.java)
            successHandler?.invoke(page)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    fun onGetArticleSuccess(data: Any?, successHandler: ((Article?) -> Unit)?) {

        try {

            if (data == null) {

                successHandler?.invoke(null)
                return

            }

            val json = (if (data !is String) convertToJSON(data) else data)

            val article = gson.fromJson(json, Article::class.java)

            successHandler?.invoke(article)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    //on a query failing

    fun onQueryFailed(queryType: String, exception: ApolloException) {

        try {

            Log.w(Queries::class.simpleName, "query failed for $queryType")
            Log.w(Queries::class.simpleName, exception.toString())
            Log.w(Queries::class.simpleName, exception.message)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }


    ///////MUTATION SUCCESS///////

    //onPageChange success

    fun onPageChangeSuccess(response: Response<OnChangePage2Subscription.Data>, editionGuid: String, context: Context) {

        try {

            val data = response.data()

            if (data != null) {

                val pageChanged = data.onChangePage2()?.pageguid()
                val newCache = data.onChangePage2()?.cache()

                if (pageChanged != null && newCache != null && DataStore.Application?.liveEditions?.firstOrNull() != null) {

                    val editionData = DataStore.Application?.editions ?: return

                    for (edition in editionData) {

                        if (editionGuid == edition.editionguid) {

                            edition.cache = "" //the new cache code passed down (BLOCKER)

                        }

                    }

                    DataStore.Application?.liveEditions?.firstOrNull()?.pageCache?.set(pageChanged, newCache)

                    val broadcastPoster = UsefulConstants.pageUpdatedBroadcastID + pageChanged
                    val broadcastIntent = Intent(Intent(broadcastPoster))
                    broadcastIntent.putExtra(UsefulConstants.ARGS_CACHE_CODE, newCache)
                    broadcastIntent.putExtra(UsefulConstants.ARGS_TYPE, UsefulConstants.BROADCAST_UPDATED_ARTICLE)

                    LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)

                }

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }


    //onEditionChange success

    @Suppress("UNUSED_PARAMETER")
    fun onEditionChangeSuccess(response: Response<OnChangeEditionSubscription.Data>, editionGuid: String, context: Context) {

        try {

            val editionData = DataStore.Application?.editions ?: return

            for (edition in editionData) {

                if (editionGuid == edition.editionguid) {

                    edition.cache = "" //the new cache code passed down (BLOCKER)

                }
            }

            val broadcastPoster = UsefulConstants.editionUpdatedBroadcastID + editionGuid
            val broadcastIntent = Intent(Intent(broadcastPoster))
            broadcastIntent.putExtra(UsefulConstants.ARGS_TYPE, UsefulConstants.BROADCAST_UPDATED_EDITION)
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    ////////Creating the queries and mutations////////


    fun getCacheKey(context: Context, applicationGuid: String, successHandler: ((String?) -> Unit)?) {

        try {

            if (mAWSAppSyncClient == null) {

                mAWSAppSyncClient = ClientFactory.getInstance(context)

            }

            val queryBuilder = GetCacheKeyQuery.builder()
            queryBuilder.shortcode(applicationGuid)

            mAWSAppSyncClient?.query(queryBuilder.build())
                    ?.responseFetcher(AppSyncResponseFetchers.NETWORK_FIRST)
                    ?.enqueue(object : GraphQLCall.Callback<GetCacheKeyQuery.Data>() {

                        override fun onResponse(response: Response<GetCacheKeyQuery.Data>) {

                            try {

                                val data = response.data()
                                onGetCacheKeySuccess(data, successHandler)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                        override fun onFailure(e: ApolloException) {

                            try {

                                onQueryFailed(UsefulConstants.QUERY_GET_APPLICATION, e)
                                successHandler?.invoke(null)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                    })

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    fun getApplication(context: Context, applicationGuid: String, cache: String, successHandler: ((Application?, String?) -> Unit)?) {

        try {

            if (mAWSAppSyncClient == null) {

                mAWSAppSyncClient = ClientFactory.getInstance(context)

            }

            val queryBuilder = GetApplicationsQuery.builder()
            queryBuilder.applicationguid(applicationGuid)
            queryBuilder.cache(cache)

            mAWSAppSyncClient?.query(queryBuilder.build())
                    ?.responseFetcher(AppSyncResponseFetchers.NETWORK_FIRST)
                    ?.enqueue(object : GraphQLCall.Callback<GetApplicationsQuery.Data>() {

                        override fun onResponse(response: Response<GetApplicationsQuery.Data>) {

                            try {

                                val data = response.data()
                                onGetApplicationSuccess(data, successHandler)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                        override fun onFailure(e: ApolloException) {

                            try {

                                onQueryFailed(UsefulConstants.QUERY_GET_APPLICATION, e)
                                successHandler?.invoke(null, null)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                    })

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    fun getApplicationByEndPoint(content: Context, appCode: String, cacheKey: String, successHandler: ((Application?, String?) -> Unit)?) {

        try {

            if (mAWSAppSyncClient == null) {

                mAWSAppSyncClient = ClientFactory.getInstance(content)

            }

            val appBuilder = GetApplicationsByEndPointQuery.builder()

            appBuilder.appcode(appCode)
            appBuilder.cacheCode(cacheKey)
            appBuilder.platformId(Utils.getPlatformId(content))

            mAWSAppSyncClient?.query(appBuilder.build())
                    ?.responseFetcher(AppSyncResponseFetchers.NETWORK_FIRST)
                    ?.enqueue(object : GraphQLCall.Callback<GetApplicationsByEndPointQuery.Data>() {

                        override fun onResponse(response: Response<GetApplicationsByEndPointQuery.Data>) {

                            try {

                                val data = response.data()
                                onGetApplicationSuccess(data, successHandler)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                        override fun onFailure(ae: ApolloException) {

                            try {

                                onQueryFailed(UsefulConstants.QUERY_GET_APPLICATION_BY_END_POINT, ae)
                                successHandler?.invoke(null, null)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                    })

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }


    fun getEdition(context: Context, editionGuid: String, cache: String, successHandler: ((Edition?) -> Unit)?) {

        try {

            val localEditionFile = File(context.filesDir.absolutePath + "/edition/$editionGuid/edition.json")

            if (localEditionFile.exists()) {

                val fileContent = Utils.loadFileAsString(localEditionFile.absolutePath)
                //Log.w("Simon", "fileContent: $fileContent")

                if (fileContent != null) {

                    onGetEditionSuccess(fileContent, successHandler)
                    return

                }

            }

            if (mAWSAppSyncClient == null) {

                mAWSAppSyncClient = ClientFactory.getInstance(context)

            }

            val appBuilder = GetEditionQuery.builder()

            appBuilder.editionguid(editionGuid)
            appBuilder.platform(Utils.getPlatformId(context))
            appBuilder.cache(cache)

            mAWSAppSyncClient?.query(appBuilder.build())
                    ?.responseFetcher(AppSyncResponseFetchers.NETWORK_FIRST)
                    ?.enqueue(object : GraphQLCall.Callback<GetEditionQuery.Data>() {

                        override fun onResponse(response: Response<GetEditionQuery.Data>) {

                            try {

                                val data = response.data()

                                onGetEditionSuccess(data?.edition, successHandler)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                        override fun onFailure(e: ApolloException) {

                            try {

                                onQueryFailed(UsefulConstants.QUERY_GET_EDITION, e)
                                successHandler?.invoke(null)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                    })

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    fun getStoredEdition(context: Context, editionGuid: String, successHandler: ((Edition?) -> Unit)?) {

        val localEditionFile = File(context.filesDir.absolutePath + "/edition/$editionGuid/edition.json")

        if (localEditionFile.exists()) {

            val fileContent = Utils.loadFileAsString(localEditionFile.absolutePath)
            //Log.w("Simon", "fileContent: $fileContent")

            if (fileContent != null) {

                onGetEditionSuccess(fileContent, successHandler)

                return
            }

        }

        successHandler?.invoke(null)

    }

    fun getPage(context: Context, pageGuid: String?, cache: String, successHandler: ((Page?) -> Unit)?) {

        try {

            if (pageGuid != null) {

                if (mAWSAppSyncClient == null) {

                    mAWSAppSyncClient = ClientFactory.getInstance(context)

                }

                val appBuilder = GetPageQuery.builder()

                appBuilder.pageguid(pageGuid)
                appBuilder.cache(cache)

                mAWSAppSyncClient?.query(appBuilder.build())
                        ?.responseFetcher(AppSyncResponseFetchers.NETWORK_FIRST)
                        ?.enqueue(object : GraphQLCall.Callback<GetPageQuery.Data>() {

                            override fun onResponse(response: Response<GetPageQuery.Data>) {

                                try {

                                    val data = response.data()

                                    onGetPageSuccess(data?.page, successHandler)

                                } catch (e: Exception) {

                                    e.printStackTrace()

                                }

                            }

                            override fun onFailure(e: ApolloException) {

                                try {

                                    onQueryFailed(UsefulConstants.QUERY_GET_PAGE, e)
                                    successHandler?.invoke(null)

                                } catch (e: Exception) {

                                    e.printStackTrace()

                                }

                            }

                        })

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    fun getArticle(context: Context, articleGuid: String, successHandler: ((Article?) -> Unit)?) {

        try {

            if (articleGuid != null) {

                if (mAWSAppSyncClient == null) {

                    mAWSAppSyncClient = ClientFactory.getInstance(context)

                }

                val articleBuilder = GetArticleQuery.builder()
                articleBuilder.articleguid(articleGuid)
                articleBuilder.cache("")

                mAWSAppSyncClient?.query(articleBuilder.build())
                        ?.responseFetcher(AppSyncResponseFetchers.NETWORK_FIRST)
                        ?.enqueue(object : GraphQLCall.Callback<GetArticleQuery.Data>() {

                            override fun onResponse(response: Response<GetArticleQuery.Data>) {

                                try {

                                    val data = response.data()

                                    onGetArticleSuccess(data?.article, successHandler)

                                } catch (e: Exception) {

                                    e.printStackTrace()

                                }

                            }

                            override fun onFailure(e: ApolloException) {

                                try {

                                    onQueryFailed(UsefulConstants.QUERY_GET_ARTICLE, e)
                                    successHandler?.invoke(null)

                                } catch (e: Exception) {

                                    e.printStackTrace()

                                }

                            }

                        })

            }

        } catch (ex: Exception) {

            ex.printStackTrace()


        }

        successHandler?.invoke(null)

    }

    fun newSubscribeToPageChange(context: Context, editionGuid: String) { //change to go through app level (BLOCKER)

        try {

            unsubscribeToPageChange()

            val editionPageSub = OnChangePage2Subscription.builder()
            editionPageSub.editionguid(editionGuid)

            val editionPageSubWatcher = ClientFactory.getInstance(context)?.subscribe(editionPageSub.build())

            if (editionPageSubWatcher != null) {

                AppSyncSubscribtionDataStore.pageChangeSubscription = editionPageSubWatcher

                editionPageSubWatcher.execute(object : AppSyncSubscriptionCall.Callback<OnChangePage2Subscription.Data> {

                    override fun onFailure(e: ApolloException) {

                        try {

                            onQueryFailed(UsefulConstants.QUERY_SUBSCRIBE_TO_PAGE, e)

                        } catch (e: Exception) {

                            e.printStackTrace()

                        }

                    }

                    override fun onResponse(response: Response<OnChangePage2Subscription.Data>) {

                        try {

                            onPageChangeSuccess(response, editionGuid, context)

                        } catch (e: Exception) {

                            e.printStackTrace()

                        }

                    }

                    override fun onCompleted() {

                        Log.d(Queries::class.simpleName, "completed")

                    }

                })

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    private fun unsubscribeToPageChange() {

        try {

            if (AppSyncSubscribtionDataStore.pageChangeSubscription != null) {

                AppSyncSubscribtionDataStore.pageChangeSubscription?.cancel()

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    fun subscribeToEditionChange(context: Context, editionGuid: String) { //change to go through the app level (BLOCKER)

        //update the edition cache codes

        //for loop around editions, check edition guid codes, if matches, then update the cache code
        //send out the notification with a base start and the edition guid to trigger the correct edition guid

        try {

            if (NetworkUtils.isConnected(context)) {

                unsubscribeToEditionChange()
                val editionPageChangeSubscription = OnChangeEditionSubscription.builder()
                editionPageChangeSubscription.editionguid(editionGuid)

                val editionChangeSubWatcher = ClientFactory.getInstance(context)?.subscribe(editionPageChangeSubscription.build())

                if (editionChangeSubWatcher != null) {

                    AppSyncSubscribtionDataStore.editionChangeSubscription = editionChangeSubWatcher

                    editionChangeSubWatcher.execute(object : AppSyncSubscriptionCall.Callback<OnChangeEditionSubscription.Data> {

                        override fun onFailure(e: ApolloException) {

                            try {

                                onQueryFailed(UsefulConstants.QUERY_SUBSCRIBE_TO_EDITION, e)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                        override fun onResponse(response: Response<OnChangeEditionSubscription.Data>) {

                            try {

                                onEditionChangeSuccess(response, editionGuid, context)

                            } catch (e: Exception) {

                                e.printStackTrace()

                            }

                        }

                        override fun onCompleted() {

                            Log.d(Queries::class.simpleName, "edition change mutation data received. subscribeToEditionChange")

                        }

                    })

                }

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    private fun unsubscribeToEditionChange() {

        try {

            if (AppSyncSubscribtionDataStore.editionChangeSubscription != null) {

                AppSyncSubscribtionDataStore.editionChangeSubscription?.cancel()

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    private fun convertToJSON(data: Any): String {

        try {

            return gson.toJson(data)

        } catch (e: Exception) {

            e.printStackTrace()

        }

        return ""

    }

}
