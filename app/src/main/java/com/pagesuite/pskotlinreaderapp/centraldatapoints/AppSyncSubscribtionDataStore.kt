package com.pagesuite.pskotlinreaderapp.centraldatapoints

import com.amazonaws.mobileconnectors.appsync.AppSyncSubscriptionCall
import com.pagesuite.pskotlinreaderapp.graphql.OnChangeEditionSubscription
import com.pagesuite.pskotlinreaderapp.graphql.OnChangePage2Subscription

object AppSyncSubscribtionDataStore {

    var pageChangeSubscription: AppSyncSubscriptionCall<OnChangePage2Subscription.Data>? = null
    var editionChangeSubscription: AppSyncSubscriptionCall<OnChangeEditionSubscription.Data>? = null

}