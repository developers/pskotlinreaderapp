package com.pagesuite.pskotlinreaderapp.centraldatapoints

import android.content.Context
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.google.gson.Gson
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.downloads.CacheRequest
import com.pagesuite.pskotlinreaderapp.downloads.VolleyRequestQueue
import com.pagesuite.pskotlinreaderapp.models.*
import com.pagesuite.pskotlinreaderapp.utils.Utils
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object EndpointQueryRequests {

    private var isLoadingStacks: Boolean = false
    private var isSearchingArticles: Boolean = false

    private val simpleFormatter = SimpleDateFormat("dd/MM/yyyy")

    fun downloadArchive(context: Context, listener: (() -> Unit)? = null, failureListener: (() -> Unit)? = null, stackCount: Int = 7) {

        if (!isLoadingStacks) {

            isLoadingStacks = true

            val targetUrl = context.getString(R.string.urls_editionStacks)
                    .replace("{APP_GUID}", DataStore.EditionApplication?.app?.guid ?: "")
                    .replace("{PLATFORM_ID}", Utils.getPlatformId(context).toString())
                    .replace("{STACK_COUNT}", stackCount.toString())
                    .replace("{CURRENT_PAGE}", DataStore.EditionApplication?.editionSkipCount.toString())

            val cacheRequest = CacheRequest(method = Request.Method.GET, url = targetUrl, mListener = Response.Listener {

                if (it.data is ByteArray) {

                    val fileContent = String(it.data)

                    if (!TextUtils.isEmpty(fileContent)) {

                        val fileRootObj = JSONObject(fileContent)

                        val jsonRootArray = fileRootObj.optJSONArray("stacks")

                        val rootArrayLength = jsonRootArray.length()

                        if (rootArrayLength > 0) {

                            var jsonRootObj: JSONObject?

                            val editionsByStack = DataStore.EditionApplication?.editionsByStack
                                    ?: TreeMap<String, ArrayList<IEditionContent>>(kotlin.Comparator { o1, o2 ->

                                        val date1 = try {

                                            simpleFormatter.parse(o1)

                                        } catch (ex: Exception) {

                                            Log.w(EndpointQueryRequests::class.simpleName, "Unable to parse o1: [$o1]")

                                            Date()
                                        }

                                        val date2 = try {

                                            simpleFormatter.parse(o2)

                                        } catch (ex: Exception) {

                                            Log.w(EndpointQueryRequests::class.simpleName, "Unable to parse o2: [$o2]")

                                            Date()
                                        }

                                        return@Comparator date2.compareTo(date1)

                                    })

                            var editionsArray: JSONArray?
                            var arrayLength: Int
                            var editionsList: ArrayList<IEditionContent>?

                            var editionObj: JSONObject?
                            var edition: Edition?

                            val gson = Gson()

                            val formatter = SimpleDateFormat("yyyy-MM-dd")

                            var date: Date? = null
                            var dateStr: String?

                            var key: String?
                            val today: Calendar = Calendar.getInstance()

                            for (stackIndex in 0..rootArrayLength) {

                                jsonRootObj = jsonRootArray.optJSONObject(stackIndex)

                                if (jsonRootObj != null && jsonRootObj.has("editions")) {

                                    editionsArray = jsonRootObj.optJSONArray("editions")

                                    arrayLength = editionsArray.length()

                                    if (arrayLength > 0) {

                                        editionsList = arrayListOf()

                                        for (i in 0..arrayLength) {

                                            editionObj = editionsArray.optJSONObject(i)

                                            if (editionObj != null) {

                                                edition = gson.fromJson(editionObj.toString(), Edition::class.java)

                                                if (edition != null) {

                                                    editionsList.add(edition)

                                                }

                                            }

                                        }

                                        editionsList.trimToSize()

                                        dateStr = jsonRootObj.optString("date")

                                        if (!TextUtils.isEmpty(dateStr)) {

                                            try {

                                                date = formatter.parse(dateStr) as Date

                                            } catch (ex: Exception) {

                                                Log.w(EndpointQueryRequests::class.simpleName, "Error - unable to parse [$dateStr]")

                                            }

                                        }

                                        if (date != null) {

                                            today.time = date
                                            key = "${today.get(Calendar.DAY_OF_MONTH)}/${today.get(Calendar.MONTH) + 1}/${today.get(Calendar.YEAR)}"

                                            if (!editionsByStack.containsKey(key)) {

                                                editionsByStack[key] = editionsList

                                            } else {

                                                val currentEditionsForDate = editionsByStack[key]

                                                if (currentEditionsForDate != null) {

                                                    currentEditionsForDate.addAll(editionsList.filter { newEdition -> !currentEditionsForDate.any { element -> element.getEditionGuid() == newEdition.getEditionGuid() } })

                                                    editionsByStack[key] = currentEditionsForDate

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                            if (editionsByStack.isNotEmpty()) {

                                val oldestStackDateStr = editionsByStack.keys.last()

                                val oldestStackDate = simpleFormatter.parse(oldestStackDateStr) as Date

                                oldestStackDate.time = oldestStackDate.time.minus(DateUtils.DAY_IN_MILLIS)

                                if (editionsByStack.size > 0) {

                                    val replicaEditions = DataStore.EditionApplication?.replica?.editions

                                    if (replicaEditions != null) {

                                        var editionAlreadyExists: Boolean
                                        var pubDate: String?

                                        for (replicaEdition: IEditionContent in replicaEditions) {

                                            pubDate = replicaEdition.getPublicationDate()

                                            if (!TextUtils.isEmpty(pubDate)) {

                                                try {

                                                    date = simpleFormatter.parse(pubDate) as Date

                                                } catch (ex: Exception) {

                                                    Log.w(EndpointQueryRequests::class.simpleName, "Unable to parse replicaEdition date: [$pubDate]")

                                                }

                                                if (date != null && date.after(oldestStackDate)) {

                                                    today.time = date
                                                    key = "${today.get(Calendar.DAY_OF_MONTH)}/${today.get(Calendar.MONTH) + 1}/${today.get(Calendar.YEAR)}"

                                                    if (!editionsByStack.containsKey(key)) {

                                                        editionsByStack[key] = arrayListOf(replicaEdition)

                                                    } else {

                                                        val currentEditionsForDate = editionsByStack[key]

                                                        if (currentEditionsForDate != null) {

                                                            editionAlreadyExists = currentEditionsForDate.any { existingEdition -> existingEdition.getEditionGuid() == replicaEdition.getEditionGuid() }

                                                            if (!editionAlreadyExists) {

                                                                currentEditionsForDate.add(replicaEdition)

                                                            }

                                                            editionsByStack[key] = currentEditionsForDate

                                                        }

                                                    }

                                                }

                                            }

                                        }

                                    }

                                    for (dateArray in editionsByStack.values) {

                                        dateArray.sortBy { dateItem -> dateItem.getDateMode() }

                                    }

                                    DataStore.EditionApplication?.editionsOrganisedByDay = arrayListOf()

                                    DataStore.EditionApplication?.editionsOrganisedByDay?.addAll(editionsByStack.values)

                                    DataStore.EditionApplication?.editionsByStack = editionsByStack

                                    DataStore.EditionApplication?.editionSkipCount = fileRootObj.optInt("editionSkipCount")

                                }

                                isLoadingStacks = false

                                listener?.invoke()

                                return@Listener

                            }

                        }

                    }

                }

                failureListener?.invoke()

            }, mErrorListener = Response.ErrorListener {

                isLoadingStacks = false
                failureListener?.invoke()

            })

            VolleyRequestQueue.getInstance(context).queueRequest(cacheRequest)

        }

    }

    fun searchArticles(context: Context, listener: ((ArrayList<SearchResult>?, Int) -> Unit)? = null, failureListener: (() -> Unit)? = null, searchUrl: String, searchTerm: String) {

        if (!isSearchingArticles) {

            isSearchingArticles = true

            Log.w("Simon", "SearchUrl: $searchUrl")

            if (!TextUtils.isEmpty(searchUrl)) {

                val cacheRequest = CacheRequest(method = Request.Method.GET, url = searchUrl, mListener = Response.Listener {

                    if (it.statusCode == 200) {

                        if (it.data is ByteArray) {

                            val fileContent = String(it.data)

                            if (!TextUtils.isEmpty(fileContent)) {

                                val rootObject = JSONObject(fileContent)

                                if (rootObject.has("totalResultCount")) {

                                    val totalResultCount = rootObject.optInt("totalResultCount")

                                    if (totalResultCount > 0) {

                                        val resultsArray = rootObject.optJSONArray("results")

                                        if (resultsArray != null) {

                                            val resultCount = resultsArray.length()

                                            if (resultCount > 0) {

                                                var resultObject: JSONObject?
                                                var editionObject: JSONObject?
                                                var articleObject: JSONObject?
                                                var highlightsArray: JSONArray?
                                                var highlightObject: JSONObject?

                                                var searchResult: SearchResult?
                                                var searchResultEdition: SearchResultEdition?
                                                var searchResultArticle: SearchResultArticle?
                                                var searchResultArticleHighlight: SearchResultArticleHighlight?

                                                var highlights: ArrayList<SearchResultArticleHighlight>?

                                                var highlightsCount = 0

                                                val results = arrayListOf<SearchResult>()

                                                for (articleIndex in 0..resultCount) {

                                                    resultObject = resultsArray.optJSONObject(articleIndex)

                                                    if (resultObject != null) {

                                                        searchResult = SearchResult()

                                                        editionObject = resultObject.optJSONObject("edition")

                                                        if (editionObject != null) {

                                                            searchResultEdition = SearchResultEdition()
                                                            searchResultEdition.editionGuid = editionObject.optString("guid")
                                                            searchResultEdition.publishDate = editionObject.optString("publishdate")

                                                            searchResult.edition = searchResultEdition
                                                        }

                                                        articleObject = resultObject.optJSONObject("article")

                                                        if (articleObject != null) {

                                                            searchResultArticle = SearchResultArticle()

                                                            searchResultArticle.articleGuid = articleObject.optString("guid")
                                                            searchResultArticle.headline = articleObject.optString("headline")
                                                            searchResultArticle.image = articleObject.optString("image")

                                                            highlightsArray = articleObject.optJSONArray("highlights")

                                                            if (highlightsArray != null) {

                                                                highlightsCount = highlightsArray.length()

                                                                if (highlightsCount > 0) {

                                                                    highlights = arrayListOf()

                                                                    for (highlightIndex in 0..highlightsCount) {

                                                                        highlightObject = highlightsArray.optJSONObject(highlightIndex)

                                                                        if (highlightObject != null) {

                                                                            searchResultArticleHighlight = SearchResultArticleHighlight()
                                                                            searchResultArticleHighlight.fieldName = highlightObject.optString("field")
                                                                            searchResultArticleHighlight.textContent = highlightObject.optString("text")

                                                                            highlights.add(searchResultArticleHighlight)

                                                                        }

                                                                    }

                                                                    searchResultArticle.highlights = highlights

                                                                }

                                                            }

                                                            searchResult.article = searchResultArticle

                                                        }

                                                        if (searchResult.article?.highlights?.isEmpty() == false) {

                                                            searchResult.hitCount = calculateHitCount(searchTerm, searchResult)

                                                            results.add(searchResult)

                                                        }

                                                    }

                                                }

                                                if (results.isNotEmpty()) {

                                                    isSearchingArticles = false

                                                    results.trimToSize()

                                                    listener?.invoke(results, totalResultCount)

                                                    return@Listener

                                                }

                                            } else {

                                                isSearchingArticles = false

                                                listener?.invoke(null, totalResultCount)

                                                return@Listener

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                    isSearchingArticles = false

                    failureListener?.invoke()

                }, mErrorListener = Response.ErrorListener {

                    isSearchingArticles = false
                    failureListener?.invoke()

                })

                VolleyRequestQueue.getInstance(context).queueRequest(cacheRequest)

            } else {

                failureListener?.invoke()

            }

        }

    }

    private fun calculateHitCount(searchTerm: String, searchResult: SearchResult): Int {

        val headline = searchResult.getArticleHeadline()?.toLowerCase() ?: ""

        val pattern = Pattern.compile(Pattern.quote(searchTerm.toLowerCase()))

        var matches = pattern.matcher(headline)

        var hitCount = 0

        while (matches.find()) {

            hitCount++

        }

        val description = searchResult.getExpandedArticleDescription()?.toLowerCase() ?: ""

        matches = pattern.matcher(description)

        while (matches.find()) {

            hitCount++

        }

        return hitCount

    }

    fun getEditionLatestDate(context: Context, listener: ((String, String) -> Unit)? = null, failureListener: (() -> Unit)? = null, applicationGuid: String) {

        val targetUrl = context.getString(R.string.urls_getEditionLatestDate).replace("{APP_GUID}", applicationGuid)

        Log.w("Simon", "TargetUrl: $targetUrl")

        val cacheRequest = CacheRequest(method = Request.Method.GET, url = targetUrl, mListener = Response.Listener {

            if (it.data is ByteArray) {

                val fileContent = String(it.data)

                if (!TextUtils.isEmpty(fileContent)) {

                    val rootObj = JSONObject(fileContent)

                    val applicationObj = rootObj.optJSONObject("application")

                    if (applicationObj != null) {

                        val cache = applicationObj.optString("cache")

                        if (!TextUtils.isEmpty(cache)) {

                            listener?.invoke(applicationGuid, cache)

                            return@Listener

                        }

                    }

                }

            }

            failureListener?.invoke()

        }, mErrorListener = Response.ErrorListener {

            failureListener?.invoke()

        })

        VolleyRequestQueue.getInstance(context).queueRequest(cacheRequest)

    }

}