package com.pagesuite.pskotlinreaderapp.archive.activity

import android.os.Bundle
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.EditionBasicActivity
import com.pagesuite.pskotlinreaderapp.archive.ArchiveHeaderHelper
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.edition.helper.bottommenu.EditionsHelper
import kotlinx.android.synthetic.main.view_edition_toolbar.*

class ArchiveActivity : EditionBasicActivity() {

    var editionsHelper: EditionsHelper? = null
    var headerHelper: ArchiveHeaderHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initArchive()

    }

    override fun initToolbar() {

        super.initToolbar()

        supportActionBar?.setDisplayShowHomeEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        headerHelper = ArchiveHeaderHelper(this)
        headerHelper?.prepHeader()

        backButton.setOnClickListener { onBackPressed() }

    }

    private fun initArchive() {

        editionsHelper = EditionsHelper(this, resources.getDimensionPixelSize(R.dimen.bottomMenuContainer_height_expanded), true)
        editionsHelper?.downloadArchive(listener = {

            editionsHelper?.openMenu()

        })

    }

    override fun getLayout(): Int {

        return R.layout.activity_archive

    }

    override fun changeEdition() {

        mApplication?.reloadEdition(this, false)

    }

    override fun alreadyLoadedEdition() {

    }

    override fun onResume() {

        super.onResume()

        DataStore.Edition = null
        DataStore.EditionApplication?.activeEdition = ""

    }

}