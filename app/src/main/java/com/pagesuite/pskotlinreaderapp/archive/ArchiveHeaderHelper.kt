package com.pagesuite.pskotlinreaderapp.archive

import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.archive.activity.ArchiveActivity
import com.pagesuite.pskotlinreaderapp.config.HeaderConfig
import com.pagesuite.pskotlinreaderapp.widget.AbstractHeaderHelper
import com.pagesuite.utilities.DeviceUtils

class ArchiveHeaderHelper(editionActivity: ArchiveActivity) : AbstractHeaderHelper(editionActivity) {

    override fun prepHeader(completionListener: (() -> Unit)?) {

        headerDate.visibility = View.GONE

        headerPaging.visibility = View.GONE

        val logoParams: ViewGroup.LayoutParams = headerLogo.layoutParams

        if (logoParams is RelativeLayout.LayoutParams) {

            logoParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0)
            logoParams.addRule(RelativeLayout.CENTER_HORIZONTAL, 1)
            headerLogo.layoutParams = logoParams

        }

        backButton.setPadding(0, 0, 0, 0)

        backButton.setImageResource(R.drawable.ic_home)

        var semaphoreCount = 2

        val listener = {

            semaphoreCount--

            if (semaphoreCount == 0) {

                completionListener?.invoke()

            }

        }

        showBackButton(listener)

        super.prepHeader(listener)

    }

    override fun getIncreasedMenuHeight(): Int {

        return if (!DeviceUtils.isPhone(basicActivity)) {

            HeaderConfig.Archive.mHomeHeight

        } else {

            getDecreasedMenuHeight()

        }

    }

    override fun getDecreasedMenuHeight(): Int {

        return HeaderConfig.Archive.mNormalHeight

    }
}