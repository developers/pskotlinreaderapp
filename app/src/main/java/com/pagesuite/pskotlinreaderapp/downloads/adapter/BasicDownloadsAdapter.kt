package com.pagesuite.pskotlinreaderapp.downloads.adapter

import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.downloads.DownloadStatus
import com.pagesuite.pskotlinreaderapp.downloads.EditionDownloads
import com.pagesuite.pskotlinreaderapp.models.IEditionContent
import com.pagesuite.pskotlinreaderapp.utils.TopCropTransformation
import com.pagesuite.pskotlinreaderapp.widget.svg.GlideApp
import java.util.*


abstract class BasicDownloadsAdapter(val context: BasicActivity, val editions: ArrayList<IEditionContent>) : RecyclerView.Adapter<BasicDownloadsAdapter.Holder>() {

    var itemClickListener: View.OnClickListener? = null
    val editionCoverHeight: Int = context.resources.getDimensionPixelSize(R.dimen.edition_bottomToolbar_edition_height)
    private val inflater: LayoutInflater = context.layoutInflater

    companion object {

        const val TYPE_DEFAULT: Int = 0
        const val TYPE_DOWNLOAD: Int = 1
        const val TYPE_DOWNLOADING: Int = 2
        const val TYPE_DOWNLOADED: Int = 3
        const val TYPE_QUEUED: Int = 4

    }

    open fun getLayoutForViewType(viewType: Int): Int {

        return when (viewType) {

            TYPE_DOWNLOAD -> {

                R.layout.edition_bottom_menu_archive_cell_download

            }

            TYPE_DOWNLOADING -> {

                R.layout.edition_bottom_menu_archive_cell_downloading

            }

            TYPE_DOWNLOADED -> {

                R.layout.edition_bottom_menu_archive_cell_downloaded

            }

            TYPE_QUEUED -> {

                R.layout.edition_bottom_menu_archive_cell_download_queued

            }

            else -> {

                R.layout.edition_bottom_menu_archive_cell

            }

        }

    }

    open fun getViewHolderForType(viewType: Int, view: View): Holder {

        return when (viewType) {

            TYPE_DOWNLOAD -> {

                DownloadHolder(view)

            }

            TYPE_DOWNLOADING -> {

                DownloadingViewHolder(view)

            }

            TYPE_DOWNLOADED -> {

                Holder(view)

            }

            TYPE_QUEUED -> {

                Holder(view)

            }

            else -> {

                Holder(view)

            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        val view = inflater.inflate(getLayoutForViewType(viewType), parent, false)

        return getViewHolderForType(viewType, view)

    }

    override fun getItemViewType(position: Int): Int {

        val edition: IEditionContent = editions[position]
        val downloadStatus = EditionDownloads.getInstance(context.mApplication).getDownloadStatus(edition)

        if (downloadStatus != null) {

            if (downloadStatus == DownloadStatus.QUEUED) {

                return TYPE_QUEUED

            } else if (downloadStatus == DownloadStatus.STARTED || downloadStatus == DownloadStatus.IN_PROGRESS) {

                return TYPE_DOWNLOADING

            } else if (downloadStatus == DownloadStatus.COMPLETED) {

                return TYPE_DOWNLOADED

            }

        } else if (EditionDownloads.getInstance(context.mApplication).isEditionDownloaded(edition)) {

            return TYPE_DOWNLOADED

        }

        return TYPE_DOWNLOAD

    }

    override fun getItemCount(): Int {

        return editions.count()

    }

    override fun onViewRecycled(holder: Holder) {

        super.onViewRecycled(holder)

        try {

            if (holder is DownloadingViewHolder) {

                holder.progressBar.progress = 0

            }

            Glide.with(holder.editionCover).clear(holder.editionCover)

            holder.editionCover.setImageResource(0)

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.bindCategory(position)

        if (holder is DownloadingViewHolder) {

            holder.progressBar.progress = 0

        }

    }

    open inner class DownloadingViewHolder(itemView: View) : DownloadHolder(itemView) {

        val progressBar: ProgressBar = itemView.findViewById(R.id.progressBar)

    }

    open inner class DownloadHolder(itemView: View) : Holder(itemView) {

        private val downloadButton: View = itemView.findViewById(R.id.downloadButton)

        init {

            if (downloadButton is ImageView) {

                downloadButton.setColorFilter(ColourConfig.Edition.Archive.mForegroundColour)

            }

        }

        override fun bindCategory(index: Int) {

            super.bindCategory(index)

            downloadButton.setTag(R.id.tag_edition, editions[index])

        }

    }

    open inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val timeLabel: TextView = itemView.findViewById(R.id.timeLabel)
        val editionCover: ImageView = itemView.findViewById(R.id.editionCover)

        open fun bindCategory(index: Int) {

            val edition = editions[index]

            if (edition.getEditionGuid() == DataStore.EditionApplication?.activeEdition) {

                editionCover.background = ContextCompat.getDrawable(context, R.drawable.edition_archive_cell_border)?.mutate()
                editionCover.background?.setColorFilter(ColourConfig.Edition.mSelectionColour, PorterDuff.Mode.SRC_ATOP)

            } else {

                editionCover.background?.clearColorFilter()
                editionCover.setBackgroundResource(0)


            }

            var editionName = edition.getEditionName()

            if (editionName != null && editionName.contains("-")) {

                editionName = editionName.split("-")[1]

            }

            timeLabel.text = editionName

            if (!TextUtils.isEmpty(edition.getEditionImage())) {

                val glideRequestOptions = RequestOptions()
                glideRequestOptions.placeholder(R.drawable.archiveplaceholder)

                GlideApp
                        .with(context)
                        .setDefaultRequestOptions(glideRequestOptions)
                        .load(edition.getEditionImage()?.replace("{HEIGHT}", editionCoverHeight.toString()))
                        .transform(TopCropTransformation())
                        .into(editionCover)

            } else {

                editionCover.setImageResource(0)

            }

            itemView.setOnClickListener(itemClickListener)

            itemView.setTag(R.id.tag_edition, edition)

        }

    }

}