package com.pagesuite.pskotlinreaderapp.downloads.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.models.IEditionContent
import com.pagesuite.pskotlinreaderapp.utils.ColouringHelper

class DownloadsAdapter(context: BasicActivity, editions: ArrayList<IEditionContent>) : BasicDownloadsAdapter(context, editions) {

    var deleteClickListener: View.OnClickListener? = null
    var editLongPressListener: View.OnLongClickListener? = null
    var isEditing: Boolean = false

    companion object {

        const val TYPE_EDITING: Int = 5

    }

    override fun getLayoutForViewType(viewType: Int): Int {

        return when (viewType) {

            TYPE_EDITING -> {

                R.layout.card_downloads_editing

            }

            TYPE_DOWNLOAD -> {

                R.layout.card_downloads_download

            }

            TYPE_DOWNLOADING -> {

                R.layout.card_downloads_downloading

            }

            TYPE_DOWNLOADED -> {

                R.layout.card_downloads_downloaded

            }

            TYPE_QUEUED -> {

                R.layout.card_downloads_queued

            }

            else -> {

                R.layout.card_downloads_default

            }

        }

    }

    override fun getViewHolderForType(viewType: Int, view: View): Holder {

        if (viewType == TYPE_EDITING) {

            return EditingDownloadViewHolder(view)

        }

        return super.getViewHolderForType(viewType, view)

    }

    override fun getItemViewType(position: Int): Int {

        if (isEditing) {

            return TYPE_EDITING

        }

        return super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        val viewHolder = super.onCreateViewHolder(parent, viewType)

        viewHolder.itemView.setOnLongClickListener(editLongPressListener)

        if (viewHolder is EditingDownloadViewHolder) {

            viewHolder.deleteButton.setOnClickListener(deleteClickListener)

        }

        viewHolder.timeLabel.setTextColor(ColourConfig.Downloads.mForegroundColour)

        return viewHolder

    }

    override fun onBindViewHolder(viewHolder: Holder, position: Int) {

        super.onBindViewHolder(viewHolder, position)

        val editionContent = editions[position]

        if (viewHolder is EditingDownloadViewHolder) {

            viewHolder.deleteButton.setTag(R.id.tag_edition, editionContent.getEditionGuid())

        }

    }

    open inner class EditingDownloadViewHolder(itemView: View) : Holder(itemView) {

        val deleteButton: TextView = itemView.findViewById(R.id.deleteButton)

        init {

            deleteButton.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Downloads.DeleteButton.mBackgroundColour,
                    pressed = ColourConfig.Downloads.DeleteButton.mPressedBackgroundColour,
                    normalBorder = ColourConfig.Downloads.DeleteButton.mBackgroundColour,
                    pressedBorder = ColourConfig.Downloads.DeleteButton.mPressedBackgroundColour)

            deleteButton.setTextColor(ColouringHelper.getSelector(normal = ColourConfig.Downloads.DeleteButton.mForegroundColour, pressed = ColourConfig.Downloads.DeleteButton.mPressedForegroundColour))

        }

    }

}