package com.pagesuite.pskotlinreaderapp.downloads.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import android.support.annotation.NonNull
import com.pagesuite.pskotlinreaderapp.downloads.DownloadStatus
import com.pagesuite.pskotlinreaderapp.models.IEditionContent

@Entity(tableName = "downloads_table")
class DownloadRecord : IEditionContent {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "editionGuid")
    private var editionGuid: String = ""

    fun setEditionGuid(updatedId: String) {

        editionGuid = updatedId

    }

    override fun getEditionGuid(): String {

        return editionGuid

    }

    @ColumnInfo(name = "editionName")
    private var editionName: String? = null

    fun setEditionName(updatedId: String?) {

        editionName = updatedId

    }

    override fun getEditionName(): String? {

        return editionName

    }

    @NonNull
    @ColumnInfo(name = "timeStamp")
    private var timeStamp: Long = 0L

    fun setTimeStamp(updatedTimeStamp: Long) {

        timeStamp = updatedTimeStamp

    }

    fun getTimeStamp(): Long {

        return timeStamp

    }

    @NonNull
    @ColumnInfo(name = "status")
    @TypeConverters(DownloadStatus::class)
    private var status: DownloadStatus = DownloadStatus.ADDED

    fun setStatus(updatedStatus: DownloadStatus) {

        status = updatedStatus

    }

    fun getStatus(): DownloadStatus {

        return status

    }

    @ColumnInfo(name = "editionImage")
    private var editionImage: String? = null

    override fun getEditionImage(): String? {

        return editionImage

    }

    fun setEditionImage(updatedPath: String?) {

        editionImage = updatedPath

    }

    @NonNull
    @ColumnInfo(name = "publicationDate")
    private var publicationDate: String? = null

    override fun getPublicationDate(): String? {

        return publicationDate

    }

    override fun setPublicationDate(input: String?) {

        publicationDate = input

    }

    @ColumnInfo(name = "dateMode")
    private var dateMode: String? = null

    override fun getDateMode(): String? {

        return dateMode

    }

    fun setDateMode(input: String?) {

        dateMode = input

    }

}