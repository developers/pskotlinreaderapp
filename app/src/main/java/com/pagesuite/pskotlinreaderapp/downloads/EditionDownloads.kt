package com.pagesuite.pskotlinreaderapp.downloads

import android.arch.lifecycle.LiveData
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v4.content.LocalBroadcastManager
import com.pagesuite.pskotlinreaderapp.AndroidFeedApp
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.downloads.db.DownloadRecord
import com.pagesuite.pskotlinreaderapp.downloads.db.DownloadsRepository
import com.pagesuite.pskotlinreaderapp.models.IEditionContent
import com.pagesuite.pskotlinreaderapp.services.FileDownloadService
import com.pagesuite.pskotlinreaderapp.utils.Utils
import java.io.File


class EditionDownloads constructor(private val mApplication: AndroidFeedApp) {

    private val downloadUrl: String = mApplication.getString(R.string.urls_editionDownload)
    private val downloadingEditions: HashMap<String, DownloadStatus> = HashMap()

    private var mDownloadRepo: DownloadsRepository? = null

    init {

        mDownloadRepo = DownloadsRepository(mApplication)

    }

    companion object {

        @Volatile
        private var INSTANCE: EditionDownloads? = null
        private const val FILE_DOWNLOADED_EDITIONS = "downloadedEditions.bodge"

        fun getInstance(context: AndroidFeedApp?) =
                INSTANCE ?: synchronized(this) {

                    INSTANCE ?: EditionDownloads(context!!).also {

                        INSTANCE = it

                    }


                }

    }

    fun listDownloads(): LiveData<List<DownloadRecord>>? {

        return mDownloadRepo?.allDownloads

    }

    fun downloadEdition(context: Context, edition: IEditionContent) {

        if (getDownloadStatus(edition) != null) return

        val targetUrl = downloadUrl.replace("{EDITION_GUID}", edition.getEditionGuid()).replace("{PLATFORM_ID}", "2")

        notifyDownloadQueued(edition)

        //makeRequest(targetUrl, edition.editionguid)

        val path = context.filesDir.absolutePath

        val fileName = "${edition.getEditionGuid()}.zip"
        val file = File(path, fileName)

        val localPath = file.absolutePath
        val unzipPath = Utils.getDataDir(context, "edition/${edition.getEditionGuid()}").absolutePath

        val downloadRequest = FileDownloadService.DownloadRequest(targetUrl, localPath)
        downloadRequest.isRequiresUnzip = true
        downloadRequest.isDeleteZipAfterExtract = true
        downloadRequest.unzipAtFilePath = unzipPath

        val listener = object : FileDownloadService.OnDownloadStatusListener {

            override fun onDownloadStarted() {

                notifyDownloadStarted(edition)

            }

            override fun onDownloadCompleted() {

                notifyDownloadComplete(edition)

            }

            override fun onDownloadFailed() {

                cancelDownload(edition)

            }

            override fun onDownloadProgress(progress: Int) {

                notifyDownloadProgress(edition, progress)

            }

        }

        val downloader = FileDownloadService.FileDownloader.getInstance(downloadRequest, listener)
        downloader.download(context)

    }

    fun getDownloadStatus(edition: IEditionContent): DownloadStatus? {

        synchronized(downloadingEditions) {

            return downloadingEditions[edition.getEditionGuid()]

        }

    }

    fun isEditionDownloaded(edition: IEditionContent): Boolean {

        val sharedPreferences: SharedPreferences = mApplication.getSharedPreferences(FILE_DOWNLOADED_EDITIONS, Context.MODE_PRIVATE)
        return sharedPreferences.contains(edition.getEditionGuid())

    }

    fun cancelDownload(edition: IEditionContent) {

        removeEdition(edition)

        val broadcastIntent = Intent(Intent(UsefulConstants.fileDownloadBroadcastID))
        broadcastIntent.putExtra(UsefulConstants.ARGS_TYPE, UsefulConstants.BROADCAST_DOWNLOAD_FAILED)
        broadcastIntent.putExtra(UsefulConstants.EDITION_GUID, edition.getEditionGuid())
        LocalBroadcastManager.getInstance(mApplication).sendBroadcast(broadcastIntent)

    }

    private fun notifyDownloadQueued(edition: IEditionContent) {

        updateStatus(edition, DownloadStatus.QUEUED)

        val broadcastIntent = Intent(Intent(UsefulConstants.fileDownloadBroadcastID))
        broadcastIntent.putExtra(UsefulConstants.ARGS_TYPE, UsefulConstants.BROADCAST_DOWNLOAD_QUEUED)
        broadcastIntent.putExtra(UsefulConstants.EDITION_GUID, edition.getEditionGuid())
        LocalBroadcastManager.getInstance(mApplication).sendBroadcast(broadcastIntent)

    }

    private fun notifyDownloadStarted(edition: IEditionContent) {

        updateStatus(edition, DownloadStatus.STARTED)

        val broadcastIntent = Intent(Intent(UsefulConstants.fileDownloadBroadcastID))
        broadcastIntent.putExtra(UsefulConstants.ARGS_TYPE, UsefulConstants.BROADCAST_DOWNLOAD_STARTED)
        broadcastIntent.putExtra(UsefulConstants.EDITION_GUID, edition.getEditionGuid())
        LocalBroadcastManager.getInstance(mApplication).sendBroadcast(broadcastIntent)

    }

    private fun notifyDownloadProgress(edition: IEditionContent, progress: Int) {

        updateStatus(edition, DownloadStatus.IN_PROGRESS)

        val broadcastIntent = Intent(Intent(UsefulConstants.fileDownloadBroadcastID))
        broadcastIntent.putExtra(UsefulConstants.ARGS_TYPE, UsefulConstants.BROADCAST_DOWNLOAD_PROGRESS)
        broadcastIntent.putExtra(UsefulConstants.EDITION_GUID, edition.getEditionGuid())
        broadcastIntent.putExtra(UsefulConstants.ARGS_PROGRESS, progress)
        LocalBroadcastManager.getInstance(mApplication).sendBroadcast(broadcastIntent)

    }

    private fun notifyDownloadComplete(edition: IEditionContent) {

        val result = removeEdition(edition)

        if (result) {

            val sharedPreferences: SharedPreferences = mApplication.getSharedPreferences(FILE_DOWNLOADED_EDITIONS, Context.MODE_PRIVATE)
            sharedPreferences.edit().putString(edition.getEditionGuid(), System.currentTimeMillis().toString()).commit()

            updateStatus(edition, DownloadStatus.COMPLETED)

            val broadcastIntent = Intent(Intent(UsefulConstants.fileDownloadBroadcastID))
            broadcastIntent.putExtra(UsefulConstants.ARGS_TYPE, UsefulConstants.BROADCAST_DOWNLOAD_COMPLETED)
            broadcastIntent.putExtra(UsefulConstants.EDITION_GUID, edition.getEditionGuid())
            LocalBroadcastManager.getInstance(mApplication).sendBroadcast(broadcastIntent)

        }

    }

    private fun removeEdition(edition: IEditionContent): Boolean {

        synchronized(downloadingEditions) {

            if (downloadingEditions.isNotEmpty()) {

                downloadingEditions.remove(edition.getEditionGuid())
                return true

            }

        }

        return false

    }

    private fun updateStatus(edition: IEditionContent, status: DownloadStatus) {

        mApplication.executors?.diskIO()?.execute {

            try {

                mDownloadRepo?.hasRecord(editionGuid = edition.getEditionGuid(), listener = { record ->

                    val downloadRecord: DownloadRecord

                    if (record != null) {

                        downloadRecord = record

                    } else {

                        downloadRecord = DownloadRecord()
                        downloadRecord.setEditionGuid(edition.getEditionGuid())
                        downloadRecord.setEditionName(edition.getEditionName())
                        downloadRecord.setDateMode(edition.getDateMode())
                        downloadRecord.setEditionImage(edition.getEditionImage())
                        downloadRecord.setPublicationDate(edition.getPublicationDate())
                        downloadRecord.setTimeStamp(System.currentTimeMillis())

                        mDownloadRepo?.insert(downloadRecord)

                    }

                    if (status != downloadRecord.getStatus()) {

                        downloadRecord.setStatus(status)

                        mDownloadRepo?.update(downloadRecord)

                    }

                })


            } catch (e: Exception) {

                e.printStackTrace()

            }

        }

        synchronized(downloadingEditions) {

            downloadingEditions[edition.getEditionGuid()] = status

        }

    }

    fun removeDownload(guid: String) {

        if (guid.isNotBlank()) {

            val sharedPreferences: SharedPreferences = mApplication.getSharedPreferences(FILE_DOWNLOADED_EDITIONS, Context.MODE_PRIVATE)
            sharedPreferences.edit().remove(guid).commit()

            mApplication.executors?.diskIO()?.execute {

                try {

                    mDownloadRepo?.hasRecord(editionGuid = guid, listener = { record ->

                        if (record != null) {

                            mDownloadRepo?.delete(record)

                        }

                    })

                } catch (ex: Exception) {

                    ex.printStackTrace()

                }
            }

            synchronized(downloadingEditions) {

                downloadingEditions.remove(guid)

            }

        }

    }

}