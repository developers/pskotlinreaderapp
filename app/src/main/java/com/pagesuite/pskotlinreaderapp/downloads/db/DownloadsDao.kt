package com.pagesuite.pskotlinreaderapp.downloads.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

@Dao
interface DownloadsDao {

    @get:Query("SELECT * from downloads_table ORDER BY editionGuid ASC")
    val allDownloadRecords: LiveData<List<DownloadRecord>>

    @Query("SELECT * from downloads_table")
    fun hasRecords(): List<DownloadRecord>

    @Query("SELECT * from downloads_table WHERE editionGuid = :selectedEditionGuid")
    fun hasRecord(selectedEditionGuid: String): DownloadRecord

    @Insert(onConflict = REPLACE)
    fun insert(downloadRecord: DownloadRecord)

    @Update
    fun update(downloadRecord: DownloadRecord)

    @Query("DELETE FROM downloads_table")
    fun deleteAll()

    @Query("DELETE FROM downloads_table WHERE editionGuid = :selectedEditionGuid")
    fun delete(selectedEditionGuid: String)

}