package com.pagesuite.pskotlinreaderapp.downloads.activity

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicToolbarActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.Queries
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.downloads.EditionDownloads
import com.pagesuite.pskotlinreaderapp.downloads.adapter.BasicDownloadsAdapter
import com.pagesuite.pskotlinreaderapp.downloads.adapter.DownloadsAdapter
import com.pagesuite.pskotlinreaderapp.downloads.db.DownloadRecord
import com.pagesuite.pskotlinreaderapp.models.IEditionContent
import kotlinx.android.synthetic.main.activity_downloads.*

class DownloadsActivity : BasicToolbarActivity() {

    private var deleteClickListener: View.OnClickListener? = null
    private var itemClickListener: View.OnClickListener? = null
    private var editLongPressListener: View.OnLongClickListener? = null

    private var isEditing: Boolean = false

    private var mAdapter: DownloadsAdapter? = null

    private var mObservableDownloads: LiveData<List<DownloadRecord>>? = null
    private val mDownloads: ArrayList<IEditionContent> = arrayListOf()

    private val mDownloadMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            val editionGuid = intent.getStringExtra(UsefulConstants.EDITION_GUID)

            if (editionGuid != null) {

                updateEdition(editionGuid, intent)

            }

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        mObservableDownloads = EditionDownloads.getInstance(mApplication).listDownloads()

        initClickHandlers()

        initObservers()

        initAdapter()

        initRecyclerView()
    }

    private fun initClickHandlers() {

        deleteClickListener = View.OnClickListener { it ->

            val tag = it.getTag(R.id.tag_edition)

            if (tag is String) {

                val index = mAdapter?.editions?.indexOfFirst { edition -> edition.getEditionGuid().equals(tag, ignoreCase = true) }
                        ?: -1

                if (index != -1) {

                    EditionDownloads.getInstance(mApplication).removeDownload(tag)
                    mAdapter?.editions?.removeAt(index)
                    mAdapter?.notifyItemRemoved(index)

                }

            }

        }

        editLongPressListener = View.OnLongClickListener {

            onEditClicked(it)
            true

        }

        itemClickListener = View.OnClickListener { it ->

            val tag = it.getTag(R.id.tag_edition)

            if (tag is IEditionContent) {

                if (EditionDownloads.getInstance(mApplication).isEditionDownloaded(tag)) {

                    val editionGuid = tag.getEditionGuid()

                    Queries.getStoredEdition(this, editionGuid) { edition ->

                        if (edition != null) {

                            mApplication?.reloadEdition(this, false)

                        }

                    }


                }

            }

        }

    }

    private fun initObservers() {

        LocalBroadcastManager.getInstance(this).registerReceiver(mDownloadMessageReceiver, IntentFilter(UsefulConstants.fileDownloadBroadcastID))

        mObservableDownloads?.observe(this, Observer { records ->

            updateDownloads(records)

        })

    }

    private fun initAdapter() {

        mAdapter = DownloadsAdapter(this, mDownloads)
        mAdapter?.deleteClickListener = deleteClickListener
        mAdapter?.editLongPressListener = editLongPressListener
        mAdapter?.itemClickListener = itemClickListener

    }

    private fun initRecyclerView() {

        recyclerView.layoutManager = getLayoutManager()

        recyclerView.adapter = mAdapter

    }

    private fun getLayoutManager(): RecyclerView.LayoutManager {

        return GridLayoutManager(this, 2)

    }

    private fun updateDownloads(records: List<DownloadRecord>?) {

        if (records != null) {

            var index: Int

            for (record: DownloadRecord in records) {

                index = mAdapter?.editions?.indexOfFirst { edition -> record.getEditionGuid().equals(edition.getEditionGuid(), ignoreCase = true) } ?: -1

                if (index == -1) {

                    mAdapter?.editions?.add(record)
                    mAdapter?.notifyItemInserted((mAdapter?.itemCount ?: 0) - 1)

                } else {

                    mAdapter?.notifyItemChanged(index)

                }

            }

        }

        checkForEmptyContent()

    }

    private fun checkForEmptyContent() {

        if (mAdapter?.itemCount ?: 0 > 0) {

            downloads_noneAvailable.post {

                downloads_noneAvailable.visibility = View.GONE

            }

        } else {

            downloads_noneAvailable.post {

                downloads_noneAvailable.visibility = View.VISIBLE

            }

            if (isEditing) {

                onEditClicked(null)

            }

        }

    }

    override fun initColourScheme() {

        super.initColourScheme()

        rootView.setBackgroundColor(ColourConfig.Downloads.mBackgroundColour)
        downloads_noneAvailable.setTextColor(ColourConfig.Downloads.mForegroundColour)

        headerBackgroundColour = ColourConfig.Downloads.Toolbar.mBackgroundColour
        headerForegroundColour = ColourConfig.Downloads.Toolbar.mForegroundColour

        toolbar_edit.setTextColor(headerForegroundColour)

    }

    @Suppress("UNUSED_PARAMETER")
    fun onEditClicked(view: View?) {

        isEditing = !isEditing

        if (isEditing) {

            toolbar_edit.text = getString(R.string.Done)

        } else {

            toolbar_edit.text = getString(R.string.toolbar_edit)

        }

        mAdapter?.isEditing = isEditing
        mAdapter?.notifyDataSetChanged()

    }

    override fun onBackPressed() {

        if (!isEditing) {

            super.onBackPressed()

        } else {

            onEditClicked(null)

        }

    }

    override fun onDestroy() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDownloadMessageReceiver)

        super.onDestroy()

    }

    override fun getLayout(): Int {

        return R.layout.activity_downloads

    }

    fun updateEdition(editionGuid: String, intent: Intent) {

        val editionsPerDay = mAdapter?.editions

        if (editionsPerDay != null) {

            var index = 0

            outerLoop@ for (edition in editionsPerDay) {

                if (edition.getEditionGuid() == editionGuid) {

                    break@outerLoop
                }

                index++

            }

            val viewHolder = recyclerView.findViewHolderForAdapterPosition(index)

            if (viewHolder != null) {

                val type = intent.getStringExtra(UsefulConstants.ARGS_TYPE)

                when (type) {

                    UsefulConstants.BROADCAST_DOWNLOAD_COMPLETED -> {

                        //mAdapter?.notifyItemChanged(index)

                    }
                    UsefulConstants.BROADCAST_DOWNLOAD_FAILED -> {

                        //mAdapter?.notifyItemChanged(index)

                    }
                    UsefulConstants.BROADCAST_DOWNLOAD_PROGRESS -> {

                        val progress = intent.getIntExtra(UsefulConstants.ARGS_PROGRESS, 0)

                        if (viewHolder is BasicDownloadsAdapter.DownloadingViewHolder) {

                            viewHolder.progressBar.progress = progress

                        }

                    }
                    UsefulConstants.BROADCAST_DOWNLOAD_STARTED -> {

                        //mAdapter?.notifyItemChanged(index)

                    }
                    UsefulConstants.BROADCAST_DOWNLOAD_QUEUED -> {

                        //mAdapter?.notifyItemChanged(index)

                    }

                }

            }

        }

    }

}