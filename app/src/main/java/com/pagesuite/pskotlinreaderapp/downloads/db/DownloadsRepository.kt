package com.pagesuite.pskotlinreaderapp.downloads.db

import android.arch.lifecycle.LiveData
import android.os.Looper
import com.pagesuite.pskotlinreaderapp.AndroidFeedApp

class DownloadsRepository internal constructor(application: AndroidFeedApp) {

    private var mDownloadsDao: DownloadsDao? = null
    internal var allDownloads: LiveData<List<DownloadRecord>>? = null

    init {

        val db = DownloadsDb.getDatabase(application)

        if (db != null) {

            mDownloadsDao = db.downloadsDao()
            allDownloads = db.downloadsDao().allDownloadRecords

        }

    }

    fun insert(downloadRecord: DownloadRecord) {

        checkThread()

        mDownloadsDao?.let {

            it.insert(downloadRecord)

        }

    }

    fun update(downloadRecord: DownloadRecord) {

        checkThread()

        mDownloadsDao?.let {

            it.update(downloadRecord)

        }

    }

    fun delete(downloadRecord: DownloadRecord) {

        checkThread()

        mDownloadsDao?.let {

            it.delete(downloadRecord.getEditionGuid())

        }

    }

    fun hasRecord(editionGuid: String, listener: ((DownloadRecord?) -> Unit)? = null) {

        checkThread()

        mDownloadsDao?.let {

            val results = it.hasRecord(editionGuid)
            listener?.invoke(results)

        }

    }

    fun hasRecords(editionGuid: List<String>, listener: ((List<DownloadRecord>?) -> Unit)? = null) {

        checkThread()

        mDownloadsDao?.let {

            val results = it.hasRecords()
            listener?.invoke(results)

        }

    }

    private fun checkThread() {

        if (Looper.getMainLooper() == Looper.myLooper()) {

            throw Exception("WrongThreadException - this must not be called from the UI thread!")

        }

    }

}