package com.pagesuite.pskotlinreaderapp.downloads.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(DownloadRecord::class), version = 1, exportSchema = false)
abstract class DownloadsDb : RoomDatabase() {

    abstract fun downloadsDao(): DownloadsDao

    companion object {

        @Volatile
        private var INSTANCE: DownloadsDb? = null

        internal fun getDatabase(context: Context): DownloadsDb? {

            if (INSTANCE == null) {

                synchronized(DownloadsDb::class.java) {

                    if (INSTANCE == null) {

                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                DownloadsDb::class.java, "downloads_database.db")
                                .build()

                    }

                }

            }

            return INSTANCE

        }

    }
}
