package com.pagesuite.pskotlinreaderapp.downloads

import android.arch.persistence.room.TypeConverter

enum class DownloadStatus constructor(val statusCode: Int?) {

    ADDED(0), QUEUED(1), STARTED(2), IN_PROGRESS(3), COMPLETED(4), FAILED(5);


    companion object {

        @TypeConverter
        @JvmStatic
        fun fromInstant(numeral: Int?): DownloadStatus? {

            for (ds in values()) {

                if (ds.statusCode === numeral) {

                    return ds

                }

            }

            return null

        }

        @TypeConverter
        @JvmStatic
        fun toInstant(instant: DownloadStatus): Int? {
            return instant.statusCode
        }

    }

}