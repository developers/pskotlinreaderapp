package com.pagesuite.pskotlinreaderapp

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import android.os.Parcelable
import android.support.multidex.MultiDexApplication
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.mobile.auth.core.IdentityHandler
import com.amazonaws.mobile.auth.core.IdentityManager
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.config.AWSConfiguration
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.security.ProviderInstaller
import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.activities.Home
import com.pagesuite.pskotlinreaderapp.appsettings.activity.AppConfigSettingsActivity
import com.pagesuite.pskotlinreaderapp.articles.activity.ArticlesBrowserActivity
import com.pagesuite.pskotlinreaderapp.articles.activity.SavedArticlesActivity
import com.pagesuite.pskotlinreaderapp.articles.activity.SearchArticlesActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.Queries
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.config.HeaderConfig
import com.pagesuite.pskotlinreaderapp.config.MenuConfig
import com.pagesuite.pskotlinreaderapp.downloads.activity.DownloadsActivity
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.gallery.activity.GalleryActivity
import com.pagesuite.pskotlinreaderapp.kiosk.activity.KioskHomeActivity
import com.pagesuite.pskotlinreaderapp.models.Application
import com.pagesuite.pskotlinreaderapp.models.IArticleContent
import com.pagesuite.pskotlinreaderapp.offline.activity.OfflineActivity
import com.pagesuite.pskotlinreaderapp.subscriptions.InAppManager
import com.pagesuite.pskotlinreaderapp.subscriptions.activity.SubscriptionsActivity
import com.pagesuite.pskotlinreaderapp.utils.AppExecutors
import com.pagesuite.pskotlinreaderapp.utils.PSSharedPreferences
import com.pagesuite.readersdk.components.ReaderSdkConsts.ReaderRelated.ARGS_EDITION
import com.pagesuite.readersdk.statics.ReaderManager
import com.pagesuite.readerui.statics.ReaderUI
import com.pagesuite.utilities.FileManipUtils
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import javax.net.ssl.SSLContext

class AndroidFeedApp : MultiDexApplication() {

    var inAppManager: InAppManager? = null

    var isFromKiosk: Boolean = false

    var mOffUiHandler: Handler? = null
    private var mOffUiThread: HandlerThread? = null

    var executors: AppExecutors? = null

    override fun onCreate() {

        super.onCreate()

        ColourConfig.loadDefaultColours(this)
        MenuConfig.loadDefaults(this)
        HeaderConfig.loadDefaults(this)

        executors = AppExecutors()

        if (BuildConfig.DEBUG) {

            /*StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build())

            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build())*/

        }

        mOffUiThread = HandlerThread("OffUiThread")
        mOffUiThread?.start()
        mOffUiHandler = Handler(mOffUiThread?.looper)

    }

    fun initializeAppSyncClient(context: Context) {

        AWSMobileClient.getInstance().initialize(context) {
            var credentialsProvider: AWSCredentialsProvider = AWSMobileClient.getInstance().credentialsProvider
            var configuration: AWSConfiguration = AWSMobileClient.getInstance().configuration

            IdentityManager.getDefaultIdentityManager().getUserID(object : IdentityHandler {

                override fun onIdentityId(identityId: String) {

                    val cachedIdentityId = IdentityManager.getDefaultIdentityManager().cachedUserID

                }

                override fun handleError(exception: Exception) {

                }

            })

        }.execute()

    }

    fun initializeInAppManager(context: AppCompatActivity, listener: ((success: Boolean, error: String?) -> Unit)? = null) {

        inAppManager = InAppManager()

        inAppManager?.init(context) { success, error ->

            if (success) {

                inAppManager?.queryInAppProducts {

                    inAppManager?.queryPurchases {

                        listener?.invoke(success, error)

                    }

                }

            } else {

                listener?.invoke(success, error)

            }

        }

    }

    fun initProviders(context: Context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {

            try {

                ProviderInstaller.installIfNeeded(this)

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

                    try {

                        val sslContext = SSLContext.getInstance("TLSv1.2")
                        sslContext.init(null, null, null)
                        sslContext.createSSLEngine()

                    } catch (e: NoSuchAlgorithmException) {

                        e.printStackTrace()

                    } catch (e: KeyManagementException) {

                        e.printStackTrace()

                    }

                }

            } catch (e: GooglePlayServicesRepairableException) {

                // Indicates that Google Play services is out of date, disabled, etc.
                // Prompt the user to install/update/enable Google Play services.
                GooglePlayServicesUtil.showErrorNotification(e.connectionStatusCode, context)
                // Notify the SyncManager that a soft error occurred.

                return

            } catch (e: GooglePlayServicesNotAvailableException) {

                // Indicates a non-recoverable error; the ProviderInstaller is not able
                // to install an up-to-date Provider.
                // Notify the SyncManager that a hard error occurred.

                if (!Build.MANUFACTURER.equals("amazon", ignoreCase = true)) {

                    return

                }

            }

        }

    }

    fun settingSelected(context: BasicActivity, type: String?) {

        when (type) {

            UsefulConstants.OPEN_SETTINGS -> moveToSettings(context)
            UsefulConstants.APP_TYPE_KIOSK -> moveToKiosk(context)
            UsefulConstants.OPEN_SAVED -> moveToSaved(context)
            UsefulConstants.OPEN_SEARCH -> moveToSearch(context)
            UsefulConstants.OPEN_DOWNLOADS -> moveToDownloads(context)
            UsefulConstants.OPEN_SUBSCRIPTIONS -> moveToSubscriptions(context)
            UsefulConstants.SETTINGS_CLEARAPPCODE -> resetAppCode(context)
            UsefulConstants.SETTINGS_CLEARCACHE -> resetCache(context)
            UsefulConstants.SETTINGS_APPFONT -> showFonts(context)
            UsefulConstants.SETTINGS_SHOWHELP -> showHelp(context)
            else -> settingUnconfigured(context)

        }

    }

    fun moveToActivity(currentActivity: AppCompatActivity, targetActivity: Class<out AppCompatActivity>, finishCurrentActivity: Boolean = true) {

        val readerIntent = Intent(this, targetActivity)

        ContextCompat.startActivity(currentActivity, readerIntent, null)

        if (finishCurrentActivity) {

            currentActivity.finish()

        }

    }

    fun moveToSettings(parentContext: Context) {

        val intent = Intent(parentContext, AppConfigSettingsActivity::class.java)
        ContextCompat.startActivity(parentContext, intent, null)

    }

    private fun moveToSearch(parentContext: BasicActivity) {

        val intent = Intent(parentContext, SearchArticlesActivity::class.java)
        ActivityCompat.startActivityForResult(parentContext, intent, UsefulConstants.REQUEST_ARTICLE_JUMP, null)

    }


    fun moveToSaved(parentContext: BasicActivity, showAllBookmarks: Boolean = false) {

        val intent = Intent(parentContext, SavedArticlesActivity::class.java)
        intent.putExtra(UsefulConstants.ARGS_SHOW_ALL, showAllBookmarks)
        ActivityCompat.startActivityForResult(parentContext, intent, UsefulConstants.REQUEST_ARTICLE_JUMP, null)

    }

    fun moveToDownloads(parentContext: BasicActivity) {

        val intent = Intent(parentContext, DownloadsActivity::class.java)
        ContextCompat.startActivity(parentContext, intent, null)

    }

    private fun moveToSubscriptions(parentContext: BasicActivity) {

        val intent = Intent(parentContext, SubscriptionsActivity::class.java)
        ContextCompat.startActivity(parentContext, intent, null)

    }

    private fun moveToKiosk(parentContext: Context) {

        val intent = Intent(parentContext, KioskHomeActivity::class.java)
        ContextCompat.startActivity(parentContext, intent, null)

    }

    private fun settingUnconfigured(parentContext: Context) {

        Toast.makeText(parentContext, "Setting ID Not Configured", Toast.LENGTH_LONG).show()

    }

    fun loadArticleBrowser(context: Context, article: IArticleContent) {

        val intent = Intent(this, ArticlesBrowserActivity::class.java)
        intent.putExtra(UsefulConstants.ARTICLE_GUID, article.getArticleGuid())
        ContextCompat.startActivity(context, intent, null)

    }

    fun loadArticleGallery(context: Context, articleGuid: String?) {

        if (articleGuid != null) {

            val intent = Intent(this, GalleryActivity::class.java)
            intent.putExtra(UsefulConstants.ARTICLE_GUID, articleGuid)
            ContextCompat.startActivity(context, intent, null)

        }

    }

    fun loadReader(context: Context, editionGuid: String) {

        val edition = ReaderManager.getInstance().findEdition(editionGuid)

        if (edition != null) {

            val reader = Intent(context, ReaderUI.getInstance().mReaderClass)

            reader.putExtra(ARGS_EDITION, edition as Parcelable)

            ContextCompat.startActivity(context, reader, null)

        }

    }

    fun showOffline(context: BasicActivity) {

        val offline = Intent(context, OfflineActivity::class.java)
        ActivityCompat.startActivityForResult(context, offline, UsefulConstants.REQUEST_OFFLINE, null)

    }

    fun reloadEdition(context: AppCompatActivity, terminateExisting: Boolean = true) {

        val pdfIntent = Intent(this, EditionActivity::class.java)

        ContextCompat.startActivity(context, pdfIntent, null)

        if (terminateExisting) {

            context.finish()

        }

    }

    fun reloadApp(context: AppCompatActivity) {

        val intent = Intent(context, Home::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        DataStore.Application = null
        DataStore.EditionApplication = null
        DataStore.Edition = null
        DataStore.Kiosk = null

        isFromKiosk = false

        ContextCompat.startActivity(context, intent, null)
        context.finish()

    }

    fun saveAppConfig(json: String, suffix: String = "default", successListener: (() -> Unit)? = null) {

        executors?.diskIO()?.execute {

            FileManipUtils.saveToFile(this, json, "appConfig_$suffix.json")
            successListener?.invoke()

        }

    }

    fun loadAppConfig(successListener: (() -> Unit)? = null, suffix: String = "default") {

        executors?.diskIO()?.execute {

            val contents = FileManipUtils.loadFile(this, "appConfig_$suffix.json")

            if (contents is String) {

                val application = Queries.gson.fromJson(contents, Application::class.java)

                DataStore.EditionApplication = application
                DataStore.Application = application
                DataStore.Application?.getLiveEditions()
                DataStore.Kiosk = DataStore.Application?.kiosk

            }

            successListener?.invoke()

        }

    }

    fun resetAppCode(context: AppCompatActivity) {

        val builder = AlertDialog.Builder(context)

        builder.setMessage(R.string.dialog_clearAppCode_message)
        builder.setTitle(R.string.dialog_clearAppCode_title)
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->

            dialog.cancel()
            DataStore.Application = null
            DataStore.Edition = null

            PSSharedPreferences().resetAppCode(context)

            reloadApp(context)

        }

        builder.setNegativeButton(android.R.string.no) { dialog, _ ->

            dialog.cancel()

        }

        builder.show()

    }

    fun resetCache(context: Context) {

        Toast.makeText(context, "Reset Cache Not Configured Yet", Toast.LENGTH_LONG).show()

    }

    fun showHelp(context: Context) {

        Toast.makeText(context, "Show Help Not Configured Yet", Toast.LENGTH_LONG).show()

    }

    fun showFonts(context: Context) {

        Toast.makeText(context, "Show Font Not Configured Yet", Toast.LENGTH_LONG).show()

    }

}