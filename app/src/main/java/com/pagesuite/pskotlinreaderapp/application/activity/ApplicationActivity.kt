package com.pagesuite.pskotlinreaderapp.application.activity

import android.os.Bundle
import android.util.Log
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers
import com.apollographql.apollo.GraphQLCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.graphql.GetApplicationsQuery
import com.pagesuite.pskotlinreaderapp.services.ClientFactory
import kotlinx.android.synthetic.main.application_data_view.*

class ApplicationActivity : BasicActivity() {

    private var mAWSAppSyncClient: AWSAppSyncClient? = null
    private var adapter: ApplicationActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        text_output.text = "test"

        this.query()

    }

    override fun getLayout(): Int {

        return R.layout.application_data_view

    }

//    override fun onResume() {
//        super.onResume()
//        query()
//    }

    private fun query() {

        if (mAWSAppSyncClient == null) {

            mAWSAppSyncClient = ClientFactory.getInstance(this)

        }

        val appBuilder = GetApplicationsQuery.builder()

        appBuilder.applicationguid("some guid")
        appBuilder.cache("1.2")

        mAWSAppSyncClient?.query(appBuilder.build())
                ?.responseFetcher(AppSyncResponseFetchers.NETWORK_ONLY)
                ?.enqueue(getApplicationCallback)

    }

    private val getApplicationCallback = object : GraphQLCall.Callback<GetApplicationsQuery.Data>() {

        override fun onResponse(response: Response<GetApplicationsQuery.Data>) {

            Log.d(TAG, response.toString())

            text_output.text = response.toString()

        }

        override fun onFailure(e: ApolloException) {

            Log.e(TAG, "Failed to get applications", e)
            Log.e(TAG, e.message)

        }

    }

    companion object {

        private val TAG = ApplicationActivity::class.java.simpleName

    }

}
