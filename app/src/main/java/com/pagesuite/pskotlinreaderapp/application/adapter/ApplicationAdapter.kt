package com.pagesuite.pskotlinreaderapp.application.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.pagesuite.pskotlinreaderapp.extensions.getLayoutInflater
import com.pagesuite.pskotlinreaderapp.graphql.GetApplicationsQuery

class ApplicationAdapter(private val mContext: Context, private var application: GetApplicationsQuery?) : BaseAdapter() {

    private val mInflater: LayoutInflater = mContext.getLayoutInflater()
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleAdaptor.Holder {
//
//        val view = LayoutInflater.from(context).inflate(R.layout.kiosk_cell, parent, false)
//        return Holder(view, itemClick)
//
//    }


    fun setApplication(application: GetApplicationsQuery) {

        this.application = application

    }

    override fun getItem(i: Int): Any {

        return application.toString()

    }

    override fun getItemId(i: Int): Long {

        return i.toLong()

    }

    override fun getCount(): Int {

        return application.toString().length

    }

    override fun getView(i: Int, convertView: View, parent: ViewGroup): View {

        //setContentView(R.layout.activity_kiosk_home)

        return convertView
    }

    private class ViewHolder {

    }
}