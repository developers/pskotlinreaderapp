package com.pagesuite.pskotlinreaderapp.offline.activity

import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.AnimationUtils
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.utils.ColouringHelper
import com.pagesuite.utilities.NetworkUtils
import kotlinx.android.synthetic.main.activity_offline.*

class OfflineActivity : BasicActivity() {

    override fun getLayout(): Int {

        return R.layout.activity_offline

    }

    override fun initColourScheme() {

        super.initColourScheme()

        rootView.setBackgroundColor(ColourConfig.Offline.mBackgroundColour)

        offline_title.setTextColor(ColourConfig.Offline.Options.mForegroundColour)

        offline_description.setTextColor(ColourConfig.Offline.Options.mDescriptionForeground)

        offline_connectionIcon.background?.setColorFilter(ColourConfig.Offline.Options.mIconBackground, PorterDuff.Mode.SRC_ATOP)

        offline_connectionIcon.setColorFilter(ColourConfig.Offline.Options.mIconTint, PorterDuff.Mode.SRC_ATOP)

        offline_button_viewEditions.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Offline.Options.Button.mBackgroundColour,
                pressed = ColourConfig.Offline.Options.Button.mPressedBackgroundColour,
                pressedBorder = ColourConfig.Offline.Options.Button.mPressedBackgroundColour,
                normalBorder = ColourConfig.Offline.Options.Button.mStrokeColour,
                borderWidth = ColourConfig.Offline.Options.Button.mStrokeWidth)

        offline_button_viewEditions.setTextColor(ColouringHelper.getSelector(normal = ColourConfig.Offline.Options.Button.mForegroundColour, pressed = ColourConfig.Offline.Options.Button.mPressedForegroundColour))

        offline_button_viewBookmarks.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Offline.Options.Button.mBackgroundColour,
                pressed = ColourConfig.Offline.Options.Button.mPressedBackgroundColour,
                pressedBorder = ColourConfig.Offline.Options.Button.mPressedBackgroundColour,
                normalBorder = ColourConfig.Offline.Options.Button.mStrokeColour,
                borderWidth = ColourConfig.Offline.Options.Button.mStrokeWidth)

        offline_button_viewBookmarks.setTextColor(ColouringHelper.getSelector(normal = ColourConfig.Offline.Options.Button.mForegroundColour, pressed = ColourConfig.Offline.Options.Button.mPressedForegroundColour))

        offline_button_retryConnection.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Offline.Options.Button.mBackgroundColour,
                pressed = ColourConfig.Offline.Options.Button.mPressedBackgroundColour,
                pressedBorder = ColourConfig.Offline.Options.Button.mPressedBackgroundColour,
                normalBorder = ColourConfig.Offline.Options.Button.mStrokeColour,
                borderWidth = ColourConfig.Offline.Options.Button.mStrokeWidth)

        offline_button_retryConnection.setTextColor(ColouringHelper.getSelector(normal = ColourConfig.Offline.Options.Button.mForegroundColour, pressed = ColourConfig.Offline.Options.Button.mPressedForegroundColour))

        if (offline_optionsContainerVisible.background is LayerDrawable) {

            val layerDrawable = offline_optionsContainerVisible?.background as LayerDrawable

            val topLayer = layerDrawable.findDrawableByLayerId(R.id.topLayer)
            topLayer.setColorFilter(ColourConfig.Offline.Options.mBackgroundTopLayer, PorterDuff.Mode.SRC_ATOP)

            val middleLayer = layerDrawable.findDrawableByLayerId(R.id.middleLayer)
            middleLayer.setColorFilter(ColourConfig.Offline.Options.mBackgroundMiddleLayer, PorterDuff.Mode.SRC_ATOP)

            val bottomLayer = layerDrawable.findDrawableByLayerId(R.id.bottomLayer)
            bottomLayer.setColorFilter(ColourConfig.Offline.Options.mBackgroundBottomLayer, PorterDuff.Mode.SRC_ATOP)

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun viewEditionsClicked(view: View) {

        mApplication?.moveToDownloads(this)

    }

    @Suppress("UNUSED_PARAMETER")
    fun viewBookmarksClicked(view: View) {

        mApplication?.moveToSaved(this, true)

    }

    @Suppress("UNUSED_PARAMETER")
    fun retryConnectionClicked(view: View) {

        if (NetworkUtils.isConnected(this)) {

            intent = Intent()
            setResult(AppCompatActivity.RESULT_OK, intent)
            finish()

        } else {

            val animShake = AnimationUtils.loadAnimation(this, R.anim.shake)
            offline_optionsContainer.startAnimation(animShake)

        }

    }

}