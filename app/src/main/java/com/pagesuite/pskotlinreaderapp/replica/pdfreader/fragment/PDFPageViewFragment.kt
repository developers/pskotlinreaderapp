package com.pagesuite.pskotlinreaderapp.replica.pdfreader.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pagesuite.pskotlinreaderapp.R

class PDFPageViewFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.activity_pdfreader_fragment, container, false)

        /*val PDFView = view.findViewById<PDFViewCtrl>(R.id.pdfViewInPager)

        PDFView.openUrlAsync("http://pages.cdn.pagesuite.com/7/b/7b3c4086-6bfd-413b-bfd9-2072857c78c6/page.pdf", null, null, null)

        PDFView.setupThumbnails(true, false, true, 2048, 1, 0.1)*/

        return view

    }


    companion object {

        fun newInstance(): PDFPageViewFragment {

            val fragment = PDFPageViewFragment()

            return fragment

        }

    }

}