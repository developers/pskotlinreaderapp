package com.pagesuite.pskotlinreaderapp.replica.pdfreader.activity


import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers
import com.apollographql.apollo.GraphQLCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.graphql.GetEditionQuery
import com.pagesuite.pskotlinreaderapp.services.ClientFactory
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import pdftron.PDF.PDFViewCtrl
import java.io.IOException

class PDFReaderActivity : BasicActivity(), ViewPager.OnPageChangeListener, PDFViewCtrl.RenderingListener, PDFViewCtrl.PageChangeListener {

    override fun onRenderingStarted() {

    }

    override fun onRenderingFinished() {

        runOnUiThread { updateUI() }

    }

    override fun onPageChange(p0: Int, p1: Int, p2: Int) {

        runOnUiThread { updateUI() }

    }

    private var mAWSAppSyncClient: AWSAppSyncClient? = null

    var currentPage = 0

    override fun getLayout(): Int {

        return R.layout.activity_pdfreader

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        /*AppUtils.initializePDFNetApplication(applicationContext, "PageSuite Limited(pagesuite.co.uk):OEM:PageSuite Digital pskotlinreaderapp.pagesuite.com.pskotlinreaderapp.Edition::WARP:AMS(20181031):C97719001F0602E5473BFA7860612FA5FB103A734454458ACD720E96529231F5C7");

        this.onPagesRequestCompleted()

        //val pubGuid = intent.getStringExtra("pubGuid")

        var placeHolderEditionGuid = intent.extras?.getString(UsefulConstants.ARGS_EDITION_GUID)

        if (placeHolderEditionGuid != null) {

            pagesQuery(placeHolderEditionGuid)

        }

        pdfView.addPageChangeListener(this)
        pdfView.setRenderingListener(this)*/

    }

    private fun pagesQuery(editionGUID: String) {

        if (mAWSAppSyncClient == null) {

            mAWSAppSyncClient = ClientFactory.getInstance(this)

        }

        var editionBuilder = GetEditionQuery.builder()

        editionBuilder.editionguid(editionGUID)
        editionBuilder.cache("1.2")

        mAWSAppSyncClient?.query(editionBuilder.build())
                ?.responseFetcher(AppSyncResponseFetchers.NETWORK_ONLY)
                ?.enqueue(getApplicationCallback)

    }

    private val getApplicationCallback = object : GraphQLCall.Callback<GetEditionQuery.Data>() {

        override fun onResponse(response: Response<GetEditionQuery.Data>) {

            Log.d(PDFReaderActivity::class.simpleName, response.toString())
            Log.d(PDFReaderActivity::class.simpleName, "RESPONSE DATA: ${response.data()}")

        }

        override fun onFailure(e: ApolloException) {

            Log.w(PDFReaderActivity::class.simpleName, e.toString())
            Log.w(PDFReaderActivity::class.simpleName, e.message)

        }

    }


    //toolbar fractionation

    fun prepViewPagerListener() {
//        PDFViewPager.addOnPageChangeListener(this)
    }

    override fun onPageSelected(position: Int) {

        updateUI()

    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    fun updateUI() {

        /*val currentPage = (pdfView.currentPage).toString()
        val totalNumberOfPages = (pdfView.pageCount).toString()

        pageNumberLabel.text = "$currentPage of $totalNumberOfPages"

        thumbnailSlider.setProgress(pdfView.currentPage)*/

    }

    fun nextPage(view: View) {
//        var currentPage = PDFViewPager.currentItem
//        var newPage = currentPage + 1
//
//        if (newPage >= PDFs.count()){
//            return
//        }
//
//        PDFViewPager.setCurrentItem(newPage, true)
//        updateUI()

    }

    fun previousPage(view: View) {

//        var currentPage = PDFViewPager.currentItem
//        var newPage = currentPage - 1
//
//        if (newPage < 0){
//           return
//        }
//
//        PDFViewPager.setCurrentItem(newPage, true)
//        updateUI()

    }

    fun lastPage(view: View) {

//        PDFViewPager.setCurrentItem(PDFs.count()-1, true)
//        updateUI()

    }

    fun firstPage(view: View) {

//        PDFViewPager.setCurrentItem(0, true)
//        updateUI()

    }

    fun returnToKiosk(view: View) {

        //val kioskIntent = Intent(this, KioskHomeActivity::class.java)
        //startActivity(kioskIntent)
        onBackPressed()

    }

    fun openBurgerMenu(view: View) {

        Toast.makeText(this, "Side Menu Unavailable (Not Yet Made)", Toast.LENGTH_LONG).show()

    }


    //PDF PagesRequest

    fun getEditionGUID(pubGUIDURL: String) {

        Log.d(PDFReaderActivity::class.simpleName, "THE GUID LINK IS ${pubGUIDURL}")

        val request = Request.Builder().url(pubGUIDURL).build()

        var client = OkHttpClient()

        client.newCall(request).enqueue(object : Callback {

            override fun onFailure(call: Call?, e: IOException?) {

                Log.w(PDFReaderActivity::class.simpleName, "edition guid data Not Recieved")

            }

            override fun onResponse(call: Call?, response: okhttp3.Response?) {

            }

        })


        val editionGUID = ""

        // when compeleted and got the data,

    }

    fun makeRequestForPage(editionGUID: String) {

    }

    fun onPagesRequestCompleted() {

        runOnUiThread {

            /*pdfView.openUrlAsync("https://s3.amazonaws.com/pdf-flattener-complete/Binder1_FLAT_THUMB_1024.pdf", null, null, null)

            pdfView.setupThumbnails(true, false, true, 2048, 1, 0.1)*/

        }


    }

    fun moveToSettings(view: View) {

        mApplication?.moveToSettings(this)

    }

}
