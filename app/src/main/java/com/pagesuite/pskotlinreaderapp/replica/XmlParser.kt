package com.pagesuite.pskotlinreaderapp.replica

import android.text.TextUtils
import com.pagesuite.pskotlinreaderapp.models.ReplicaEdition
import com.pagesuite.pskotlinreaderapp.models.ReplicaProperties
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.ByteArrayInputStream
import java.nio.charset.Charset
import java.util.*

class EditionsParser {

    fun parse(content: String?, publicationGuid: String?, rootImageUrl: String): ArrayList<ReplicaEdition>? {

        if (content != null && publicationGuid != null) {

            var stubs: ArrayList<ReplicaEdition>? = null
            var stub: ReplicaEdition? = null

            try {

                val contentStream = ByteArrayInputStream(content.toByteArray(Charset.forName("UTF-8")))

                // get a new XmlPullParser object from Factory

                val parser = XmlPullParserFactory.newInstance().newPullParser()

                // set input source

                parser.setInput(contentStream, null)

                // get event type

                var eventType = parser.eventType

                // process tag while not reaching the end of document

                while (eventType != XmlPullParser.END_DOCUMENT) {

                    when (eventType) {

                        // at start of document: START_DOCUMENT
                        XmlPullParser.START_DOCUMENT -> stubs = ArrayList()

                        // at start of a tag: START_TAG
                        XmlPullParser.START_TAG -> {

                            // get tag name
                            val startTagName = parser.name

                            if (startTagName.equals(ReplicaProperties.EDITION, ignoreCase = true)) {

                                stub = ReplicaEdition()
                                //stub.mEditionGuid = "ddb7b6b1-8928-43fc-891f-238768098112";
                                stub.mEditionGuid = parser.getAttributeValue(null, ReplicaProperties.EDITION_GUID)
                                stub.mDate = parser.getAttributeValue(null, ReplicaProperties.EDITION_DATE)
                                stub.mName = parser.getAttributeValue(null, ReplicaProperties.EDITION_NAME)
                                stub.mPageCount = parser.getAttributeValue(null, ReplicaProperties.EDITION_PAGES)
                                stub.mLastModified = parser.getAttributeValue(null, ReplicaProperties.EDITION_LASTMODIFIED)
                                stub.mPubName = parser.getAttributeValue(null, ReplicaProperties.EDITION_PUB_NAME)
                                stub.imageUrl = rootImageUrl.replace("{EID}", stub.mEditionGuid
                                        ?: "")

                                if (TextUtils.isEmpty(stub.mPubName)) {

                                    stub.mPubName = parser.getAttributeValue(null, ReplicaProperties.EDITION_PUBLICATION_NAME)

                                }

                                //stub.mPubGuid = parser.getAttributeValue(null, EditionStub.EDITION_PUB_GUID);

                                stub.mPubGuid = publicationGuid

                            }

                        }

                        XmlPullParser.END_TAG -> {

                            val endTagName = parser.name

                            if (endTagName.equals(ReplicaProperties.EDITION, ignoreCase = true)) {

                                if (stub != null) {

                                    stubs?.add(stub)

                                }

                            }

                        }

                        XmlPullParser.END_DOCUMENT -> {

                        }

                    }

                    // jump to next event
                    eventType = parser.next()

                }

                // exception stuffs

            } catch (e: Exception) {

                e.printStackTrace()

            }

            stubs?.trimToSize()

            return stubs

        }

        return null

    }

}