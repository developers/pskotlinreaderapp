package com.pagesuite.pskotlinreaderapp.replica.pdfreader.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.pagesuite.pskotlinreaderapp.replica.pdfreader.fragment.PDFPageViewFragment

class PDFPageViewAdaptor(private val fragmentManager: FragmentManager, private val PDFs: ArrayList<String>) : FragmentStatePagerAdapter(fragmentManager) {

    // 2
    override fun getItem(index: Int): Fragment {

        return PDFPageViewFragment.newInstance()

    }

    // 3
    override fun getCount(): Int {

        return PDFs.count()

    }

}