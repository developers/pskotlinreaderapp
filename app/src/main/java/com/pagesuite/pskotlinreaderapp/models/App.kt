package com.pagesuite.pskotlinreaderapp.models


class App {

    var phonetype: String? = null
    var tablettype: String? = null
    var readerskin: String? = null
    var backgroundcolor: String? = null
    var accountguid: String? = null
    var shortname: String? = null
    var guid: String? = null
    var name: String? = null
    var splashurl: String? = null
    var apikey: String? = null
    var sharedsecret: String? = null
    var openmode: String? = null

}