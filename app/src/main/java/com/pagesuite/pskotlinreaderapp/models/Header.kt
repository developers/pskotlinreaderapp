package com.pagesuite.pskotlinreaderapp.models

class Header {

    var headerlogo: String? = null
    var homeheight: Float = 0.0f
    var naturalheight: Float = 0.0f
    var buttoncolor: String? = null
    var backgroundcolor: String? = null

}
