package com.pagesuite.pskotlinreaderapp.models

class Item {

    var text: String? = null
    var type: String? = null
    var value: String? = null

}