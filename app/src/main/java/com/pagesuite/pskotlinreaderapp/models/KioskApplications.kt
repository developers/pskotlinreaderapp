package com.pagesuite.pskotlinreaderapp.models

class KioskApplications {

    var name: String? = null
    var image: String? = null
    var apptype: String? = null
    var shortcode: String? = null
    var applicationguid: String? = null
    var publicationguid: String? = null

}
