package com.pagesuite.pskotlinreaderapp.models

import java.io.Serializable

class Page : Serializable {

    companion object {

        private const val serialVersionUID: Long = 101

    }

    //var id: ID
    var cache: String? = null
    var html: String? = null
    var type: String? = null
    var section: String? = null
    var contenturl: String? = null
    var pageguid: String? = null
    var orderof: Int = -1
    var articles: Array<Article>? = null
    var editionguid: String? = null

    var advertcode: String? = null

    var isfullpage: Boolean = false
    var navigateurl: String? = null

    var promotions: Array<String>? = null
    var screenshoturl: String? = null

    var templatetype: String? = null

    var level: String? = null


}