package com.pagesuite.pskotlinreaderapp.models

import android.text.TextUtils

class Menu {

    var backgroundcolor: String? = null
    var highlightcolor: String? = null
    var headertext: String? = null
    var itemgroups: Array<ItemGroup>? = null

    fun getTotalItemCount(): Int {

        var total = 0
        total += itemgroups?.count() ?: 0

        if (shouldShowTopHeader()) {

            total += 1

        }

        return total

    }

    fun shouldShowTopHeader(): Boolean {

        return (!TextUtils.isEmpty(headertext))

    }

}