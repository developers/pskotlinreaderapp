package com.pagesuite.pskotlinreaderapp.models

class ReplicaEdition : IEditionContent {

    var mName: String? = null
    var mDate: String? = null
    var mEditionGuid: String? = null
    var mPageCount: String? = null
    var mLastModified: String? = null
    var mPubName: String? = null
    var mPubGuid: String? = null
    var sectionString: String? = null
    var imageUrl: String? = null

    override fun getEditionName(): String? {

        return mName

    }

    override fun getEditionGuid(): String {

        return mEditionGuid ?: ""

    }

    override fun getEditionImage(): String? {

        return imageUrl

    }

    override fun getPublicationDate(): String? {

        return mDate

    }

    override fun setPublicationDate(input: String?) {

        mDate = input

    }

    override fun getDateMode(): String? {

        return ""

    }
}

object ReplicaProperties {

    const val EDITION = "edition"
    const val EDITION_GUID = "editionguid"
    const val EDITION_NAME = "name"
    const val EDITION_DATE = "date"
    const val EDITION_PAGES = "pages"
    const val EDITION_LASTMODIFIED = "lastmodified"
    const val EDITION_PUB_NAME = "pubname"
    const val EDITION_PUB_GUID = "pubGUID"
    const val EDITION_PUBLICATION_NAME = "publication"

    const val EDITION_EID = "eid"
    const val EDITION_PUBID = "pubid"
    const val SETTINGS = "settings"
    const val VALUE = "const value"
    const val KEY = "key"
    const val PAGE = "page"

    const val EDITION_SINGLE_PUBLICATION_NAME = "PublicationName"
    const val EDITION_SINGLE_EDITION_NAME = "EditionName"
    const val EDITION_SINGLE_PUBLISH_DATE = "PublishDate"
    const val EDITION_SINGLE_LASTMODIFIED = "LastModified"

}