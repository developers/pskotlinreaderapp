package com.pagesuite.pskotlinreaderapp.models

class Font {

    var name: String? = null
    var size: Float = 0.0f
    var color: String? = null

}
