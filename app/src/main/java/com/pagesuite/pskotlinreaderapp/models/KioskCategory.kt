package com.pagesuite.pskotlinreaderapp.models

class KioskCategory {

    var name: String? = null
    var showall: Boolean = false
    var applications: Array<KioskApplications?>? = null

}
