package com.pagesuite.pskotlinreaderapp.models

import android.content.Context
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.replica.EditionsParser
import com.pagesuite.readersdk.objects.EditionStub
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties

class Replica {

    var pubguid: String? = null

    var editions: ArrayList<out IEditionContent>? = null

    fun populateEditions(context: Context, input: String? = null) {

        if (input != null) {

            val rootImageUrl = context.getString(R.string.urls_getEditionImage)
            editions = EditionsParser().parse(input, pubguid, rootImageUrl)

        }

    }

    fun populatedEditionsFromStubs(rootImageUrl: String, stubs: ArrayList<EditionStub>?) {

        if (stubs != null && stubs.isNotEmpty()) {

            val replicaEditions: ArrayList<ReplicaEdition> = arrayListOf()

            var edition: ReplicaEdition?

            var targetPropertyValue: Any?
            var property: Any?

            for (stub in stubs) {

                edition = ReplicaEdition()

                for (prop in EditionStub::class.memberProperties) {

                    targetPropertyValue = prop.get(stub)

                    property = edition::class.memberProperties.find { it.name == prop.name }

                    if (property is KMutableProperty<*>) {

                        property.setter.call(edition, targetPropertyValue)

                    }

                }

                edition.imageUrl = rootImageUrl.replace("{EID}", stub.mEditionGuid)

                replicaEditions.add(edition)

            }

            editions = replicaEditions

        }

    }

}