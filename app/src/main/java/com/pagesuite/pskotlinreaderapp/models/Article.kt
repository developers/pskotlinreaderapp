package com.pagesuite.pskotlinreaderapp.models

import android.content.Context
import android.text.Spanned
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.utils.Utils
import java.io.File
import java.io.Serializable

class Article : Serializable, IArticleContent {

    companion object {
        private const val serialVersionUID: Long = 101
    }

    //  var id: ID = ID()
    var cache: String? = null
    //var image: String = ""
    var author: String? = null
    var section: String? = null
    var headline: String? = null
    var uniqueid: String? = null
    var sharelink: String? = null
    var articleguid: String? = null
    var description: String? = null
    var textdescription: String? = null
    var descriptionnoscript: String? = null
    var subheadline: String? = null
    var images: Array<Image>? = null
    var type: String? = null

    @Transient
    var sectionData: Section? = null

    var pubDate: String? = null
    var editionGuid: String? = null

    /**
     * To be used whenever the headline will be visible to the user
     */
    override fun getSpannedHeadline(): Spanned? {

        return Utils.getSpanned(headline)

    }

    fun getPageForArticle(): Int {

        val pages = DataStore.Edition?.pages

        if (pages != null) {

            var articles: Array<Article>?

            pageLoop@ for ((index, page) in pages.withIndex()) {

                if (page.type == "" || page.type == UsefulConstants.TYPE_SECTION) {

                    continue@pageLoop

                }

                articles = page.articles

                if (articles != null) {

                    for (article in articles) {

                        if (article.articleguid == this.articleguid) {

                            return index

                        }

                    }

                }

            }

        }

        return -1

    }

    fun getFirstImagePath(context: Context?): String? {

        return getImagePath(context, 0)

    }

    fun getImagePath(context: Context?, index: Int): String? {

        if (images?.isNotEmpty() == true) {

            val image = images?.getOrNull(index)

            if (image != null) {

                var imageUrl: String? = image.image

                if (imageUrl != null) {

                    if (!imageUrl.startsWith("http") && !imageUrl.startsWith("file://")) {

                        val imageFilePath = "${context?.filesDir?.absolutePath}/edition/${DataStore.Edition?.editionguid}/$imageUrl"
                        val imageFile = File(imageFilePath)
                        imageUrl = (if (imageFile.exists()) "file://$imageFilePath" else "")
                        setFirstImagePath(imageUrl)

                    }

                }

                return imageUrl

            }

        }

        return null

    }

    fun setFirstImagePath(imagePath: String) {

        setImagePath(imagePath, 0)

    }

    fun setImagePath(imagePath: String, index: Int) {

        if (images?.isNotEmpty() == true) {

            val image = images?.getOrNull(index)

            if (image != null) {

                image.image = imagePath

                images?.set(index, image)

            }

        }

    }

    override fun getArticleGuid(): String? {

        return articleguid

    }

    override fun getArticleImage(context: Context?): String? {

        return getFirstImagePath(context)

    }

    override fun getArticleHeadline(): String? {

        return headline

    }

    override fun getArticleDescription(): String? {

        return descriptionnoscript

    }

    override fun getPublishDate(): String? {

        return pubDate

    }
}