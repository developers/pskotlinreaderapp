package com.pagesuite.pskotlinreaderapp.models

import android.text.TextUtils

class ItemGroup {

    var name: String? = null
    var items: Array<Item>? = null
    //new
    var type: String? = null

    var itemOffset = 0

    private fun getItemsOffset(): Int {

        itemOffset = if (!TextUtils.isEmpty(name)) {

            0

        } else {

            1

        }

        return itemOffset

    }

    fun getItemCount(): Int {

        if (items != null) {

            return items?.count() ?: 0+getItemsOffset()

        }

        return 0

    }

}