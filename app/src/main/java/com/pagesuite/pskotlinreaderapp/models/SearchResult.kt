package com.pagesuite.pskotlinreaderapp.models

import android.content.Context
import android.text.Spanned
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.utils.Utils

class SearchResult : IArticleContent {

    var edition: SearchResultEdition? = null
    var article: SearchResultArticle? = null

    private var headlineHighlight: String? = null
    private var descriptionHighlight: String? = null
    private var expandedDescription: String? = null

    var hitCount: Int = -1

    override fun getArticleGuid(): String? {

        return article?.articleGuid

    }

    override fun getArticleImage(context: Context?): String? {

        return article?.image

    }

    override fun getArticleHeadline(): String? {

        if (headlineHighlight == null) {

            headlineHighlight = article?.highlights?.firstOrNull { highlight -> highlight.fieldName?.equals("headline") == true }?.textContent ?: article?.headline

        }

        return headlineHighlight

    }

    override fun getSpannedHeadline(): Spanned? {

        return Utils.getSpanned(getArticleHeadline())

    }

    override fun getArticleDescription(): String? {

        if (descriptionHighlight == null) {

            descriptionHighlight = article?.highlights?.firstOrNull { highlight -> highlight.fieldName?.equals(UsefulConstants.SEARCH_FIELD_DESCRIPTION) == true }?.textContent ?: ""

        }

        return descriptionHighlight

    }

    fun getExpandedArticleDescription(): String? {

        if (expandedDescription == null) {

            expandedDescription = article?.highlights?.filter { content ->
                content.fieldName?.equals(UsefulConstants.SEARCH_FIELD_DESCRIPTION, ignoreCase = true) == true
            }
                    ?.joinToString(separator = "<br /><br />", transform = { content ->
                        "${content.textContent}"
                                .removePrefix("\n\n")
                                .removePrefix("<br /><br />")
                                .removeSuffix("\n\n")
                                .removeSuffix("<br /><br />")
                    }) ?: ""

        }

        return expandedDescription

    }

    override fun getPublishDate(): String? {

        return edition?.publishDate

    }

}

class SearchResultEdition {

    var editionGuid: String? = null
    var publishDate: String? = null

}

class SearchResultArticle {

    var articleGuid: String? = null
    var image: String? = null
    var headline: String? = null
    var highlights: ArrayList<SearchResultArticleHighlight>? = null

}

class SearchResultArticleHighlight {

    var fieldName: String? = null
    var textContent: String? = null


}