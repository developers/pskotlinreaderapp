package com.pagesuite.pskotlinreaderapp.models

import android.util.Log
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants

class Edition : IEditionContent {

    //EAS412 - coverToCover
    //FLOWA0 - coverToCover
    //FLOWB0 - sectionToSection
    //NEWS10 - sectionToSection

    //var id: ID
    var cache: String? = ""
    var name: String? = null
    var image: String? = null
    var islive: Boolean = false
    var pubdate: String? = null
    var datemode: String? = null
    var articlemode: String? = null
    var lastupdated: String? = null
    var lastprocessed: String? = null
    var sectionfronts: String? = null
    var orderof: Int = -1
    var editionguid: String? = null
    var sections: Array<Section>? = null
    var pages: Array<Page>? = null
    var pageCache: HashMap<String, String>? = null
    var activeSection = 0
    var activePage = 0
    var flow: String? = null

    var listOfArticles: ArrayList<Article>? = null

    private var visiblePages: Array<Page>? = null
    private var articleCarouselPages: Array<Page>? = null

    fun getSectionFronts(): Boolean {

        return (sectionfronts == "On")

    }


    fun populateListOfHeadlines() {

        sections?.let {

            listOfArticles = arrayListOf()

            var sectionArticles: Array<Article>? = null

            for (section in it) {

                Log.d(Edition::class.simpleName, "THE SECTION COLOR IS ${section.color}")

                if (this.getSectionFronts()) {

                    val article = Article()
                    article.headline = "Section: ${section.name}"
                    listOfArticles?.add(article)
                    article.type = UsefulConstants.TYPE_COVER
                    article.sectionData = section

                }

                sectionArticles = section.articles

                if (sectionArticles != null) {

                    for (article in sectionArticles) {

                        listOfArticles?.add(article)
                        article.type = UsefulConstants.TYPE_ARTICLE
                        article.sectionData = section

                    }

                }

            }

        }

    }

    fun getEditionPages(): Array<Page>? {

        if (flow?.equals(UsefulConstants.FLOW_SECTION_TO_SECTION, ignoreCase = true) == true) {

            if (visiblePages == null) {

                visiblePages = pages?.filter { page -> page.level?.equals(UsefulConstants.LEVEL_SECTION, ignoreCase = true) == true }?.toTypedArray()

            }

            return visiblePages

        }

        return pages

    }

    fun getArticlePages(): Array<Page>? {

        if (flow?.equals(UsefulConstants.FLOW_SECTION_TO_SECTION, ignoreCase = true) == true) {

            if (articleCarouselPages == null) {

                articleCarouselPages = pages?.filter { page -> page.level?.equals(UsefulConstants.LEVEL_ARTICLE, ignoreCase = true) == true }?.toTypedArray()

            }

            return articleCarouselPages

        }

        return pages

    }

    fun getPagePosition(currentPage: Page?, sectionName: String?, selectedPages: Array<Page>? = getEditionPages()): Int {

        val sectionPages: List<Page>? = selectedPages?.filter { page -> page.section?.equals(sectionName, ignoreCase = true) == true }

        if (sectionPages != null) {

            var interstitialCount = 0

            val pageIndex = sectionPages.indexOf(currentPage)

            var page: Page?

            for (i in 0..pageIndex) {

                page = sectionPages[i]

                if (page.type?.equals(UsefulConstants.TYPE_INTERSTITIAL, ignoreCase = true) == true) {

                    interstitialCount++

                }

            }

            return pageIndex - interstitialCount
        }

        return 1

    }

    fun getPageCount(sectionName: String?, selectedPages: Array<Page>? = getEditionPages()): Int {

        //if (flow?.equals(UsefulConstants.FLOW_SECTION_TO_SECTION, ignoreCase = true) == true) {

        return selectedPages?.count { page ->
            (page.section?.equals(sectionName, ignoreCase = true) == true)
                    && (page.type?.equals(UsefulConstants.TYPE_INTERSTITIAL, ignoreCase = true) != true)
        }
                ?: 0

        //}

        //return selectedPages?.size ?: 0

    }

    override fun getEditionName(): String? {

        return name

    }

    override fun getEditionGuid(): String {

        return editionguid ?: ""

    }

    override fun getEditionImage(): String? {

        return image

    }

    override fun getPublicationDate(): String? {

        return pubdate

    }

    override fun setPublicationDate(input: String?) {

        pubdate = input

    }

    override fun getDateMode(): String? {

        return datemode

    }
}



