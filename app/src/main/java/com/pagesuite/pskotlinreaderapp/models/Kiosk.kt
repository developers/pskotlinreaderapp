package com.pagesuite.pskotlinreaderapp.models

class Kiosk {
    var backgroundcolor: String? = null
    var headercolor: String? = null
    var fontcolor: String? = null
    var categories: Array<KioskCategory> = arrayOf()
    private var allApplications: ArrayList<KioskApplications> = arrayListOf()
    var appHeader: Header? = null

    var availablecolor: String? = null

    var backgroundgradientbottom: String? = null
    var backgroundgradienttop: String? = null
    var backgroundimage: String? = null

    var font: PageFonts? = null

    var indicatorcolor: String? = null
    var indicatorsize: Int? = null
    var landingpagephone: String? = null
    var landingpagetablet: String? = null
    var phonelockrotation: String? = null
    var selectedcolor: String? = null
    var tabletlockrotation: String? = null

    private fun populateAllApplicationsList() {

        var applications: Array<KioskApplications?>? = null

        for (category in categories) {

            applications = category.applications

            if (applications != null) {

                for (application in applications) {

                    if (application == null) continue
                    allApplications.add(application)

                }

            }

        }

    }

    fun getAllApplicationsList(): Array<KioskApplications?> {

        if (allApplications.isEmpty()) {

            populateAllApplicationsList()

        }

        return allApplications.toTypedArray()

    }

}
