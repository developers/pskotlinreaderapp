package com.pagesuite.pskotlinreaderapp.models


class AppSection {

    var name: String? = null
    var feedguid: String? = null
    var feedcolor: String? = null
    var sectionguid: String? = null
    var sectionfronttemplateguid: String? = null

}