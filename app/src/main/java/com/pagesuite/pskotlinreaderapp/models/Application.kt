package com.pagesuite.pskotlinreaderapp.models

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class Application {

    var app: App? = null
    var editionrules: EditionRules? = null
    var fonts: Fonts? = null
    var menu: Menu? = null
    var advertising: Advertising? = null
    var replica: Replica? = null
    var header: Header? = null
    var footer: Footer? = null
    var editions: Array<Edition>? = null
    var liveEditions: ArrayList<Edition>? = null
    var kiosk: Kiosk? = null
    var id: String? = null
    var cache: String? = null

    var activeEdition = ""

    var editionsOrganisedByDay: ArrayList<ArrayList<IEditionContent>>? = null
    var editionsByStack: TreeMap<String, ArrayList<IEditionContent>>? = null

    var editionSkipCount: Int = 0

    fun getLiveEditions() {

        editions?.let {

            liveEditions = arrayListOf()

            for (edition in it) {

                if (!edition.islive) continue

                liveEditions?.add(edition)
                //println("LIVE EDITIONS ARRAY is $liveEditions")

            }

        }

    }

    fun getEditionDate(editionDateISO: String, format: String): String? {

        //  val editionDateISO = this.editionsOrganisedByDay[index].first().pubdate
        try {

            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.UK)
            val date = sdf.parse(editionDateISO)

            return SimpleDateFormat(format).format(date)

        } catch (ex: ParseException) {

            Log.w(Application::class.simpleName, "Unable to parse Date: \"$editionDateISO\"")

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return editionDateISO

    }

}