package com.pagesuite.pskotlinreaderapp.models

class SavedArticle {

    var article: Article? = null
    var page: Page? = null
    var template: String? = null

}