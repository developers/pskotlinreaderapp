package com.pagesuite.pskotlinreaderapp.models

import java.io.Serializable

class Image : Serializable {

    companion object {
        private const val serialVersionUID: Long = 102
    }

    var image: String? = null
    var caption: String? = null

}