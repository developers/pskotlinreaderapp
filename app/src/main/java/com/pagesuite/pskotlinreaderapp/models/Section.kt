package com.pagesuite.pskotlinreaderapp.models

import android.util.Log
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import java.io.Serializable

class Section : Serializable {

    companion object {

        private const val serialVersionUID: Long = 103

    }

    var color: String? = null
    var name: String? = null
    var feedguid: String? = null
    var sectionguid: String? = null
    var sectionheader: String? = null
    var subsections: Array<Section>? = null
    var articles: Array<Article>? = null
    var adverts: Advert? = null

    fun getFirstPageInSection(): Int? {

        val pages = DataStore.Edition?.getEditionPages()

        Log.d(Section::class.simpleName, "running in the data store. count is ${pages?.count()
                ?: 0}")

        if (pages != null) {

            pageLoop@ for (page in pages) {

                Log.d(Section::class.simpleName, "running loop. page section is ${page.section}, this name is ${this.name}")

                if (page.section == this.name) {

                    return pages.indexOf(page)

                }

            }

        }

        return null

    }

}