package com.pagesuite.pskotlinreaderapp.models

class Footer {

    var buttoncolor: String? = null
    var backgroundcolor: String? = null
    var items: Array<Item>? = null
    //new
    var highlightcolor: String? = null

}
