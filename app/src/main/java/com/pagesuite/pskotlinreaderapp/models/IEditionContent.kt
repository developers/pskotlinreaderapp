package com.pagesuite.pskotlinreaderapp.models

interface IEditionContent {

    fun getEditionName(): String?
    fun getEditionGuid(): String
    fun getEditionImage(): String?
    fun getPublicationDate(): String?
    fun setPublicationDate(input: String?)
    fun getDateMode(): String?

}