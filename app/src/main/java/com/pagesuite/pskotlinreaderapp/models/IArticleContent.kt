package com.pagesuite.pskotlinreaderapp.models

import android.content.Context
import android.text.Spanned

interface IArticleContent {

    fun getArticleGuid(): String?
    fun getArticleImage(context: Context? = null): String?
    fun getArticleHeadline(): String?
    fun getArticleDescription(): String?
    fun getSpannedHeadline(): Spanned?
    fun getPublishDate(): String?

}