package com.pagesuite.pskotlinreaderapp.models

class Template {

    var name: String? = null
    var type: String? = null
    var cache: String? = null
    var templateurl: String? = null

}