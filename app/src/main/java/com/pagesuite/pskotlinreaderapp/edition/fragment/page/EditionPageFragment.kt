package com.pagesuite.pskotlinreaderapp.edition.fragment.page

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.webkit.JavascriptInterface
import android.webkit.WebView
import com.android.volley.Request
import com.android.volley.Response
import com.pagesuite.pskotlinreaderapp.BuildConfig
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.downloads.CacheRequest
import com.pagesuite.pskotlinreaderapp.downloads.VolleyRequestQueue
import com.pagesuite.pskotlinreaderapp.edition.replaceArticlePlaceHolders
import com.pagesuite.pskotlinreaderapp.edition.replaceHTML
import com.pagesuite.pskotlinreaderapp.extensions.getApplication
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.pskotlinreaderapp.utils.Utils


class EditionPageFragment : WebViewFragment() {

    private var loadFileRunner: Runnable? = null
    private var loadHtmlRunner: Runnable? = null

    var templateContent: String? = null

    @Suppress("unused")
    @JavascriptInterface
    fun articleSelected(test: String) {

        Log.d(EditionPageFragment::class.simpleName, "java script interface method triggered - String - $test")

        moveToArticleFromCover((test.toInt() - 1))

    }

    @Suppress("unused")
    @JavascriptInterface
    fun callGallery() {

        if (isAdded) {

            val basicActivity = activity as? BasicActivity

            if (basicActivity != null) {

                basicActivity.mApplication?.loadArticleGallery(basicActivity, pageData?.articles?.firstOrNull()?.getArticleGuid())

            }

        }

    }

    private fun moveToArticleFromCover(articleSelected: Int) {

        val articleCount = pageData?.articles?.size ?: 0

        if (articleSelected in 0..(articleCount - 1)) {

            val article = pageData?.articles?.get(articleSelected)

            if (article != null) {

                Log.d(EditionPageFragment::class.simpleName, "article guid is ${article.articleguid}")

                /*val pageIndex = article.getPageForArticle()
            if (pageIndex != null) {
                //(context as EditionActivity).moveToPage(pageIndex)
            }*/

                if (listener != null) {

                    listener?.invoke(article.articleguid)

                }

            }

        } else {

            Log.w(EditionPageFragment::class.simpleName, "Selected article outside of available range: $articleSelected of $articleCount")

        }

    }

    override fun prepFontSize() {

        val fontSize = UsefulConstants.getFontSize()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            if (BuildConfig.BUILD_TYPE.equals("debug", true)) {

                WebView.setWebContentsDebuggingEnabled(true)

            }

            /*htmlView?.evaluateJavascript("javascript:(function() { " +
                    "document.getElementsByClassName('body')[0].style.fontSize = '${fontSize.size}px';" +
                    "})()", null)*/

            htmlView?.evaluateJavascript("javascript:multiplyFontSize(${fontSize.size})", null)

        } else {

            htmlView?.loadUrl("javascript:multiplyFontSize(${fontSize.size})")
        }

    }

    override fun renderPage(page: Page?) {

        if (isAdded) {

            if (TextUtils.isEmpty(templateContent)) {

                //val url = "https://s3-eu-west-1.amazonaws.com/dev-feed-editions/HTMLArticle/Heaney+friday+v1.html"//"https://s3-eu-west-1.amazonaws.com/dev-feed-editions/HTMLArticle/Heaney+v3.html" //placeholder. will change back to htmlTemplate

                val templateHtml: String? = page?.contenturl
                        ?: getString(R.string.urls_defaultArticle_html)

                if (templateHtml != null) {

                    if (templateHtml.startsWith("http")) {

                        val stringRequest = CacheRequest(Request.Method.GET, templateHtml,
                                Response.Listener { response ->

                                    if (isAdded) {

                                        val input = String(response.data)

                                        loadHtmlContent(input)

                                    }

                                },

                                Response.ErrorListener {

                                    Log.w(EditionPageFragment::class.simpleName, "FAILED")

                                })

                        stringRequest.setShouldCache(true)

                        VolleyRequestQueue.getInstance(context as Context).queueRequest(stringRequest)

                    } else {

                        loadFileRunner = Runnable {

                            if (isAdded) {

                                val filePath = context?.filesDir?.absolutePath + "/edition/${DataStore.Edition?.editionguid}/$templateHtml"
                                val fileContent = Utils.loadFileAsString(filePath)

                                if (fileContent != null) {

                                    loadHtmlContent(fileContent)

                                }

                            }

                        }

                        getApplication()?.mOffUiHandler?.post(loadFileRunner)

                    }

                }

            } else {

                templateContent?.let {

                    loadHtmlContent(it)

                }

            }

        }

    }

    @SuppressLint("SetJavaScriptEnabled", "AddJavascriptInterface")
    override fun initWebView() {

        htmlView?.addJavascriptInterface(this, "AndroidInterface")

        super.initWebView()

    }

    override fun loadHtmlContent(input: String, filePath: String?) {

        if (isAdded) {

            templateContent = input

            loadHtmlRunner = Runnable {

                if (isAdded) {

                    val replacementValues = replaceArticlePlaceHolders(context as Context, pageData, mPageArticles)

                    val replacedWebView = replaceHTML(input, replacementValues)

                    htmlView?.post {

                        if (isAdded) {

                            htmlView?.loadDataWithBaseURL(null, replacedWebView, "text/html", "utf-8", null)

                        }

                    }

                }

            }

            getApplication()?.mOffUiHandler?.post(loadHtmlRunner)

        }

    }

    override fun onDestroy() {

        htmlView?.removeJavascriptInterface("AndroidInterface")

        if (loadFileRunner != null) {

            getApplication()?.mOffUiHandler?.removeCallbacks(loadFileRunner)

        }

        if (loadHtmlRunner != null) {

            getApplication()?.mOffUiHandler?.removeCallbacks(loadHtmlRunner)

        }

        super.onDestroy()
    }

    override fun failedToLoad() {

    }

}




