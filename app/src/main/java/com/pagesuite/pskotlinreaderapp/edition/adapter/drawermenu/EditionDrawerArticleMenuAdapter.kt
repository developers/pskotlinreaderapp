package com.pagesuite.pskotlinreaderapp.edition.adapter.drawermenu

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.models.Section


//This is the adaptor for the list of articles in the drawer menu in the edition

class EditionDrawerArticleMenuAdapter(val context: Context, val section: Section, private val itemClickListener: View.OnClickListener) : RecyclerView.Adapter<EditionDrawerArticleMenuAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        val view = LayoutInflater.from(context).inflate(R.layout.edition_article_menu_cell, parent, false)
        view.setOnClickListener(itemClickListener)
        return Holder(view)

    }

    override fun getItemCount(): Int {

        return section.articles?.count() ?: 0

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.bindCategory(position)

    }

    override fun onViewRecycled(holder: Holder) {

        super.onViewRecycled(holder)

        Glide.with(context).clear(holder.headlineImage)

        holder.headlineImage.setImageResource(0)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var sectionName: TextView = itemView.findViewById(R.id.sectionName)
        var headlineText: TextView = itemView.findViewById(R.id.headlineText)
        var headlineImage: ImageView = itemView.findViewById(R.id.headlineImage)

        init {

            sectionName.setTextColor(ColourConfig.Edition.NavigationDrawer.Article.mSectionForegroundColour)
            headlineText.setTextColor(ColourConfig.Edition.NavigationDrawer.Article.mForegroundColour)

            itemView.setBackgroundColor(ColourConfig.Edition.NavigationDrawer.Article.mBackgroundColour)

        }

        fun bindCategory(index: Int) {

            sectionName.text = section.name

            val article = section.articles?.getOrNull(index)

            if (article != null) {

                headlineText.text = article.getSpannedHeadline()

                val image = article.getFirstImagePath(context)

                if (!TextUtils.isEmpty(image)) {

                    Glide
                            .with(context)
                            .load(image)
                            .into(headlineImage)

                } else {

                    headlineImage.setImageResource(0)

                }

                itemView.setTag(R.id.article_guid, article.articleguid)

            }

        }

    }


}



