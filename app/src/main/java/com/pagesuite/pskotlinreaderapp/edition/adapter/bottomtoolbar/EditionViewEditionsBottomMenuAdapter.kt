package com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar

import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.downloads.adapter.BasicDownloadsAdapter
import com.pagesuite.pskotlinreaderapp.models.IEditionContent
import java.util.*


class EditionViewEditionsBottomMenuAdapter(context: BasicActivity, editions: ArrayList<IEditionContent>, private val shouldShowDownload: Boolean) : BasicDownloadsAdapter(context, editions) {

    override fun getItemViewType(position: Int): Int {

        if (shouldShowDownload) {

            return super.getItemViewType(position)

        }

        return TYPE_DEFAULT

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        super.onBindViewHolder(holder, position)

        holder.timeLabel.setTextColor(ColourConfig.Edition.Archive.mForegroundColour)

    }


}