package com.pagesuite.pskotlinreaderapp.edition.fragment.page

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.RelativeLayout
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.models.Page

open class WebViewFragment : EditionBasicPageFragment() {

    var htmlView: WebView? = null

    override fun updateMargins() {

        val layoutParams = htmlView?.layoutParams

        if (layoutParams is RelativeLayout.LayoutParams) {

            layoutParams.topMargin = topMargin
            layoutParams.bottomMargin = bottomMargin

            htmlView?.layoutParams = layoutParams

        }

    }

    override fun getLayout(): Int {

        return R.layout.fragment_page_webview

    }

    override fun renderPage(page: Page?) {

        loadHtmlContent(page?.contenturl ?: "")

    }

    override fun prepFontSize() {


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        htmlView = view.findViewById(R.id.pageHTMLView)

        initWebView()

    }

    @SuppressLint("SetJavaScriptEnabled", "AddJavascriptInterface")
    open fun initWebView() {

        htmlView?.webChromeClient = WebChromeClient()

        htmlView?.settings?.javaScriptEnabled = true

        //htmlView.loadUrl("https://s3-eu-west-1.amazonaws.com/dev-feed-editions/HTMLArticle/Heaney+base+backup.html") //placeholder. will change back to htmlTemplate)

        htmlView?.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {

                super.onPageFinished(view, url)

                val currentPageNumber = DataStore.Edition?.activePage ?: return

                if (pageData?.orderof ?: 0 < currentPageNumber) {

                    Log.d(WebViewFragment::class.simpleName, "page order test. setting to end")
                    view.scrollTo(view.width * 3, 0)

                } else {

                    Log.d(WebViewFragment::class.simpleName, "page order test. setting to start")
                    view.scrollTo(0, 0)

                }

                prepFontSize()
                Log.d(WebViewFragment::class.simpleName, "finished loading")

            }

            /*override fun shouldInterceptRequest(view: WebView?, url: String?): WebResourceResponse? {

                val response = allowAppIntercept(view, url)
                return response ?: super.shouldInterceptRequest(view, url)
            }

            @SuppressLint("NewApi")
            override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {

                val response = allowAppIntercept(view, request?.url.toString())
                return response ?: super.shouldInterceptRequest(view, request)
            }*/
        }

        initWebViewSettings()

        htmlView?.isHorizontalScrollBarEnabled = true
        htmlView?.isVerticalScrollBarEnabled = true

    }

    @SuppressLint("SetJavaScriptEnabled")
    open fun initWebViewSettings() {

        val cacheDir = context?.cacheDir?.absolutePath

        val s = htmlView?.settings

        if (s != null) {

            //s.useWideViewPort = true
            //s.loadWithOverviewMode = true

            s.javaScriptEnabled = true
            s.javaScriptCanOpenWindowsAutomatically = true
            s.databaseEnabled = true
            s.domStorageEnabled = true
            s.loadsImagesAutomatically = true
            s.setAppCacheEnabled(true)
            s.allowFileAccess = true
            s.setAppCachePath(cacheDir)

        }

    }

    open fun loadHtmlContent(input: String, filePath: String? = null) {

        if (isAdded) {

            htmlView?.post {

                htmlView?.loadUrl(input)

            }

        }

    }

    override fun onDestroy() {

        htmlView?.stopLoading()
        htmlView?.loadUrl("about:blank")
        htmlView?.clearHistory()
        htmlView?.clearMatches()

        super.onDestroy()
    }

    override fun failedToLoad() {

    }

}