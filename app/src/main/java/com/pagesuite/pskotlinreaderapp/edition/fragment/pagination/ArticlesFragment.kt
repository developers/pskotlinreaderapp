package com.pagesuite.pskotlinreaderapp.edition.fragment.pagination

import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.edition.adapter.EditionViewPagerAdaptor
import com.pagesuite.pskotlinreaderapp.edition.fragment.EditionContentFragment
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.utilities.DeviceUtils

class ArticlesFragment : EditionContentFragment() {

    var selectedArticle: Article? = null

    override fun getLayout(): Int {

        return R.layout.fragment_page_articles

    }

    override fun prepViewPager(completionListener: (() -> Unit)?) {

        viewPager?.visibility = View.INVISIBLE

        val edition = DataStore.Edition

        if (edition != null) {

            //if (!hasRun) {

            hasRun = true
            val pages = edition.getArticlePages()
            val cache = edition.cache
            val articleMode = ((edition.articlemode?.equals(UsefulConstants.ARTICLE_DISPLAY_COLUMNS, true)
                    ?: false) || edition.articlemode?.equals(UsefulConstants.ARTICLE_DISPLAY_MULTICOLUMN, true)
                    ?: false) && !DeviceUtils.isPhone(activity)

            if (pages != null && cache != null) {

                adapter = EditionViewPagerAdaptor(childFragmentManager, pages, cache, articleMode)
                adapter?.listener = listener
                adapter?.marginListener = marginListener
                viewPager?.adapter = adapter
                viewPager?.post {

                    if (selectedArticle != null) {

                        val pageIndex = pages.indexOfFirst { page -> page.articles?.firstOrNull { article: Article -> article.articleguid?.equals(selectedArticle?.articleguid, ignoreCase = true) == true } != null }

                        if (pageIndex != -1) {

                            viewPager?.setCurrentItem(pageIndex, false)

                        }

                    }

                    viewPager?.post {

                        viewPager?.visibility = View.VISIBLE

                    }

                }

            }

            //}

        }

        completionListener?.invoke()

    }

    override fun getCurrentPage(): Int {

        return viewPager?.currentItem ?: 0

    }

}