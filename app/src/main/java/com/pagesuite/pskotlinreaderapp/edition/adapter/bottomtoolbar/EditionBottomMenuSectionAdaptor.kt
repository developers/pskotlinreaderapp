package com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.models.Section

class EditionBottomMenuSectionAdaptor(val context: Context, val sections: Array<Section>, val includeSectionCovers: Boolean, val coverClickListener: View.OnClickListener, val itemClickListener: View.OnClickListener) : RecyclerView.Adapter<EditionBottomMenuSectionAdaptor.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        val view = LayoutInflater.from(context).inflate(R.layout.edition_bottom_menu_section_cell, parent, false)
        return Holder(view)

    }

    override fun getItemCount(): Int {

        return (if (includeSectionCovers) sections.count() + 1 else sections.count())

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindCategory(position)

    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var sectionName: TextView = itemView.findViewById(R.id.sectionName)

        fun bindCategory(index: Int) {

            if (includeSectionCovers) {

                if (index == 0) {

                    sectionName.text = context.getString(R.string.covers)
                    itemView.setOnClickListener(coverClickListener)

                } else {

                    prepContent(index - 1)

                }

            } else {

                prepContent(index)

            }

        }

        private fun prepContent(index: Int) {

            itemView.setTag(R.id.tag_index, index)
            sectionName.text = sections[index].name
            itemView.setOnClickListener(itemClickListener)

        }

    }

}