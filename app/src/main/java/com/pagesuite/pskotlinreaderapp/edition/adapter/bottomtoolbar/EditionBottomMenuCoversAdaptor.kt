package com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.models.Section


class EditionBottomMenuCoversAdaptor(val context: Context, val sections: Array<Section>, private val itemClickListener: View.OnClickListener) : RecyclerView.Adapter<EditionBottomMenuCoversAdaptor.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        val view = LayoutInflater.from(context).inflate(R.layout.edition_bottom_menu_cover_cell, parent, false)
        view.setOnClickListener(itemClickListener)
        return Holder(view)

    }

    override fun getItemCount(): Int {

        return sections.count()

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.bindCategory(position)

    }

    override fun onViewRecycled(holder: Holder) {

        super.onViewRecycled(holder)

        Glide.with(context).clear(holder.coverImage)

        holder.coverImage.setImageResource(0)

    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var coverImage: ImageView = itemView.findViewById(R.id.articleImage)

        fun bindCategory(index: Int) {

            val section = sections[index]

            Glide
                    .with(context)
                    .load("https://pbs.twimg.com/profile_images/580380183791857664/OtC0AMfJ.png")//section[index].frontCoverImagePropety)
                    .into(coverImage)

            itemView.setTag(R.id.tag_index, index)

        }

    }

}



