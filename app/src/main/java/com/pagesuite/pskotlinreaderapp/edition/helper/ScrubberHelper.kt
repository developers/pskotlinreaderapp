package com.pagesuite.pskotlinreaderapp.edition.helper

import android.animation.ValueAnimator
import android.graphics.PorterDuff
import android.graphics.Rect
import android.graphics.drawable.LayerDrawable
import android.support.constraint.ConstraintLayout
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.SeekBar
import com.bumptech.glide.Glide
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.BottomMenuType
import com.pagesuite.pskotlinreaderapp.edition.EditionProgressBarState
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.pskotlinreaderapp.utils.ColouringHelper
import com.pagesuite.utilities.DeviceUtils
import kotlinx.android.synthetic.main.activity_edition.*
import kotlinx.android.synthetic.main.view_article_scrubber.*

class ScrubberHelper(val editionActivity: EditionActivity) {

    private val scrubberBar = editionActivity.ScrubberBar
    private val scrubberArrow = editionActivity.scrubberArrow
    private val scrubberDetailsArticle = editionActivity.scrubberDetailsArticle
    private val scrubberDetailsImageContainer = editionActivity.scrubberDetailsImageContainer
    private val scrubberDetailsSection = editionActivity.scrubberDetailsSection
    private val scrubberDetailsLayout = editionActivity.scrubberDetailsLayout
    private val scrubberDetailsText = editionActivity.scrubberDetailsText
    private val scrubberDetailsImage = editionActivity.scrubberDetailsImage
    private val scrubberBarrier = editionActivity.scrubberBarrier
    private val progressBar = editionActivity.ProgressBar
    private val scrubberBarContainer = editionActivity.ScrubberBarContainer
    private val progressBarBuffer = editionActivity.progressBarBuffer
    private val progressBarBase = editionActivity.ProgressBarBase

    fun initScrubberBarAppearance() {

        createScrubberArticleBackground()

        createScrubberProgressBackground()

        progressBarBase.setBackgroundColor(ColourConfig.Edition.Scrubber.mProgressBackgroundColour)
        progressBar.setBackgroundColor(ColourConfig.Edition.Scrubber.mProgressForegroundColour)

        scrubberBar.thumb.bounds = Rect(0, 0, 0, 0)
        scrubberBar.thumb.alpha = 0

    }

    private fun createScrubberArticleBackground() {

        scrubberDetailsArticle.background = ColouringHelper.getDrawable(normal = ColourConfig.Edition.Scrubber.Popup.mBackgroundColour,
                border = ColourConfig.Edition.Scrubber.Popup.mBorderColour,
                cornerRadius = 5.0f,
                strokeWidth = ColourConfig.Edition.Scrubber.Popup.mBorderWidth)

    }

    private fun createScrubberProgressBackground() {

        val progressDrawable = scrubberBar.progressDrawable

        if (progressDrawable is LayerDrawable) {

            val background = progressDrawable.findDrawableByLayerId(android.R.id.background)
            background.setColorFilter(ColourConfig.Edition.Scrubber.mSeekbarBackgroundColour, PorterDuff.Mode.SRC_ATOP)

            val foreground = progressDrawable.findDrawableByLayerId(android.R.id.progress)
            foreground.setColorFilter(ColourConfig.Edition.Scrubber.mSeekbarForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

    }

    private fun calculateCustomSeekerPosition(blockSize: Float) {

        try {

            val width = scrubberBar.width - scrubberBar.paddingLeft - scrubberBar.paddingRight
            val position = scrubberBar.paddingLeft + width * scrubberBar.progress / scrubberBar.max

            val newArrowPosition = position.toFloat() - (scrubberArrow.width / 2)

            scrubberArrow.x = newArrowPosition

            // the devices is evidently not big enough for the box to move horizontally, stop here
            //if (scrubberDetailsArticle.layoutParams.width == ViewGroup.LayoutParams.MATCH_PARENT) return

            val minXpos: Int = editionActivity.resources.getDimensionPixelSize(R.dimen.margin_eight)

            if (DeviceUtils.isPhone(editionActivity) && editionActivity.showingArticles) {

                scrubberDetailsArticle.x = minXpos.toFloat()

            } else {

                var newDetailsBoxPosition = position.toFloat()

                if (newDetailsBoxPosition + blockSize + minXpos > width) {

                    newDetailsBoxPosition = width - blockSize - minXpos - minXpos - minXpos

                }

                if (newDetailsBoxPosition < minXpos) {

                    newDetailsBoxPosition = minXpos.toFloat()

                }

                scrubberDetailsArticle.x = newDetailsBoxPosition

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }


    fun moveCustomSeekerThumb() {

        try {

            val pages = (if (DataStore.Edition?.flow?.equals(UsefulConstants.FLOW_SECTION_TO_SECTION, ignoreCase = true) == true) {

                if (editionActivity.showingArticles) {

                    DataStore.Edition?.getArticlePages()

                } else {

                    DataStore.Edition?.getEditionPages()

                }

            } else {

                DataStore.Edition?.getEditionPages()

            })

            if (pages != null) {

                if (scrubberBar.progress < pages.size) {

                    val data = pages[scrubberBar.progress]

                    if (data.type == UsefulConstants.TYPE_ARTICLE) {

                        val selectedArticle = data.articles?.getOrNull(0)

                        if (selectedArticle != null) {

                            val selectedSection = DataStore.Edition?.sections?.firstOrNull { section -> section.name?.equals(selectedArticle.section, ignoreCase = true) == true }

                            if (selectedSection != null) {

                                val article = selectedSection.articles?.firstOrNull { article -> article.articleguid?.equals(selectedArticle.articleguid, ignoreCase = true) == true }

                                setThumbToArticleDrawable(data, article, pages)

                            }

                        }

                    } else if (data.type == UsefulConstants.TYPE_INTERSTITIAL) {

                        setThumbToSectionDrawable(data, editionActivity.getString(R.string.interstitial), pages)

                    } else if (data.type == UsefulConstants.TYPE_PUZZLE) {

                        setThumbToScreenshot(data, pages)

                    } else {

                        setThumbToSectionDrawable(data, data.section, pages)

                    }

                }

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    private fun setThumbToSectionDrawable(page: Page, sectionName: String?, pages: Array<Page>) {

        try {

            Log.d(ScrubberHelper::class.simpleName, "setting to section drawable")

            scrubberDetailsImageContainer.visibility = View.GONE
            scrubberDetailsSection.visibility = View.VISIBLE

            scrubberDetailsSection.text = sectionName

            if (page.type?.equals(UsefulConstants.TYPE_INTERSTITIAL, ignoreCase = true) != true) {

                scrubberDetailsText.visibility = View.VISIBLE

                scrubberDetailsText.text = if (!TextUtils.isEmpty(sectionName)) {

                    getScrubberDetailsText(page, pages)

                } else {

                    editionActivity.getString(R.string.scrubber_section)

                }

            } else {

                scrubberDetailsText.text = ""
                scrubberDetailsText.visibility = View.GONE

            }

            scrubberDetailsLayout.post {

                val width = scrubberDetailsLayout.measuredWidth
                calculateCustomSeekerPosition(width.toFloat())

            }

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

    }


    private fun setThumbToArticleDrawable(page: Page, articleData: Article?, pages: Array<Page>) {

        try {

            Log.d(ScrubberHelper::class.simpleName, "setting to article drawable: ${articleData
                    ?: "null"}")

            scrubberDetailsImageContainer.visibility = View.VISIBLE
            scrubberDetailsSection.visibility = View.VISIBLE

            scrubberDetailsText.visibility = View.VISIBLE

            scrubberDetailsImage.setImageResource(0)

            if (articleData != null) {

                scrubberDetailsText.text = (if (!TextUtils.isEmpty(articleData.headline)) articleData.getSpannedHeadline() else editionActivity.getString(R.string.scrubber_section).toUpperCase())
                scrubberDetailsSection.text = (if (!TextUtils.isEmpty(articleData.sectionData?.name)) {

                    "${articleData.sectionData?.name} ${getScrubberDetailsText(page, pages)}"

                } else {

                    editionActivity.getString(R.string.scrubber_name).toUpperCase()

                })

                val image = articleData.getFirstImagePath(editionActivity)

                if (image != null) {

                    scrubberDetailsImage.visibility = View.VISIBLE

                    Glide.with(editionActivity).load(image).into(scrubberDetailsImage)

                } else {

                    scrubberDetailsImage.visibility = View.GONE

                }

            }

            scrubberDetailsLayout.post {

                val width = scrubberDetailsArticle.measuredWidth
                calculateCustomSeekerPosition(width.toFloat())

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    private fun setThumbToScreenshot(pageData: Page?, pages: Array<Page>) {

        try {

            scrubberDetailsImageContainer.visibility = View.VISIBLE
            scrubberDetailsSection.visibility = View.VISIBLE

            scrubberDetailsText.visibility = View.VISIBLE

            scrubberDetailsImage.visibility = View.GONE

            if (pageData != null) {

                scrubberDetailsText.text = getScrubberDetailsText(pageData, pages)
                scrubberDetailsSection.text = pageData.section

                val image = pageData.screenshoturl

                if (image != null) {

                    scrubberDetailsImage.visibility = View.VISIBLE

                    Glide.with(editionActivity).load(image).into(scrubberDetailsImage)

                } else {

                    scrubberDetailsImage.setImageResource(0)

                }

            }

            scrubberDetailsLayout.post {

                val width = scrubberDetailsArticle.measuredWidth
                calculateCustomSeekerPosition(width.toFloat())

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }

    private fun getScrubberDetailsText(page: Page, pages: Array<Page>): String {

        val pagePosition = (DataStore.Edition?.getPagePosition(page, page.section, pages) ?: 0) + 1
        val pageCount = DataStore.Edition?.getPageCount(page.section, pages) ?: 0

        return "$pagePosition " + editionActivity.getString(R.string.of) + " $pageCount"

    }

    fun prepScrubberBar(completionListener: (() -> Unit)? = null) {

        scrubberArrow.bringToFront()
        scrubberBar.progress = 0
        moveCustomSeekerThumb()

        val articles = (if (DataStore.Edition?.flow?.equals(UsefulConstants.FLOW_SECTION_TO_SECTION, ignoreCase = true) == true) {

            if (editionActivity.showingArticles) {

                DataStore.Edition?.getArticlePages()

            } else {

                DataStore.Edition?.getEditionPages()

            }

        } else {

            DataStore.Edition?.getArticlePages()

        })

        if (articles == null || articles.isEmpty()) {

            completionListener?.invoke()

            Log.w(ScrubberHelper::class.simpleName, "edition activity, prep scrubber bar, pages")
            return

        }

        scrubberBar.max = articles.count() - 1

        scrubberBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {

                moveCustomSeekerThumb()

            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

                Log.d(ScrubberHelper::class.simpleName, "touching")

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

                editionActivity.moveToPage(seekBar.progress, false)
                transitionFromScrubberToProgressBarIfNeeded()

            }

        })

        completionListener?.invoke()

    }

    fun transitionFromProgressBarToScrubberIfNeeded() {

        if (editionActivity.currentProgressBarState == EditionProgressBarState.SCRUBBER) return
        if (editionActivity.currentBottomMenuType != BottomMenuType.NONE) return

        editionActivity.bottomMenuHelper?.onFontMenuCloseIfNeeded()
        editionActivity.bottomMenuHelper?.closeMenu()

        editionActivity.drawerHelper?.disableDrawerMenuSwipeIn()

        editionActivity.currentProgressBarState = EditionProgressBarState.SCRUBBER
        scrubberBarrier.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        scrubberBarContainer.visibility = View.VISIBLE
        progressBarBuffer.visibility = View.GONE
        animateProgressBarBaseSizeChange(editionActivity.resources.getDimensionPixelSize(R.dimen.scrubber_bar))

        scrubberDetailsArticle.visibility = View.VISIBLE
        scrubberArrow.visibility = View.VISIBLE

    }

    fun transitionFromScrubberToProgressBarIfNeeded() {

        if (editionActivity.currentProgressBarState == EditionProgressBarState.PROGRESSBAR) return

        editionActivity.currentProgressBarState = EditionProgressBarState.PROGRESSBAR

        editionActivity.drawerHelper?.enableDrawerMenuSwipeIn()

        progressBar.visibility = View.VISIBLE
        scrubberBarContainer.visibility = View.GONE
        progressBarBuffer.visibility = View.VISIBLE
        scrubberBarrier.visibility = View.GONE

        animateProgressBarBaseSizeChange(editionActivity.resources.getDimensionPixelSize(R.dimen.progress_bar))
        initScrubberBarAppearance()

        scrubberDetailsArticle.visibility = View.GONE
        scrubberArrow.visibility = View.GONE

    }

    private fun animateProgressBarBaseSizeChange(newSize: Int) {

        val anim = ValueAnimator.ofInt(progressBarBase.measuredHeight, newSize)

        anim.addUpdateListener { valueAnimator ->

            val heightVal = valueAnimator.animatedValue as Int
            val layoutParams = progressBarBase.layoutParams
            layoutParams.height = heightVal
            progressBarBase.layoutParams = layoutParams

        }

        anim.duration = editionActivity.animationDuration.toLong()
        anim.start()
    }

    /////////////MARKER: PROGRESS/SCRUBBER BAR////////////

    fun updateProgressBar(animate: Boolean) {

        val editionNumberOfPages: Int = if (editionActivity.showingArticles) {

            editionActivity.articlesContent?.getPageCount()

        } else {

            editionActivity.editionContent?.getPageCount()

        } ?: 0

        if (editionNumberOfPages == 0) return

        val params = progressBar.layoutParams

        val currentPage = if (editionActivity.showingArticles) {

            editionActivity.articlesContent?.getCurrentPage()

        } else {

            editionActivity.editionContent?.getCurrentPage() ?: 0

        } ?: 0

        val progress = (currentPage / editionNumberOfPages.toFloat())

        if (params is ConstraintLayout.LayoutParams) {

            if (!animate) {

                params.matchConstraintPercentWidth = progress
                progressBar.requestLayout()
                return

            }

            val anim = ValueAnimator.ofFloat(params.matchConstraintPercentWidth, progress)

            anim.addUpdateListener { valueAnimator ->

                val widthVal = valueAnimator.animatedValue as Float
                params.matchConstraintPercentWidth = widthVal
                progressBar.requestLayout()

            }

            anim.duration = editionActivity.animationDuration.toLong()
            anim.start()

        }

        scrubberBar.progress = currentPage
        DataStore.Edition?.activePage = currentPage

    }

}