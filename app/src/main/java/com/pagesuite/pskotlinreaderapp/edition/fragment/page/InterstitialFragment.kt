package com.pagesuite.pskotlinreaderapp.edition.fragment.page

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.google.android.gms.ads.doubleclick.PublisherAdView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.models.Interstitial
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.utilities.DeviceUtils
import com.pagesuite.utilities.NetworkUtils

class InterstitialFragment : EditionBasicPageFragment() {

    private var advertView: PublisherAdView? = null
    private var advertText: TextView? = null
    var progressSpinner: ProgressBar? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        advertText = view.findViewById(R.id.advertText)
        progressSpinner = view.findViewById(R.id.loadingSpinner)

    }

    override fun getLayout(): Int {

        return R.layout.fragment_page_interstitial

    }

    override fun prepPage(cache: String?) {

        super.prepPage(cache)

        if (NetworkUtils.isConnected(activity)) {

            loadingAd()

        } else {

            failedToLoadAd()

        }

    }

    override fun renderPage(page: Page?) {

        if (pageData != null) {

            val section = DataStore.Edition?.sections?.firstOrNull { section -> section.name?.equals(pageData?.section, ignoreCase = true) == true }

            if (section != null) {

                val adverts = section.adverts

                if (adverts != null) {

                    val interstitial = adverts.interstitial

                    if (interstitial != null) {

                        loadInterstitial(interstitial)

                        return

                    }

                }

            }

        }

        failedToLoadAd()

    }

    override fun failedToLoad() {

        failedToLoadAd()

    }

    private fun loadInterstitial(interstitial: Interstitial) {

        if (NetworkUtils.isConnected(activity)) {

            if (interstitial.type?.equals(UsefulConstants.ADVERT_TYPE_DFP_GOOGLE) == true) {

                if (interstitial.values != null) {

                    if (advertView == null && isAdded) {

                        advertView = PublisherAdView(activity)

                        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
                        params.addRule(RelativeLayout.CENTER_IN_PARENT, 1)

                        advertView?.layoutParams = params

                    }

                    val screenSize = DeviceUtils.getScreenSize(activity)
                    val screenDensity = DeviceUtils.getScreenDensityScale(activity)

                    val deviceSize = AdSize(Math.round(screenSize[0].toFloat() / screenDensity), Math.round(screenSize[1].toFloat() / screenDensity))

                    val smallerDeviceSize = AdSize(resources.getInteger(R.integer.adverts_interstitial_width), resources.getInteger(R.integer.adverts_interstitial_height))

                    advertView?.setAdSizes(deviceSize, smallerDeviceSize) //, AdSize.MEDIUM_RECTANGLE)

                    val adUnit = interstitial.values?.joinToString(separator = "/")

                    //Log.w(InterstitialFragment::class.simpleName, "AdUnit: $adUnit")

                    advertView?.adUnitId = "/$adUnit"

                    advertView?.adListener = object : AdListener() {

                        override fun onAdImpression() {

                        }

                        override fun onAdLeftApplication() {

                        }

                        override fun onAdClicked() {

                        }

                        override fun onAdFailedToLoad(p0: Int) {

                            Log.w(InterstitialFragment::class.simpleName, "Ad failed to load: $p0")

                            failedToLoadAd()

                        }

                        override fun onAdClosed() {

                        }

                        override fun onAdOpened() {

                        }

                        override fun onAdLoaded() {

                            progressSpinner?.post {

                                progressSpinner?.visibility = View.GONE

                            }

                        }
                    }

                    rootView?.post {

                        (rootView as? ViewGroup)?.addView(advertView)

                    }

                    requestAd()

                    return

                }

            }

        }

        failedToLoadAd()

    }

    private fun loadingAd() {

        progressSpinner?.post {

            progressSpinner?.visibility = View.VISIBLE

        }

        advertText?.post {

            advertText?.visibility = View.GONE

        }

    }

    private fun failedToLoadAd() {

        progressSpinner?.post {

            progressSpinner?.visibility = View.GONE

        }

        advertText?.post {

            advertText?.visibility = View.VISIBLE

        }

    }

    private fun requestAd() {

        rootView?.post {

            val adRequest = PublisherAdRequest.Builder()

            advertView?.loadAd(adRequest.build())

        }

    }

    override fun prepFontSize() {

    }

    override fun updateMargins() {

    }

    override fun onResume() {

        super.onResume()

        advertView?.resume()

    }

    override fun onPause() {

        advertView?.pause()

        super.onPause()

    }

    override fun onDestroy() {

        advertView?.destroy()

        super.onDestroy()


    }

}