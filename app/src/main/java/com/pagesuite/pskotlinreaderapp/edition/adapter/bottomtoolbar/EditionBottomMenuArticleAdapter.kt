package com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.pskotlinreaderapp.utils.TopCropTransformation
import com.pagesuite.pskotlinreaderapp.widget.svg.GlideApp

class EditionBottomMenuArticleAdapter(val context: Context, val mPages: Array<Page>, val section: Int, private val includeCovers: Boolean, private val itemClickListener: View.OnClickListener) : RecyclerView.Adapter<EditionBottomMenuArticleAdapter.Holder>() {

    var offSet = (if (includeCovers) 1 else 0)

    //val interstitialDrawable: Drawable = context.resources.getDrawable(R.drawable.page_advert_gray)
    //val puzzleDrawable: Drawable = context.resources.getDrawable(R.drawable.page_puzzle_gray)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        return (when (viewType) {

            0 -> {

                val layoutID = R.layout.edition_bottom_menu_article_cell
                val view = LayoutInflater.from(context).inflate(layoutID, parent, false)
                view.setOnClickListener(itemClickListener)
                ArticleHolder(view)

            }
            3 -> {

                val layoutID = R.layout.card_placeholder_interstitial
                val view = LayoutInflater.from(context).inflate(layoutID, parent, false)
                view.setOnClickListener(itemClickListener)
                InterstitialPlaceHolder(view)

            }
            2 -> {

                val layoutID = R.layout.card_placeholder_puzzle
                val view = LayoutInflater.from(context).inflate(layoutID, parent, false)
                view.setOnClickListener(itemClickListener)
                PuzzlePlaceHolder(view)

            }
            else -> {

                val layoutID = R.layout.edition_bottom_menu_cover_cell
                val view = LayoutInflater.from(context).inflate(layoutID, parent, false)
                view.setOnClickListener(itemClickListener)
                Holder(view)

            }

        })

    }


    override fun getItemViewType(position: Int): Int {

        val page = mPages[position]

        if (page.type?.equals(UsefulConstants.TYPE_INTERSTITIAL, ignoreCase = true) == true) {

            return 3

        } else if (page.type?.equals(UsefulConstants.TYPE_PUZZLE, ignoreCase = true) == true) {

            return 2

        } else {

            if (includeCovers && position == 0) {

                return 1

            }
        }

        return super.getItemViewType(position)

    }

    override fun getItemCount(): Int {

        return if (!includeCovers) {

            mPages.count()

        } else {

            mPages.count() + 1

        }

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.bindCategory(position)

    }

    override fun onViewRecycled(holder: Holder) {

        super.onViewRecycled(holder)

        Glide.with(context).clear(holder.headlineImage)

        holder.headlineImage.setImageBitmap(null)
        holder.headlineImage.setImageDrawable(null)
        holder.headlineImage.setImageResource(0)

    }

    open inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var headlineImage: ImageView = itemView.findViewById(R.id.articleImage)

        open fun bindCategory(index: Int) {

            val sectionData = DataStore.Edition?.sections?.getOrNull(section)

            if (sectionData != null) {

                Glide
                        .with(context)
                        .load("https://pbs.twimg.com/profile_images/580380183791857664/OtC0AMfJ.png") //section.coverImagePropety
                        .into(headlineImage)

                val sectionIndex = mPages.indexOfFirst { it.section == sectionData.name }
                itemView.setTag(R.id.tag_index, sectionIndex)

            }

        }

    }

    inner class InterstitialPlaceHolder(itemView: View) : Holder(itemView) {

        override fun bindCategory(index: Int) {

            headlineImage.setImageResource(R.drawable.ic_advert)

        }

    }

    inner class PuzzlePlaceHolder(itemView: View) : Holder(itemView) {

        override fun bindCategory(index: Int) {

            headlineImage.setImageResource(R.drawable.ic_puzzle)

        }

    }

    inner class ArticleHolder(itemView: View) : Holder(itemView) {

        override fun bindCategory(index: Int) {

            val position = index - offSet

            if (position >= 0 && position < mPages.size) {

                val page = mPages[position]

                val imageUrl: String? = page.screenshoturl

                if (!TextUtils.isEmpty(imageUrl)) {

                    //headlineImage.tag = imageUrl
                    //ImageHandler.getInstance().loadImage(headlineImage, imageUrl)

                    val glideRequestOptions = RequestOptions()
                    glideRequestOptions.placeholder(R.drawable.archiveplaceholder)

                    GlideApp
                            .with(context)
                            .setDefaultRequestOptions(glideRequestOptions)
                            .load(imageUrl)
                            .transform(TopCropTransformation())
                            .into(headlineImage)


                } else {

                    headlineImage.setImageResource(0)

                }

                itemView.setTag(R.id.tag_index, position)

            }

        }

    }

}