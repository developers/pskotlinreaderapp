package com.pagesuite.pskotlinreaderapp.edition.helper

import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.config.MenuConfig
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.edition.adapter.drawermenu.EditionDrawerArticleMenuAdapter
import com.pagesuite.pskotlinreaderapp.edition.adapter.drawermenu.EditionDrawerSectionMenuAdapter
import com.pagesuite.pskotlinreaderapp.extensions.slideIn
import com.pagesuite.pskotlinreaderapp.extensions.slideOut
import com.pagesuite.pskotlinreaderapp.models.ItemGroup
import com.pagesuite.utilities.DeviceUtils
import kotlinx.android.synthetic.main.activity_edition.*
import kotlinx.android.synthetic.main.view_edition_menu.*

class DrawerHelper(val editionActivity: EditionActivity) {

    private val drawerMenu = editionActivity.drawerMenu
    private val drawerSettingsMenus = editionActivity.drawerSettingsMenus
    private val drawerArticleMenus = editionActivity.drawerArticleMenus

    private val drawerContent = editionActivity.ll_main_drawer

    private var currentSectionIndex = 0

    private var afterClose: (() -> Unit)? = null

    init {

        drawerContent.setBackgroundColor(ColourConfig.Edition.NavigationDrawer.mBackgroundColour)

        drawerSettingsMenus.setBackgroundColor(ColourConfig.Edition.NavigationDrawer.MainMenu.mBackgroundColour)
        drawerArticleMenus.setBackgroundColor(ColourConfig.Edition.NavigationDrawer.ArticlesMenu.mBackgroundColour)

    }

    fun prepDrawerMenu(completionListener: (() -> Unit)? = null) {

        drawerMenu.addDrawerListener(object : DrawerLayout.DrawerListener {

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

                editionActivity.runOnUiThread { editionActivity.bottomMenuHelper?.onFontMenuCloseIfNeeded() }

            }

            override fun onDrawerOpened(drawerView: View) {}

            override fun onDrawerClosed(drawerView: View) {

                hideArticles(animationDuration = 0)

                afterClose?.invoke()

            }

            override fun onDrawerStateChanged(newState: Int) {}

        })

        val injectedMenuItems: ArrayList<ItemGroup> = arrayListOf()

        val currentMenuItems = MenuConfig.Edition.mItemGroups

        if (currentMenuItems != null) {

            injectedMenuItems.addAll(currentMenuItems)

        }

        drawerSettingsMenus.adapter = EditionDrawerSectionMenuAdapter(editionActivity, injectedMenuItems, View.OnClickListener {

            val index = it.getTag(R.id.tag_index)

            if (index is Int) {

                val type = it.getTag(R.id.tag_type)

                sectionSelected(index, type)

            }

        }, View.OnClickListener {

            val tag = it.getTag(R.id.tag_type)

            if (tag is String) {

                editionActivity.mApplication?.settingSelected(editionActivity, tag)

            }

        })

        val settingsLayoutManager = LinearLayoutManager(editionActivity)
        drawerSettingsMenus.layoutManager = settingsLayoutManager
        drawerSettingsMenus.setHasFixedSize(true)

        if (DeviceUtils.isPhone(editionActivity)) {

            hideArticles(completionListener = completionListener)

        } else {

            prepHeadlineDrawerMenu(0, completionListener = completionListener)

        }

    }

    private fun sectionSelected(index: Int, type: Any?) {

        prepHeadlineDrawerMenu(index)

        val section = DataStore.Edition?.sections?.getOrNull(index)?.getFirstPageInSection()

        if (section != null) {

            editionActivity.moveToPage(section, shouldUpdateArticles = false)

        }

        if (type == null || (type is String && !type.equals(UsefulConstants.TYPE_HOME, ignoreCase = true))) {

            showArticles()

        }

    }

    private fun prepHeadlineDrawerMenu(sectionIndex: Int, completionListener: (() -> Unit)? = null) {

        if (drawerArticleMenus != null) {

            if (sectionIndex < DataStore.Edition?.sections?.size ?: 0) {

                val section = DataStore.Edition?.sections?.getOrNull(sectionIndex)

                if (section != null) {

                    drawerArticleMenus.adapter = EditionDrawerArticleMenuAdapter(editionActivity, section, View.OnClickListener {

                        val articleGuid = it.getTag(R.id.article_guid)

                        if (articleGuid is String) {

                            afterClose = {

                                hideArticles(forceHide = true)

                                editionActivity.addArticlesFragment(articleGuid)

                                afterClose = null

                            }

                            closeDrawer()

                        }

                    })

                }

                val layoutManager = LinearLayoutManager(editionActivity)
                drawerArticleMenus.layoutManager = layoutManager
                drawerArticleMenus.setHasFixedSize(true)

                val decorationCount = drawerArticleMenus.itemDecorationCount

                if (decorationCount <= 1) {

                    val drawable = ContextCompat.getDrawable(editionActivity, R.drawable.vertical_divider)?.mutate()

                    if (drawable != null) {

                        drawable.setColorFilter(ColourConfig.Edition.NavigationDrawer.ArticlesMenu.mVerticalDividerColour, PorterDuff.Mode.SRC_ATOP)

                        val verticalDivider = DividerItemDecoration(editionActivity, DividerItemDecoration.VERTICAL)

                        verticalDivider.setDrawable(drawable)

                        drawerArticleMenus.addItemDecoration(verticalDivider)

                    }

                    drawerArticleMenus.addItemDecoration(DividerItemDecoration(editionActivity, DividerItemDecoration.HORIZONTAL))

                }

            }

        }

        completionListener?.invoke()

    }

    fun updateMenu() {

        if (currentSectionIndex != DataStore.Edition?.activeSection) {

            currentSectionIndex = DataStore.Edition?.activeSection ?: 0

            prepHeadlineDrawerMenu(currentSectionIndex)

        } else {

            drawerArticleMenus?.adapter?.notifyDataSetChanged()

        }

        drawerSettingsMenus?.adapter?.notifyDataSetChanged()

    }

    fun openDrawerMenu() {

        drawerMenu.openDrawer(Gravity.END)
        editionActivity.bottomMenuHelper?.onFontMenuCloseIfNeeded()
        editionActivity.scrubberHelper?.transitionFromScrubberToProgressBarIfNeeded()

    }

    fun disableDrawerMenuSwipeIn() {

        drawerMenu.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

    }

    fun enableDrawerMenuSwipeIn() {

        drawerMenu.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

    }

    private fun showArticles() {

        if (drawerMenu.isDrawerOpen(Gravity.END) && DeviceUtils.isPhone(editionActivity) && drawerArticleMenus.visibility != View.VISIBLE) {

            drawerArticleMenus.slideIn(editionActivity.animationDuration)

        }

    }

    private fun hideArticles(forceHide: Boolean = false, completionListener: (() -> Unit)? = null, animationDuration: Int = editionActivity.animationDuration) {

        if (DeviceUtils.isPhone(editionActivity) && (forceHide || drawerArticleMenus.visibility == View.VISIBLE)) {

            drawerArticleMenus.slideOut(duration = animationDuration, postHandler = completionListener)

        } else {

            completionListener?.invoke()

        }

    }

    fun onBackPressed(): Boolean {

        if (drawerMenu.isDrawerOpen(Gravity.END) && drawerArticleMenus.visibility == View.VISIBLE && DeviceUtils.isPhone(editionActivity)) {

            hideArticles()

            return true

        } else if (drawerMenu.isDrawerOpen(Gravity.END)) {

            closeDrawer()

            return true

        }

        return false

    }

    fun closeDrawer() {

        drawerMenu.closeDrawer(Gravity.END)

    }

}