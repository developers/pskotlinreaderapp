package com.pagesuite.pskotlinreaderapp.edition.helper.header

import android.text.SpannableString
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.config.HeaderConfig
import com.pagesuite.pskotlinreaderapp.edition.MenuSizeType
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.utils.Utils
import com.pagesuite.utilities.DeviceUtils

open class TypeBetaHeaderHelper(editionActivity: EditionActivity) : BasicEditionHeaderHelper(editionActivity) {

    init {

        visibilityType = View.INVISIBLE

        val logoParams: ViewGroup.LayoutParams = headerLogo.layoutParams

        if (logoParams is RelativeLayout.LayoutParams) {

            logoParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0)
            logoParams.addRule(RelativeLayout.CENTER_HORIZONTAL, 1)

            //logoParams.width = RelativeLayout.LayoutParams.MATCH_PARENT

            val margin = editionActivity.resources.getDimensionPixelSize(R.dimen.margin_eight)
            headerLogo.setPadding(0, margin, 0, margin)

            headerLogo.layoutParams = logoParams

        }

        val headerPagingParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0.0f)
        headerPaging.layoutParams = headerPagingParam

        val headerLogoParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        //headerLogoParam.leftMargin = basicActivity.resources.getDimensionPixelSize(R.dimen.margin_eight)
        headerLogoContainer.layoutParams = headerLogoParam

        //menuButton.visibility = visibilityType
        backButton.visibility = visibilityType
    }

    override fun loadEditionName() {

        headerDate.text = DataStore.Edition?.getEditionName()
        //headerDate.visibility = View.VISIBLE
        headerDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, editionActivity.resources.getDimension(R.dimen.edition_header_typeBeta_textSize))

    }


    override fun reduceTopMenuSize() {

        if (currentMenuType == MenuSizeType.MINIMIZED) return

        currentMenuType = MenuSizeType.MINIMIZED

        headerPaging.visibility = View.GONE
        //headerPaging.bringToFront()

        animateToolbar(getDecreasedMenuHeight(), 0.0f, 1.0f)

        //headerDate.visibility = View.GONE

        showMenuButton()

    }

    override fun increaseTopMenuSize(completionListener: (() -> Unit)?) {

        if (currentMenuType == MenuSizeType.EXPANDED) return

        currentMenuType = MenuSizeType.EXPANDED

        loadEditionName()

        //headerDate.visibility = View.VISIBLE

        var semaphoreCount = 1

        val listener = {

            semaphoreCount--

            if (semaphoreCount == 0) {

                completionListener?.invoke()

            }

        }

        //hideMenuButton(listener)

        if (!DeviceUtils.isPhone(editionActivity)) {

            animateToolbar(getIncreasedMenuHeight(), 1.0f, 0.0f, listener)

            /*val pagingLogoParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0.0f)
            headerPaging.layoutParams = pagingLogoParam

            val headerLogoParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 80.0f)
            headerLogoParam.leftMargin = basicActivity.resources.getDimensionPixelSize(R.dimen.margin_eight)
            headerLogoContainer.layoutParams = headerLogoParam*/

        } else {

            animateToolbar(getDecreasedMenuHeight(), 0.0f, 1.0f, listener)

        }

    }

    override fun animateToolbar(menuSize: Int, dateAlpha: Float, pagingAlpha: Float, completionListener: (() -> Unit)?) {

        Log.d(TypeBetaHeaderHelper::class.simpleName, "height to go to is ${R.dimen.edition_bottomToolbar_icon_width}")

        var semaphoreCount = 1

        val listener = {

            semaphoreCount--

            if (semaphoreCount == 0) {

                completionListener?.invoke()

            }

        }

        Utils.animateObjectHeight(toolbar, menuSize, listener)

        //headerDate.animate().alpha(dateAlpha).duration = 1000
        headerPaging.animate().alpha(pagingAlpha).duration = 1000

    }

    override fun updatePageNumber(display: SpannableString?, simpleDisplay: String?, sectionColour: Int) {

        headerDate.setTextColor(sectionColour)

        if (editionActivity.editionContent?.getCurrentPage() == 0 && !editionActivity.showingArticles) {

            loadEditionName()

        } else {

            if (display != null) {

                headerDate.text = display

            } else {

                headerDate.text = simpleDisplay

            }

        }

    }

    override fun getIncreasedMenuHeight(): Int {

        return if (!DeviceUtils.isPhone(basicActivity)) {

            /*val homeHeight: Float = DataStore.EditionApplication?.header?.homeheight ?: 0f

            return if (homeHeight != 0f) {

                Math.round(homeHeight * basicActivity.mScreenDensityScale)

            } else {

                basicActivity.resources.getDimensionPixelSize(R.dimen.toolbar_expanded_size)

            }*/

            HeaderConfig.Edition.Beta.mHomeHeight

        } else {

            getDecreasedMenuHeight()

        }

    }

    override fun getDecreasedMenuHeight(): Int {

        /*val normalHeight: Float = DataStore.EditionApplication?.header?.naturalheight ?: 0f

        return if (normalHeight != 0f) {

            Math.round(normalHeight * editionActivity.mScreenDensityScale)

        } else {

            editionActivity.resources.getDimensionPixelSize(R.dimen.toolbar_beta_size)

        }*/

        return HeaderConfig.Edition.Beta.mNormalHeight

    }

}