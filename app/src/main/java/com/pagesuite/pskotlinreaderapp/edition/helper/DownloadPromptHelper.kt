package com.pagesuite.pskotlinreaderapp.edition.helper

import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.extensions.slideDown
import com.pagesuite.pskotlinreaderapp.extensions.slideUp
import com.pagesuite.pskotlinreaderapp.utils.ColouringHelper
import kotlinx.android.synthetic.main.view_edition_downloadprompt.*

class DownloadPromptHelper(val editionActivity: EditionActivity) {

    private val rootView = editionActivity.downloadPrompt_rootView
    private val optionsContainer = editionActivity.downloadPrompt_optionsContainer
    private val optionsContainerVisible = editionActivity.downloadPrompt_optionsContainerVisible
    private val downloadIcon = editionActivity.downloadPrompt_downloadIcon
    private val downloadButton = editionActivity.downloadPrompt_button_download
    private val cancelButton = editionActivity.downloadPrompt_button_cancel
    private val title = editionActivity.downloadPrompt_title
    private val description = editionActivity.downloadPrompt_description

    init {

        rootView.setBackgroundColor(ColourConfig.Edition.DownloadPrompt.mBackgroundColour)

        title.setTextColor(ColourConfig.Edition.DownloadPrompt.Options.mForegroundColour)

        description.setTextColor(ColourConfig.Edition.DownloadPrompt.Options.mDescriptionForeground)

        downloadIcon.background?.setColorFilter(ColourConfig.Edition.DownloadPrompt.Options.mIconBackground, PorterDuff.Mode.SRC_ATOP)

        downloadIcon.setColorFilter(ColourConfig.Edition.DownloadPrompt.Options.mIconTint, PorterDuff.Mode.SRC_ATOP)

        downloadButton.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Edition.DownloadPrompt.Options.DownloadButton.mBackgroundColour,
                pressed = ColourConfig.Edition.DownloadPrompt.Options.DownloadButton.mPressedBackgroundColour,
                pressedBorder = ColourConfig.Edition.DownloadPrompt.Options.DownloadButton.mPressedBackgroundColour,
                normalBorder = ColourConfig.Edition.DownloadPrompt.Options.DownloadButton.mStrokeColour,
                borderWidth = ColourConfig.Edition.DownloadPrompt.Options.DownloadButton.mStrokeWidth)

        downloadButton.setTextColor(ColouringHelper.getSelector(normal = ColourConfig.Edition.DownloadPrompt.Options.DownloadButton.mForegroundColour, pressed = ColourConfig.Edition.DownloadPrompt.Options.DownloadButton.mPressedForegroundColour))

        cancelButton.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Edition.DownloadPrompt.Options.CancelButton.mBackgroundColour,
                pressed = ColourConfig.Edition.DownloadPrompt.Options.CancelButton.mPressedBackgroundColour,
                pressedBorder = ColourConfig.Edition.DownloadPrompt.Options.CancelButton.mPressedBackgroundColour,
                normalBorder = ColourConfig.Edition.DownloadPrompt.Options.CancelButton.mStrokeColour,
                borderWidth = ColourConfig.Edition.DownloadPrompt.Options.CancelButton.mStrokeWidth)

        cancelButton.setTextColor(ColouringHelper.getSelector(normal = ColourConfig.Edition.DownloadPrompt.Options.CancelButton.mForegroundColour, pressed = ColourConfig.Edition.DownloadPrompt.Options.CancelButton.mPressedForegroundColour))

        if (optionsContainerVisible.background is LayerDrawable) {

            val layerDrawable = optionsContainerVisible?.background as LayerDrawable

            val topLayer = layerDrawable.findDrawableByLayerId(R.id.topLayer)
            topLayer.setColorFilter(ColourConfig.Edition.DownloadPrompt.Options.mBackgroundTopLayer, PorterDuff.Mode.SRC_ATOP)

            val middleLayer = layerDrawable.findDrawableByLayerId(R.id.middleLayer)
            middleLayer.setColorFilter(ColourConfig.Edition.DownloadPrompt.Options.mBackgroundMiddleLayer, PorterDuff.Mode.SRC_ATOP)

            val bottomLayer = layerDrawable.findDrawableByLayerId(R.id.bottomLayer)
            bottomLayer.setColorFilter(ColourConfig.Edition.DownloadPrompt.Options.mBackgroundBottomLayer, PorterDuff.Mode.SRC_ATOP)

        }

        closePrompt()

    }

    fun isPromptOpen(): Boolean {

        return rootView.visibility == View.VISIBLE

    }

    fun closePrompt() {

        optionsContainer.slideDown(duration = editionActivity.animationDuration, outToBottom = true, postHandler = {

            rootView.visibility = View.GONE

        })

    }

    fun openPrompt(successListener: (() -> Unit)? = null) {

        rootView.visibility = View.VISIBLE

        optionsContainer.slideUp(duration = editionActivity.animationDuration, inFromBottom = true, postHandler = {

            successListener?.invoke()

        })

    }


}