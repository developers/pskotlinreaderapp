package com.pagesuite.pskotlinreaderapp.edition.fragment.page

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pagesuite.pskotlinreaderapp.activities.BasicActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.Queries
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.downloads.EditionDownloads
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.utilities.NetworkUtils


abstract class EditionBasicPageFragment : Fragment() {

    var pageData: Page? = null

    var editionCache: String? = null
    var mPageArticles: Array<Article>? = null
    var listener: ((articleGuid: String?) -> Unit)? = null
    var topMargin: Int = 0
    var bottomMargin: Int = 0
    var rootView: View? = null

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            if (intent.action == UsefulConstants.fontSizeChangeNotifications) {

                prepFontSize()

            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(getLayout(), container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rootView = view

    }

    abstract fun updateMargins()

    abstract fun getLayout(): kotlin.Int

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

        if ((DataStore.EditionApplication == null || DataStore.Edition == null) && NetworkUtils.isConnected(activity)) return

        if (isAdded) {

            updateMargins()

            val pageCache = DataStore.EditionApplication?.liveEditions?.firstOrNull()?.pageCache
            val cache: String? = pageCache?.get(pageData?.pageguid) ?: editionCache

            prepPage(cache)

            val broadcaster = UsefulConstants.pageUpdatedBroadcastID + pageData?.pageguid
            LocalBroadcastManager.getInstance(context as Context).registerReceiver(mMessageReceiver, IntentFilter(broadcaster))
            LocalBroadcastManager.getInstance(context as Context).registerReceiver(mMessageReceiver, IntentFilter(UsefulConstants.fontSizeChangeNotifications))

        }

    }

    open fun prepPage(cache: String?) {

        if (isAdded) {

            makePageQuery(pageData?.pageguid, cache)
            Log.d(EditionBasicPageFragment::class.simpleName, "THIS PAGE GUID IS ${pageData?.pageguid}")

        }

    }

    private fun makePageQuery(pageGuid: String?, editionCache: String?) {

        if (pageData != null && mPageArticles != null && mPageArticles?.isNotEmpty() == true) {

            pageData?.let {

                renderPage(it)

                return

            }

        } else {

            val edition = DataStore.Edition

            if (edition != null) {

                if (EditionDownloads.getInstance((activity as? BasicActivity)?.mApplication).isEditionDownloaded(edition)) {

                    val pages = edition.pages

                    if (pages != null) {

                        var articleGuid: String?
                        val pageArticles: Array<Article>?
                        var sectionArticles: Array<Article>?

                        val page = pages.firstOrNull { page -> page.pageguid == pageGuid }

                        if (page != null) {

                            if (page.articles?.isNotEmpty() == true) {

                                val sections = edition.sections

                                if (sections != null) {

                                    val articles = LinkedHashMap<String, Article>()

                                    pageArticles = page.articles

                                    if (pageArticles != null) {

                                        for (article in pageArticles) {

                                            articleGuid = article.articleguid

                                            if (articleGuid != null) {

                                                if (!articles.containsKey(articleGuid)) {

                                                    for (section in sections) {

                                                        sectionArticles = section.articles

                                                        if (sectionArticles != null) {

                                                            for (sectionArticle in sectionArticles) {

                                                                if (sectionArticle.articleguid == articleGuid) {

                                                                    articles[articleGuid] = sectionArticle

                                                                }

                                                            }

                                                        }

                                                    }

                                                }

                                            }

                                        }

                                    }

                                    mPageArticles = articles.values.toTypedArray()

                                    renderPage(page)

                                    return

                                }

                            }
                            // the page exists, but doesn't have any articles configured
                            // stop looping here

                        }

                    }

                } else {

                    Queries.getPage(context as Context, pageGuid, editionCache ?: "") {

                        if (isAdded) {

                            if (it != null) {

                                mPageArticles = it.articles

                                renderPage(it)

                                return@getPage

                            }

                        } else {

                            Log.w("Simon", "Not added: ${this.javaClass.simpleName}")

                        }

                        failedToLoad()

                    }

                }

            }

        }

        failedToLoad()

    }

    abstract fun renderPage(page: Page?)

    abstract fun failedToLoad()

    abstract fun prepFontSize()

    override fun onDestroy() {

        LocalBroadcastManager.getInstance(context as Context).unregisterReceiver(mMessageReceiver)
        super.onDestroy()

    }

}




