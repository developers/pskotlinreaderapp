package com.pagesuite.pskotlinreaderapp.edition.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.SparseArray
import android.view.ViewGroup
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.edition.fragment.page.*
import com.pagesuite.pskotlinreaderapp.models.Page

class EditionViewPagerAdaptor(fragmentManager: FragmentManager, val pages: Array<Page>, private var editionCache: String, private var isColumnView: Boolean = false) : FragmentStatePagerAdapter(fragmentManager) {

    val fragments: SparseArray<EditionBasicPageFragment> = SparseArray()
    var listener: ((articleGuid: String?) -> Unit)? = null
    var marginListener: ((position: Int) -> IntArray)? = null

    override fun getItem(index: Int): Fragment {

        val pageData = pages[index]

        val frags = (if (isColumnView && pageData.type == UsefulConstants.TYPE_ARTICLE) {

            EditionColumnPageFragment()

        } else if (pageData.type == UsefulConstants.TYPE_INTERSTITIAL) {

            InterstitialFragment()

        } else if (pageData.type == UsefulConstants.TYPE_PUZZLE) {

            WebViewFragment()

        } else {

            EditionPageFragment()

        })

        frags.pageData = pageData
        frags.editionCache = editionCache
        frags.listener = listener

        val margins = marginListener?.invoke(index)

        if (margins != null && margins.size > 1) {

            frags.topMargin = margins[0]
            frags.bottomMargin = margins[1]

        }

        fragments.put(index, frags)

        return frags

    }

    override fun getCount(): Int {

        return pages.count()

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        fragments.remove(position)
        super.destroyItem(container, position, `object`)

    }

}