package com.pagesuite.pskotlinreaderapp.edition.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.EditionBasicActivity
import com.pagesuite.pskotlinreaderapp.articles.SavedArticlesManager
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.Queries
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.downloads.DownloadStatus
import com.pagesuite.pskotlinreaderapp.downloads.EditionDownloads
import com.pagesuite.pskotlinreaderapp.edition.BottomMenuType
import com.pagesuite.pskotlinreaderapp.edition.EditionProgressBarState
import com.pagesuite.pskotlinreaderapp.edition.fragment.EditionContentFragment
import com.pagesuite.pskotlinreaderapp.edition.fragment.page.EditionBasicPageFragment
import com.pagesuite.pskotlinreaderapp.edition.fragment.page.EditionPageFragment
import com.pagesuite.pskotlinreaderapp.edition.fragment.pagination.ArticlesFragment
import com.pagesuite.pskotlinreaderapp.edition.fragment.pagination.SectionsFragment
import com.pagesuite.pskotlinreaderapp.edition.helper.BottomMenuHelper
import com.pagesuite.pskotlinreaderapp.edition.helper.DownloadPromptHelper
import com.pagesuite.pskotlinreaderapp.edition.helper.DrawerHelper
import com.pagesuite.pskotlinreaderapp.edition.helper.ScrubberHelper
import com.pagesuite.pskotlinreaderapp.edition.helper.header.TypeAlphaHeaderHelper
import com.pagesuite.pskotlinreaderapp.edition.helper.header.TypeBetaHeaderHelper
import com.pagesuite.pskotlinreaderapp.edition.shareSelectedArticle
import com.pagesuite.pskotlinreaderapp.extensions.slideDown
import com.pagesuite.pskotlinreaderapp.extensions.slideUp
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.Edition
import com.pagesuite.pskotlinreaderapp.models.IEditionContent
import com.pagesuite.pskotlinreaderapp.utils.Utils
import com.pagesuite.pskotlinreaderapp.widget.AbstractHeaderHelper
import com.pagesuite.readersdk.components.Listeners
import com.pagesuite.readersdk.objects.EditionStub
import com.pagesuite.readersdk.statics.ReaderManager
import com.pagesuite.utilities.DeviceUtils
import kotlinx.android.synthetic.main.activity_edition.*
import kotlinx.android.synthetic.main.view_edition_bottommenu.*
import kotlinx.android.synthetic.main.view_edition_toolbar.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.grantland.widget.AutofitHelper
import java.util.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.set

class EditionActivity : EditionBasicActivity() {

    var currentProgressBarState = EditionProgressBarState.PROGRESSBAR
    private var shouldAnimateProgressBar = true

    var currentBottomMenuType: BottomMenuType = BottomMenuType.NONE
    private var hasRun = false

    var editionArchiveFillsScreen: Boolean = true
    var sectionsFillsScreen: Boolean = true

    var expandedHeight: Int = 0

    //var editionViewPagerAdaptor: EditionViewPagerAdaptor? = null

    var scrubberHelper: ScrubberHelper? = null
    var drawerHelper: DrawerHelper? = null
    var bottomMenuHelper: BottomMenuHelper? = null
    var headerHelper: AbstractHeaderHelper? = null
    var downloadPromptHelper: DownloadPromptHelper? = null

    var showingArticles = false

    var editionContent: EditionContentFragment? = null
    var articlesContent: EditionContentFragment? = null

    var sempaphoreCount: Int = 0

    override fun getLayout(): Int {

        return R.layout.activity_edition

    }

    override fun initToolbar() {

        super.initToolbar()

        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        expandedHeight = resources.getDimensionPixelSize(R.dimen.bottomMenuContainer_height_expanded)

        addContentFragment()

        AutofitHelper.create(headerDate)

        //headerHelper = TypeAlphaHeaderHelper(this) //TypeBetaHeaderHelper(this)

        drawerHelper = DrawerHelper(this)

        bottomMenuHelper = BottomMenuHelper(this)

        scrubberHelper = ScrubberHelper(this)
        scrubberHelper?.initScrubberBarAppearance()

        menuButton.setOnClickListener { drawerHelper?.openDrawerMenu() }

        backButton.setOnClickListener { onBackPressed() }

        scrubberHelper?.updateProgressBar(false)

        /*ReplicaButton.setOnClickListener {

            mApplication?.loadReader(this)

        }*/

        downloadPromptHelper = DownloadPromptHelper(this)

        applyColourScheme()

        bottomMenuHelper?.hideFontButtons()

        bottomMenuHelper?.hideArticleOptions()

        hideDownloadProgress()

        //requestEdition()

    }

    override fun initColourScheme() {

        //super.initColourScheme()

        headerForegroundColour = ColourConfig.Edition.Toolbar.mForegroundColour
        headerBackgroundColour = ColourConfig.Edition.Toolbar.mBackgroundColour

    }

    fun applyColourScheme() {

        scrubberArrow.setColorFilter(ColourConfig.Edition.Scrubber.mScrubberArrow)
        loadingScreen.setBackgroundColor(ColourConfig.Edition.LoadingScreen.mBackgroundColour)

        loading_text.setTextColor(ColourConfig.Edition.LoadingScreen.mForegroundColour)

        val colours = IntArray(2)
        colours[0] = ColourConfig.Edition.LoadingScreen.mForegroundColour
        colours[1] = ColourConfig.Edition.LoadingScreen.mForegroundColour
        loadingIndicator.setColours(colours)

        edition_downloadProgress.setBackgroundColor(ColourConfig.Edition.Toolbar.mDownloadProgressBackground)
        edition_downloadProgress.progressDrawable?.setColorFilter(ColourConfig.Edition.Toolbar.mDownloadProgressForeground, PorterDuff.Mode.SRC_ATOP)

    }

    fun addContentFragment() {

        editionContent = SectionsFragment()

        editionContent?.marginListener = fun(position: Int): IntArray {

            val page = editionContent?.adapter?.pages?.get(position)

            val margins = IntArray(2)

            margins[0] = if (page?.isfullpage == true) {
                0
            } else {
                headerHelper?.getToolbarHeight(position) ?: 0
            }

            margins[1] = if (page?.isfullpage == true) {

                0

            } else {

                if (page?.type?.equals(UsefulConstants.TYPE_PUZZLE, ignoreCase = true) == true) {

                    0

                } else {

                    bottomMenuHelper?.getToolbarHeight() ?: 0

                }

            }

            return margins

        }

        editionContent?.listener = { articleGuid: String? ->

            addArticlesFragment(articleGuid)

        }

        editionContent?.pageListener = { position ->

            updateCurrentEditionPage(position)

        }

        editionContent?.let {

            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.editionViewPager, it)
            transaction.commit()

            handler?.postDelayed({

                requestEdition()

            }, 333)

        }

    }

    fun addArticlesFragment(articleGuid: String?) {

        bottomMenuHelper?.closeMenu(completionListener = {

            val flow = DataStore.Edition?.flow?.equals(UsefulConstants.FLOW_SECTION_TO_SECTION, ignoreCase = true) == true

            if (flow) {

                val alreadyShowing = showingArticles
                showingArticles = true

                bottomMenuHelper?.showToolbar({

                    bottomMenuHelper?.showArticleOptions()

                })

                articlesViewPager.post {

                    headerHelper?.reduceTopMenuSize()

                    scrubberHelper?.prepScrubberBar()

                    articlesViewPager.visibility = View.VISIBLE

                    headerHelper?.showBackButton()

                    var selectedPage = editionContent?.adapter?.pages?.indexOfFirst { page -> page.articles?.firstOrNull { article -> article.articleguid?.equals(articleGuid, ignoreCase = true) == true } != null }
                            ?: -1

                    if (selectedPage == 0) {

                        updateCurrentArticlePage(0)

                    }

                    if (articlesContent == null) {

                        articlesContent = ArticlesFragment()

                        articlesContent?.pageListener = { position: Int ->

                            updateCurrentArticlePage(position)

                        }

                        articlesContent?.marginListener = fun(position: Int): IntArray {

                            val margins = IntArray(2)
                            margins[0] = headerHelper?.getToolbarHeight(position) ?: 0

                            margins[1] = if (articlesContent?.adapter?.pages?.get(position)?.type?.equals(UsefulConstants.TYPE_PUZZLE, ignoreCase = true) == true) {

                                0

                            } else {

                                bottomMenuHelper?.getToolbarHeight() ?: 0

                            }

                            return margins

                        }
                    }

                    (articlesContent as? ArticlesFragment)?.selectedArticle = DataStore.Edition?.listOfArticles?.firstOrNull { article -> article.articleguid?.equals(articleGuid, ignoreCase = true) == true }

                    if (!alreadyShowing) {

                        val fragment = articlesContent

                        if (fragment is Fragment) {

                            supportFragmentManager.beginTransaction()
                                    .replace(R.id.articlesViewPager, fragment)
                                    .setCustomAnimations(R.anim.slide_up, 0, 0, R.anim.slide_down)
                                    .show(fragment)
                                    .commit()

                        }

                        handler?.post {

                            articlesContent?.prepViewPager()

                        }

                    } else {

                        val articleIndex = articlesContent?.adapter?.pages?.indexOfFirst { page -> page.articles?.firstOrNull { article -> article.articleguid?.equals(articleGuid, ignoreCase = true) == true } != null }
                                ?: -1

                        if (articleIndex != -1) {

                            handler?.post {

                                articlesContent?.moveToPage(articleIndex)

                            }

                        }

                    }

                    bottomMenuHelper?.closeMenu()

                }

            } else {

                var selectedPage = editionContent?.adapter?.pages?.indexOfFirst {

                    page ->

                    page.articles?.firstOrNull {

                        article ->

                        (article.articleguid?.equals(articleGuid, ignoreCase = true) == true) && (page.type?.equals(UsefulConstants.TYPE_ARTICLE, ignoreCase = true) == true)

                    } != null

                }
                        ?: -1

                if (selectedPage != -1) {

                    editionContent?.moveToPage(selectedPage)

                }

            }

        })

    }

    fun closeArticles() {

        showingArticles = false

        bottomMenuHelper?.hideArticleOptions()

        articlesContent?.let {

            updateCurrentEditionPage(editionContent?.getCurrentPage() ?: 0)

            scrubberHelper?.prepScrubberBar()

            supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_up, R.anim.slide_down, R.anim.slide_up, R.anim.slide_down).hide(it).commit()
            //it.clearContent()

            handler?.postDelayed({

                articlesContent?.viewPager?.visibility = View.INVISIBLE

            }, animationDuration.toLong())

        }

        if (mApplication?.isFromKiosk != true) {

            headerHelper?.hideBackButton()

        }

    }

    fun updateCurrentArticlePage(position: Int) {

        val articlePage = DataStore.Edition?.getArticlePages()?.getOrNull(position)

        if (articlePage != null) {

            if (articlePage.articles?.isNotEmpty() == true) {

                val pageArticle: Article? = articlePage.articles?.getOrNull(0)

                if (pageArticle != null) {

                    var selectedPage = editionContent?.adapter?.pages?.indexOfFirst { page -> page.articles?.firstOrNull { article -> article.articleguid?.equals(pageArticle.articleguid, ignoreCase = true) == true } != null }
                            ?: -1

                    if (selectedPage == -1) {

                        val sectionOfPage = DataStore.Edition?.getEditionPages()?.getOrNull(position)?.section
                                ?: ""

                        if (!sectionOfPage.equals(pageArticle.section, ignoreCase = true)) {

                            selectedPage = editionContent?.adapter?.pages?.indexOfFirst { page -> page.section?.equals(pageArticle.section, ignoreCase = true) == true }
                                    ?: -1

                        }

                    }

                    if (selectedPage != -1) {

                        editionContent?.setCurrentPage(selectedPage)

                    }

                }

            } else {

                updateXofY("", 0, 0)

            }

            val pagePosition = (DataStore.Edition?.getPagePosition(articlePage, articlePage.section, articlesContent?.adapter?.pages)
                    ?: 0) + 1
            val pageCount = DataStore.Edition?.getPageCount(articlePage.section, articlesContent?.adapter?.pages)
                    ?: 0

            updateXofY(articlePage.section ?: "", pagePosition, pageCount)

        }

    }

    fun updateCurrentEditionPage(position: Int, completionListener: (() -> Unit)? = null) {

        if (position == 0 && !showingArticles) {

            headerHelper?.increaseTopMenuSize()

        } else {

            headerHelper?.reduceTopMenuSize()

        }

        val sectionOfPage = DataStore.Edition?.getEditionPages()?.getOrNull(position)?.section ?: ""

        val page = editionContent?.adapter?.pages?.get(editionContent?.getCurrentPage() ?: 0)

        val pagePosition = (DataStore.Edition?.getPagePosition(page, sectionOfPage) ?: 0) + 1
        val pageCount = DataStore.Edition?.getPageCount(sectionOfPage) ?: 0

        DataStore.Edition?.activeSection = DataStore.Edition?.sections?.indexOfFirst { it.name.equals(sectionOfPage, true) } ?: 0

        if (!showingArticles) {

            updateXofY(sectionOfPage + " " + getString(R.string.header_section), pagePosition, pageCount)

            if (page?.isfullpage == true) {

                bottomMenuHelper?.hideToolbar()

                headerHelper?.hideToolbar()

            } else {

                if (mApplication?.isFromKiosk == true) {

                    headerHelper?.showBackButton()

                }

                if (mApplication?.isFromKiosk != true) {

                    headerHelper?.hideBackButton()

                }

                bottomMenuHelper?.showToolbar()

            }

        }

        scrubberHelper?.transitionFromScrubberToProgressBarIfNeeded()
        scrubberHelper?.updateProgressBar(shouldAnimateProgressBar)

        bottomMenuHelper?.onFontMenuCloseIfNeeded()

        shouldAnimateProgressBar = true

        drawerHelper?.updateMenu()

        completionListener?.invoke()

    }

    fun getCurrentPage(): Int {

        return if (showingArticles) {

            articlesContent?.getCurrentPage() ?: 0

        } else {

            editionContent?.getCurrentPage() ?: 0

        }

    }

    fun getCurrentPageType(): String? {

        return if (showingArticles) {

            articlesContent?.adapter?.pages?.get(articlesContent?.getCurrentPage() ?: 0)?.type

        } else {

            editionContent?.adapter?.pages?.get(editionContent?.getCurrentPage() ?: 0)?.type

        }

    }

    fun updateXofY(sectionName: String, pagePosition: Int, pageCount: Int) {

        val pageType = getCurrentPageType()

        if (pageType != null) {

            if (pageType.equals(UsefulConstants.TYPE_INTERSTITIAL, ignoreCase = true) == true) {

                headerHelper?.updatePageNumber(simpleDisplay = "")

                hideToolbars()

            } else {

                val flow = DataStore.Edition?.flow?.equals(UsefulConstants.FLOW_SECTION_TO_SECTION, ignoreCase = true) == true

                val stringBuilder = StringBuilder(sectionName)
                if (DeviceUtils.isPhone(applicationContext) && !flow) {

                    stringBuilder.append("\r\n")

                } else {

                    stringBuilder.append(" ")

                }
                stringBuilder.append("$pagePosition ")
                stringBuilder.append(getString(R.string.of))
                stringBuilder.append(" $pageCount")

                /*val str = SpannableString(sectionName + (if (DeviceUtils.isPhone(applicationContext) && flow) {
                "\r\n"
            } else " ") +
                    "$pagePosition " + getString(R.string.of) + " $pageCount")*/

                val str = SpannableString(stringBuilder.toString())

                //str.setSpan(StyleSpan(Typeface.BOLD), 0, sectionName.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

                var sectionColour = Color.BLACK

                if (!flow) {

                    val sectionNameOnly = sectionName.replace(" " + getString(R.string.header_section), "")

                    var sectionColourStr = DataStore.Edition?.sections?.firstOrNull { section -> section.name?.equals(sectionNameOnly, ignoreCase = true) == true }?.color

                    sectionColourStr = (if (sectionColourStr?.contains(",") == true) {

                        "rgb(${sectionColourStr})"

                    } else {

                        sectionColourStr ?: ""

                    })

                    if (!TextUtils.isEmpty(sectionColourStr)) {

                        sectionColour = Utils.convertRgbToColour(sectionColourStr)

                    }

                }

                headerHelper?.updatePageNumber(str, sectionColour = sectionColour)

                if (pageType.equals(UsefulConstants.TYPE_ARTICLE, ignoreCase = true) == true) {

                    bottomMenuHelper?.showArticleOptions()

                } else {

                    bottomMenuHelper?.hideArticleOptions()

                }

                showToolbars()

            }

        }

    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            // Get extra data included in the Intent

            val type = intent.getStringExtra(UsefulConstants.ARGS_TYPE)

            if (type == UsefulConstants.BROADCAST_UPDATED_ARTICLE) {

                Log.d(EditionActivity::class.simpleName, "RECIEVED IN THE EDITION VIEW")
                finish()
                startActivity(getIntent())

            } else if (type == UsefulConstants.BROADCAST_UPDATED_EDITION) {

                onEditionMutation()

            }

        }

    }

    private val mDownloadMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            val editionGuid = intent.getStringExtra(UsefulConstants.EDITION_GUID)

            if (editionGuid != null) {

                bottomMenuHelper?.editionsHelper?.updateEdition(editionGuid, intent)

                if (editionGuid.equals(DataStore.Edition?.getEditionGuid(), ignoreCase = true)) {

                    val type = intent.getStringExtra(UsefulConstants.ARGS_TYPE)

                    when (type) {

                        UsefulConstants.BROADCAST_DOWNLOAD_COMPLETED -> {

                            hideDownloadProgress()

                        }
                        UsefulConstants.BROADCAST_DOWNLOAD_FAILED -> {

                            hideDownloadProgress()

                        }
                        UsefulConstants.BROADCAST_DOWNLOAD_PROGRESS -> {

                            val progress = intent.getIntExtra(UsefulConstants.ARGS_PROGRESS, 0)
                            edition_downloadProgress.progress = progress

                        }
                        UsefulConstants.BROADCAST_DOWNLOAD_STARTED -> {

                            edition_downloadProgress.isIndeterminate = false
                            showDownloadProgress()

                        }
                        UsefulConstants.BROADCAST_DOWNLOAD_QUEUED -> {

                            edition_downloadProgress.isIndeterminate = true
                            showDownloadProgress()

                        }

                    }

                } else {

                    val currentEdition = DataStore.Edition as? IEditionContent

                    if (currentEdition != null) {

                        if (EditionDownloads.getInstance(mApplication).getDownloadStatus(currentEdition) == null) {

                            hideDownloadProgress()

                        }

                    }

                }

            }

        }

    }

    fun hideDownloadProgress(listener: (() -> Unit)? = null) {

        if (edition_downloadProgress.visibility != View.GONE) {

            edition_downloadProgress?.slideDown(animationDuration, outToBottom = false, postHandler = {

                edition_downloadProgress?.progress = 0
                listener?.invoke()

            })

        } else {

            listener?.invoke()

        }

    }

    fun showDownloadProgress(listener: (() -> Unit)? = null) {

        if (edition_downloadProgress.visibility != View.VISIBLE) {

            edition_downloadProgress.slideUp(animationDuration, inFromBottom = false, postHandler = listener)

        } else {

            listener?.invoke()

        }

    }

    ///////////MARKER: QUERIES AND MUTATION///////////


    fun onEditionMutation() {

        val editionRefreshAlert: AlertDialog.Builder = AlertDialog.Builder(this)
        editionRefreshAlert.setMessage(R.string.dialog_edition_changed)
        editionRefreshAlert.setPositiveButton(R.string.refresh) { _, _ ->

            reloadEditionOnChange()
            Log.d(EditionActivity::class.simpleName, "TAPPED REFRESH")

        }

        editionRefreshAlert.setNegativeButton(R.string.str_cancel) { _, _ ->

            editionRefreshAlert.setCancelable(true)

        }

        editionRefreshAlert.setTitle(R.string.alert)
        editionRefreshAlert.show()

    }

    private fun requestEdition() {

        showLoadingScreen()

        val editionGUID = DataStore.Edition?.editionguid
        val editionCache: String? = DataStore.Edition?.cache

        DataStore.Edition?.pageCache = HashMap()

        if (editionGUID != null && editionCache != null) {

            LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, IntentFilter(UsefulConstants.editionUpdatedBroadcastID + editionGUID))

            LocalBroadcastManager.getInstance(this).registerReceiver(mDownloadMessageReceiver, IntentFilter(UsefulConstants.fileDownloadBroadcastID))

            Queries.subscribeToEditionChange(this, editionGUID)

            downloadReplicaEditions(completionListener = {

                Queries.getEdition(this, editionGUID, editionCache) {

                    if (it != null) {

                        loadEdition(editionGUID)

                    }

                }

            })

        }

    }

    fun loadEdition(editionGUID: String) {

        runOnUiThread {

            val flow = DataStore.Edition?.flow?.equals(UsefulConstants.FLOW_COVER_TO_COVER, ignoreCase = true) == true

            headerHelper = (if (flow) {

                TypeAlphaHeaderHelper(this)

            } else {

                TypeBetaHeaderHelper(this)

            })

            bottomMenuHelper?.hideArticleOptions()

            /*if (flow) {

                bottomMenuHelper?.showArticleOptions()

            }*/

            Queries.newSubscribeToPageChange(this, editionGUID)

            sempaphoreCount = 8

            val listener = {

                updateSemaphoreCount()

            }

            headerHelper?.prepHeader(listener)

            prepViewPager(listener)

            scrubberHelper?.updateProgressBar(false)

            drawerHelper?.prepDrawerMenu(listener)

            bottomMenuHelper?.prepBottomMenuArchive(listener)

            scrubberHelper?.prepScrubberBar(listener)

            headerHelper?.hideBackButton(listener)

            updateCurrentEditionPage(0, completionListener = listener)

            val status = EditionDownloads.getInstance(mApplication).getDownloadStatus(DataStore.Edition as IEditionContent)

            if (status != null) {

                when (status) {

                    DownloadStatus.ADDED,
                    DownloadStatus.STARTED,
                    DownloadStatus.QUEUED,
                    DownloadStatus.IN_PROGRESS -> showDownloadProgress(listener)
                    else -> {
                        hideDownloadProgress(listener)
                    }

                }

            } else {

                downloadPromptHelper?.openPrompt(listener)

            }

            //mApplication?.inAppManager?.purchaseInAppItem(this, "android.test.purchased")

        }

    }

    fun updateSemaphoreCount() {

        sempaphoreCount--

        if (sempaphoreCount == 0) {

            hideLoadingScreen()

        }

    }

    override fun changeEdition() {

        mApplication?.reloadEdition(this)

    }

    override fun alreadyLoadedEdition() {

        bottomMenuHelper?.editionsHelper?.closeMenu()
        bottomMenuHelper?.closeMenu()

    }

    private fun reloadEditionOnChange() {

        mApplication?.reloadEdition(this)

    }

    /////////////MARKER: Loading screen hide and show////////////

    private fun showLoadingScreen() {

        loadingIndicator.start()
        loadingScreen.visibility = View.VISIBLE

    }

    private fun hideLoadingScreen() {

        loadingScreen.visibility = View.GONE
        loadingIndicator.stop()
    }

    /////////////MARKER: Edition View Pager////////////

    private fun prepViewPager(listener: (() -> Unit)? = null) {

        editionContent?.prepViewPager(listener)

    }

    @Suppress("UNUSED_PARAMETER")
    fun moveToSettings(view: View) {

        drawerHelper?.closeDrawer()

        mApplication?.moveToSettings(this)

    }

    @Suppress("UNUSED_PARAMETER")
    fun clickedFavourites(view: View) {

        var targetContent = if (!showingArticles) {

            editionContent

        } else {

            articlesContent

        }

        if (targetContent != null) {

            if (targetContent.adapter != null) {

                val currentPageFragment: Fragment? = targetContent.adapter?.fragments?.get(targetContent.getCurrentPage())

                if (currentPageFragment is EditionBasicPageFragment) {

                    val selectedArticle: Article? = currentPageFragment.mPageArticles?.getOrNull(0)

                    if (selectedArticle != null) {

                        GlobalScope.launch {

                            val pageData = currentPageFragment.pageData

                            if (pageData != null) {

                                val template = if (currentPageFragment is EditionPageFragment) {
                                    currentPageFragment.templateContent
                                } else {
                                    null
                                }

                                val success = SavedArticlesManager.saveArticle(editionViewPager.context, selectedArticle, DataStore.Edition?.editionguid
                                        ?: "", pageData, template)

                                runOnUiThread {

                                    if (success) {

                                        Toast.makeText(editionViewPager.context, R.string.toast_savedArticles_success, Toast.LENGTH_SHORT).show()

                                    } else {

                                        Toast.makeText(editionViewPager.context, R.string.toast_savedArticles_failed, Toast.LENGTH_SHORT).show()

                                    }

                                }

                            }

                        }

                    }

                }

            }

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun shareArticle(view: View) {

        var targetContent = if (!showingArticles) {

            editionContent

        } else {

            articlesContent

        }

        if (targetContent != null) {

            if (targetContent.adapter != null) {

                val currentPageFragment: Fragment? = targetContent.adapter?.fragments?.get(targetContent.getCurrentPage())

                if (currentPageFragment != null && currentPageFragment is EditionBasicPageFragment) {

                    val selectedArticle: Article? = currentPageFragment.mPageArticles?.getOrNull(0)

                    if (selectedArticle != null) {

                        val articleGuid = selectedArticle.articleguid

                        if (articleGuid != null) {

                            shareSelectedArticle(this, selectedArticle)

                        }

                    }

                }

            }

        }

    }

    /////////MARKER - BOTTOM MENU FONT MENU/////////

    fun fontButtonTapped(view: View) {

        bottomMenuHelper?.fontButtonTapped(view)

    }

    fun onBottomMenuSectionButtonPushed(view: View) {

        val buttonTag = view.tag

        if (buttonTag is String) {

            bottomMenuHelper?.articlesHelper?.prepBottomMenuArticles(buttonTag.toInt())

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun onFilterTapped(view: View) {

        bottomMenuHelper?.closeMenu()
        scrubberHelper?.transitionFromScrubberToProgressBarIfNeeded()

    }

    @Suppress("UNUSED_PARAMETER")
    fun onOpenEditionMenuIfNeeded(view: View) {

        bottomMenuHelper?.openEditionsMenu()

    }

    @Suppress("UNUSED_PARAMETER")
    fun onOpenArticleMenuIfNeeded(view: View) {

        bottomMenuHelper?.openArticlesMenu()

    }

    @Suppress("UNUSED_PARAMETER")
    fun onEditionGroupButtonClicked(view: View) {

        bottomMenuHelper?.editionsHelper?.toggleEditionGroups()

    }

    @Suppress("UNUSED_PARAMETER")
    fun onDownloadAllEditionsClicked(view: View) {


    }

    fun onEditionGroupClicked(view: View) {

        val tag = view.getTag(R.id.tag_index)

        if (tag is Int) {

            bottomMenuHelper?.editionsHelper?.setSelectedEditionGroup(tag)

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun onCloseButtonClicked(view: View) {

        //onBackPressed()

        bottomMenuHelper?.closeMenu(true)

    }

    @Suppress("UNUSED_PARAMETER")
    fun onEditionGroupZoomClicked(view: View) {

        bottomMenuHelper?.editionsHelper?.toggleEditionGroupZoom()

    }

    @Suppress("UNUSED_PARAMETER")
    fun onLogoClicked(view: View) {

        moveToPage(0)

    }

    @Suppress("UNUSED_PARAMETER")
    fun downloadPromptClicked(view: View) {

        val currentEdition = DataStore.Edition

        if (currentEdition != null) {

            EditionDownloads.getInstance(mApplication).downloadEdition(mApplication as Context, currentEdition)

            downloadPromptHelper?.closePrompt()

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun cancelPromptClicked(view: View) {

        downloadPromptHelper?.closePrompt()

    }

    fun moveToPage(pageNumber: Int, animateProgressBar: Boolean = true, shouldUpdateArticles: Boolean = true) {

        runOnUiThread {

            shouldAnimateProgressBar = animateProgressBar
            //editionViewPager.setCurrentItem(pageNumber, false)

            if (showingArticles && shouldUpdateArticles) {

                articlesContent?.moveToPage(pageNumber)

            } else {

                editionContent?.moveToPage(pageNumber)

            }
            //onPageSelected(pageNumber)

        }

    }

    fun downloadEdition(view: View) {

        val tag = view.getTag(R.id.tag_edition)

        if (tag is Edition) {

            val editionDownloads = EditionDownloads.getInstance(mApplication)

            if (editionDownloads.getDownloadStatus(tag) == null) {

                editionDownloads.downloadEdition(this, tag)

            } else {

                editionDownloads.cancelDownload(tag)

            }

        }

    }

    @Suppress("UNUSED_PARAMETER")
    fun scrubberBarrierClicked(view: View) {

        scrubberBarrier.visibility = View.GONE
        scrubberHelper?.transitionFromScrubberToProgressBarIfNeeded()

    }

    @Suppress("UNUSED_PARAMETER")
    fun onProgressBarTapped(view: View) {

        scrubberHelper?.transitionFromProgressBarToScrubberIfNeeded()

    }

    fun hideToolbars() {

        headerHelper?.hideToolbar()

        bottomMenuHelper?.hideToolbar()

    }

    fun showToolbars() {

        headerHelper?.showToolbar()

        //if (editionContent?.getCurrentPage() != 0) {

        val currentPageType = getCurrentPageType()

        if (currentPageType != null) {

            if (currentPageType.equals(UsefulConstants.TYPE_PUZZLE, ignoreCase = true) == false) {

                bottomMenuHelper?.showToolbar()

            } else {

                bottomMenuHelper?.hideToolbar()

            }

        }

        //}

    }

    private fun downloadReplicaEditions(completionListener: (() -> Unit)? = null) {

        val pubGuid = DataStore.EditionApplication?.replica?.pubguid

        if (pubGuid != null && !TextUtils.isEmpty(pubGuid)) {

            ReaderManager.getInstance().mEditionsDownloadlisteners[pubGuid] = object : Listeners.EditionsDownloadListener {

                override fun editionsDownloaded(editions: ArrayList<EditionStub>?) {

                    ReaderManager.getInstance().mEditionsDownloadlisteners.remove(pubGuid)

                    val rootImageUrl = getString(R.string.urls_getEditionImage)

                    DataStore.EditionApplication?.replica?.populatedEditionsFromStubs(rootImageUrl, editions)

                    completionListener?.invoke()

                }

                override fun editionsFailed() {

                    ReaderManager.getInstance().mEditionsDownloadlisteners.remove(pubGuid)

                    completionListener?.invoke()

                }

            }

            ReaderManager.getInstance().downloadEditions(pubGuid)

        } else {

            completionListener?.invoke()

        }

    }

    //////////MARKER: Activity Logic////////

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == AppCompatActivity.RESULT_OK) {

            if (requestCode == UsefulConstants.REQUEST_ARTICLE_JUMP) {

                val articleGuid = data?.extras?.getString(UsefulConstants.ARTICLE_GUID)

                drawerHelper?.closeDrawer()

                handler?.postDelayed({

                    addArticlesFragment(articleGuid)

                }, animationDuration.toLong())


            }

        }

    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)

        if (currentBottomMenuType == BottomMenuType.ARCHIVE) {

            bottomMenuEditionRecycler.adapter?.notifyDataSetChanged()

        }

    }

    override fun onBackPressed() {

        if (drawerHelper?.onBackPressed() == true) {

            return

        } else if (currentBottomMenuType != BottomMenuType.NONE) {

            bottomMenuHelper?.closeMenu()

            return

        } else if (currentProgressBarState == EditionProgressBarState.SCRUBBER) {

            scrubberHelper?.transitionFromScrubberToProgressBarIfNeeded()

            return

        } else if (showingArticles) {

            closeArticles()

            return

        } else if (downloadPromptHelper?.isPromptOpen() == true) {

            downloadPromptHelper?.closePrompt()

            return

        }

        super.onBackPressed()

    }

    override fun onDestroy() {

        if (mApplication?.isFromKiosk == true) {

            DataStore.Edition = null
            DataStore.EditionApplication?.activeEdition = ""

        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDownloadMessageReceiver)

        super.onDestroy()

    }

}
