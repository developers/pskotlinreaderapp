package com.pagesuite.pskotlinreaderapp.edition.helper

import android.graphics.PorterDuff
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.BottomMenuType
import com.pagesuite.pskotlinreaderapp.edition.EditionViewFontStates
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.edition.helper.bottommenu.ArticlesHelper
import com.pagesuite.pskotlinreaderapp.edition.helper.bottommenu.EditionsHelper
import com.pagesuite.pskotlinreaderapp.extensions.slideDown
import com.pagesuite.pskotlinreaderapp.extensions.slideIn
import com.pagesuite.pskotlinreaderapp.extensions.slideOut
import com.pagesuite.pskotlinreaderapp.extensions.slideUp
import com.pagesuite.pskotlinreaderapp.utils.Utils
import kotlinx.android.synthetic.main.activity_edition.*
import kotlinx.android.synthetic.main.view_edition_bottommenu.*
import kotlinx.android.synthetic.main.view_edition_bottomtoolbar.*

class BottomMenuHelper(private val editionActivity: EditionActivity) {

    val editionsHelper: EditionsHelper = EditionsHelper(editionActivity, editionActivity.expandedHeight, editionActivity.editionArchiveFillsScreen)
    val articlesHelper: ArticlesHelper = ArticlesHelper(editionActivity)

    private val progressBarBuffer = editionActivity.progressBarBuffer

    private val backgroundFilter = editionActivity.backgroundFilter

    private val fontButtonLarge: View = editionActivity.FontButtonLarge
    private val fontButtonMedium: View = editionActivity.FontButtonMedium
    private val fontButtonSmall: View = editionActivity.FontButtonSmall

    private val saveArticleButton: View = editionActivity.FavouriteButton
    private val shareArticleButton: View = editionActivity.ShareButton

    //private val replicaButton: View = basicActivity.ReplicaButton
    private val progressBarBase: View = editionActivity.ProgressBarBase

    private val editionsButton: View = editionActivity.EditionsButton
    private val sectionsButton: View = editionActivity.SectionsButton

    private val bottomToolbar: ViewGroup = editionActivity.bottomToolbar

    private val articleOptionsContainer = editionActivity.articleOptionsContainer

    private var fontCurrentState: EditionViewFontStates = EditionViewFontStates.CLOSE

    init {

        bottomToolbar.setBackgroundColor(ColourConfig.Edition.BottomToolbar.mBackgroundColour)

        applyColourScheme()

    }

    private fun prepareForBottomMenu() {

        onFontMenuCloseIfNeeded()

        editionActivity.scrubberHelper?.transitionFromScrubberToProgressBarIfNeeded()
        editionActivity.drawerHelper?.disableDrawerMenuSwipeIn()

        hideArticleOptions()

    }

    private fun applyColourScheme() {

        if (fontButtonLarge is ImageView) {

            fontButtonLarge.setColorFilter(ColourConfig.Edition.BottomToolbar.mForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

        if (fontButtonMedium is ImageView) {

            fontButtonMedium.setColorFilter(ColourConfig.Edition.BottomToolbar.mForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

        if (fontButtonSmall is ImageView) {

            fontButtonSmall.setColorFilter(ColourConfig.Edition.BottomToolbar.mForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

        if (saveArticleButton is ImageView) {

            saveArticleButton.setColorFilter(ColourConfig.Edition.BottomToolbar.mForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

        if (shareArticleButton is ImageView) {

            shareArticleButton.setColorFilter(ColourConfig.Edition.BottomToolbar.mForegroundColour, PorterDuff.Mode.SRC_ATOP)

        }

        if (editionsButton is TextView) {

            editionsButton.setTextColor(ColourConfig.Edition.BottomToolbar.mForegroundColour)

        }

        if (sectionsButton is TextView) {

            sectionsButton.setTextColor(ColourConfig.Edition.BottomToolbar.mForegroundColour)

        }

    }

    fun fontButtonTapped(view: View) {

        Log.d(BottomMenuHelper::class.simpleName, "THIS TAG IS ${view.tag}")

        if (fontCurrentState == EditionViewFontStates.OPEN) {

            when (view.tag) {

                UsefulConstants.TEXT_SIZE_NORMAL -> onChangeFontSize(Utils.FontSizes.NORMAL)
                UsefulConstants.TEXT_SIZE_LARGE -> onChangeFontSize(Utils.FontSizes.LARGE)
                UsefulConstants.TEXT_SIZE_LARGER -> onChangeFontSize(Utils.FontSizes.LARGER)
                else -> return

            }

            onFontMenuCloseIfNeeded()

        } else {

            articlesHelper.onCloseArticleMenuIfNeeded()
            editionActivity.drawerHelper?.closeDrawer()
            editionsHelper.onCloseEditionMenuIfNeeded()
            editionActivity.scrubberHelper?.transitionFromScrubberToProgressBarIfNeeded()
            onFontMenuOpen()

        }

    }


    private fun onChangeFontSize(fontSize: Utils.FontSizes) {

        UsefulConstants.setFontSize(fontSize, editionActivity)

    }

    private fun onFontMenuOpen() {

        if (fontCurrentState == EditionViewFontStates.OPEN) return

        fontCurrentState = EditionViewFontStates.OPEN

        /*SettingsButton.visibility = View.GONE
        ShareButton.visibility = View.GONE
        FavouriteButton.visibility = View.GONE*/

        fontButtonMedium.slideIn(postHandler = {

            //replicaButton.visibility = (if (basicActivity.resources.getBoolean(R.bool.edition_hideReplicaButton)) View.GONE else View.VISIBLE)

            fontButtonSmall.slideIn()

        })

    }

    fun onFontMenuCloseIfNeeded() {

        if (fontCurrentState == EditionViewFontStates.CLOSE) return

        fontCurrentState = EditionViewFontStates.CLOSE

        /*SettingsButton.visibility = View.VISIBLE
        ShareButton.visibility = View.VISIBLE
        FavouriteButton.visibility = View.VISIBLE*/

        hideFontButtons()

    }

    fun hideFontButtons() {

        fontButtonSmall.slideOut(postHandler = {

            fontButtonMedium.slideOut(postHandler = {

                /*if (DataStore.Application?.replica != null) {

                    replicaButton.visibility = View.VISIBLE

                } else {

                    replicaButton.visibility = View.GONE

                }*/

            })

        })

    }

    fun openArticlesMenu() {

        if (editionActivity.currentBottomMenuType == BottomMenuType.ARTICLES) {

            articlesHelper.onCloseArticleMenuIfNeeded()
            closeMenu()
            return

        }

        prepareForBottomMenu()

        editionActivity.currentBottomMenuType = BottomMenuType.ARTICLES

        //bottomArticleMenuContainer.visibility = View.VISIBLE

        progressBarBuffer.visibility = View.GONE

        if (DataStore.Edition != null) {

            articlesHelper.prepBottomMenuForSections()

            articlesHelper.showMenu { editionsHelper.onCloseEditionMenuIfNeeded() }

            editionActivity.headerHelper?.showSimpleToolbar()

        }

    }

    fun openEditionsMenu() {

        if (editionActivity.currentBottomMenuType == BottomMenuType.ARCHIVE) {

            editionsHelper.onCloseEditionMenuIfNeeded()
            closeMenu()
            return

        }

        prepareForBottomMenu()

        editionActivity.currentBottomMenuType = BottomMenuType.ARCHIVE

        //bottomEditionMenuContainer.visibility = View.VISIBLE

        progressBarBuffer.visibility = View.GONE

        editionsHelper.openMenu {

            articlesHelper.onCloseArticleMenuIfNeeded()

        }

        editionActivity.headerHelper?.showSimpleToolbar()

    }

    fun prepBottomMenuArchive(completionListener: (() -> Unit)? = null) {

        //editionsHelper.prepareArchive()
        editionsHelper.downloadArchive()

        var semaphoreCount = 2
        val listener = {

            semaphoreCount--

            if (semaphoreCount == 0) {

                completionListener?.invoke()

            }

        }

        articlesHelper.onCloseArticleMenuIfNeeded(listener)
        editionsHelper.onCloseEditionMenuIfNeeded(listener)

    }

    fun closeMenu(isFromCloseButton: Boolean = false, completionListener: (() -> Unit)? = null): Boolean {

        val closedEditionsMenu = editionsHelper.closeMenu()

        if (closedEditionsMenu && !isFromCloseButton) {

            return true

        }

        var semaphoreCount = 3

        val listener = {

            semaphoreCount--

            if (semaphoreCount == 0) {

                completionListener?.invoke()

            }

        }

        editionsHelper.onCloseEditionMenuIfNeeded(listener)
        articlesHelper.onCloseArticleMenuIfNeeded(listener)

        editionActivity.currentBottomMenuType = BottomMenuType.NONE

        //bottomArticleMenuContainer.visibility = View.INVISIBLE

        progressBarBuffer.post {

            progressBarBuffer.visibility = View.VISIBLE

        }

        backgroundFilter.post {

            backgroundFilter.visibility = View.GONE

        }

        editionActivity.drawerHelper?.enableDrawerMenuSwipeIn()

        editionActivity.headerHelper?.hideSimpleToolbar(editionActivity.getCurrentPage(), listener)

        if (editionActivity.getCurrentPageType() == UsefulConstants.TYPE_ARTICLE) {

            showArticleOptions()

        }

        return true

    }

    fun showToolbar(listener: (() -> Unit)? = null) {

        if (bottomToolbar.visibility != View.VISIBLE) {

            bottomToolbar.slideUp(duration = editionActivity.animationDuration, postHandler = {

                progressBarBase.visibility = View.VISIBLE

                progressBarBuffer.visibility = View.VISIBLE

                listener?.invoke()

            })

        } else {

            listener?.invoke()

        }

    }

    fun hideToolbar() {

        progressBarBase.visibility = View.INVISIBLE

        progressBarBuffer.visibility = View.INVISIBLE

        if (bottomToolbar.visibility != View.GONE) {

            bottomToolbar.slideDown(duration = editionActivity.animationDuration)

        }

    }

    fun getToolbarHeight(): Int {

        return editionActivity.resources.getDimensionPixelSize(R.dimen.edition_bottomToolbar_height) + progressBarBase.height

    }

    fun showArticleOptions() {

        if (articleOptionsContainer.visibility != View.VISIBLE) {

            articleOptionsContainer.slideUp(duration = editionActivity.animationDuration)

        }

    }

    fun hideArticleOptions() {

        if (articleOptionsContainer.visibility != View.GONE) {

            articleOptionsContainer.slideDown(duration = editionActivity.animationDuration)

        }

    }


}