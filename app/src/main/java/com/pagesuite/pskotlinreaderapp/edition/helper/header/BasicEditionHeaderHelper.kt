package com.pagesuite.pskotlinreaderapp.edition.helper.header

import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.widget.AbstractHeaderHelper

abstract class BasicEditionHeaderHelper(basicActivity: EditionActivity) : AbstractHeaderHelper(basicActivity) {

    val editionActivity: EditionActivity = basicActivity

    init {

        headerBuffer.setBackgroundColor(ColourConfig.Edition.Toolbar.mHeaderBufferBackground)

    }

    override fun prepHeader(completionListener: (() -> Unit)?) {

        if (DataStore.EditionApplication == null || DataStore.Edition == null) {

            completionListener?.invoke()

            return
        }

        super.prepHeader(completionListener)

    }

    override fun getToolbarHeight(position: Int): Int {

        return if (position == 0 && !editionActivity.showingArticles) {

            getIncreasedMenuHeight()

        } else {

            getDecreasedMenuHeight()

        }

    }

}