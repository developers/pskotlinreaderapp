package com.pagesuite.pskotlinreaderapp.edition.adapter.drawermenu

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.models.Item
import com.pagesuite.pskotlinreaderapp.models.ItemGroup


class EditionSideMenuAdapter(private val parentContext: Context, val item: ItemGroup, private val itemClickListener: View.OnClickListener, private val settingClickListener: View.OnClickListener) : RecyclerView.Adapter<EditionSideMenuAdapter.Holder>() {

    var sectionData = DataStore.Edition?.sections

    private var internalItemClickListener: View.OnClickListener = View.OnClickListener {

        if (item.type == UsefulConstants.TYPE_SECTIONS) {

            itemClickListener.onClick(it)

            val tag = it.getTag(R.id.tag_index)

            if (tag is Int) {

                val cellToUpdate = DataStore.Edition?.activeSection
                DataStore.Edition?.activeSection = tag

                if (cellToUpdate != null) {

                    notifyItemChanged(cellToUpdate + item.itemOffset)

                }

                val activeSection = DataStore.Edition?.activeSection

                if (activeSection != null) {

                    notifyItemChanged(activeSection + item.itemOffset)

                }

            }

        } else {

            settingClickListener.onClick(it)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        val view = LayoutInflater.from(parentContext).inflate(R.layout.edition_settings_menu_cell, parent, false)
        view.setOnClickListener(internalItemClickListener)
        return Holder(view)
    }

    override fun getItemCount(): Int {

        var numberOfItems = (if (item.type == UsefulConstants.TYPE_SECTIONS) sectionData?.count()
                ?: 0 else item.items?.count() ?: 0)

        if (!TextUtils.isEmpty(item.name)) {

            item.itemOffset = 1
            numberOfItems += 1

        }

        return numberOfItems

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        if (position == 0 && !TextUtils.isEmpty(item.name)) {

            holder.bindHeader(item.name ?: "")

        } else {

            if (item.type == UsefulConstants.TYPE_SECTIONS) {

                holder.bindSectionCell(position - item.itemOffset)

            } else {

                val settingItem = item.items?.getOrNull(position - item.itemOffset)
                holder.bindSettingCell(settingItem)

            }

        }
    }


    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var buttonTitle: TextView = itemView.findViewById(R.id.SectionName)

        private val rgbHighlight = ColourConfig.Edition.NavigationDrawer.Normal.mBackgroundHighlightColour //Utils.covertStringToRGB(DataStore.EditionApplication?.menu?.highlightcolor)
        private val rgbHighlightForeground = ColourConfig.Edition.NavigationDrawer.Normal.mForegroundHighlightColour //Utils.calculateFontColourBasedOnBackgroundColour(rgbHighlight))

        private val rgbBackground = ColourConfig.Edition.NavigationDrawer.Normal.mBackgroundColour //Utils.covertStringToRGB(DataStore.EditionApplication?.menu?.backgroundcolor)
        private val rgbForeground = ColourConfig.Edition.NavigationDrawer.Normal.mForegroundColour //Utils.calculateFontColourBasedOnBackgroundColour(rgbBackground)


        fun bindHeader(title: String) {

            buttonTitle.text = title
            buttonTitle.setTextColor(ColourConfig.Edition.NavigationDrawer.Header.mForegroundColour)
            itemView.setBackgroundColor(ColourConfig.Edition.NavigationDrawer.Header.mBackgroundColour)

        }

        fun bindSectionCell(sectionSelected: Int) {

            val section = sectionData?.getOrNull(sectionSelected)
            buttonTitle.text = section?.name ?: ""
            buttonTitle.setTypeface(null, Typeface.BOLD)

            if (DataStore.Edition?.activeSection == sectionSelected) {

                itemView.setBackgroundColor(rgbHighlight)
                buttonTitle.setTextColor(rgbHighlightForeground)

            } else {

                itemView.setBackgroundColor(rgbBackground)
                buttonTitle.setTextColor(rgbForeground)

            }

            itemView.setTag(R.id.tag_index, sectionSelected)

        }


        fun bindSettingCell(item: Item?) {

            if (item != null) {

                buttonTitle.text = item.text
                buttonTitle.setTypeface(null, Typeface.BOLD)
                itemView.setBackgroundColor(rgbBackground)
                buttonTitle.setTextColor(rgbForeground)
                itemView.setTag(R.id.tag_type, item.type)
                //itemView.setOnClickListener { settingSelected(item.type) }

            }

        }

    }

}



