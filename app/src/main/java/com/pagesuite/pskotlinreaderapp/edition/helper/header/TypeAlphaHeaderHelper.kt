package com.pagesuite.pskotlinreaderapp.edition.helper.header

import android.text.SpannableString
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.config.HeaderConfig
import com.pagesuite.pskotlinreaderapp.edition.MenuSizeType
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.utils.Utils
import com.pagesuite.utilities.DeviceUtils
import kotlinx.android.synthetic.main.view_edition_toolbar.*

class TypeAlphaHeaderHelper(editionActivity: EditionActivity) : BasicEditionHeaderHelper(editionActivity) {

    private val headerPageNumber: TextView = editionActivity.headerPageNumber

    override fun loadEditionName() {

        headerDate.visibility = View.GONE

    }

    override fun increaseTopMenuSize(completionListener: (() -> Unit)?) {

        // page 0

        if (currentMenuType == MenuSizeType.EXPANDED) return

        currentMenuType = MenuSizeType.EXPANDED

        var semaphoreCount = 1

        val listener = {

            semaphoreCount--

            if (semaphoreCount == 0) {

                completionListener?.invoke()

            }

        }

        Utils.animateObjectHeight(toolbar, getIncreasedMenuHeight(), completionListener = listener)

        //hideMenuButton(listener)
        headerPaging.visibility = View.GONE

        val pagingLogoParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0.0f)
        headerPaging.layoutParams = pagingLogoParam

        val headerLogoParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 100.0f)
        headerLogoParam.leftMargin = editionActivity.resources.getDimensionPixelSize(R.dimen.margin_eight)
        headerLogoParam.rightMargin = editionActivity.resources.getDimensionPixelSize(R.dimen.margin_eight)
        headerLogoContainer.layoutParams = headerLogoParam

        val logoParams: ViewGroup.LayoutParams = headerLogo.layoutParams

        if (logoParams is RelativeLayout.LayoutParams) {

            logoParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0)
            logoParams.addRule(RelativeLayout.CENTER_HORIZONTAL, 1)
            headerLogo.layoutParams = logoParams

        }

        semaphoreCount--
        listener.invoke()

    }

    override fun reduceTopMenuSize() {

        // page 1+

        if (currentMenuType == MenuSizeType.MINIMIZED) return

        currentMenuType = MenuSizeType.MINIMIZED

        headerPaging.visibility = View.VISIBLE
        headerPaging.bringToFront()

        showMenuButton()

        Utils.animateObjectHeight(toolbar, getDecreasedMenuHeight())

        val headerPagingParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 50.0f)
        headerPaging.layoutParams = headerPagingParam

        val headerLogoParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 50.0f)
        headerLogoParam.leftMargin = editionActivity.resources.getDimensionPixelSize(R.dimen.margin_eight)
        headerLogoContainer.layoutParams = headerLogoParam

        val logoParams: ViewGroup.LayoutParams = headerLogo.layoutParams

        if (logoParams is RelativeLayout.LayoutParams) {

            logoParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 1)
            logoParams.addRule(RelativeLayout.CENTER_HORIZONTAL, 0)
            headerLogo.layoutParams = logoParams

        }

    }

    override fun updatePageNumber(display: SpannableString?, simpleDisplay: String?, sectionColour: Int) {

        headerPageNumber.setTextColor(sectionColour)

        if (display != null) {

            headerPageNumber.text = display

        } else {

            headerPageNumber.text = simpleDisplay

        }

    }

    override fun getIncreasedMenuHeight(): Int {

        return if (!DeviceUtils.isPhone(basicActivity)) {

            /*val homeHeight: Float = DataStore.EditionApplication?.header?.homeheight ?: 0f

            return if (homeHeight != 0f) {

                Math.round(homeHeight * basicActivity.mScreenDensityScale)

            } else {

                basicActivity.resources.getDimensionPixelSize(R.dimen.toolbar_expanded_size)

            }*/

            HeaderConfig.Edition.Alpha.mHomeHeight

        } else {

            getDecreasedMenuHeight()

        }

    }

    override fun getDecreasedMenuHeight(): Int {

        /*val normalHeight: Float = DataStore.EditionApplication?.header?.naturalheight ?: 0f

        return if (normalHeight != 0f) {

            Math.round(normalHeight * editionActivity.mScreenDensityScale)

        } else {

            editionActivity.resources.getDimensionPixelSize(R.dimen.toolbar_reduced_size)

        }*/

        return HeaderConfig.Edition.Alpha.mNormalHeight

    }

}