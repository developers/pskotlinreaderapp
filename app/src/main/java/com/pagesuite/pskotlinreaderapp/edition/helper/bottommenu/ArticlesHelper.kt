package com.pagesuite.pskotlinreaderapp.edition.helper.bottommenu

import android.support.design.widget.TabLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.activity.EditionActivity
import com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar.EditionBottomMenuArticleAdapter
import com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar.EditionBottomMenuCoversAdaptor
import com.pagesuite.pskotlinreaderapp.extensions.slideDown
import com.pagesuite.pskotlinreaderapp.extensions.slideUp
import com.pagesuite.pskotlinreaderapp.utils.Utils
import com.pagesuite.pskotlinreaderapp.widget.MyItemDecoration
import kotlinx.android.synthetic.main.view_edition_bottommenu.*


class ArticlesHelper(private val editionActivity: EditionActivity) {

    private val bottomMenuSectionRecycler = editionActivity.bottomMenuSectionRecycler
    private val bottomMenuArticleRecycler = editionActivity.bottomMenuArticleRecycler
    private val backgroundFilter = editionActivity.backgroundFilter

    private val bottomArticleMenuContainer = editionActivity.BottomArticleMenuContainer
    private val articleHeadline = editionActivity.bottomArticlesHeadline
    private val articlePageTitle = editionActivity.bottomArticlesPageTitle
    private val bottomMenuArticleRecyclerContainer = editionActivity.bottomMenuArticleRecyclerContainer
    private var mArticleAdapter: EditionBottomMenuArticleAdapter? = null

    private val articleXofY: String = editionActivity.getString(R.string.articleXofY)

    private var currentVisibleArticleIndex = -1

    private var isFromScroll: Boolean = false

    private val pageTitleColour = "#${String.format("%X", ColourConfig.Edition.Pages.mSubtitleColour).substring(2)}"

    init {

        bottomArticleMenuContainer.setBackgroundColor(ColourConfig.Edition.Pages.mBackgroundColour)

        bottomMenuSectionRecycler.setSelectedTabIndicatorColor(ColourConfig.Edition.Pages.Tabs.mSelectionColour)
        bottomMenuSectionRecycler.setTabTextColors(ColourConfig.Edition.Pages.Tabs.mForegroundColour, ColourConfig.Edition.Pages.Tabs.mSelectTabForeground)

        articlePageTitle.setTextColor(ColourConfig.Edition.Pages.mForegroundColour)
        articleHeadline.setTextColor(ColourConfig.Edition.Pages.mForegroundColour)

    }

    fun prepBottomMenuForSections() {

        val edition = DataStore.Edition ?: return
        val sections = DataStore.Edition?.sections ?: return

        bottomMenuSectionRecycler.removeAllTabs()

        var tab: TabLayout.Tab?

        for (section in sections) {

            tab = bottomMenuSectionRecycler.newTab()
            tab.text = section.name
            bottomMenuSectionRecycler.addTab(tab)

        }

        bottomMenuSectionRecycler.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                with(bottomMenuArticleRecycler.adapter as EditionBottomMenuArticleAdapter) {

                    if (!isFromScroll) {

                        val index = this.mPages.indexOfFirst {
                            it.section == DataStore.Edition?.sections?.getOrNull(tab?.position
                                    ?: 0)?.name
                        }

                        if (index != -1) {

                            bottomMenuArticleRecycler.layoutManager?.scrollToPosition(index)

                        }

                    }

                    isFromScroll = false

                }

            }

        })

        val layoutManager = LinearLayoutManager(editionActivity, LinearLayoutManager.HORIZONTAL, false)
        bottomMenuArticleRecycler.layoutManager = layoutManager

        bottomMenuArticleRecycler.clearOnScrollListeners()

        if (edition.getSectionFronts()) {

            prepBottomMenuCovers()

            if (bottomMenuArticleRecycler.itemDecorationCount <= 1) {

                bottomMenuArticleRecycler.addItemDecoration(DividerItemDecoration(editionActivity, DividerItemDecoration.HORIZONTAL))

            }

        } else {

            prepBottomMenuArticles(0)

            if (bottomMenuArticleRecycler.itemDecorationCount <= 1) {

                bottomMenuArticleRecycler.addItemDecoration(MyItemDecoration(editionActivity))

            }

        }

    }

    fun showMenu(listener: (() -> Unit)? = null) {

        //1 is the bottom bar

        val recyclerLayoutParams: RelativeLayout.LayoutParams = bottomMenuArticleRecyclerContainer.layoutParams as RelativeLayout.LayoutParams
        val sectionsLayoutParams: RelativeLayout.LayoutParams = bottomMenuSectionRecycler.layoutParams as RelativeLayout.LayoutParams

        if (!editionActivity.sectionsFillsScreen) {

            bottomArticleMenuContainer.layoutParams.height = editionActivity.expandedHeight
            bottomMenuArticleRecyclerContainer.layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT
            backgroundFilter.visibility = View.VISIBLE
            recyclerLayoutParams.addRule(RelativeLayout.ABOVE, bottomMenuSectionRecycler.id)

        }
        //else is for a view that will cover the entire screen. The toolbar will move up to cover the screen, and the recycler will adapt to be bigger and wider.
        else {

            bottomArticleMenuContainer.layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT
            bottomMenuArticleRecyclerContainer.layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT

            recyclerLayoutParams.addRule(RelativeLayout.ABOVE, 0)
            recyclerLayoutParams.addRule(RelativeLayout.BELOW, bottomMenuSectionRecycler.id)

            sectionsLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1)
            sectionsLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0)

        }

        bottomArticleMenuContainer.requestLayout()

        bottomArticleMenuContainer.slideUp(duration = editionActivity.animationDuration, postHandler = {

            listener?.invoke()

        })

    }

    private fun prepBottomMenuCovers() {

        val sections = DataStore.Edition?.sections

        if (sections != null) {

            bottomMenuArticleRecycler.adapter = EditionBottomMenuCoversAdaptor(editionActivity, sections, View.OnClickListener {

                val sectionIndex = it.getTag(R.id.tag_index)

                if (sectionIndex is Int) {

                    val clickedSection = sections[sectionIndex]

                    Log.d(ArticlesHelper::class.simpleName, "Cover Tapped ${clickedSection.name}")

                    val pageIndex = clickedSection.getFirstPageInSection()

                    if (pageIndex != null) {

                        editionActivity.moveToPage(pageIndex)

                    }

                    onCloseArticleMenuIfNeeded()

                }

                // (context as EditionActivity).onCloseArticleMenuIfNeeded(
                //  (context as EditionActivity).moveToPage(articles[index].getPageForArticle())

            })

        }

    }

    fun prepBottomMenuArticles(index: Int) {

        val edition = DataStore.Edition

        if (edition != null) {

            val pages = edition.getArticlePages()

            if (pages != null) {

                mArticleAdapter = EditionBottomMenuArticleAdapter(editionActivity, pages, index, edition.getSectionFronts(), View.OnClickListener {

                    val page = it.getTag(R.id.tag_index)

                    if (page is Int) {

                        val article = pages[page].articles?.getOrNull(0)

                        if (article != null) {

                            editionActivity.addArticlesFragment(article.articleguid)

                        }

                        //basicActivity.moveToPage(page)

                    }

                })

                bottomMenuArticleRecycler.adapter = mArticleAdapter

                bottomMenuArticleRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)

                        updateArticleLabel(recyclerView)

                    }
                })

            }

            //bottomMenuArticleRecycler.addItemDecoration(DividerItemDecoration(basicActivity, DividerItemDecoration.HORIZONTAL))

        }

    }

    fun updateArticleLabel(recyclerView: RecyclerView) {

        val itemIndex = (recyclerView.layoutManager as? LinearLayoutManager)?.findFirstCompletelyVisibleItemPosition()
                ?: -1

        if (currentVisibleArticleIndex != itemIndex) {

            currentVisibleArticleIndex = itemIndex

            val pageCount = mArticleAdapter?.mPages?.size ?: 0

            if (itemIndex != -1 && itemIndex < pageCount) {

                val page = mArticleAdapter?.mPages?.getOrNull(itemIndex)

                if (page != null) {

                    //Log.w("Simon", "${visibleArticle.headline} Section: ${visibleArticle.sectionData.name}")

                    val sectionIndex = DataStore.Edition?.sections?.indexOfFirst { it.name == page.section }
                            ?: -1

                    if (sectionIndex != -1) {

                        val section = DataStore.Edition?.sections?.getOrNull(sectionIndex)

                        if (section != null) {

                            if (page.articles?.isNotEmpty() == true) {

                                val zeroArticle = page.articles?.getOrNull(0)
                                val sectionArticle = section.articles?.firstOrNull { article -> article.articleguid?.equals(zeroArticle?.articleguid) == true }

                                articleHeadline.post {

                                    articleHeadline.text = sectionArticle?.getSpannedHeadline() ?: ""

                                }

                                val articleIndex = DataStore.Edition?.getPagePosition(page, section.name, mArticleAdapter?.mPages)
                                        ?: 0

                                articlePageTitle.post {

                                    articlePageTitle.text = Utils.getSpanned(String.format(articleXofY, section.name, editionActivity.getString(R.string.page), (articleIndex + 1), pageTitleColour))

                                }

                            } else {

                                articleHeadline.post {

                                    articleHeadline.text = ""

                                }

                                articlePageTitle.post {

                                    articlePageTitle.text = ""

                                }

                            }

                            val tab = bottomMenuSectionRecycler.getTabAt(sectionIndex)

                            if (tab != null && !tab.isSelected) {

                                isFromScroll = true

                                bottomMenuSectionRecycler.getTabAt(sectionIndex)?.select()

                            }

                        }

                    }

                }

            }

        }

    }

    fun onCloseArticleMenuIfNeeded(completionListener: (() -> Unit)? = null) {

        bottomArticleMenuContainer.slideDown(duration = editionActivity.animationDuration)
        bottomArticleMenuContainer.layoutParams.height = editionActivity.resources.getDimensionPixelSize(R.dimen.bottomMenuContainer_height_collapsed)

        bottomArticleMenuContainer.post {

            bottomArticleMenuContainer.requestLayout()

            completionListener?.invoke()

        }

    }

}