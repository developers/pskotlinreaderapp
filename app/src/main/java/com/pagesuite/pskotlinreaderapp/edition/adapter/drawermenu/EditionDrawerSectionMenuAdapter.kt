package com.pagesuite.pskotlinreaderapp.edition.adapter.drawermenu

import android.content.Context
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.models.ItemGroup
import com.pagesuite.pskotlinreaderapp.widget.VerticalItemDecoration

//This is the adaptor for the list of articles in the drawer menu in the edition

class EditionDrawerSectionMenuAdapter(val parentContext: Context, private val menuItems: ArrayList<ItemGroup>, val itemClickListener: View.OnClickListener, val settingClickListener: View.OnClickListener) : RecyclerView.Adapter<EditionDrawerSectionMenuAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        if (viewType == 1) {

            val view = LayoutInflater.from(parentContext).inflate(R.layout.edition_settings_menu_cell, parent, false)
            view.setOnClickListener(itemClickListener)
            return SingleHolder(view)

        }

        val view = LayoutInflater.from(parentContext).inflate(R.layout.edition_settings_menu_recycler, parent, false)
        view.setOnClickListener(itemClickListener)
        return Holder(view)

    }

    override fun getItemCount(): Int {

        return menuItems.size

    }

    override fun getItemViewType(position: Int): Int {

        val itemGroup = menuItems.getOrNull(position)

        if (itemGroup != null && itemGroup.type?.equals(UsefulConstants.TYPE_SECTIONS, ignoreCase = true) == false) {

            if (itemGroup.items == null || itemGroup.items?.isEmpty() == true) {

                return 1

            }

        }

        return super.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        val itemGroup = menuItems.getOrNull(position)

        if (itemGroup != null) {

            holder.bindHolder(itemGroup, position)

        }
    }


    inner class SingleHolder(itemView: View) : Holder(itemView) {

        val title: TextView = itemView.findViewById(R.id.SectionName)

        override fun bindHolder(itemGroup: ItemGroup, position: Int) {

            title.text = itemGroup.name

            if (itemGroup.type?.equals(UsefulConstants.TYPE_HOME, ignoreCase = true) == true) {

                title.setTextColor(ColourConfig.Edition.NavigationDrawer.Home.mForegroundColour)
                itemView.setBackgroundColor(ColourConfig.Edition.NavigationDrawer.Home.mBackgroundColour)

            } else {

                title.setTextColor(ColourConfig.Edition.NavigationDrawer.Normal.mForegroundColour)
                itemView.setBackgroundColor(ColourConfig.Edition.NavigationDrawer.Normal.mBackgroundColour)

            }

            itemView.setTag(R.id.tag_index, position)
            itemView.setTag(R.id.tag_type, itemGroup.type)

        }

    }

    open inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val sectionRecycler: RecyclerView? = itemView.findViewById(R.id.menuRecycler)
                ?: null

        open fun bindHolder(itemGroup: ItemGroup, position: Int) {

            val recycler = sectionRecycler ?: return

            recycler.adapter = EditionSideMenuAdapter(parentContext, itemGroup, itemClickListener, settingClickListener)
            recycler.layoutManager = LinearLayoutManager(parentContext)
            recycler.setHasFixedSize(true)

            val decorationCount = recycler.itemDecorationCount

            if (decorationCount <= 1) {

                val drawable = ContextCompat.getDrawable(parentContext, R.drawable.vertical_divider)?.mutate()

                if (drawable != null) {

                    drawable.setColorFilter(ColourConfig.Edition.NavigationDrawer
                            .mVerticalDividerColour, PorterDuff.Mode.SRC_ATOP)

                    val verticalDivider = VerticalItemDecoration(parentContext)

                    verticalDivider.mDivider = drawable

                    recycler.addItemDecoration(verticalDivider)

                }

            }

        }

    }

}



