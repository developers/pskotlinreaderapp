package com.pagesuite.pskotlinreaderapp.edition.fragment.pagination

import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.edition.adapter.EditionViewPagerAdaptor
import com.pagesuite.pskotlinreaderapp.edition.fragment.EditionContentFragment
import com.pagesuite.utilities.DeviceUtils

class SectionsFragment : EditionContentFragment() {

    override fun getLayout(): Int {

        return R.layout.fragment_page_sections

    }

    override fun getCurrentPage(): Int {

        return (viewPager?.currentItem ?: 0)

    }

    override fun prepViewPager(completionListener: (() -> Unit)?) {

        val edition = DataStore.Edition

        if (edition != null) {

            if (!hasRun) {

                hasRun = true
                val pages = edition.getEditionPages()
                val cache = edition.cache
                val articleMode = ((edition.articlemode?.equals(UsefulConstants.ARTICLE_DISPLAY_COLUMNS, true)
                        ?: false) || edition.articlemode?.equals(UsefulConstants.ARTICLE_DISPLAY_MULTICOLUMN, true)
                        ?: false) && !DeviceUtils.isPhone(activity)

                if (pages != null && cache != null) {

                    adapter = EditionViewPagerAdaptor(childFragmentManager, pages, cache, articleMode)
                    adapter?.listener = listener
                    adapter?.marginListener = marginListener
                    viewPager?.adapter = adapter

                }

            }

        }

        completionListener?.invoke()

    }

}