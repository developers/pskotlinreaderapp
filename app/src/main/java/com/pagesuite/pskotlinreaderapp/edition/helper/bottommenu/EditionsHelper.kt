package com.pagesuite.pskotlinreaderapp.edition.helper.bottommenu

import android.content.Intent
import android.graphics.PorterDuff
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.EditionBasicActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.centraldatapoints.EndpointQueryRequests
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.downloads.adapter.BasicDownloadsAdapter
import com.pagesuite.pskotlinreaderapp.edition.MenuSizeType
import com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar.EditionGroupsAdapter
import com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar.EditionViewEditionsBottomMenuAdapter
import com.pagesuite.pskotlinreaderapp.extensions.slideDown
import com.pagesuite.pskotlinreaderapp.extensions.slideUp
import com.pagesuite.pskotlinreaderapp.models.Edition
import com.pagesuite.pskotlinreaderapp.models.IEditionContent
import com.pagesuite.pskotlinreaderapp.models.ReplicaEdition
import com.pagesuite.pskotlinreaderapp.utils.ColouringHelper
import com.pagesuite.pskotlinreaderapp.widget.MyItemDecoration
import kotlinx.android.synthetic.main.view_edition_bottommenu.*

class EditionsHelper(private val editionActivity: EditionBasicActivity, private val expandedHeight: Int, private val editionArchiveFillsScreen: Boolean = false) : GestureDetector.SimpleOnGestureListener() {

    private val editionTitle: TextView = editionActivity.editionsTitle
    private val bottomEditionMenuContainer = editionActivity.BottomEditionMenuContainer
    private val bottomMenuEditionRecycler = editionActivity.bottomMenuEditionRecycler
    private var editionsAdapter: EditionViewEditionsBottomMenuAdapter? = null
    private val backgroundFilter = editionActivity.backgroundFilter

    private val editionGroupButton = editionActivity.bottomMenuEdition_groupButton
    private val editionGroupsRecyclerView = editionActivity.editionGroupsRecyclerView
    private val editionGroupsOptionsContainer = editionActivity.editionGroupsOptionsContainer
    private var editionGroupsAdapter: EditionGroupsAdapter? = null
    private var editionGroupsMenuState: MenuSizeType = MenuSizeType.MINIMIZED
    private var editionGroupState: MenuSizeType = MenuSizeType.MINIMIZED
    private val editionGroupsOptions = editionActivity.editionGroupsOptions
    private val editionGroupsLoadingSpinner = editionActivity.editionGroupsLoadingSpinner
    private val editionGroupsZoomButton = editionActivity.editionGroupsZoomButton
    private val editionGroupsFilterButton = editionActivity.editionGroupsFilterButton

    private val editionGroupsBlocker = editionActivity.editionGroupsBlocker

    private var mDetector: GestureDetectorCompat? = null

    private var itemDecoration: MyItemDecoration? = null

    private var selectedIndex: Int = 0

    private val dateFormat: String = editionActivity.getString(R.string.dateFormat)

    private val stackCount: Int = editionActivity.resources.getInteger(R.integer.editions_archive_stackCount)

    private var isLoadingStacks: Boolean = false

    init {

        bottomEditionMenuContainer.setBackgroundColor(ColourConfig.Edition.Archive.mBackgroundColour)
        editionGroupsRecyclerView.setBackgroundColor(ColourConfig.Edition.Archive.mGroupsBackgroundColour)
        editionGroupsOptionsContainer.setBackgroundColor(ColourConfig.Edition.Archive.mGroupsBackgroundColour)
        editionGroupsZoomButton.setColorFilter(ColourConfig.Edition.Archive.mForegroundColour, PorterDuff.Mode.SRC_ATOP)
        editionGroupButton.setColorFilter(ColourConfig.Edition.Archive.mGroupsToggleButtonForeground, PorterDuff.Mode.SRC_ATOP)

        editionGroupsFilterButton.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Edition.Archive.Button.mBackgroundColour,
                normalBorder = ColourConfig.Edition.Archive.Button.mStrokeColour,
                pressed = ColourConfig.Edition.Archive.Button.mPressedBackgroundColour,
                pressedBorder = ColourConfig.Edition.Archive.Button.mPressedBackgroundColour,
                borderWidth = ColourConfig.Edition.Archive.Button.mStrokeWidth)

        editionGroupsFilterButton.setTextColor(ColouringHelper.getSelector(ColourConfig.Edition.Archive.Button.mForegroundColour, ColourConfig.Edition.Archive.Button.mPressedForegroundColour))

        editionTitle.setTextColor(ColourConfig.Edition.Archive.mForegroundColour)

        hideEditionGroupsLoading()

        mDetector = GestureDetectorCompat(editionActivity, this)

        editionGroupsBlocker.setOnTouchListener { _, event ->

            mDetector?.onTouchEvent(event)

            true

        }

    }

    private fun prepareArchive() {

        val editions = DataStore.EditionApplication?.editionsOrganisedByDay?.get(selectedIndex)

        if (editions != null && editions.isNotEmpty()) {

            val pubDate = editions[0].getPublicationDate()

            if (pubDate != null) {

                editionTitle.text = DataStore.EditionApplication?.getEditionDate(pubDate, dateFormat)

            }

            editionsAdapter = EditionViewEditionsBottomMenuAdapter(editionActivity, editions, shouldShowDownload = true) //EditionViewEditionsBottomMenuAdapter(basicActivity, editions)
            editionsAdapter?.itemClickListener = View.OnClickListener {

                val tag = it.getTag(R.id.tag_edition)

                if (tag is IEditionContent) {

                    if (DataStore.Edition?.editionguid?.equals(tag.getEditionGuid(), ignoreCase = true) != true) {

                        if (tag is Edition) {

                            DataStore.Edition = tag
                            DataStore.EditionApplication?.activeEdition = tag.getEditionGuid()

                            editionActivity.changeEdition()

                        } else if (tag is ReplicaEdition) {

                            editionActivity.mApplication?.loadReader(editionActivity, tag.getEditionGuid())

                        }

                    } else {

                        editionActivity.alreadyLoadedEdition()

                    }

                }

            }


            /*editionsAdapter?.editions = arrayListOf()
            val singleEdition = arrayListOf<Edition>()
            singleEdition.addAll(editions)
            editionsAdapter?.editions?.add(singleEdition)*/

            bottomMenuEditionRecycler.adapter = editionsAdapter

            val layoutManager = LinearLayoutManager(editionActivity, LinearLayoutManager.HORIZONTAL, false)
            bottomMenuEditionRecycler.layoutManager = layoutManager

            prepareGroups()

        }

    }

    private fun prepareGroups(isFromZoomChange: Boolean = false, completionListener: (() -> Unit)? = null) {

        val sortedEditions = DataStore.EditionApplication?.editionsOrganisedByDay

        if (sortedEditions != null) {

            if (editionGroupsAdapter == null) {

                editionGroupsAdapter = EditionGroupsAdapter(editionActivity.getString(R.string.dateFormat))

            }

            editionGroupsAdapter?.editionGroupState = editionGroupState

            val tileCount = if (editionGroupState == MenuSizeType.MINIMIZED) {

                editionActivity.resources.getInteger(R.integer.editionGroup_tileCount_normal)

            } else {

                editionActivity.resources.getInteger(R.integer.editionGroup_tileCount_shrunk)

            }

            editionGroupsAdapter?.maxRowCount = tileCount

            editionGroupsAdapter?.coverHeight = if (editionGroupState == MenuSizeType.MINIMIZED) {

                editionActivity.resources.getDimensionPixelSize(R.dimen.editionGroup_card_height_normal)

            } else {

                editionActivity.resources.getDimensionPixelSize(R.dimen.editionGroup_card_height_shrunk)

            }

            editionGroupsAdapter?.trimToFirstRow = editionGroupsMenuState == MenuSizeType.MINIMIZED

            if (itemDecoration == null) {

                if (editionGroupsRecyclerView.itemDecorationCount <= 1) {

                    itemDecoration = MyItemDecoration(editionActivity)

                    itemDecoration?.let {

                        editionGroupsRecyclerView.addItemDecoration(it)

                    }

                }

            }

            if (editionGroupsRecyclerView.layoutManager == null) {

                editionGroupsRecyclerView.layoutManager = GridLayoutManager(editionActivity, tileCount)

            } else {

                (editionGroupsRecyclerView.layoutManager as GridLayoutManager).spanCount = tileCount

            }

            editionGroupsRecyclerView.clearOnScrollListeners()

            editionGroupsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                    if (!recyclerView.canScrollVertically(1) && !isLoadingStacks && editionGroupsMenuState == MenuSizeType.EXPANDED) {

                        downloadArchive()

                    }

                }

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {


                }

            })

            if (editionGroupsRecyclerView.adapter == null) {

                editionGroupsRecyclerView.adapter = editionGroupsAdapter

            }

            editionGroupsAdapter?.editions = sortedEditions
            editionGroupsAdapter?.notifyDataSetChanged()

        }

        completionListener?.invoke()

    }

    fun downloadArchive(listener: (() -> Unit)? = null) {

        if (!isLoadingStacks) {

            isLoadingStacks = true

            showEditionGroupsLoading()

            EndpointQueryRequests.downloadArchive(context = editionActivity, listener = {

                isLoadingStacks = false

                prepareGroups(isFromZoomChange = false, completionListener = listener)

                hideEditionGroupsLoading()

            }, failureListener = {

                failedToLoadEditions()

            }, stackCount = stackCount)

        }

    }

    private fun hideEditionGroupsLoading() {

        editionGroupsLoadingSpinner.postDelayed({

            editionGroupsLoadingSpinner.slideDown(duration = editionActivity.animationDuration)

        }, (editionActivity.animationDuration * 3).toLong())

    }

    private fun showEditionGroupsLoading() {

        editionGroupsLoadingSpinner.slideUp(duration = editionActivity.animationDuration)

    }

    private fun failedToLoadEditions() {

        isLoadingStacks = false

        hideEditionGroupsLoading()

        Log.w(EditionsHelper::class.simpleName, "Failed to download list of editions for the archive")

    }

    fun openMenu(listener: (() -> Unit)? = null) {

        if (!editionArchiveFillsScreen) {

            bottomEditionMenuContainer.layoutParams.height = expandedHeight
            bottomMenuEditionRecycler.layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT
            backgroundFilter.visibility = View.VISIBLE

        }
        //else is for a view that will cover the entire screen. The toolbar will move up to cover the screen, and the recycler will adapt to be bigger and wider.
        else {

            bottomEditionMenuContainer.layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT
            bottomMenuEditionRecycler.layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT

        }

        bottomEditionMenuContainer.requestLayout()

        prepareArchive()

        bottomEditionMenuContainer.slideUp(duration = editionActivity.animationDuration, postHandler = {

            listener?.invoke()

        })

    }


    fun onCloseEditionMenuIfNeeded(completionListener: (() -> Unit)? = null) {

        bottomEditionMenuContainer.slideDown(duration = editionActivity.animationDuration)
        bottomEditionMenuContainer.layoutParams.height = editionActivity.resources.getDimensionPixelSize(R.dimen.bottomMenuContainer_height_collapsed)

        bottomEditionMenuContainer.post {

            bottomEditionMenuContainer.requestLayout()

            completionListener?.invoke()

        }

    }

    fun updateEdition(editionGuid: String, intent: Intent) {

        val editionsPerDay = editionsAdapter?.editions

        if (editionsPerDay != null) {

            var index = 0

            outerLoop@ for (edition in editionsPerDay) {

                if (edition.getEditionGuid() == editionGuid) {

                    break@outerLoop
                }

                index++

            }

            val viewHolder = bottomMenuEditionRecycler.findViewHolderForAdapterPosition(index)

            if (viewHolder != null) {

                val type = intent.getStringExtra(UsefulConstants.ARGS_TYPE)

                when (type) {

                    UsefulConstants.BROADCAST_DOWNLOAD_COMPLETED -> {

                        if (UsefulConstants.IS_DEBUG) {
                            Log.w(EditionsHelper::class.simpleName, "Download complete: $editionGuid")
                        }

                        bottomMenuEditionRecycler.adapter?.notifyItemChanged(index)

                    }
                    UsefulConstants.BROADCAST_DOWNLOAD_FAILED -> {

                        if (UsefulConstants.IS_DEBUG) {
                            Log.w(EditionsHelper::class.simpleName, "Download failed: $editionGuid")
                        }

                        bottomMenuEditionRecycler.adapter?.notifyItemChanged(index)

                    }
                    UsefulConstants.BROADCAST_DOWNLOAD_PROGRESS -> {

                        val progress = intent.getIntExtra(UsefulConstants.ARGS_PROGRESS, 0)

                        if (UsefulConstants.IS_DEBUG) {
                            Log.w(EditionsHelper::class.simpleName, "Download progress $progress%: $editionGuid")
                        }

                        if (viewHolder is BasicDownloadsAdapter.DownloadingViewHolder) {

                            viewHolder.progressBar.progress = progress

                        }

                    }
                    UsefulConstants.BROADCAST_DOWNLOAD_STARTED -> {

                        if (UsefulConstants.IS_DEBUG) {
                            Log.w(EditionsHelper::class.simpleName, "Download started: $editionGuid")
                        }

                        bottomMenuEditionRecycler.adapter?.notifyItemChanged(index)

                    }
                    UsefulConstants.BROADCAST_DOWNLOAD_QUEUED -> {

                        if (UsefulConstants.IS_DEBUG) {
                            Log.w(EditionsHelper::class.simpleName, "Download queued: $editionGuid")
                        }

                        bottomMenuEditionRecycler.adapter?.notifyItemChanged(index)

                    }

                }

            }

        }

    }

    fun toggleEditionGroups() {

        if (editionGroupsMenuState == MenuSizeType.MINIMIZED) {

            openEditionGroups()

        } else {

            closeEditionGroups()

        }

    }

    private fun openEditionGroups() {

        if (editionGroupsMenuState == MenuSizeType.EXPANDED) return

        editionGroupsMenuState = MenuSizeType.EXPANDED

        val layoutParams = editionGroupButton.layoutParams as RelativeLayout.LayoutParams
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0)
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1)

        editionGroupButton.post {

            editionGroupButton.layoutParams = layoutParams
            editionGroupButton.setImageResource(R.drawable.down_arrow)

        }

        editionTitle.post {

            editionTitle.visibility = View.GONE

        }

        editionGroupsAdapter?.trimToFirstRow = false

        editionGroupsRecyclerView.post {

            editionGroupsAdapter?.notifyDataSetChanged()

        }

        editionGroupsOptions.post {

            editionGroupsOptions.visibility = View.VISIBLE

        }

        editionGroupsBlocker.post {

            editionGroupsBlocker.visibility = View.GONE

        }

    }

    private fun closeEditionGroups() {

        if (editionGroupsMenuState == MenuSizeType.MINIMIZED) return

        editionGroupsMenuState = MenuSizeType.MINIMIZED

        val layoutParams = editionGroupButton.layoutParams as RelativeLayout.LayoutParams
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1)
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)

        editionGroupButton.post {

            editionGroupButton.layoutParams = layoutParams
            editionGroupButton.setImageResource(R.drawable.up_arrow)

        }

        editionTitle.post {

            editionTitle.visibility = View.VISIBLE

        }

        editionGroupsAdapter?.trimToFirstRow = true

        editionGroupsRecyclerView.post {

            editionGroupsAdapter?.notifyDataSetChanged()

        }

        editionGroupsOptions.post {

            editionGroupsOptions.visibility = View.GONE

        }

        editionGroupsBlocker.post {

            editionGroupsBlocker.visibility = View.VISIBLE

        }

    }

    fun setSelectedEditionGroup(index: Int) {

        if (editionGroupsMenuState == MenuSizeType.EXPANDED) {

            selectedIndex = index

            prepareArchive()

            closeEditionGroups()

        } else {

            openEditionGroups()

        }

    }

    fun closeMenu(): Boolean {

        if (editionGroupsMenuState == MenuSizeType.EXPANDED) {

            closeEditionGroups()

            return true

        }

        return false

    }

    fun toggleEditionGroupZoom() {

        editionGroupState = if (editionGroupState == MenuSizeType.MINIMIZED) {

            MenuSizeType.EXPANDED

        } else {

            MenuSizeType.MINIMIZED

        }

        if (editionGroupState == MenuSizeType.EXPANDED) {

            if (editionGroupsZoomButton is ImageView) {

                editionGroupsZoomButton.setImageResource(R.drawable.ic_squares_four)

            }

        } else {

            if (editionGroupsZoomButton is ImageView) {

                editionGroupsZoomButton.setImageResource(R.drawable.ic_squares_six)

            }

        }

        prepareGroups(isFromZoomChange = true)

    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {

        if (editionGroupsMenuState != MenuSizeType.EXPANDED && velocityY < 0) {

            toggleEditionGroups()

            return true

        }

        return super.onFling(e1, e2, velocityX, velocityY)
    }

}