package com.pagesuite.pskotlinreaderapp.edition.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.edition.adapter.EditionViewPagerAdaptor

abstract class EditionContentFragment : Fragment(), ViewPager.OnPageChangeListener {

    var viewPager: ViewPager? = null
    var adapter: EditionViewPagerAdaptor? = null
    var hasRun = false
    var listener: ((articleGuid: String?) -> Unit)? = null
    var pageListener: ((position: Int) -> Unit)? = null
    var marginListener: ((position: Int) -> IntArray)? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        viewPager = view.findViewById(R.id.viewPager)
        viewPager?.addOnPageChangeListener(this)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(getLayout(), container, false)

    }

    abstract fun getLayout(): kotlin.Int

    abstract fun prepViewPager(listener: (() -> Unit)? = null)

    abstract fun getCurrentPage(): Int

    fun getPageCount(): Int {

        return viewPager?.adapter?.count ?: 0

    }

    fun setCurrentPage(position: Int) {

        viewPager?.currentItem = position

    }

    fun clearContent() {

        viewPager?.adapter = null
        adapter?.fragments?.clear()
        adapter = null
        hasRun = false

    }

    fun moveToPage(pageNumber: Int) {

        viewPager?.post {

            viewPager?.setCurrentItem(pageNumber, false)

        }

    }

    override fun onPageScrollStateChanged(p0: Int) {

    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

    }

    override fun onPageSelected(position: Int) {

        pageListener?.invoke(position)

    }
}