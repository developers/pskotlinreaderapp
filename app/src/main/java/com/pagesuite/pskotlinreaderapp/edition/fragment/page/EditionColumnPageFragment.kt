package com.pagesuite.pskotlinreaderapp.edition.fragment.page

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.text.TextUtils
import android.util.SparseIntArray
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.pagesuite.coretext.CoreText
import com.pagesuite.coretext.widget.PaginatedTextView
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.Page

class EditionColumnPageFragment : EditionBasicPageFragment() {

    var article: Article? = null
    private var paginatedTextView: PaginatedTextView? = null

    /*var headlineTextView: TextView? = null
    var articleImageView: ImageView? = null*/

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        paginatedTextView = view.findViewById(R.id.paginatedTextView)
        /*headlineTextView = view.findViewById(R.id.articleHeadline)
        articleImageView = view.findViewById(R.id.articleImage)*/

    }

    override fun getLayout(): Int {

        return R.layout.fragment_edition_column_page

    }

    override fun renderPage(page: Page?) {

        mPageArticles?.let {

            if (it.isNotEmpty()) {

                article = it[0]

                loadArticle()


            }

        }

    }

    private fun loadArticle() {

        paginatedTextView?.mFragmentManager = childFragmentManager
        paginatedTextView?.setColumnPaddingLeft((16 * resources.displayMetrics.density).toInt())
        prepFontSize()

        val layouts = SparseIntArray()
        layouts.put(CoreText.ARGS_DEFAULT_LAYOUT, R.layout.edition_article_page_default)
        layouts.put(0, R.layout.edition_article_page_zero)

        paginatedTextView?.setLayouts(layouts)
        paginatedTextView?.mPageListener = object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {

                updateArticlePage(position)

            }

        }

        paginatedTextView?.postDelayed({

            if (isAdded) {

                paginatedTextView?.setColumnCount(resources.getInteger(R.integer.edition_article_columnCount))
                loadText()

            }

        }, 333)

    }

    private fun loadText() {

        paginatedTextView?.setText(article?.textdescription?.replace("|||", "\r\n\r\n"))

    }

    fun updateArticlePage(position: Int) {

        val fragment: Fragment? = paginatedTextView?.getPageFragment(position)

        if (position == 0) {

            val content = fragment?.view
            loadPageZero(content)

        }

    }

    private fun loadPageZero(view: View?) {

        if (view != null) {

            val articleImageView: ImageView = view.findViewById(R.id.articleImage)
            val imageUrl = article?.getFirstImagePath(context as Context)

            if (!TextUtils.isEmpty(imageUrl)) {

                articleImageView.let {

                    it.post {

                        Glide.with(it).load(imageUrl).into(it)

                    }

                }

            }

            val headlineView: TextView = view.findViewById(R.id.articleHeadline)
            headlineView.text = article?.getSpannedHeadline()

        }

    }

    override fun prepFontSize() {

        val fontSize = UsefulConstants.getFontSize()
        val density = resources.displayMetrics.density
        paginatedTextView?.setTextSize((fontSize.size * density * 12).toInt())

    }

    override fun updateMargins() {

        val layoutParams = paginatedTextView?.layoutParams

        if (layoutParams is RelativeLayout.LayoutParams) {

            layoutParams.topMargin = topMargin
            layoutParams.bottomMargin = bottomMargin

            paginatedTextView?.layoutParams = layoutParams

        }

    }

    override fun failedToLoad() {

    }
}