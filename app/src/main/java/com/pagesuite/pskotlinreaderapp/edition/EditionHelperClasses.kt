package com.pagesuite.pskotlinreaderapp.edition

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.text.Html
import android.text.TextUtils
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.models.Article
import com.pagesuite.pskotlinreaderapp.models.Image
import com.pagesuite.pskotlinreaderapp.models.Page
import com.pagesuite.utilities.NetworkUtils
import java.net.URL
import java.util.*
import kotlin.reflect.full.memberProperties

enum class MenuSizeType {
    EXPANDED, MINIMIZED
}

enum class EditionViewFontStates {
    OPEN, CLOSE
}

enum class EditionProgressBarState {
    PROGRESSBAR, SCRUBBER
}

enum class BottomMenuType {
    NONE, ARCHIVE, ARTICLES
}

enum class SearchType {
    EDITION_ONLY, ALL_EDITIONS, DATE_RANGE
}

class ReplacementTextObject(val oldValue: String, val newValue: String)

fun findArticle(articleGuid: String): Article? {

    val edition = DataStore.Edition

    if (edition != null) {

        val sections = edition.sections

        if (sections != null && sections.isNotEmpty()) {

            var articles: Array<Article>?

            for (section in sections) {

                articles = section.articles

                if (articles != null && articles.isNotEmpty()) {

                    for (article in articles) {

                        if (article.articleguid?.equals(articleGuid, true) == true) {

                            return article

                        }

                    }

                }

            }

        }

    }

    return null

}

fun shareSelectedArticle(context: Context, selectedArticle: Article) {

    val image = selectedArticle.getFirstImagePath(context)

    if (!TextUtils.isEmpty(image)) {

        val imageURL = URL(image)

        Log.d("EditionHelperClasses", "the image url is $imageURL")

        Glide.with(context).asBitmap().load(imageURL).into(object : SimpleTarget<Bitmap>() {

            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {

                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.type = "plain/text"
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "${selectedArticle.getSpannedHeadline()}")
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "${context.getString(R.string.readMore)}: ${selectedArticle.sharelink
                        ?: context.getString(R.string.shareLink_unavailable)}")

                try {

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                        val path = MediaStore.Images.Media.insertImage(context.contentResolver, resource, selectedArticle.getSpannedHeadline().toString(), null)
                        val imageURI = Uri.parse(path)
                        sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, imageURI)

                    } else {

                        Log.w("EditionHelperClasses", "Permission not allowed to WRITE_EXTERNAL_STORE")

                    }

                } catch (e: Exception) {

                    e.printStackTrace()

                }

                val intent = Intent.createChooser(sharingIntent, "${context.getString(R.string.shareArticle)}: '${selectedArticle.getSpannedHeadline()}'")
                ContextCompat.startActivity(context, intent, null)

            }

        })

    } else {

        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "plain/text"
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "${selectedArticle.getSpannedHeadline()}")
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "${context.getString(R.string.readMore)}: ${selectedArticle.sharelink
                ?: context.getString(R.string.shareLink_unavailable)}")

        val intent = Intent.createChooser(sharingIntent, "${context.getString(R.string.shareArticle)}: '${selectedArticle.getSpannedHeadline()}'")
        ContextCompat.startActivity(context, intent, null)

    }

}

fun replaceHTML(baseString: String, replacementValues: ArrayList<ReplacementTextObject>): String {

    var newString = baseString

    for (stringReplacement in replacementValues) {

        newString = newString.replace(stringReplacement.oldValue, stringReplacement.newValue, false)

    }

    return newString

}

fun replaceArticlePlaceHolders(context: Context, pageData: Page?, articles: Array<Article>?): ArrayList<ReplacementTextObject> {

    val replacementValues = arrayListOf<ReplacementTextObject>()

    if (articles != null) {

        var imagePath: String?
        var images: Array<Image>?

        for ((index, article) in articles.withIndex()) {

            images = article.images

            if (images != null && images.isNotEmpty()) {

                imagePath = article.getFirstImagePath(context)
                replacementValues.add(ReplacementTextObject("{{{Articles.Image.${index + 1}}}}", imagePath
                        ?: ""))

                replacementValues.add(ReplacementTextObject("{{{Articles.Caption.${index + 1}}}}", article.images?.firstOrNull()?.caption
                        ?: ""))

                for (image in images) {

                    imagePath = article.getImagePath(context, index)

                    if (imagePath != null) {

                        if (NetworkUtils.isConnected(context) && imagePath.startsWith("http")) {

                            Glide.with(context).downloadOnly().load(imagePath).submit()

                        }

                    }

                }

            } else {

                replacementValues.add(ReplacementTextObject("{{{Articles.Image.${index + 1}}}}", ""))
                replacementValues.add(ReplacementTextObject("{{{Articles.Caption.${index + 1}}}}", ""))

            }

            /*var childReplacements: ArrayList<ReplacementTextObject>?

            val bannedList = arrayOf("sectionData", "textdescription", "descriptionnoscript", "images")

            for (prop in Article::class.declaredMemberProperties) {

                if (!bannedList.contains(prop.name)) {

                    Log.w("Simon", "${prop.name}")

                    property = prop.get(article)

                    if (property != null) {

                        childReplacements = replaceContents(prop.name, property, index, 0)

                        if (childReplacements != null) {

                            replacementValues.addAll(childReplacements)

                        }

                    }

                }

            }*/

            replacementValues.add(ReplacementTextObject("{{{Articles.Headline.${index + 1}}}}", article.headline
                    ?: ""))
            replacementValues.add(ReplacementTextObject("{{{Articles.Description.${index + 1}}}}", article.description
                    ?: ""))
            replacementValues.add(ReplacementTextObject("{{{Articles.Author.${index + 1}}}}", article.author
                    ?: ""))
            replacementValues.add(ReplacementTextObject("{{{Articles.SubHeadline.${index + 1}}}}", article.subheadline
                    ?: ""))
            replacementValues.add(ReplacementTextObject("{{{Articles.Section.${index + 1}}}}", article.section
                    ?: ""))


        }

    }

    replacementValues.add(ReplacementTextObject("{{{Edition.Logo}}}", DataStore.EditionApplication?.header?.headerlogo
            ?: ""))

    replacementValues.add(ReplacementTextObject("{{{Edition.Name}}}", DataStore.Edition?.name
            ?: ""))
    replacementValues.add(ReplacementTextObject("{{{EditionDate}}}", DataStore.Edition?.name
            ?: ""))
    replacementValues.add(ReplacementTextObject("{{{Edition.Date}}}", DataStore.Edition?.name
            ?: ""))

    replacementValues.add(ReplacementTextObject("{{{AMPM}}}", DataStore.Edition?.datemode ?: ""))

    var sectionColour = ""

    val sectionName = pageData?.section

    if (sectionName != null) {

        val section = DataStore.Edition?.sections?.firstOrNull { section -> section.name?.equals(sectionName, ignoreCase = true) == true }

        if (section != null && !TextUtils.isEmpty(section.color)) {

            sectionColour = (if (section.color?.contains(",") == true) {

                "rgb(${section.color})"

            } else {

                section.color ?: ""

            })

        }

    }

    if (!TextUtils.isEmpty(sectionColour)) {

        replacementValues.add(ReplacementTextObject("lightseagreen", sectionColour))

    }

    val fonts = DataStore.EditionApplication?.fonts

    if (fonts != null) {

        var fontName1: String? = ""
        var fontName2: String? = ""
        var fontName3: String? = ""
        var fontName4: String? = ""

        val articleFonts = fonts.article

        if (articleFonts != null) {

            val headlineFont = articleFonts.headline

            if (headlineFont != null) {

                replacementValues.add(ReplacementTextObject("StyleFont3Name", headlineFont.name
                        ?: ""))

                replacementValues.add(ReplacementTextObject("dodgerblue", headlineFont.color ?: ""))

                replacementValues.add(ReplacementTextObject("3000px", "${headlineFont.size}px"))

                fontName3 = headlineFont.name

            }

            val bodyFont = articleFonts.body

            if (bodyFont != null) {

                replacementValues.add(ReplacementTextObject("StyleFont4Name", bodyFont.name ?: ""))

                replacementValues.add(ReplacementTextObject("lightgoldenrodyellow", bodyFont.color
                        ?: ""))

                replacementValues.add(ReplacementTextObject("4000px", "${bodyFont.size}px"))

                fontName4 = bodyFont.name

            }

        }

        val sectionFrontFonts = fonts.sectionfront

        if (sectionFrontFonts != null) {

            val headlineFont = sectionFrontFonts.headline

            if (headlineFont != null) {

                replacementValues.add(ReplacementTextObject("StyleFont1Name", headlineFont.name
                        ?: ""))

                replacementValues.add(ReplacementTextObject("darkgoldenrod", headlineFont.color
                        ?: ""))

                replacementValues.add(ReplacementTextObject("1000px", "${headlineFont.size}px"))

                fontName1 = headlineFont.name

            }

            val bodyFont = sectionFrontFonts.body

            if (bodyFont != null) {

                replacementValues.add(ReplacementTextObject("StyleFont2Name", bodyFont.name ?: ""))

                replacementValues.add(ReplacementTextObject("darkolivegreen", bodyFont.color ?: ""))

                replacementValues.add(ReplacementTextObject("2000px", "${bodyFont.size}px"))

                fontName2 = bodyFont.name

            }

        }

        val fontStr =
                "@font-face {font-family: '$fontName4';src: local('$fontName2'),url('$fontName4.ttf')}" +
                        "@font-face {font-family: '$fontName3';src: local('$fontName2'),url('$fontName3.ttf')}" +
                        "@font-face {font-family: '$fontName1';src: local('$fontName2'),url('$fontName1.ttf')}" +
                        "@font-face {font-family: '$fontName2';src: local('$fontName2'),url('$fontName2.ttf')}"

        replacementValues.add(ReplacementTextObject("{{{Fonts}}}", fontStr))

    }

    val promotions = pageData?.promotions

    if (promotions != null && promotions.isNotEmpty()) {

        for ((index, promotion) in promotions.withIndex()) {

            replacementValues.add(ReplacementTextObject("{{{Promotions.$index}}}", promotion))

        }

    }

    replacementValues.add(ReplacementTextObject("window.webkit.messageHandlers[\"imageGallery\"].postMessage(\"\");", "window.AndroidInterface.callGallery();"))

    replacementValues.add(ReplacementTextObject("{{{Articles.Section.}}}", pageData?.section ?: ""))

    return replacementValues

}

fun replaceContents(targetKey: String, target: Any?, index: Int, depth: Int, maxDepth: Int = 4): ArrayList<ReplacementTextObject>? {

    val replacementValues = arrayListOf<ReplacementTextObject>()

    if (target is String) {

        val replacement = replaceString(targetKey, target, index)

        if (replacement != null) {

            replacementValues.add(replacement)

        }

    } else if (target != null && depth < maxDepth) {

        var property: Any?

        var childValues: ArrayList<ReplacementTextObject>?

        if (target is Array<*>) {

            for ((childIndex, childTarget) in target.withIndex()) {

                if (childTarget != null) {

                    childValues = replaceContents("$targetKey.$childIndex", childTarget, childIndex, depth + 1)

                    if (childValues != null) {

                        replacementValues.addAll(childValues)

                    }

                }

            }

        } else {

            for (prop in target::class.memberProperties) {

                property = prop.getter.call(target)

                if (property != null) {

                    childValues = replaceContents(prop.name, property, index, depth + 1)

                    if (childValues != null) {

                        replacementValues.addAll(childValues)

                    }

                } else {

                    replacementValues.add(replaceNull(prop.name, index))

                }

            }

        }

    } else {

        replacementValues.add(replaceNull(targetKey, index))

    }

    return replacementValues

}

fun replaceNull(fieldName: String, index: Int): ReplacementTextObject {

    val key = getKeyForField(fieldName, index)

    return ReplacementTextObject("{{{Articles.$key}}}", "")

}

fun replaceString(fieldName: String, fieldValue: String, index: Int): ReplacementTextObject? {

    val key = getKeyForField(fieldName, index)

    val outputValue = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {

        Html.fromHtml(fieldValue).toString()

    } else {

        Html.fromHtml(fieldValue, Html.FROM_HTML_MODE_LEGACY).toString()

    }

    return ReplacementTextObject("{{{Articles.$key}}}", outputValue)

}

fun getKeyForField(fieldName: String, index: Int): String {

    return if (fieldName.equals("subheadline", ignoreCase = true)) {

        "SubHeadline.${index + 1}"

    } else {

        "${fieldName.capitalize()}.${index + 1}"

    }

}