package com.pagesuite.pskotlinreaderapp.edition.adapter.bottomtoolbar

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.centraldatapoints.DataStore
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.MenuSizeType
import com.pagesuite.pskotlinreaderapp.extensions.getLayoutInflater
import com.pagesuite.pskotlinreaderapp.models.IEditionContent
import com.pagesuite.pskotlinreaderapp.utils.ColouringHelper
import com.pagesuite.pskotlinreaderapp.utils.TopCropTransformation
import com.pagesuite.pskotlinreaderapp.widget.svg.GlideApp
import java.util.*

class EditionGroupsAdapter(private val dateFormat: String) : RecyclerView.Adapter<EditionGroupsAdapter.EditionGroupHolder>() {

    var editions: ArrayList<ArrayList<IEditionContent>>? = null
    var trimToFirstRow: Boolean = true
    var maxRowCount: Int = 2
    var editionGroupState: MenuSizeType = MenuSizeType.MINIMIZED
    var coverHeight: Int = 0

    companion object {

        const val VIEW_TYPE_SINGLE_NORMAL = 0
        const val VIEW_TYPE_DOUBLE_NORMAL = 1
        const val VIEW_TYPE_TRIPLE_NORMAL = 2

        const val VIEW_TYPE_SINGLE_SHRUNK = 3
        const val VIEW_TYPE_DOUBLE_SHRUNK = 4
        const val VIEW_TYPE_TRIPLE_SHRUNK = 5

    }

    private val glideRequestOptions = RequestOptions()

    init {

        glideRequestOptions.placeholder(R.drawable.archiveplaceholder)

    }

    override fun getItemViewType(position: Int): Int {

        val editionGroup: ArrayList<out IEditionContent>? = editions?.get(position)

        if (editionGroup != null) {

            val length = editionGroup.size

            if (editionGroupState == MenuSizeType.MINIMIZED) {

                if (length > 2) {

                    return VIEW_TYPE_TRIPLE_NORMAL

                } else if (length > 1) {

                    return VIEW_TYPE_DOUBLE_NORMAL

                }

                return VIEW_TYPE_SINGLE_NORMAL

            } else {

                if (length > 2) {

                    return VIEW_TYPE_TRIPLE_SHRUNK

                } else if (length > 1) {

                    return VIEW_TYPE_DOUBLE_SHRUNK

                }

                return VIEW_TYPE_SINGLE_SHRUNK

            }

        }

        return super.getItemViewType(position)

    }

    override fun onCreateViewHolder(parent: ViewGroup, itemType: Int): EditionGroupHolder {

        val layoutId =

                when (itemType) {

                    VIEW_TYPE_DOUBLE_NORMAL -> R.layout.card_edition_stack_old
                    VIEW_TYPE_TRIPLE_NORMAL -> R.layout.card_edition_stack_oldest
                    VIEW_TYPE_TRIPLE_SHRUNK -> R.layout.card_edition_stack_oldest_shrunk
                    VIEW_TYPE_DOUBLE_SHRUNK -> R.layout.card_edition_stack_old_shrunk
                    VIEW_TYPE_SINGLE_SHRUNK -> R.layout.card_edition_stack_shrunk
                    else -> R.layout.card_edition_stack

                }

        val itemView = parent.context.getLayoutInflater().inflate(layoutId, parent, false)

        return when (itemType) {

            VIEW_TYPE_TRIPLE_SHRUNK -> ShrunkEditionGroupOldestHolder(itemView)
            VIEW_TYPE_DOUBLE_SHRUNK -> ShrunkEditionGroupOldHolder(itemView)
            VIEW_TYPE_SINGLE_SHRUNK -> ShrunkEditionGroupHolder(itemView)
            VIEW_TYPE_TRIPLE_NORMAL -> EditionGroupOldestHolder(itemView)
            VIEW_TYPE_DOUBLE_NORMAL -> EditionGroupOldHolder(itemView)
            else -> EditionGroupHolder(itemView)
        }

    }

    override fun getItemCount(): Int {

        val size = editions?.size ?: 0

        return (if (trimToFirstRow) {

            Math.min(size, maxRowCount)

        } else {

            size

        })

    }

    override fun onViewRecycled(holder: EditionGroupHolder) {

        super.onViewRecycled(holder)

        Glide.with(holder.itemView.context).clear(holder.editionCover)

        holder.editionCover.setImageResource(0)

    }

    override fun onBindViewHolder(holder: EditionGroupHolder, position: Int) {

        val editionGroup: ArrayList<out IEditionContent>? = editions?.get(position)

        if (editionGroup != null && editionGroup.isNotEmpty()) {

            val edition = editionGroup[0]

            val pubDate = edition.getPublicationDate()

            if (pubDate != null) {

                holder.title.text = DataStore.EditionApplication?.getEditionDate(pubDate, dateFormat)

            }

            holder.itemView.setTag(R.id.tag_index, position)

            holder.downloadAllButton.setTag(R.id.tag_index, position)

            if (!TextUtils.isEmpty(edition.getEditionImage())) {

                GlideApp
                        .with(holder.editionCover)
                        .setDefaultRequestOptions(glideRequestOptions)
                        .load(edition.getEditionImage()?.replace("{HEIGHT}", coverHeight.toString()))
                        .transform(TopCropTransformation())
                        .into(holder.editionCover)

            } else {

                holder.editionCover.setImageResource(0)

            }

        }

    }

    open inner class ShrunkEditionGroupHolder(itemView: View) : EditionGroupHolder(itemView)
    open inner class ShrunkEditionGroupOldHolder(itemView: View) : EditionGroupOldHolder(itemView)
    open inner class ShrunkEditionGroupOldestHolder(itemView: View) : EditionGroupOldestHolder(itemView)

    open inner class EditionGroupOldestHolder(itemView: View) : EditionGroupOldHolder(itemView) {

        var editionCoverOldest: ImageView = itemView.findViewById(R.id.editionCoverOldest)

        init {

            editionCoverOldest.setBackgroundColor(ColourConfig.Edition.Archive.mOldestEditionColour)

        }

    }

    open inner class EditionGroupOldHolder(itemView: View) : EditionGroupHolder(itemView) {

        var editionCoverOld: ImageView = itemView.findViewById(R.id.editionCoverOld)

        init {

            editionCoverOld.setBackgroundColor(ColourConfig.Edition.Archive.mOldEditionColour)

        }
    }

    open inner class EditionGroupHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView = itemView.findViewById(R.id.title)
        var downloadAllButton: Button = itemView.findViewById(R.id.downloadAllButton)
        var editionCover: ImageView = itemView.findViewById(R.id.editionCover)

        init {

            editionCover.setBackgroundColor(ColourConfig.Edition.Archive.mEditionColour)

            title.setTextColor(ColourConfig.Edition.Archive.mForegroundColour)

            downloadAllButton.background = ColouringHelper.getButtonDrawable(normal = ColourConfig.Edition.Archive.Button.mBackgroundColour,
                    normalBorder = ColourConfig.Edition.Archive.Button.mStrokeColour,
                    pressed = ColourConfig.Edition.Archive.Button.mPressedBackgroundColour,
                    pressedBorder = ColourConfig.Edition.Archive.Button.mPressedBackgroundColour,
                    borderWidth = ColourConfig.Edition.Archive.Button.mStrokeWidth)

            downloadAllButton.setTextColor(ColouringHelper.getSelector(ColourConfig.Edition.Archive.Button.mForegroundColour, ColourConfig.Edition.Archive.Button.mPressedForegroundColour))

        }

    }

}