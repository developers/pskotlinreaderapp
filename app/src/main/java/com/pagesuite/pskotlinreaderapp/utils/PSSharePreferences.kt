package com.pagesuite.pskotlinreaderapp.utils

import android.content.Context
import android.content.SharedPreferences


class PSSharedPreferences {

    private val APP_SETTINGS = "APP_SETTINGS"

    // properties
    private val APP_CODE = "APP_CODE"
    private val FONT_SIZE = "FONT_SIZE"

    // other properties...

    private fun SharedPreferencesManager() {}

    private fun getSharedPreferences(context: Context): SharedPreferences {

        return context.getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE)

    }


    fun saveFontSize(context: Context, fontSize: Utils.FontSizes) {

        val newValue = when (fontSize) {

            Utils.FontSizes.NORMAL -> 1
            Utils.FontSizes.LARGE -> 2
            Utils.FontSizes.LARGER -> 3

        }

        val editor = getSharedPreferences(context).edit()
        editor.putInt(FONT_SIZE, newValue)
        editor.apply()

    }

    fun getSavedFontSize(context: Context): Utils.FontSizes {

        val savedFont = getSharedPreferences(context).getInt(FONT_SIZE, 1)

        return when (savedFont) {

            1 -> Utils.FontSizes.NORMAL
            2 -> Utils.FontSizes.LARGE
            3 -> Utils.FontSizes.LARGER
            else -> Utils.FontSizes.NORMAL

        }

    }


    fun getAppCode(context: Context): String? {

        return getSharedPreferences(context).getString(APP_CODE, null)

    }

    fun setAppCode(context: Context, newValue: String) {

        val editor = getSharedPreferences(context).edit()
        editor.putString(APP_CODE, newValue)
        editor.apply()

    }

    fun resetAppCode(context: Context) {

        val editor = getSharedPreferences(context).edit()
        editor.putString(APP_CODE, "")

        editor.apply()

    }

}