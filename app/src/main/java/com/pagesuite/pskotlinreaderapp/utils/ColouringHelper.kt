package com.pagesuite.pskotlinreaderapp.utils

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable

object ColouringHelper {

    fun getSelector(normal: Int, pressed: Int, checked: Int = Color.TRANSPARENT, selected: Int = Color.TRANSPARENT): ColorStateList? {

        try {

            return ColorStateList(

                    arrayOf(intArrayOf(android.R.attr.state_selected), intArrayOf(android.R.attr.state_checked), intArrayOf(android.R.attr.state_pressed), intArrayOf()),

                    intArrayOf(selected, checked, pressed, normal))

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return null

    }

    fun getButtonDrawable(normal: Int,
                          pressed: Int = Color.TRANSPARENT,
                          selected: Int = Color.TRANSPARENT,
                          checked: Int = Color.TRANSPARENT,
                          pressedBorder: Int = Color.TRANSPARENT,
                          normalBorder: Int = Color.TRANSPARENT,
                          selectedBorder: Int = Color.TRANSPARENT,
                          checkBorder: Int = Color.TRANSPARENT,
                          cornerRadius: Float = 4.0f,
                          cornerRadii: FloatArray? = null,
                          borderWidth: Int = 2): Drawable {

        val stateListDrawable = StateListDrawable()

        if (selected != Color.TRANSPARENT) {

            stateListDrawable.addState(intArrayOf(android.R.attr.state_selected), getDrawable(selected, selectedBorder, cornerRadius, cornerRadii, borderWidth))

        }

        if (checked != Color.TRANSPARENT) {

            stateListDrawable.addState(intArrayOf(android.R.attr.state_checked), getDrawable(checked, checkBorder, cornerRadius, cornerRadii, borderWidth))

        }

        if (pressed != Color.TRANSPARENT) {

            stateListDrawable.addState(intArrayOf(android.R.attr.state_pressed), getDrawable(pressed, pressedBorder, cornerRadius, cornerRadii, borderWidth))

        }

        stateListDrawable.addState(intArrayOf(), getDrawable(normal, normalBorder, cornerRadius, cornerRadii, borderWidth))

        return stateListDrawable

    }

    fun getDrawable(normal: Int, border: Int, cornerRadius: Float, cornerRadii: FloatArray? = null, strokeWidth: Int): Drawable? {

        try {


            val gd = GradientDrawable()

            gd.setColor(normal) // Changes this drawable to use a single color instead of a gradient

            if (cornerRadii != null) {

                gd.cornerRadii = cornerRadii

            } else {

                gd.cornerRadius = cornerRadius

            }

            gd.setStroke(strokeWidth, border)

            return gd


        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return null

    }

}