package com.pagesuite.pskotlinreaderapp.utils

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import java.io.*


object Utils {

    enum class FontSizes(val size: Double) {
        NORMAL(1.0), LARGE(1.25), LARGER(1.5)
    }


    fun covertStringToRGB(rgbString: String?): Int {

        if (rgbString == null) {

            return Color.TRANSPARENT

        }

        val split = rgbString.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val red = split[0].trim().toInt()
        val green = split[1].trim().toInt()
        val blue = split[2].trim().toInt()

        return Color.rgb(red, green, blue)

    }

    fun convertRgbToColour(colourStr: String?, opacity: String = "1"): Int {

        try {

            if (colourStr != null && !TextUtils.isEmpty(colourStr)) {

                if (colourStr.startsWith("#")) {

                    val length = colourStr.length

                    if (length > 3) {

                        val a = Math.round(java.lang.Float.parseFloat(opacity) * 255)

                        var r = 0
                        var g = 0
                        var b = 0

                        if (length == 7) {

                            r = Integer.valueOf(colourStr.substring(1, 3), 16)
                            g = Integer.valueOf(colourStr.substring(3, 5), 16)
                            b = Integer.valueOf(colourStr.substring(5, 7), 16)

                        } else if (length > 3) {

                            r = Integer.valueOf(colourStr.substring(1, 2) + colourStr.substring(1, 2), 16)
                            g = Integer.valueOf(colourStr.substring(2, 3) + colourStr.substring(2, 3), 16)
                            b = Integer.valueOf(colourStr.substring(3, 4) + colourStr.substring(3, 4), 16)

                        }

                        //return Color.parseColor(colourStr);

                        return Color.argb(a, r, g, b)
                    }

                } else {

                    var input = colourStr.replace("rgb(", "")
                    input = input.replace(")", "")
                    input = input.replace(" ", "")

                    val splitColour = input.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                    val length = splitColour.size

                    if (length >= 3) {

                        val a = if (length == 4) {

                            Integer.parseInt(splitColour[3], 255)

                        } else {

                            Math.round(java.lang.Float.parseFloat(opacity) * 255)

                        }

                        val r = Integer.parseInt(splitColour[0])

                        val g = Integer.parseInt(splitColour[1])

                        val b = Integer.parseInt(splitColour[2])


                        return Color.argb(a, r, g, b)
                    }

                }

            }

        } catch (ex: Exception) {

            Log.w(Utils::class.java.simpleName, "Error processing: [$colourStr, $opacity]")
            ex.printStackTrace()

        } catch (e: Error) {

            e.printStackTrace()

        }

        return Color.TRANSPARENT

    }

    fun calculateFontColourBasedOnBackgroundColour(inputColour: Int): Int {

        return (if (((Color.red(inputColour) * 0.299) + (Color.green(inputColour) * 0.587) + (Color.blue(inputColour) * 0.114)) > 186) Color.BLACK else Color.WHITE)

    }

    fun loadFileAsString(file: String): String? {

        try {

            var inputStream: BufferedReader? = null

            try {

                val buf = StringBuilder()
                val `is` = FileInputStream(file)
                inputStream = BufferedReader(InputStreamReader(`is`))

                var str: String? = null
                var isFirst = true
                //while ({ count = input.read(data); count }() != -1) {

                while ({ str = inputStream.readLine(); str }() != null) {

                    buf.append(str)

                }

                return buf.toString()

            } catch (e: IOException) {

                e.printStackTrace()

            } finally {

                if (inputStream != null) {

                    try {

                        inputStream.close()

                    } catch (e: IOException) {

                        e.printStackTrace()

                    }

                }

            }

        } catch (ex: Exception) {

            ex.printStackTrace()

        }

        return null

    }

    fun getPlatformId(context: Context): Int {

        return context.resources.getInteger(R.integer.platformId)

    }

    fun getDataDir(context: Context, folder: String): File {

        val path = context.filesDir.absolutePath + "/" + folder

        val file = File(path)

        if (!file.exists()) {

            file.mkdirs()

        }

        return file

    }

    fun getSpanned(input: String?): Spanned? {

        if (!TextUtils.isEmpty(input)) {

            val replacedHeadline = input?.replace("&amp;", "&")

            return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {

                Html.fromHtml(replacedHeadline)

            } else {

                Html.fromHtml(replacedHeadline, Html.FROM_HTML_MODE_LEGACY)

            }

        }

        return null

    }

    fun animateObjectHeight(animatedView: View, size: Int, completionListener: (() -> Unit)? = null, duration: Long = 333) {

        val anim = ValueAnimator.ofInt(animatedView.measuredHeight, size)

        anim.addUpdateListener { valueAnimator ->

            val heightVal = valueAnimator.animatedValue as Int

            val layoutParams = animatedView.layoutParams
            layoutParams.height = heightVal
            animatedView.layoutParams = layoutParams
            animatedView.requestLayout()

            completionListener?.invoke()

        }

        anim.duration = duration
        anim.start()

    }

    fun animateObjectWidth(animatedView: View, size: Int) {

        val anim = ValueAnimator.ofInt(animatedView.measuredWidth, size)

        anim.addUpdateListener { valueAnimator ->

            val widthVal = valueAnimator.animatedValue as Int
            Log.d("EditionHelperClasses", "hit width animator, width value is $widthVal")
            val layoutParams = animatedView.layoutParams
            layoutParams.width = widthVal
            animatedView.layoutParams = layoutParams
            animatedView.requestLayout()

        }

        anim.duration = 500
        anim.start()

    }

}