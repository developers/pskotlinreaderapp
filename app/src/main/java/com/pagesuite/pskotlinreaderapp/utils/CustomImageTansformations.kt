package com.pagesuite.pskotlinreaderapp.utils

import android.graphics.Bitmap
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import java.nio.charset.Charset
import java.security.MessageDigest


class TopCropTransformation : BitmapTransformation() {

    private val ID = this::class.java.canonicalName

    private val ID_BYTES = ID.toByteArray(Charset.forName("UTF-8"))

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {

        messageDigest.update(ID_BYTES)

    }

    override fun hashCode(): Int {

        return ID.hashCode()

    }

    override fun equals(other: Any?): Boolean {

        return other is TopCropTransformation

    }

    override fun transform(pool: BitmapPool, toTransform: Bitmap, outWidth: Int, outHeight: Int): Bitmap {

        try {

            if (toTransform.height.toFloat() > outHeight) {

                val heightRatio = toTransform.height.toFloat() / toTransform.width.toFloat()

                val newHeight = Math.round(heightRatio * outWidth)

                val scaled = Bitmap.createScaledBitmap(toTransform, outWidth, newHeight, false)

                return Bitmap.createBitmap(scaled, 0, 0, outWidth, outHeight)

            }

        } catch (e: Exception) {

            e.printStackTrace()

        }

        return toTransform

    }
}