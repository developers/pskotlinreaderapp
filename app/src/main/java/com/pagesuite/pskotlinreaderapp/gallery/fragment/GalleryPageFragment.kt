package com.pagesuite.pskotlinreaderapp.gallery.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.models.Image
import com.pagesuite.pskotlinreaderapp.widget.ContentsWebView
import com.pagesuite.readerui.utils.RawManipUtils

class GalleryPageFragment : Fragment() {

    var image: Image? = null

    private var mGestureDetector: GestureDetector? = null
    private var mScaleGestureDetector: ScaleGestureDetector? = null

    private val MIN_ZOOM_LEVEL = 1
    var mZoomLevel = MIN_ZOOM_LEVEL
    private val MAX_ZOOM_LEVEL = 4

    private var scaleX: Float = 0.toFloat()
    private var scaleY: Float = 0.toFloat()

    var htmlView: ContentsWebView? = null

    private var toggleImageLayout: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_gallery_page, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        htmlView = view.findViewById(R.id.imageView)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initTouchHandler()

        loadHtmlContent()

    }

    private fun initTouchHandler() {

        if (htmlView != null) {

            scaleX = htmlView?.scaleX ?: 0f
            scaleY = htmlView?.scaleY ?: 0f

            mGestureDetector = GestureDetector(object : GestureDetector.SimpleOnGestureListener() {

            })

            mScaleGestureDetector = ScaleGestureDetector(activity, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {

                override fun onScale(detector: ScaleGestureDetector): Boolean {

                    return true

                }

                override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {

                    return true

                }

                override fun onScaleEnd(detector: ScaleGestureDetector) {

                    try {

                        val scale = (htmlView?.contentWidth
                                ?: 1).toDouble() / (htmlView?.measuredWidth ?: 1).toDouble()

                        mZoomLevel = when {
                            scale > 3.5 -> MAX_ZOOM_LEVEL
                            scale > 2.0 -> MIN_ZOOM_LEVEL + 2
                            scale > 1.0 -> MIN_ZOOM_LEVEL + 1
                            else -> MIN_ZOOM_LEVEL
                        }

                    } catch (ex: Exception) {

                        ex.printStackTrace()

                    }

                }

            })

            mGestureDetector?.setOnDoubleTapListener(object : GestureDetector.OnDoubleTapListener {

                override fun onSingleTapConfirmed(event: MotionEvent): Boolean {

                    return false

                }

                override fun onDoubleTap(event: MotionEvent): Boolean {

                    try {

                        if (mZoomLevel < MAX_ZOOM_LEVEL) {

                            var pageWidth = htmlView?.contentWidth ?: 0
                            var pageHeight = htmlView?.contentHeight ?: 0

                            val xPos = event.x
                            val yPos = event.y

                            val scrollX = htmlView?.scrollX ?: 0
                            val scrollY = htmlView?.scrollY ?: 0

                            val calculatedX = (xPos + scrollX) / pageWidth
                            val calculatedY = (yPos + scrollY) / pageHeight

                            for (i in 0 until MAX_ZOOM_LEVEL) {

                                htmlView?.zoomOut()
                                htmlView?.zoomOut()

                            }

                            for (i in 0 until mZoomLevel) {

                                htmlView?.zoomIn()
                                htmlView?.zoomIn()

                            }

                            mZoomLevel++

                            pageWidth = htmlView?.contentWidth ?: 0
                            pageHeight = htmlView?.contentHeight ?: 0

                            val offsetX = pageWidth * calculatedX - xPos
                            val offsetY = pageHeight * calculatedY - yPos

                            htmlView?.scrollTo(offsetX.toInt(), offsetY.toInt())

                        } else {

                            mZoomLevel = MIN_ZOOM_LEVEL

                            for (i in 0 until MAX_ZOOM_LEVEL) {

                                htmlView?.zoomOut()
                                htmlView?.zoomOut()

                            }

                        }


                    } catch (ex: Exception) {

                        ex.printStackTrace()

                    }

                    return true

                }

                override fun onDoubleTapEvent(event: MotionEvent): Boolean {

                    return true

                }

            })

            htmlView?.setOnTouchListener { v, event ->

                mScaleGestureDetector?.onTouchEvent(event)
                mGestureDetector?.onTouchEvent(event) ?: false

            }

        }

    }

    private fun loadHtmlContent() {

        val input = if (!toggleImageLayout) {

            RawManipUtils.loadRawResourceAsString(activity, R.raw.gallery_page_fit_width)

        } else {

            RawManipUtils.loadRawResourceAsString(activity, R.raw.gallery_page_fit_height)

        }

        val pageStr = input
                .replace("{IMAGE_SRC}", image?.image ?: "")
                .replace("{BGCOLOR}", String.format("#%06X", 0xFFFFFF and ColourConfig.ImageGallery.mBackgroundColour))

        htmlView?.loadDataWithBaseURL(image?.image, pageStr, "text/html", "utf-8", "")

    }

    fun toggleImageLayout() {

        toggleImageLayout = !toggleImageLayout

        loadHtmlContent()

    }

}