package com.pagesuite.pskotlinreaderapp.gallery.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.SparseArray
import android.view.ViewGroup
import com.pagesuite.pskotlinreaderapp.gallery.fragment.GalleryPageFragment
import com.pagesuite.pskotlinreaderapp.models.Image

class GalleryPageAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    var images: Array<Image>? = null
    private val fragments = SparseArray<GalleryPageFragment>()

    override fun getItem(position: Int): Fragment {

        val fragment = GalleryPageFragment()

        fragment.image = images?.get(position)

        fragments.put(position, fragment)

        return fragment

    }

    override fun getCount(): Int {

        return images?.size ?: 0

    }

    override fun destroyItem(container: ViewGroup, position: Int, fragment: Any) {

        fragments.remove(position)

        super.destroyItem(container, position, fragment)

    }

    fun toggleImageLayout() {

        val length = fragments.size()

        if (length > 0) {

            var fragment: GalleryPageFragment?

            for (i in 0..length) {

                fragment = fragments.valueAt(i)

                fragment?.toggleImageLayout()

            }

        }

    }

}