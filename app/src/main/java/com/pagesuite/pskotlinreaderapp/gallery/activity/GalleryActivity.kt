package com.pagesuite.pskotlinreaderapp.gallery.activity

import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicToolbarActivity
import com.pagesuite.pskotlinreaderapp.centraldatapoints.UsefulConstants
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.edition.findArticle
import com.pagesuite.pskotlinreaderapp.gallery.adapter.GalleryPageAdapter
import com.pagesuite.pskotlinreaderapp.models.Article
import kotlinx.android.synthetic.main.activity_gallery.*

class GalleryActivity : BasicToolbarActivity() {

    var article: Article? = null
    private var imagePageAdapter: GalleryPageAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        val extras = intent?.extras

        if (extras != null) {

            val articleGuid = extras.getString(UsefulConstants.ARTICLE_GUID, null)

            if (articleGuid != null) {

                article = findArticle(articleGuid)

                if (article != null) {

                    initGallery()

                }

            }

        }

    }

    override fun initColourScheme() {

        super.initColourScheme()

        headerForegroundColour = ColourConfig.ImageGallery.Toolbar.mForegroundColour
        headerBackgroundColour = ColourConfig.ImageGallery.Toolbar.mBackgroundColour

        galleryTitle.setTextColor(headerForegroundColour)
        gallerySubtitle.setTextColor(headerForegroundColour)

        imageToggleButton.setColorFilter(headerForegroundColour, PorterDuff.Mode.SRC_ATOP)

        rootView.setBackgroundColor(ColourConfig.ImageGallery.mBackgroundColour)

    }

    private fun initGallery() {

        imagePageAdapter = getAdapter()
        imagePageAdapter?.images = article?.images

        viewPager.adapter = imagePageAdapter

        viewPager.clearOnPageChangeListeners()

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {

                updatePageDetails()

            }

        })

        updatePageDetails()

    }

    private fun updatePageDetails() {

        galleryTitle.text = article?.images?.get(viewPager.currentItem)?.caption ?: article?.headline
        gallerySubtitle.text = String.format(getString(R.string.imageGalleryXofY), (viewPager.currentItem + 1), imagePageAdapter?.count)

    }

    private fun getAdapter(): GalleryPageAdapter {

        return GalleryPageAdapter(supportFragmentManager)

    }

    override fun getLayout(): Int {

        return R.layout.activity_gallery

    }

    @Suppress("UNUSED_PARAMETER")
    fun imageToggleClicked(view: View) {

        imagePageAdapter?.toggleImageLayout()

    }

}
