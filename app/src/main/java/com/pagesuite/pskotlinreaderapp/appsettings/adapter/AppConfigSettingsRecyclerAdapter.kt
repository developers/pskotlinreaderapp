package com.pagesuite.pskotlinreaderapp.appsettings.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.models.ItemGroup

class AppConfigSettingsRecyclerAdapter(val context: Context, private val settingsButtonsInfo: ArrayList<ItemGroup>, private val itemClickListener: View.OnClickListener) : RecyclerView.Adapter<AppConfigSettingsRecyclerAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        val view = LayoutInflater.from(context).inflate(R.layout.edition_settings_menu_recycler, parent, false)
        view.setOnClickListener(itemClickListener)
        return Holder(view)

    }

    override fun getItemCount(): Int {

        return settingsButtonsInfo.count()

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.bindHolder(settingsButtonsInfo[position], position)

    }

    open inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val sectionRecycler: RecyclerView? = itemView.findViewById(R.id.menuRecycler)
                ?: null

        open fun bindHolder(itemGroup: ItemGroup, position: Int) {

            val recycler = sectionRecycler ?: return

            recycler.adapter = SettingsMenuAdapter(context, itemGroup, itemClickListener)
            recycler.layoutManager = LinearLayoutManager(context)
            recycler.setHasFixedSize(true)

        }

    }

}



