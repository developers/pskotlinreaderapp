package com.pagesuite.pskotlinreaderapp.appsettings.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.pagesuite.pskotlinreaderapp.R
import com.pagesuite.pskotlinreaderapp.activities.BasicToolbarActivity
import com.pagesuite.pskotlinreaderapp.appsettings.adapter.AppConfigSettingsRecyclerAdapter
import com.pagesuite.pskotlinreaderapp.config.ColourConfig
import com.pagesuite.pskotlinreaderapp.config.MenuConfig
import com.pagesuite.pskotlinreaderapp.models.ItemGroup
import kotlinx.android.synthetic.main.activity_app_config_settings.*

class AppConfigSettingsActivity : BasicToolbarActivity() {

    private var settingItems: ArrayList<ItemGroup> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        val defaultMenuItems = MenuConfig.Settings.mItemGroups

        if (defaultMenuItems != null) {

            settingItems.addAll(defaultMenuItems)

        }

        settingsButtonList.adapter = AppConfigSettingsRecyclerAdapter(this, settingItems, View.OnClickListener {

            val tag = it.getTag(R.id.tag_type)

            if (tag is String) {

                mApplication?.settingSelected(this, tag)

            }

        })

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        settingsButtonList.layoutManager = layoutManager

    }

    override fun initColourScheme() {

        super.initColourScheme()

        headerBackgroundColour = ColourConfig.Settings.Toolbar.mBackgroundColour
        headerForegroundColour = ColourConfig.Settings.Toolbar.mForegroundColour

        rootView.setBackgroundColor(ColourConfig.Settings.mBackgroundColour)

    }

    override fun getLayout(): Int {

        return R.layout.activity_app_config_settings

    }

}
