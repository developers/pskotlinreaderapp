package pskotlinreaderapp.pagesuite.com.pskotlinreaderapp

import com.pagesuite.pskotlinreaderapp.application.activity.ApplicationActivity
import org.junit.Assert.assertNotNull
import org.junit.Test

class ApplicationDataTest {
    @Test
    fun application_activity_can_be_created() {

        val appactivity = ApplicationActivity()

        assertNotNull(appactivity)

    }
}