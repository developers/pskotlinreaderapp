package pskotlinreaderapp.pagesuite.com.pskotlinreaderapp


import android.content.Context
import org.junit.Before
import org.junit.Test
import org.mockito.Mock


class PSSharedPreferencesTest {

    var home: Home = Home()

    val sharedPrefs = PSSharedPrefences()


    @Mock
    var mMockContext: Context? = null

    @Before
    fun createPageSuiteTileController() {

        home = Home()

        // getInstance().init(mMockContext)
    }


    @Test
    fun application_can_save_app_code() {

        home.saveAppCode("TESTCODE")

        val result = sharedPrefs.getAppCode(home)

        if (result == null) {
            assert(false)
        } else {
            assert(true)
        }

    }


}